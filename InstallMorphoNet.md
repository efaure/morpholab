# MorphoNet Server Installation


Installation of Morphonet Server on [Debian 9.5.0](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.5.0-amd64-xfce-CD-1.iso) with SSH server and Standard System Utilities already installed.

## Download MorphoNet 

Clone git
```
cd /var/www
git clone git@gitlab.inria.fr:efaure/morpholab.git
cd morpholab
```

Installation Path
```
main_path=`pwd`
db_install_path="${main_path}/DB"
morphonet_path="${main_path}/${MorphoNet_Version}"
```


## Parameters

MorphoNet Version
```
MorphoNet_Version="MorphoNet-V-3.1.9"
```

DataBase Account
```
user_db="root"  #User root account of the sql database
password_db="morphonet" #put the password you want 
```


MorphoNet Admin Account
```
user_morphonet="morphonet"  # User morphonet account 
password_morphonet="morphonet" # put the password you want 
```



Your server address
``` 
server="http://localhost:8080" 
echo "${server}/${MorphoNet_Version}"> ${morphonet_path}/server_adress.txt
```


## Dependencies installation

```
apt-get install apache2 php mysql-server libapache2-mod-php php-mysql # Install Apache, PHP, MySQL:
apt-get install phpmyadmin # Install phpadmin (for admin db) choose apache2 , and configure phpadmin with dbconfig-common
```



## MorphoNet Database

Installation procedure of the mysql database on the server

Specify the database access
```
mysqlconf="/etc/mysql/my.cnf" # Create a configuration file to directly connect to mysql
echo "[mysql]">>$mysqlconf
echo "user=$user_db">>$mysqlconf
echo "password=$password_db">>$mysqlconf

echo "[mysqldump]">>$mysqlconf
echo "user=$user_db">>$mysqlconf
echo "password=$password_db">>$mysqlconf
```

Create a new user for MorphoNet DB
```
mysql  -e  "CREATE USER '${user_morphonet}'@'localhost' IDENTIFIED BY '${password_morphonet}';"
mysql  -e  "GRANT ALL PRIVILEGES ON *.* to '${user_morphonet}'@'localhost';"
mysql  -e  "FLUSH PRIVILEGES;"
```

Create the database 
```
mysql < ${db_install_path}/CreateMorphoNet.sql #Create the MorphoNet Database and tables 
```

Add an admin user
```
now=`date '+%Y-%m-%d %H:%M:%S'`
mysql   -e  "USE MorphoNet; INSERT INTO people (id,name, login, password,id_right) VALUES (1,'Admin MorphoNet', '$user_morphonet', MD5('$password_morphonet'),1);"
mysql   -e  "USE MorphoNet; INSERT INTO groups (id, name, id_owner, date, status, privacy) VALUES (1, 'admin', 1, '${now}', 0, 'private');"
mysql   -e  "USE MorphoNet; INSERT INTO people_group (id_people,id_group,status,date) VALUES (1,1,'manager','${now}');"
```

You can access your database if necessary using the command line 
```
mysql -u ${user_db} -p MorphoNet
```

## MorphoNet Server Installation 


Define the html repository
```
php_install_path=/var/www/html/
```

Link HTML to  MorphoNet in HTML path
```
ln -s ${morphonet_path} ${php_install_path}
```


Update DB Password in PHP files
```
sed -i -e  "s/user_morphonet/${user_morphonet}/g" ${php_install_path}/connection.php
sed -i -e  "s/password_morphonet/${password_morphonet}/g" ${php_install_path}/connection.php
```

You can check your log apache error using the command line
```
tail -n 100 /var/log/apache2/error.log
```

## Installation of MorphoNet Help, API, Tutorials and Converters
```
ln -s ${main_path}/HELP ${php_install_path}/HELP
ln -s ${main_path}/API ${php_install_path}/API
ln -s ${main_path}/Tutorials ${php_install_path}/Tutorials
ln -s ${main_path}/Converters ${php_install_path}/Converters
```

## Update rights for data upload

Add right To upload new data from the website
```
chown www-data:www-data ${php_install_path}/UploadDATASET
```
Update DB Password in Python files
```
sed -i -e  "s/user_morphonet/${user_morphonet}/g" ${php_install_path}/UploadDATASET/uploadNewDataset.py
sed -i -e  "s/password_morphonet/${password_morphonet}/g" ${php_install_path}/UploadDATASET/uploadNewDataset.py
```

You can increase the upload file size limit  (up to 100mb in this example )
```
filesizelimit=`cat /etc/php/7.0/apache2/php.ini | grep post_max_size`
sed -i -e  "s/${filesizelimit}/post_max_size = 100M/g" /etc/php/7.0/apache2/php.ini
filesizelimit=`cat /etc/php/7.0/apache2/php.ini | grep upload_max_filesize`
sed -i -e  "s/${filesizelimit}/upload_max_filesize = 100M/g" /etc/php/7.0/apache2/php.ini
 /etc/init.d/apache2 restart #Restart apache
```

You have to install the MySQLdb module for Python 2.7
```
apt-get install python-pip python-dev python-mysqldb
```


## NCBI Taxonomy Installation

This part is used to import the NCBI Taxonomy in MorphoNet 
The original Taxonomy can be find here : https://www.ncbi.nlm.nih.gov/taxonomy

To Install the Taxonomy, please first create the MorphoNet Database as previously explained.

You need this python package :
* [MySQLdb](http://mysql-python.sourceforge.net/MySQLdb.html)


You first must specify your database name in the file import_NCBI.py.

Then run the commands:
```
cd ${main_path}/DB/Taxonomy
python import_NCBI.py
```

## Hash Code


Modify the hash-code in the Instantiation.cs file in MorphoNet Unity 
And then in the uni-functions.php



## Configuration of the MorphoNet Movie Maker and Snapshot

Autrorize php to write
```
mkdir -p ${php_install_path}/Snapshot
chown www-data:www-data  ${php_install_path}/Snapshot
mkdir -p ${php_install_path}/Movies
chown www-data:www-data  ${php_install_path}/Movies
```

Install opencv to create Movies
```
apt-get install libsm6 libxrender-dev ffmpeg
pip install opencv-python
```


## You can now access Morphonet  

```
echo "GO ON ${server}/${MorphoNet_Version}"
```



