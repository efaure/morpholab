﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Linq;
using UnityEngine.SceneManagement;

public class LoadParameters : MonoBehaviour {

	// Use this for initialization
	void Start () {
		string urlSERVER= "http://embryon3d.crbm.cnrs.fr/MorphoNetDev/";
		#if UNITY_EDITOR
			//urlSERVER="http://localhost/";
			urlSERVER= "http://embryon3d.crbm.cnrs.fr/MorphoNetDev/";
		#else
			urlSERVER=Application.absoluteURL;
		#endif
		string []taburl=urlSERVER.Split ('/');
		urlSERVER = "";
		for (int i = 0; i < taburl.Count()-1; i++)
			urlSERVER += taburl [i] + "/";
		Instantiation.setURLServer (urlSERVER);
	

		string parameters;
		#if UNITY_ANDROID
			parameters="{\"id\":\"1\",\"name\":\"140317-Patrick-St8\",\"date\":\"2016-10-14 18:59:06\",\"minTime\":\"1\",\"maxTime\":\"180\",\"id_people\":\"1\",\"bundle\":\"1\",\"id_dataset_type\":\"2\",\"rotation\":\"(0.0, 0.0, -1.0, 0.0)\",\"translation\":\"(0, 1, 0.0)\",\"scale\":\"(1.0, 1.0, 1.0)\",\"spf\":\"13963\",\"dt\":\"89\",\"quality\":\"4\",\"u_ID\":\"1\",\"u_name\":\"Emmanuel\",\"u_surname\":\"Faure\",\"u_level\":\"admin\",\"u_dataset\":\"all\",\"u_unity\":\"1\",\"u_manage\":\"1\",\"u_git\":\"1\"}";
			getParameters (parameters);
		#elif UNITY_EDITOR
				//parameters="{\"id\":\"1\",\"name\":\"140317-Patrick-St8\",\"date\":\"2016-10-14 18:59:06\",\"minTime\":\"1\",\"maxTime\":\"180\",\"id_people\":\"1\",\"bundle\":\"1\",\"id_dataset_type\":\"2\",\"rotation\":\"(0.0, 0.0, -1.0, 0.0)\",\"translation\":\"(0, 1, 0.0)\",\"scale\":\"(1.0, 1.0, 1.0)\",\"spf\":\"13963\",\"dt\":\"89\",\"quality\":\"3\",\"u_ID\":\"1\",\"u_name\":\"Emmanuel\",\"u_surname\":\"Faure\",\"u_level\":\"admin\",\"u_dataset\":\"all\",\"u_unity\":\"1\",\"u_manage\":\"1\",\"u_git\":\"1\"}";
				//parameters="{\"id\":\"81\",\"name\":\"Janelia-Medulla-Connectome\",\"date\":\"2017-09-04 12:54:26\",\"minTime\":\"0\",\"maxTime\":\"0\",\"id_people\":\"1\",\"bundle\":\"-1\",\"id_dataset_type\":\"11\",\"rotation\":\"(0.0, 0.0, 0.0, 0.0)\",\"translation\":\"(3.8, -2.8, 0.0)\",\"scale\":\"(0.5, 0.5, 0.5)\",\"spf\":\"1\",\"dt\":\"0\",\"quality\":null,\"u_ID\":\"1\",\"u_name\":\"Emmanuel\",\"u_surname\":\"Faure\",\"u_level\":\"admin\",\"u_dataset\":\"all\",\"u_unity\":\"1\",\"u_manage\":\"1\",\"u_git\":\"1\"}";
				//parameters="{\"id\":\"88\",\"name\":\"170830-RalphRevision-St8\",\"date\":\"2017-09-12 05:12:56\",\"minTime\":\"1\",\"maxTime\":\"5\",\"id_people\":\"4\",\"bundle\":\"-1\",\"id_dataset_type\":\"2\",\"rotation\":\"(0.1, 0.1, -0.7, 0.7)\",\"translation\":\"(0.0, 0.8, 0.0)\",\"scale\":\"(1.1, 1.1, 1.1)\",\"spf\":\"1\",\"dt\":\"0\",\"quality\":\"4\",\"u_ID\":\"1\",\"u_name\":\"Emmanuel\",\"u_surname\":\"Faure\",\"u_level\":\"admin\",\"u_dataset\":\"all\",\"u_unity\":\"1\",\"u_manage\":\"1\",\"u_git\":\"1\"}";
				//parameters="{\"id\":\"104\",\"name\":\"Virtual Worm\",\"date\":\"2017-10-10 09:55:25\",\"minTime\":\"0\",\"maxTime\":\"0\",\"id_people\":\"1\",\"bundle\":\"0\",\"id_dataset_type\":\"16\",\"rotation\":\"(-0.1, 0.7, -0.1, 0.7)\",\"translation\":\"(2.0, 1.0, 0.0)\",\"scale\":\"(1.1, 1.1, 1.1)\",\"spf\":\"1\",\"dt\":\"0\",\"quality\":\"0\",\"u_ID\":\"0\",\"u_name\":null,\"u_surname\":null,\"u_level\":null,\"u_dataset\":null,\"u_unity\":null,\"u_manage\":null,\"u_git\":null}";
				//getParameters (parameters);

			#else
				Application.ExternalEval ("sendParameters()"); //Get The parameters from the HTML WEB BPAGE
		#endif

	}

	public void getParameters(string parameters)
	{
		Debug.Log (parameters);

		var N = JSONNode.Parse (parameters);
		Instantiation.id_dataset=int.Parse(N  ["id"].ToString ().Replace('"',' ').Trim());
		Instantiation.embryo_name = N  ["name"].ToString ().Replace('"',' ').Trim();
		//date = N  ['date'].ToString ().Replace('"',' ').Trim();
		Instantiation.MinTime=int.Parse(N  ["minTime"].ToString ().Replace('"',' ').Trim());
		Instantiation.MaxTime=int.Parse(N  ["maxTime"].ToString ().Replace('"',' ').Trim());
		Instantiation.id_owner=int.Parse(N  ["id_people"].ToString ().Replace('"',' ').Trim());
		Instantiation.isBundle=int.Parse(N  ["bundle"].ToString ().Replace('"',' ').Trim());

		if(N  ["id_dataset_type"]!=null &&  Instantiation.convertJSON(N  ["id_dataset_type"])!="null")
			Instantiation.id_type= Instantiation.convertJSONToInt(N["id_dataset_type"]);
		//int id_type=int.Parse(N  ["id_dataset_type"].ToString ().Replace('"',' ').Trim());


		string []rot=Instantiation.convertJSON(N  ["rotation"]).Replace('(',' ').Replace(')',' ').Trim().Split(',');
		if (rot.Count() == 4)
			ArcBall.InitialRotation = new Quaternion (float.Parse (rot [0]), float.Parse (rot [1]), float.Parse (rot [2]), float.Parse (rot [3]));

		string []trans=Instantiation.convertJSON(N  ["translation"]).Replace('(',' ').Replace(')',' ').Trim().Split(',');
		if (trans.Count() == 3)
			ArcBall.InitialTranslation = new Vector3 (float.Parse (trans [0]), float.Parse (trans [1]), float.Parse (trans [2]));

		string []sca=Instantiation.convertJSON(N  ["scale"]).Replace('(',' ').Replace(')',' ').Trim().Split(',');
		if (sca.Count() == 3)
			ArcBall.InitialScale  = new Vector3 (float.Parse (sca [0]), float.Parse (sca [1]), float.Parse (sca [2]));
		if(N  ["spf"]!=null &&  Instantiation.convertJSON(N  ["spf"])!="null")
			Instantiation.spf = Instantiation.convertJSONToInt(N  ["spf"]);
		if(N  ["dt"]!=null &&  Instantiation.convertJSON(N  ["dt"])!="null")
			Instantiation.dt = Instantiation.convertJSONToInt(N  ["dt"]);
		if(N  ["quality"]!=null && Instantiation.convertJSON(N["quality"])!="null")
			Instantiation.Quality=Instantiation.convertJSONToInt(N  ["quality"]);
		//Debug.Log ("Instantiation.Quality=" + Instantiation.Quality);

		//USER
		Instantiation.IDUser = int.Parse (N  ["u_ID"].ToString ().Replace('"',' ').Trim());
		if(N  ["u_name"]!=null &&  Instantiation.convertJSON(N  ["u_name"])!="null")
			Instantiation.nameUser =Instantiation.convertJSON( N  ["u_name"]);
		if(N  ["u_surname"]!=null &&  Instantiation.convertJSON(N  ["u_surname"])!="null")
			Instantiation.surnameUser = Instantiation.convertJSON( N  ["u_surname"]);
		
		if(N  ["u_right"]!=null &&  Instantiation.convertJSON(N  ["u_right"])!="null")
			Instantiation.userRight = int.Parse(N  ["u_right"]);


		//Stage
		/*Instantiation.start_stage = "Stage "+convertTimeToStage (Instantiation.id_type, Instantiation.spf, Instantiation.dt,Instantiation.MinTime);
		Instantiation.end_stage = "Stage "+convertTimeToStage (Instantiation.id_type, Instantiation.spf, Instantiation.dt,Instantiation.MaxTime);

		Debug.Log ("Instantiation.id_type=" + Instantiation.id_type);
		Debug.Log ("Instantiation.spf=" + Instantiation.spf);
		Debug.Log ("Instantiation.MinTime=" + Instantiation.MinTime);
		Debug.Log ("Instantiation.MaxTime=" + Instantiation.MaxTime);
		Debug.Log ("Instantiation.start_stage=" + Instantiation.start_stage);
		Debug.Log ("Instantiation.end_stage=" + Instantiation.end_stage);*/
		SceneManager.LoadScene("SceneEmbryo");

	}

}
