﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using System.Text;
using AssemblyCSharp;
using UnityEngine.Networking;

public class SelectionDB : MonoBehaviour
{
	public GameObject MenuLoadSelection;
	public GameObject MenuObjects;
	public GameObject MenuShare;

	//Colormap
	public InputField  NameColormap;
	public Dropdown ListColormap;
	private Dictionary<int,int>CorrespondIdColormap;
	private Dictionary<int,int> CorrespondPeopleColormap;
	public GameObject BdeleteColormap;
	public GameObject BshareColormap;
	public GameObject BloadColormap;



	public void Start(){
		BdeleteColormap.SetActive (false);
		BshareColormap.SetActive (false);
		BloadColormap.SetActive (false);

		if (Instantiation.userRight>=2) {//PUBLIC ACCESS
			//UNACCESS  SAVE COLOR MAP
			MenuLoadSelection.transform.Find("ColormapName").gameObject.SetActive(false);
			MenuLoadSelection.transform.Find("save").gameObject.SetActive(false);
			MenuLoadSelection.transform.Find("saveColormap").gameObject.SetActive(false);
		}
	}


	/// //////////////////////////////////// COLORMAPS 

	//LIST COLORMAPS
	public void  listAllColormap(){
		StartCoroutine (queryColormapList ());
	}
	public  IEnumerator queryColormapList () {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("colormap", "0");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlColormap, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			ListColormap.ClearOptions ();;
			CorrespondIdColormap = new Dictionary<int,int> ();
			CorrespondPeopleColormap= new Dictionary<int,int> ();

			//Return id,name,date
			var N = JSONNode.Parse (www.downloadHandler.text);
			//Debug.Log("Found " + N.Count+ " colormaps");
			List<string> ColormapList= new List<string>();
			ColormapList.Add ("None");
			if (N!=null && N.Count > 0) {
				for (int i = 0; i < N.Count; i++) { //Number of Uploaded Data
					int id_colormap=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
					string name = N [i] [1].ToString ().Replace('"',' ').Trim();
					//string date = N [i] [2].ToString ().Replace('"',' ').Trim();
					int id_people=int.Parse(N [i] [3].ToString ().Replace('"',' ').Trim());
					//string []dates = date.Split (' ');

					string colormapnname =  name;//dates [0] + " " +
					if (id_people != Instantiation.IDUser)
						colormapnname += " (shared)"; 
					ColormapList.Add(colormapnname);
					CorrespondIdColormap [i+1] = id_colormap;
					CorrespondPeopleColormap [i+1] = id_people;
				}
			} 
			ListColormap.AddOptions(ColormapList);
			ListColormap.value = 0;
			onChooseColormap (0);
		}
		www.Dispose (); 
	}

	//When we change the list of colormap we appear/diseapar delete and shared button
	public void onChooseColormap(int v){
		if(v==-1) v = ListColormap.value;
		BdeleteColormap.SetActive (false);
		BshareColormap.SetActive (false);
		if (v == 0)
			BloadColormap.SetActive (false);
		else {
			BloadColormap.SetActive (true);
			//int v = ListSelection.value;
			if (CorrespondIdColormap.ContainsKey (v)) {
				if (CorrespondPeopleColormap.ContainsKey (v)) {
					if (CorrespondPeopleColormap [v] == Instantiation.IDUser) {
						BdeleteColormap.SetActive (true);
						//BshareColormap.SetActive (true);
					}
				}
			}
		}
	}

	//When we decide to share a colormap
	public void onShareColormap(){
		int v = ListColormap.value;
		if (CorrespondIdColormap.ContainsKey (v)) {
			if (CorrespondPeopleColormap.ContainsKey (v)) {
				if (CorrespondPeopleColormap [v] == Instantiation.IDUser) {
					if(MenuShare.activeSelf)
						MenuShare.SetActive(false);
					else{
						MenuShare.SetActive(true);
						MenuShare.transform.position = new Vector3 (Mathf.RoundToInt(-Instantiation.canvasWidth/2f+533)*Instantiation.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
						MenuShare.GetComponent<ShareWith> ().basename="colormap";
						MenuShare.GetComponent<ShareWith> ().id_base=CorrespondIdColormap[v];
						MenuShare.GetComponent<ShareWith> ().listPeople ();
					}
				}
			}
		}
	}


	//LOAD A COLORMAP
	public void loadColormap(){
		int v = ListColormap.value;
		if (v!=0 && CorrespondIdColormap.ContainsKey (v))
			StartCoroutine (queryLoadColormap (CorrespondIdColormap [v]));
	}
	public  IEnumerator queryLoadColormap (int id_colormap) {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("colormap", "2");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("id_colormap", id_colormap.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlColormap, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			//Return colors ....
			var N = JSONNode.Parse (www.downloadHandler.text);
			//Debug.Log (N.ToString ());
			if (N.Count > 0) {
				for (int i = 0; i < N.Count; i++) { //Number of Uploaded Data
					string shader=N [i.ToString()]["shader"];
					Material m = ChangeColorSelection.getMaterialByName (shader);
					int R = int.Parse (N [i.ToString ()] ["R"]);
					int G = int.Parse (N [i.ToString ()] ["G"]);
					int B = int.Parse (N [i.ToString ()] ["B"]);
					int A = int.Parse (N [i.ToString ()] ["A"]);
					m.color = new Color((float)R/255f,(float)G/255f,(float)B/255f,(float)A/255f);
					SelectionColor.materials [i] = m;
				}
			} 
			MenuObjects.gameObject.transform.Find("Selection").gameObject.GetComponent<SelectionColor> ().resetAllMaterials ();
		}
		www.Dispose (); 
		MenuLoadSelection.SetActive (false);
	}
	//DELETE A COLORMAP
	public void deleteColormap(){
		int v = ListColormap.value;
		if (v!=0 && CorrespondIdColormap.ContainsKey (v)) {
			if (CorrespondPeopleColormap.ContainsKey (v)) {
				if (CorrespondPeopleColormap[v] == Instantiation.IDUser)
					StartCoroutine (queryDeleteColormap (CorrespondIdColormap[v]));
			}
		}
	}

	public  IEnumerator queryDeleteColormap (int id_colormap) {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("colormap", "3");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("id_colormap", id_colormap.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlColormap, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			if (www.downloadHandler.text != "")
				Debug.Log ("Error Delete " + www.downloadHandler.text);
		}
		www.Dispose (); 
		MenuLoadSelection.SetActive (false);

	}
	//SAVE A COLORMAP
	public void saveColormap(){
		if(NameColormap.text!="") StartCoroutine (querySaveColormap (NameColormap.text));
	}

	public  IEnumerator querySaveColormap (string colormapname) {
		//Fist we look for all colormap value
		var S = new JSONClass();
		for (int i = 0; i < SelectionColor.indexcolors.Length; i++) {
			string shader = Instantiation.Default.name;
			int R = 0;int G = 0;int B = 0;int A = 0;
			if (SelectionColor.materials [i] != null) {
				shader = SelectionColor.materials [i].name;
				R = (int)Mathf.Round(255F * SelectionColor.materials [i].color.r);
				G = (int)Mathf.Round(255F * SelectionColor.materials [i].color.g);
				B = (int)Mathf.Round(255F * SelectionColor.materials [i].color.b);
				A = (int)Mathf.Round(255F * SelectionColor.materials [i].color.a);
			} else {
				string hexcol = SelectionColor.indexcolors [SelectionColor.colororder[i]];
				Color newColor = new Color();
				ColorUtility.TryParseHtmlString (hexcol, out newColor);
				R = (int)Mathf.Round(255F * newColor.r);
				G = (int)Mathf.Round(255F * newColor.g);
				B = (int)Mathf.Round(255F * newColor.b);
				A = (int)Mathf.Round(255F * newColor.a);
			}
			S[i.ToString()]["shader"] = shader;
			S[i.ToString()]["R"].AsInt = R;
			S[i.ToString()]["G"].AsInt = G;
			S[i.ToString()]["B"].AsInt = B;
			S[i.ToString()]["A"].AsInt = A;
		}
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("colormap", "1");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("name", colormapname);
		byte[] bytes = System.Text.Encoding.UTF8.GetBytes(S.ToString());
		form.AddBinaryData ("colormap.txt", bytes);
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlColormap, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			if(www.downloadHandler.text!="") Debug.Log ("Error saving colormap  " +www.downloadHandler.text);
		}
		www.Dispose (); 
		//StartCoroutine (querySelectionList ());
		NameColormap.text="";
		MenuLoadSelection.SetActive (false);
	}


}

