﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;
using System.Text;
using UnityEngine.Networking;


public class EmbryosList : MonoBehaviour
{

    public GameObject Parent;
    public GameObject DataType;
    public GameObject NewDataset;
    public static Material Default;
    public GameObject canvas;
    public Camera cam;

    int width = 0;
    // Use this for initialization
    void Start()
    {
        #if UNITY_EDITOR
            Instantiation.setURLServer("http://embryon3d.crbm.cnrs.fr/Dev/");
            Instantiation.IDUser = 1;
            Instantiation.nameUser = "Faure";
            Instantiation.surnameUser = "Emmanuel";
        #endif
        scrollbar.SetActive(false);
        Default = Resources.Load("StandardCell", typeof(Material)) as Material;
        DataType.SetActive(false);
        NewDataset.SetActive(false);
        StartCoroutine(listDataType()); //téléchargement du data_file contenant les embryons etc



    }

    //Canvas Scale
    public static float canvasScale = 1F;
    public static float initcanvasScale = 0.01302083F; //Standard Scale for the Canvas 
    public static float canvasHeight = 718;
    public static float canvasWidth = 1007;//Default Exprort Value
    void Update()
    {
    #if UNITY_ANDROID
        if (width != cam.pixelWidth)
        {
            width = cam.pixelWidth;
            Debug.Log("SCreen " + width);
            //canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
            reorganiseDatasetType();
          
        }


        if (Input.touchCount == 1)
        {
            //Debug.Log("okokok");
            if (moving)
            {
                diffMvt -= Input.mousePosition.x - prevX;
                prevX = Input.mousePosition.x;

                reorganiseDatasetType();
            }
            else
            {
                Touch touch = Input.touches[0];
                if (touch.phase == TouchPhase.Began)
                {
                    prevX = Input.mousePosition.x;
                    moving = true;
                }
            }
        }
        else { moving = false; }
        #endif

    }

    //SCROLLBAR 
    public GameObject scrollbar;
    public static int marging = 14;
    public static int MaxHeight = 700;
    public static int nbSCroll = 0;
    public void onScroll()
    {
        int nbSteps = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
        float value = scrollbar.GetComponent<Scrollbar>().value;
        nbSCroll = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
        reorganiseDatasetType();
    }




    public float diffMvt = 0;
    public float prevX = 0;
    public bool moving = false;

    /// <summary>
    /// ////////////////////////////////////// DATASET TYPE
    /// </summary>
    //Type of dataset
    public class dataSetType
    {
        public int id;
        public string type;
        public dataSetType parent;
        public int id_parent;
        public int developmental_table;
        public GameObject dt; //Associated Game Object

        public Dictionary<int, dataset> DataSets;
        public dataSetType(int id, string type, int id_parent, dataSetType parent, int developmental_table) //Constructeur 
        {
            this.id = id;
            this.type = type;
            this.id_parent = id_parent;
            this.parent = parent;
            this.developmental_table = developmental_table;
            this.dt = null;
            DataSets = new Dictionary<int, dataset>();
        }
    }
    public static Dictionary<int, dataSetType> DataSetTypes;

    public IEnumerator listDataType()
    {
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("download", "8");
        form.AddField("id_people", Instantiation.IDUser.ToString());
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
            Debug.Log("Error : " + www.error);
        else
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            //Debug.Log("Found " + N.Count);
            DataSetTypes = new Dictionary<int, dataSetType>();
            DataSetTypes[0] = null;
            if (N != null && N.Count > 0)
            {
                for (int i = 0; i < N.Count; i++)
                { //Number of Uploaded Data
                    int id_type = int.Parse(N[i][0].ToString().Replace('"', ' ').Trim());
                    string type = N[i][1].ToString().Replace('"', ' ').Trim();
                    int id_parent = int.Parse(N[i][2].ToString().Replace('"', ' ').Trim());
                    int developmental_table = int.Parse(N[i][3].ToString().Replace('"', ' ').Trim());
                    dataSetType dt = DataSetTypes[id_parent];
                    DataSetTypes[id_type] = new dataSetType(id_type, type, id_parent, DataSetTypes[id_parent], developmental_table);
                }
            }
        }
        www.Dispose();

        CreateDataType(0); //We create all gamobbject associated with dataset type 

        StartCoroutine(listDataSet());
    }



    //We have to create dataset separatly to the class due to unstatic event 
    public void CreateDataType(int id_parent)
    {
        foreach (KeyValuePair<int, dataSetType> item in DataSetTypes)
        {
            dataSetType dst = item.Value;
            if (dst != null && dst.id_parent == id_parent)
            {
                dst.dt = (GameObject)Instantiate(DataType);
                if (id_parent == 0) dst.dt.transform.SetParent(canvas.transform, false);
                else dst.dt.transform.SetParent(DataSetTypes[id_parent].dt.transform, false);
                dst.dt.gameObject.name = "TYPE_" + dst.type;
                dst.dt.transform.Find("label").gameObject.GetComponent<Text>().text = dst.type;
                dst.dt.SetActive(true);
                CreateDataType(dst.id);
            }
        }
    }



    private int heightShiftMenu = 40;
    private int HeightMenu = 0;
    private int widthShiftMenu = 25;


    public void reorganiseDatasetType() { 
        HeightMenu = 0; 
        MoveDataSetType(0, 0); 
        //Debug.Log("HeightMenu=" + HeightMenu);
        if (HeightMenu > MaxHeight)
        {
            if (!scrollbar.activeSelf){
                scrollbar.SetActive(true);
                scrollbar.GetComponent<Scrollbar>().numberOfSteps = 1 + (HeightMenu - MaxHeight) / heightShiftMenu; //DEFINE SCROLLBAR LENGTH
                scrollbar.GetComponent<Scrollbar>().size = 1f / (float)(1 + (HeightMenu - MaxHeight) / heightShiftMenu);
                scrollbar.GetComponent<Scrollbar>().value = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
            }
        }
        else scrollbar.SetActive(false);
    }
    public void MoveDataSetType(int id_parent, int level)
    {
        //Debug.Log ("Found Elements with parent  " + id_parent.ToString ());
        float xxr = 4f;
        float yr = 3.5f;
        if (DataSetTypes != null)
            foreach (KeyValuePair<int, dataSetType> item in DataSetTypes)
            {
                dataSetType dst = item.Value;
                if (dst != null && dst.id_parent == id_parent)
                {
                    //Debug.Log ("Found" + dst.type);
                    if (dst.dt.activeSelf)
                    {//Only if Active)
#if UNITY_ANDROID
                    dst.dt.transform.position = new Vector2(widthShiftMenu * level - cam.pixelWidth / xxr, -HeightMenu + cam.pixelHeight / yr + diffMvt);
#else
                    dst.dt.transform.position = new Vector2(20+widthShiftMenu * level, -HeightMenu+nbSCroll*heightShiftMenu);
#endif
                        HeightMenu += heightShiftMenu;
                        int hm = 0;
                        foreach (KeyValuePair<int, dataset> itemD in dst.DataSets)
                        {
                            dataset dt = itemD.Value;
#if UNITY_ANDROID
                        dt.dtgo.transform.position = new Vector2(50 - cam.pixelWidth / xxr, 5 - HeightMenu + cam.pixelHeight / yr + diffMvt);
#else
                        dt.dtgo.transform.position = new Vector2(70 , 5 - HeightMenu+nbSCroll*heightShiftMenu);
#endif
                            HeightMenu += heightShiftMenu;

                        }

                        //dst.dt.transform.Translate (widthShiftMenu, -(nbElement + 1) * heightShiftMenu, 0);
                        MoveDataSetType(dst.id, level + 1);
                    }
                }
            }
        

    }





    /// <summary>
    /// ////////////////////////////////////// DATASET 
    /// </summary>
    /// 


    //DATASET
    public class dataset
    {
        public int id_dataset;
        public string name;
        public string date;
        public int minTime;
        public int maxTime;
        public int id_owner;
        public int isBundle;
        public int id_type;
        public Quaternion rotation;
        public Vector3 translation;
        public Vector3 scale;
        public int spf;
        public int dt;
        public int quality;
        public GameObject dtgo;
        public GameObject dtgoSmall;

        public dataset()
        {
            this.dtgoSmall = null;
        }
    }



    //Check the uploaded data in the DB
    public IEnumerator listDataSet()
    {
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("download", "1");
        form.AddField("id_people", Instantiation.IDUser.ToString());
        //Debug.Log (Instantiation.urlDataset + "?hash=" + Instantiation.hash + "&download=1&id_people=" + Instantiation.IDUser.ToString ());
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
            Debug.Log("Error : " + www.error);
        else
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            //Debug.Log("Found " + N.Count+" dataset");
            if (N != null && N.Count > 0)
            {
                for (int i = 0; i < N.Count; i++)
                { //Number of Uploaded Data
                    //Debug.Log(N[i]);

                    dataset dst = new dataset();
                    dst.id_dataset = int.Parse(N[i][0].ToString().Replace('"', ' ').Trim());
                    dst.name = N[i][1].ToString().Replace('"', ' ').Trim();
                    dst.date = N[i][2].ToString().Replace('"', ' ').Trim();
                    dst.minTime = int.Parse(N[i][3].ToString().Replace('"', ' ').Trim());
                    dst.maxTime = int.Parse(N[i][4].ToString().Replace('"', ' ').Trim());
                    dst.id_owner = int.Parse(N[i][5].ToString().Replace('"', ' ').Trim());
                    dst.isBundle = int.Parse(N[i][6].ToString().Replace('"', ' ').Trim());
                    dst.id_type = int.Parse(N[i][7].ToString().Replace('"', ' ').Trim());
                    string[] rot = N[i][8].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');

                    dst.rotation = Quaternion.Euler(new Vector3(0.0f, 1.0f, 0.0f));//Quaternion.identity;
                    if (rot.Count() == 4)
                        dst.rotation = new Quaternion(float.Parse(rot[0]), float.Parse(rot[1]), float.Parse(rot[2]), float.Parse(rot[3]));
                    //Debug.Log ("rotation=" + dst.rotation.ToString ());

                    dst.translation = Vector3.zero;
                    string[] trans = N[i][9].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                    if (trans.Count() == 3)
                        dst.translation = new Vector3(float.Parse(trans[0]), float.Parse(trans[1]), float.Parse(trans[2]));
                    //Debug.Log ("translation=" + dst.translation.ToString ());

                    dst.scale = Vector3.zero;
                    string[] sca = N[i][10].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                    if (sca.Count() == 3)
                        dst.scale = new Vector3(float.Parse(sca[0]), float.Parse(sca[1]), float.Parse(sca[2]));
                    //Debug.Log ("scale=" + scale.ToString ());

                    dst.spf = int.Parse(N[i][11].ToString().Replace('"', ' ').Trim());
                    dst.dt = int.Parse(N[i][12].ToString().Replace('"', ' ').Trim());
                    dst.quality = 0;
                    int.TryParse(N[i]["quality"].ToString().Replace('"', ' ').Trim(), out dst.quality);
                    DataSetTypes[dst.id_type].DataSets[dst.id_dataset] = dst;

                    //Create a Gameobcet for referenc
                    dst.dtgo = (GameObject)Instantiate(NewDataset);
                    dst.dtgo.transform.SetParent(DataSetTypes[dst.id_type].dt.transform, false);
                    dst.dtgo.gameObject.name = dst.name;

                    dst.dtgo.transform.Find("date").gameObject.GetComponent<Text>().text = "(" + dst.id_dataset + ") " + dst.date;
                    if (dst.minTime != dst.maxTime) dst.dtgo.transform.Find("label").gameObject.GetComponent<Text>().text = dst.name + " (" + dst.minTime + "-" + dst.maxTime + ") ";
                    else dst.dtgo.transform.Find("label").gameObject.GetComponent<Text>().text = dst.name;
                    Button loadButton = dst.dtgo.transform.Find("load").gameObject.GetComponent<Button>().GetComponent<Button>();
                    EmbryonSelectedListen(loadButton, dst.dtgo, dst.id_type, dst.id_dataset, dst.minTime, dst.maxTime, dst.name, dst.isBundle, dst.rotation, dst.translation, dst.scale, dst.id_owner, dst.dt, dst.spf, dst.quality);

                    dst.dtgo.SetActive(true);


                }
            }
        }
        www.Dispose();
        reorganiseDatasetType();
    }



    //VERY WEIRED BUT NECESSARY OTHER WISE IT DOES NOT TAKE IN ACCOUNT THE VARIABLE CHANGEMENT
    void EmbryonSelectedListen(Button b, GameObject emb, int dataset_type, int id_dataset, int minTime, int maxTime, string name, int isBundle, Quaternion rotation, Vector3 translation, Vector3 scale, int id_owner, int dt, int spf, int quality)
    {
        b.onClick.AddListener(() => EmbryonSelected(emb, dataset_type, id_dataset, minTime, maxTime, name, isBundle, rotation, translation, scale, id_owner, dt, spf, quality));
    }


    //nom:EmbronSelected
    //sémantique:fonction qui s'éxécute lorsque l'on clique sur un bouton généré par le DataFile
    //précond: url fonctionne
    public void EmbryonSelected(GameObject emb, int dataset_type, int id_dataset, int minTime, int maxTime, string name, int isBundle, Quaternion rotation, Vector3 translation, Vector3 scale, int id_owner, int dt, int spf, int quality)
    {
        //Debug.Log("Choose "+id_dataset+" "+name+" from " + minTime + " to " + maxTime );
        Instantiation.id_dataset = id_dataset;
        Instantiation.MinTime = minTime;
        Instantiation.MaxTime = maxTime;
        Instantiation.embryo_name = name;
        Instantiation.isBundle = isBundle;
        Instantiation.id_owner = id_owner;
        Instantiation.dt = dt;
        Instantiation.spf = spf;
        Instantiation.start_stage = "Stage " + convertTimeToStage(dataset_type, spf, dt, minTime);
        Instantiation.end_stage = "Stage " + convertTimeToStage(dataset_type, spf, dt, maxTime);
        Instantiation.Quality = quality;
        ArcBall.InitialRotation = rotation;
        ArcBall.InitialTranslation = translation;
        ArcBall.InitialScale = scale;

        SceneManager.LoadScene("SceneEmbryo");


    }


    //Download Developmental Table
    public static Dictionary<int, List<Dvpt>> Dvpts = new Dictionary<int, List<Dvpt>>();

    public class Dvpt
    {
        public string period;
        public string stage;
        public string developmentaltstage;
        public string description;
        public string hpf;
        public string hatch;
        public Dvpt(string period, string stage, string developmentaltstage, string description, string hpf, string hatch) //Constructeur 
        {
            this.period = period;
            this.stage = stage;
            this.developmentaltstage = developmentaltstage;
            this.description = description;
            this.hpf = hpf;
            this.hatch = hatch;
        }
    }

    public static string convertTimeToStage(int id_datasettype, int spf, int dt, int t)
    {

        float hpf = (spf + t * dt) / 3600f;
        //Debug.Log ("hpf==" + hpf+" spf="+spf+" dt="+dt+" t="+t);
        return convertHPFtoStage(id_datasettype, hpf);
    }

    public static string convertHPFtoStage(int id_datasettype, float HPF)
    {
        //Lookforborder 
        string stage_begin = "";
        float distance_to_begin = 10000000f;
        string stage_end = "";
        float distance_to_end = 10000000f;
        if (!Dvpts.ContainsKey(id_datasettype)) return "0";
        List<Dvpt> dvptL = Dvpts[id_datasettype];
        foreach (Dvpt dvpt in dvptL)
        {
            float dvptHPF = float.Parse(dvpt.hpf);
            if (dvptHPF > HPF)
            {
                float d = dvptHPF - HPF;
                if (d < distance_to_end)
                {
                    distance_to_end = d;
                    stage_end = dvpt.stage;
                }
            }
            if (dvptHPF <= HPF)
            {
                float d = HPF - dvptHPF;
                if (d < distance_to_begin)
                {
                    distance_to_begin = d;
                    stage_begin = dvpt.stage;
                }
            }
        }
        if (distance_to_begin > distance_to_end) return stage_end;
        return stage_begin;
    }
    public static float convertHPFToHatch(int id_datasettype, float HPF)
    {

        //Lookfor min max
        float MaxHatch = 0;
        float MaxHPF = 0;
        List<Dvpt> dvptL = Dvpts[id_datasettype];
        foreach (Dvpt dvpt in dvptL)
        {
            if (float.Parse(dvpt.hatch) > MaxHatch)
            {
                MaxHatch = float.Parse(dvpt.hatch);
                MaxHPF = float.Parse(dvpt.hpf);
            }
        }
        //Debug.Log ("HPF=" + HPF + " *MaxHatch=" + MaxHatch + " / MaxHPF=" + MaxHPF);
        return HPF * MaxHatch / MaxHPF;
    }
    public static float HatchLength = 9f;
    public static IEnumerator downloadDevelopmentalTable(int id_datasettype, GameObject DevptTable)
    {

        DevptTable.SetActive(false);
        Dvpts[id_datasettype] = new List<Dvpt>();
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("developmental_table", id_datasettype.ToString());
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
            Debug.Log("Error : " + www.error);
        else
        {
            if (www.downloadHandler.text != "")
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N.Count > 0)
                {
                    GameObject hpfsGO = DevptTable.transform.Find("hpfs").gameObject;
                    hpfsGO.SetActive(true);
                    GameObject hpfGO = hpfsGO.gameObject.transform.Find("hpf").gameObject;
                    //Debug.Log ("Found a dpvt table for " + id_datasettype);
                    DevptTable.transform.Translate(20f, 0f, 0f);
                    DevptTable.AddComponent<LineRenderer>();
                    DevptTable.GetComponent<LineRenderer>().useWorldSpace = false;
                    DevptTable.GetComponent<LineRenderer>().SetWidth(3, 3);
                    DevptTable.GetComponent<LineRenderer>().SetVertexCount(N.Count * 3 + 4);

                    //float shiftX = dtparent.transform.position.x;
                    //Debug.Log ("shiftX=" + shiftX);
                    int j = 0;
                    DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(0, 0, -1)); j += 1;
                    DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(0, 10, -1)); j += 1;
                    DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(0, 0, -1)); j += 1;

                    int shiftHPF = 50;
                    GameObject goi = Instantiate(hpfGO);
                    goi.SetActive(true);
                    goi.transform.SetParent(hpfsGO.transform);
                    goi.name = "0";
                    goi.GetComponent<Text>().text = "0";
                    goi.transform.transform.Translate(shiftHPF, -101f, 0);
                    goi.transform.transform.Rotate(new Vector3(0f, 0f, 90f));


                    for (int i = 0; i < N.Count; i++)
                    {

                        //Debug.Log (i + "->" + N [i].ToString ());
                        string period = N[i][0].ToString().Replace('"', ' ').Trim();
                        string stage = N[i][1].ToString().Replace('"', ' ').Trim();
                        string developmentaltstage = N[i][2].ToString().Replace('"', ' ').Trim();
                        string description = N[i][3].ToString().Replace('"', ' ').Trim();
                        string hpf = N[i][4].ToString().Replace('"', ' ').Trim();
                        string hatch = N[i][5].ToString().Replace('"', ' ').Trim();
                        Dvpts[id_datasettype].Add(new Dvpt(period, stage, developmentaltstage, description, hpf, hatch));
                        float hatchF = float.Parse(hatch);
                        DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(hatchF * HatchLength, 0, -1)); j += 1;
                        DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(hatchF * HatchLength, 10, -1)); j += 1;
                        DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(hatchF * HatchLength, 0, -1)); j += 1;

                        GameObject go = Instantiate(hpfGO);

                        go.SetActive(true);
                        go.transform.SetParent(hpfsGO.transform);
                        go.name = hatch;
                        go.GetComponent<Text>().text = hpf;
                        //go.transform.position=new Vector3(hatchF*HatchLength,60f,0f);
                        //go.transform.localScale=new Vector3(1.0f,1.0f,1.0f);
                        go.transform.transform.Translate(hatchF * HatchLength + shiftHPF, -101f, 0);
                        go.transform.transform.Rotate(new Vector3(0f, 0f, 90f));


                    }
                    hpfGO.transform.Translate(-25f, -60f, 0); //LEGEND HPF
                    DevptTable.GetComponent<LineRenderer>().SetPosition(j, new Vector3(0, 0, -1)); j += 1;
                    DevptTable.GetComponent<LineRenderer>().sharedMaterial = Default;


                }
            }
        }
        www.Dispose();

    }


}
