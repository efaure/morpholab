﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class ArcBall : MonoBehaviour {

	public static Quaternion InitialRotation;
	public static Vector3 InitialTranslation;
	public static Vector3 InitialScale;

	public static Quaternion CurrentRotation;
	public static Vector3 CurrentPosition;
	public static Vector3 CurrentScale;


	//To Turn arround
	private Vector3 dragPosition;
	private Vector3 prevMousePosition;
	private float radius= 5f;
	private Vector3 startDrag;
	private Vector3 endDrag;
	private Quaternion downR;
	public static bool dragging;


	//To move embryon center
	public static bool moving;
	private float speedmoving=0.01f;


	//ZOOm
	private float zoomRatio=0.1f;
	public static bool zoomning;

	public static bool update; //When update from somewhere else

	//To plot the Embryo Center
	public GameObject rotationAxis;

	public static Toggle fixedplan;

	void Start(){
		update = false;
		dragging = false;
		moving = false;
		zoomning = false;
	

		CurrentRotation=Quaternion.Euler(new Vector3(0, 0, 0));
		if (InitialRotation != null) CurrentRotation = InitialRotation;
		else InitialRotation = CurrentRotation;

		CurrentPosition=new Vector3(0, 0, 0);
		if (InitialTranslation != null) CurrentPosition = InitialTranslation;
		else InitialTranslation = CurrentPosition;

		CurrentScale = new Vector3 (1f, 1f, 1f);
		if(InitialScale==new Vector3 (0f, 0f, 0f)) InitialScale=new Vector3 (1f, 1f, 1f);
		if (InitialScale != null) CurrentScale = InitialScale;
		else InitialScale = CurrentScale;
		
		
		transform.rotation = ArcBall.CurrentRotation;
		transform.position = ArcBall.CurrentPosition;
		transform.localScale = ArcBall.CurrentScale;



		rotationAxis=Instantiate(Resources.Load("Axis",typeof(GameObject)) as GameObject);
		rotationAxis.transform.localScale = new Vector3 (Instantiation.canvasScale, Instantiation.canvasScale, Instantiation.canvasScale);
		rotationAxis.transform.position =ArcBall.CurrentPosition;
		rotationAxis.transform.rotation =ArcBall.CurrentRotation;
		rotationAxis.SetActive(false);

		fixedplan = Instantiation.canvas.transform.Find ("MenuDataset").gameObject.transform.Find ("fixeplan").gameObject.GetComponent<Toggle> ();
	}

	public static void resetRotation(){
		Instantiation.EmbryosGO.transform.rotation = new Quaternion (0F, 0F, 0F, 0F);
		Instantiation.EmbryosGO.transform.position =new Vector3(0F,0F,0F);
		Instantiation.EmbryosGO.transform.localScale=new Vector3 (1f, 1f, 1f);

		Instantiation.Tracking.transform.rotation = new Quaternion (0F, 0F, 0F, 0F);
		Instantiation.Tracking.transform.position =new Vector3(0F,0F,0F);
		Instantiation.Tracking.transform.localScale=new Vector3 (1f, 1f, 1f);
	}


	public static void updateRotation(){
		Instantiation.EmbryosGO.transform.rotation = ArcBall.CurrentRotation;
		Instantiation.EmbryosGO.transform.position = ArcBall.CurrentPosition;
		Instantiation.EmbryosGO.transform.localScale =ArcBall.CurrentScale;

		Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;
		Instantiation.Tracking.transform.position = ArcBall.CurrentPosition;
		Instantiation.Tracking.transform.localScale =ArcBall.CurrentScale;
	}
		
	//Rotate the embryo
	public static void rotate(Quaternion rotation){
		ArcBall.CurrentRotation = rotation;
		Instantiation.EmbryosGO.transform.rotation = ArcBall.CurrentRotation;
		Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;
	}
	//Add a angle of riation
	public static void addRotate(Quaternion rotation){
		if(ArcBall.CurrentRotation.x==0 && ArcBall.CurrentRotation.y==0 && ArcBall.CurrentRotation.z==0)
			ArcBall.CurrentRotation = rotation;
		else
		ArcBall.CurrentRotation = ArcBall.CurrentRotation*rotation;
		Instantiation.EmbryosGO.transform.rotation = ArcBall.CurrentRotation;
		Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;
	}
	//Scale the embryo
	public static void rescale(float sca){
		ArcBall.CurrentScale = new Vector3 (sca, sca, sca);
		Instantiation.EmbryosGO.transform.localScale =ArcBall.CurrentScale;
		Instantiation.Tracking.transform.localScale =ArcBall.CurrentScale;
	}
	public static void rescale(Vector3 sca){
		ArcBall.CurrentScale = sca;
		Instantiation.EmbryosGO.transform.localScale =ArcBall.CurrentScale;
		Instantiation.Tracking.transform.localScale =ArcBall.CurrentScale;
	}
	//Move the embryo
	public static void move(Vector3 position){
		ArcBall.CurrentPosition = position;
		Instantiation.EmbryosGO.transform.position = ArcBall.CurrentPosition;
		Instantiation.Tracking.transform.position = ArcBall.CurrentPosition;
	}

	public static bool turnEmbryo=false;
	public static bool activeTurnEmbryo=false;



	void Update () {
		

		if (update) {
			transform.rotation = ArcBall.CurrentRotation;
			transform.position = ArcBall.CurrentPosition;
			transform.localScale = ArcBall.CurrentScale;
			Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;
			Instantiation.Tracking.transform.position = ArcBall.CurrentPosition;
			Instantiation.Tracking.transform.localScale = ArcBall.CurrentScale;

			rotationAxis.transform.rotation = ArcBall.CurrentRotation;
			rotationAxis.transform.position =ArcBall.CurrentPosition;
			update = false;
		} else {

			#if UNITY_ANDROID
				// ZOOM If there are two touches on the device...
				if(Input.touchCount==0){dragging=false;moving = false;zoomning=false;}

				if (Input.touchCount == 3){ //DRAG
					Touch touch=Input.touches[0];
					if (touch.phase == TouchPhase.Began) {
						prevMousePosition = Input.mousePosition;
					} else if(touch.phase == TouchPhase.Moved){
						
						dragging=true;
						transform.position += (Input.mousePosition - prevMousePosition) * speedmoving;

							prevMousePosition = Input.mousePosition;
							ArcBall.CurrentPosition = transform.position;
							rotationAxis.transform.position = ArcBall.CurrentPosition;
							Instantiation.Tracking.transform.position = ArcBall.CurrentPosition;
					}
				}else if (Input.touchCount == 2) { //ZOOM
					zoomning=true;
					// Store both touches.
					Touch touchZero = Input.GetTouch(0);
					Touch touchOne = Input.GetTouch(1);

					// Find the position in the previous frame of each touch.
					Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
					Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

					// Find the magnitude of the vector (the distance) between the touches in each frame.
					float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
					float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

					// Find the difference in the distances between each frame.
					float deltaMagnitudeDiff = zoomRatio*zoomRatio*zoomRatio*(prevTouchDeltaMag - touchDeltaMag);
					
					transform.localScale -= new Vector3 (deltaMagnitudeDiff, deltaMagnitudeDiff, deltaMagnitudeDiff);
					ArcBall.CurrentScale = transform.localScale;
					Instantiation.Tracking.transform.localScale = ArcBall.CurrentScale;
				}
				else if (Input.touchCount==1){
					if(moving){
						dragPosition += Input.mousePosition - prevMousePosition;
						endDrag = MapToSphere (dragPosition);

						Vector3 axis = Vector3.Cross (startDrag, endDrag).normalized;
						float angle = Vector3.Angle (startDrag, endDrag);

						Quaternion dragR = Quaternion.AngleAxis (angle, axis);

						transform.rotation = dragR * downR;

						prevMousePosition = Input.mousePosition;
						ArcBall.CurrentRotation = transform.rotation;
						rotationAxis.transform.rotation = ArcBall.CurrentRotation;
						Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;
					}else{
						Touch touch=Input.touches[0];
						if (touch.phase == TouchPhase.Began){
							RaycastHit hitInfo = new RaycastHit ();
							bool hiting = Physics.Raycast (Camera.main.ScreenPointToRay (touch.position), out hitInfo);
							if (hiting) {
								string clickedname = hitInfo.transform.gameObject.name;
								if (clickedname == "Background"){
									downR = transform.rotation;
									prevMousePosition = dragPosition = Input.mousePosition;
									startDrag = MapToSphere (dragPosition);
									moving=true;
								}
							}
						}
					}
			}

			

					

			#else
			////FOR ROTATION WITHOUT CONTROL
			//if (Input.GetMouseButtonUp (0)) { turnEmbryo = false; 	activeTurnEmbryo = false; }
			//if (Input.GetMouseButtonDown (0)) activeTurnEmbryo = true;
			//if(activeTurnEmbryo && Input.GetAxis("Mouse X")!=0){
			//		turnEmbryo = true;

			if (!Input.GetMouseButton (0) && !Input.GetMouseButton (1) && Input.GetKey (KeyCode.LeftControl)) { //Turn the embryo (Arcball)
				//rotationAxis.SetActive(true);
				if (!dragging) {
					downR = transform.rotation;
					prevMousePosition = dragPosition = Input.mousePosition;
					startDrag = MapToSphere (dragPosition);
					dragging = true;
				} else {
					dragPosition += Input.mousePosition - prevMousePosition;
					endDrag = MapToSphere (dragPosition);

					Vector3 axis = Vector3.Cross (startDrag, endDrag).normalized;
					float angle = Vector3.Angle (startDrag, endDrag);

					Quaternion dragR = Quaternion.AngleAxis (angle, axis);

					transform.rotation = dragR * downR;

					prevMousePosition = Input.mousePosition;
					ArcBall.CurrentRotation = transform.rotation;
					rotationAxis.transform.rotation = ArcBall.CurrentRotation;
					Instantiation.Tracking.transform.rotation = ArcBall.CurrentRotation;

				
				}
			} else {
				
				dragging = false;

				//rotationAxis.GetComponent<Renderer> ().enabled = false;
			}
				
			if (!Input.GetMouseButton (0) && !Input.GetMouseButton (1) && Input.GetKey (KeyCode.LeftAlt)) { //Move the embryo center
				//rotationAxis.SetActive (true);
				if (!moving) {
					prevMousePosition = Input.mousePosition;
					moving = true;
				} else {
					transform.position += (Input.mousePosition - prevMousePosition) * speedmoving;
					prevMousePosition = Input.mousePosition;
					ArcBall.CurrentPosition = transform.position;
					rotationAxis.transform.position = ArcBall.CurrentPosition;
					Instantiation.Tracking.transform.position = ArcBall.CurrentPosition;
				}
			} else {
				
				moving = false;
			}
			if(moving || dragging)rotationAxis.SetActive (true);
			else rotationAxis.SetActive (false);

			if (Input.GetAxis("Mouse ScrollWheel") < 0) // Zoom forward
			{
				transform.localScale  += new Vector3 (zoomRatio, zoomRatio, zoomRatio);
				//Debug.Log ("forward transform.localScale=" + transform.localScale);
				ArcBall.CurrentScale = transform.localScale;
				Instantiation.Tracking.transform.localScale = ArcBall.CurrentScale;
			}
			if (Input.GetAxis("Mouse ScrollWheel") > 0) // Zoom backward
			{
					transform.localScale -= new Vector3 (zoomRatio, zoomRatio, zoomRatio);
					if(transform.localScale.x<=0)transform.localScale= new Vector3 (zoomRatio, zoomRatio, zoomRatio);
					//Debug.Log ("backward transform.localScale=" + transform.localScale);
					ArcBall.CurrentScale = transform.localScale;
					Instantiation.Tracking.transform.localScale = ArcBall.CurrentScale;

			}
			#endif
		}

	}
		

	/// <summary>
	/// Maps mouse position onto the arcball sphere as local unit vector
	/// </summary>
	private Vector3 MapToSphere(Vector3 position){
		Ray ray = Camera.main.ScreenPointToRay(position);

		Vector3 normal = (transform.position - Camera.main.transform.position).normalized;
		Plane plane = new Plane(normal, transform.position);
		float dist = 0;
		plane.Raycast(ray, out dist);
		Vector3 hitPoint = ray.GetPoint(dist);
		//Debug.Log ("hitPoint="+hitPoint);
		float length = Vector3.Distance(hitPoint, transform.position);
		if(length<radius){//on arcball
			float k = (float) (Mathf.Sqrt(radius - length));
			hitPoint -= normal*k;
		} else {//outside.
			Vector3 dir = (hitPoint - transform.position).normalized;
			hitPoint = transform.position + dir*radius;
		}

		return (hitPoint-transform.position).normalized;
	}

	public static bool outOfPlan(Vector3  gr){
		Vector3 grC = new Vector3 ((gr[0] + Instantiation.embryoCenter.x), (gr [1] - Instantiation.embryoCenter.y), (gr [2] - Instantiation.embryoCenter.z));
		Vector3 coordrotate =  ArcBall.CurrentRotation * grC;
		if (coordrotate. z< Instantiation.SliderFixedPlan.value) return true;
		return false;
	}




}