﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class onMouseOver : MonoBehaviour {

	private Vector3 prevMousePosition;
	private int wait=0;
	public GameObject current;

	private bool isMainMenu(){
		if (Instantiation.canvas.transform.Find ("MenuDataset").gameObject.activeSelf) return true;
		if (Instantiation.canvas.transform.Find ("MenuInfos").gameObject.activeSelf) return true;
		if (Instantiation.canvas.transform.Find ("MenuObjects").gameObject.activeSelf) return true;
		if (Instantiation.canvas.transform.Find ("MenuGroups").gameObject.activeSelf) return true;
		if (Instantiation.canvas.transform.Find ("MenuGenetic").gameObject.activeSelf) return true;
		if (Instantiation.canvas.transform.Find ("MenuMovie").gameObject.activeSelf) return true;
		return false;

	}
	void Update () {

		if (Input.mousePosition == prevMousePosition) wait++;
		else wait = 0;
		if (wait >= 100) {
			current = null;
			PointerEventData pe = new PointerEventData (EventSystem.current);
			pe.position = Input.mousePosition;
			List<RaycastResult> hits = new List<RaycastResult> ();
			EventSystem.current.RaycastAll (pe, hits);
			foreach (RaycastResult h in hits) {
				bool toCheck = true;
				if (h.gameObject.name == "BoutonDataset" && isMainMenu()) toCheck = false;
				if (h.gameObject.name == "BoutonInfos" && isMainMenu()) toCheck = false;
				if (h.gameObject.name == "BoutonObjects" && isMainMenu()) toCheck = false;
				if (h.gameObject.name == "BoutonGroups" && isMainMenu()) toCheck = false;
				if (h.gameObject.name == "BoutonGenetic" && isMainMenu()) toCheck = false;
				if (h.gameObject.name == "BoutonMoovie" && isMainMenu()) toCheck = false;
				if (toCheck && h.gameObject.transform.Find ("comment") != null) current = h.gameObject.transform.Find ("comment").gameObject;
			}
			if (current != null) current.SetActive (true);

		} else if (current != null) {
			current.SetActive (false);
			current = null;
		}
		prevMousePosition = Input.mousePosition;
	}
		
}