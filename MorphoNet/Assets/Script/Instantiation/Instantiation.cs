﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using SimpleJSON;
using System.Text;
using System;
using ICSharpCode.SharpZipLib.BZip2;
using System.IO;
using UnityEngine.Networking;


public class Instantiation : MonoBehaviour
{
	public static Dictionary<int,List<meshType> > meshToDownload; //List of Mesh To Donwload
	public static int id_dataset; //id of dataset -1 if is public
	public static int isBundle;//is the embryo data set exist in assetbunde ?
	public static string bundleType;
	public static int MinTime; //Starting point
	public static int MaxTime; //Last Time point
	public static string embryo_name; //string contenant le nom de l'embryon courant;
	public Text datasetname; //Just to show the embryo name
	public static int Quality=0;//Value of quality of the mesh (stored in the DB)
	public static int id_owner; //ID Propriétaire du dataset
	public static int id_type;
	public static GameObject EmbryosGO; //Main Embryo Game Object
	public static GameObject [] EmbryoT; //List of Embryons, tous les embryons MQ sont préchargés
	public static Dictionary<string, Cell> []  CellT; //List of Cells for each Embryons
	public GameObject MorphoNetLogo;

	//Canvas Scale
	public static float canvasScale=1F;
	public static float initcanvasScale=0.01302083F; //Standard Scale for the Canvas 
	public static float canvasHeight=718;
	public static float canvasWidth=1007;//Default Exprort Value
	public static GameObject canvas;


	//Cut On Zoom
	public static Vector3 ZommCutPlan;


	//Gestion du temps
	public static int CurrentTime; // on le commence à 1 
	public static int LoadingTime;  //Temps à chargé en MQ 
	private static int PreviousTime;


	//Developmettal ...
	public static int dt; //Difference between 2 stacks in seconds
	public static int spf;//Hours post fertilization in seconds
	public Text hpf;
	public static string start_stage;
	public static string end_stage;


	//Boutton pour bouger dans le temps
	public GameObject TimeBar; //Main Time Bar
	public GameObject BoutonLeft;
	public GameObject BoutonRight;
	public static int scrolledVal; //Valeur de la scrollbar temporel
	public GameObject scrollbarTime;
	public GameObject textTime;

	//For Multiple Link Bundle Per Download
	public static string [] BundleLinks;
	public static int nbCurentLink = 0;

	public InputField SearchField; //Search Input text
	public Text currenttextloaded; //Text draw for informations loading 
	public GameObject debugText;

	//Embryo cut
	public static int XMin=0;
	public static int XMax=int.MaxValue;
	public static int YMin=int.MinValue;
	public static int YMax=int.MaxValue;
	public static int ZMin=int.MinValue;
	public static int ZMax=int.MaxValue;
	public static Slider SliderXMin;
	public static Slider SliderXMax;
	public static Slider SliderYMin;
	public static Slider SliderYMax;
	public static Slider SliderZMin;
	public static Slider SliderZMax;

	public static Slider SliderFixedPlan;

	//Main GO Tracking
	public static GameObject Tracking;

	//Tissue
	public GameObject MenuCells;

	//SHARE
	public GameObject MenuShare;


	//Light
	public Light DirectionalLight;
	public LightShadows shadows=LightShadows.None;
	public void turnShadows(){ 
		if (shadows == LightShadows.None)
			shadows = DirectionalLight.shadows;
		if (DirectionalLight.shadows == LightShadows.None)
			DirectionalLight.shadows = shadows;
		else
			DirectionalLight.shadows = LightShadows.None;
	}

	//Font
	public static Font mainFont;
	public static Material Text3D;

	//Shaders
	public static Material Selected;
	public static Material Default;
	public static Material Diamond;
	public static Material Dark;
	public static Material Vertex;
	public static Material Bumped;
	public static Material Brick;
	public static Material Stars;
    public static Material Wireframe;

	//Keyboard action
	private float keyDelay = 0.2f;  //1 Second
	private float timePassed=0f;

	//User Attribute
	public Text login;
	public static string nameUser;
	public static string surnameUser;
	public static int IDUser;
	public static int userRight; //0:CREATOR; 1:USER; 2:READER
   

    //FOR DB
    public static string hash = "yourhashcode";  //For safety communication...
	public static string urlSERVER;
    public static string urlLog ; 
	public static string urlUsers ; 
	public static string urlUpload;
	public static string urlDataset;
	public static string urlSelection;
	public static string urlColormap;
	public static string urlShare;
	public static string urlScenario;
	public static string urlMovie;
	public static string urlAdmin;
	public static string urlPlayAPI;
	public static string urlCurration;
	public static string urlAPI;
	public static string urlLineage;
	public static string urlCorrespondence;

	public static string urlAniseed;


	//FOR VECTOR FIELD
	public static Vector3 embryoCenter=Vector3.zero;
	//FOR POINTS 3D
	public static GameObject sphere;

	//For Channel CELL VISDUALISATION
	public static Dictionary<string, ChannelObject> Channels; //List of all channels
	public GameObject channelDefault;
	public static int timeChangeShader=10; //Number of update before changing shader for multiple material

	//When we search a cell
	public static bool cellsearched;

    //Cut
    public static bool realcut;

	public static GameObject goShowString;//To Show String Information

	//List Morpho Format
	public static Dictionary<string,string>MorphoFormat;

    public GameObject MenuShortcuts;

    //Main initialisation
    void Start()
    {
        createMorphoFormat();
        Channels = new Dictionary<string, ChannelObject>();
        channelDefault.SetActive(false);

        realcut = false;
        canvas = GameObject.Find("Canvas").gameObject;
        if (canvas == null) Debug.LogError("Canvas not found....");
        canvasScale = canvas.transform.localScale.x;
        canvasWidth = canvas.GetComponent<RectTransform>().sizeDelta.x;
        canvasHeight = canvas.GetComponent<RectTransform>().sizeDelta.y;

        cellsearched = false;
        CurrentTime = 1;
        LoadingTime = 1;
        PreviousTime = -1;

        bundleType = "WebGL";
#if UNITY_ANDROID
			bundleType="Android";
#endif
        Application.runInBackground = true;



        if (Quality == -1) Quality = 0;
        //Load Shaders
        Selected = Resources.Load("SelectedCell", typeof(Material)) as Material;
        Default = Resources.Load("StandardCell", typeof(Material)) as Material;
        Diamond = Resources.Load("WhiteGem", typeof(Material)) as Material;
        Dark = Resources.Load("Rough Textured Metal", typeof(Material)) as Material;
        Vertex = Resources.Load("Robot MatCap Plain Additive", typeof(Material)) as Material;
        Bumped = Resources.Load("Bubble wrap pattern", typeof(Material)) as Material;
        Brick = Resources.Load("Lavabrick", typeof(Material)) as Material;
        Stars = Resources.Load("Stars", typeof(Material)) as Material;
        Wireframe = Resources.Load("Wireframe - ShadedUnlit", typeof(Material)) as Material;

        //Background Default Color
        Material BackgroundMat = Resources.Load("BackgroundMat", typeof(Material)) as Material;
        Color newCol;
        if (ColorUtility.TryParseHtmlString("#61BAE9", out newCol))
            BackgroundMat.color = newCol;

        //For Text visualisation (cell names etc..)
        Text3D = Resources.Load("Shaders/Text3D", typeof(Material)) as Material;
        mainFont = Resources.Load("fonts/Ayuthaya", typeof(Font)) as Font;


        //Load Sphere elemnt to plot center or upload Point
        sphere = Resources.Load("Sphere", typeof(GameObject)) as GameObject;
        //TODO TEST  sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        //We hide all unecessarry button according the file infos downlaoded
        //SPACE 
        canvas.transform.Find("MenuClicked").gameObject.transform.Find("Actions").gameObject.transform.Find("NeighborsSelect").gameObject.SetActive(false);
        //TIME
        initializeTime();

        //Create Embryos
        EmbryosGO = GameObject.Find("Embryos");
        EmbryoT = new GameObject[MaxTime + 1];
        for (int t = 0; t <= MaxTime; t++) EmbryoT[t] = null;

        //Create Cells
        CellT = new Dictionary<string, Cell>[MaxTime + 1];
        for (int t = 0; t <= MaxTime; t++) CellT[t] = new Dictionary<string, Cell>();

        //For Tracking
        Tracking = GameObject.Find("Tracking");

       // MenuShortcuts.SetActive(true);

        CurrentTime = MinTime;
        LoadingTime = MinTime;

        login.text = surnameUser + " " + nameUser;

        //Initialize Show string on element
        goShowString = GameObject.Find("ShowString");
        goShowString.name = "ShowString";
        goShowString.transform.localScale = new Vector3(canvasScale * 10F, canvasScale * 10F, canvasScale * 10F);
        goShowString.AddComponent<TextMesh>();
        goShowString.GetComponent<TextMesh>().text = "";
        goShowString.GetComponent<TextMesh>().font = Instantiation.mainFont;
        goShowString.GetComponent<Renderer>().material = Instantiation.Text3D;

        if(embryo_name.Contains('(')){
            string[] enames = embryo_name.Split('(');
            datasetname.text = enames[0].Trim();
        }
        else datasetname.text = embryo_name.Trim();

        //DOWNLOAD COMMENTS FOR THE DATASET
        StartCoroutine(downloadComments());
        //DONWLOAD DEVELOPMENTAL TABLE
        StartCoroutine(DevelopmentTable.downloadDevelopmentalTable(Instantiation.id_type));

        //DONWLOAD INFORMATIONS
        MenuCurrated.init(); //Initialize Curation Menu
        MenuCell.Init();
        StartCoroutine(MenuCell.start_Download_Correspondences()); //WE START TO DOWNLOAD ALL INFOS 

        //DONWLOAD MESHS
        meshToDownload = new Dictionary<int, List<meshType>>(); //TODO WE CAN DELETE THIS 2 VARIABLES AT THE END OF THE LOADING PROCESS
        StartCoroutine(start_Download_Mesh_Type()); // WE START TO TO DOWNLOAD ALL MESHS

        //RESCALE BUTTONS AND MENU
        canvas.GetComponent<Menus>().organizeButtons(); //RESCALE BUTTONS AND MENU

        //RESCALE SLIDER FOR CUTTING
        updateEmbryoCutting();

        //REMOVE HPF IF DT IS NOT PRECISE
        if (spf == -1 || dt <= 0) canvas.transform.Find("TimeBar").gameObject.transform.Find("hpf").gameObject.SetActive(false);
        else hpf.text = convertTimeToHpf(0);

        if (userRight >= 2)
        {
            canvas.transform.Find("MenuDataset").gameObject.transform.Find("downloadMesh").gameObject.SetActive(false);
            canvas.transform.Find("MenuDataset").gameObject.transform.Find("downloadMeshes").gameObject.SetActive(false);
        }

    }

    public void enLoad()
    {
        MenuShortcuts.SetActive(false);
    }


	public void setComment(string n){
		if(currenttextloaded.text!=n) currenttextloaded.text=n;
		if(debugText.activeSelf)debugText.GetComponent<Text>().text=debugText.GetComponent<Text>().text+"\n"+n;
		#if UNITY_EDITOR
			Debug.Log(n);
		#endif
	}
	public static void setURLServer(string t_url){
        Instantiation.urlSERVER = t_url;
		Instantiation.urlLog = urlSERVER+"checkUsers.php"; 
		Instantiation.urlUsers = urlSERVER+"changeUsers.php"; 
		Instantiation.urlUpload=urlSERVER+"upload.php";
		Instantiation.urlDataset=urlSERVER+"dataset.php";
		Instantiation.urlSelection=urlSERVER+"selection.php";
		Instantiation.urlColormap=urlSERVER+"colormap.php";
		Instantiation.urlShare=urlSERVER+"share.php";
		Instantiation.urlScenario=urlSERVER+"scenario.php";
		Instantiation.urlMovie=urlSERVER+"movie.php";
		Instantiation.urlAdmin=urlSERVER+"admin.php";
		Instantiation.urlPlayAPI=urlSERVER+"playapi.php";
		Instantiation.urlCurration=urlSERVER+"curration.php";
		Instantiation.urlAPI=urlSERVER+"VirtualEmbryo.py";
		Instantiation.urlLineage=urlSERVER+"lineage.php";
		Instantiation.urlCorrespondence=urlSERVER+"correspondence.php";
		Instantiation.urlAniseed=urlSERVER+"askDB.php";
	}

	public static void createMorphoFormat (){
		MorphoFormat = new  Dictionary<string,string> ();
		MorphoFormat ["time"] = " t,id,ch:t,id,ch";
		MorphoFormat ["space"] = "t,id,ch:t,id,ch";
		MorphoFormat ["float"] = "t,id,ch:float";
		MorphoFormat ["string"] = "t,id,ch:string";
		MorphoFormat ["group"] = "t,id,ch:string";
		MorphoFormat ["selection"] = "t,id,ch:int";
		MorphoFormat ["color"] = "t,id,ch:r,g,b";
		MorphoFormat ["dict"] = "t,id,ch:t,id,ch:float";
		MorphoFormat ["sphere"] = "t,id,ch:x,y,z,r";
		MorphoFormat ["vector"] = "t,id,ch:x,y,z,r:x,y,z,r";
	}
	public static string getIdxFormat(int idx){
		int i = 0;
		foreach (KeyValuePair<string,string> mf in MorphoFormat) {
			if (i == idx)
				return mf.Key;
			i += 1;
		}
		return "";
	}
	//Define Plan On Zomm Cut
	public void OnCutZoom(float v){ ZommCutPlan = new Vector3 (v, v, v); 	}

	//On click on the button reset rotation
	public void resetRotation(){ 
		ArcBall.CurrentRotation = ArcBall.InitialRotation;
		ArcBall.CurrentPosition = ArcBall.InitialTranslation;
		ArcBall.CurrentScale=ArcBall.InitialScale;
		ArcBall.update = true;
	}
	public void saveRotation(){ if(Instantiation.userRight<=1)StartCoroutine (launchSaveRotation ());}
	public IEnumerator launchSaveRotation(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "9");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("rotation", ArcBall.CurrentRotation.ToString ());
		form.AddField ("translation", ArcBall.CurrentPosition.ToString ());
		form.AddField ("scale", ArcBall.CurrentScale.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError) Debug.Log ("Error : " + www.error); 
		www.Dispose (); 
	}

	//To REally Cut the embryo
	public void onClickCut(){ realcut = !realcut;}


	//Share Dataset
	public void shareDataset(){
		if (MenuShare.activeSelf)
			MenuShare.SetActive (false);
		else {
			MenuShare.SetActive (true);
			MenuShare.transform.position = new Vector3 (Mathf.RoundToInt(-Instantiation.canvasWidth/2f+273)*Instantiation.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
			MenuShare.GetComponent<ShareWith> ().basename = "dataset";
			MenuShare.GetComponent<ShareWith> ().id_base = id_dataset;
			MenuShare.GetComponent<ShareWith> ().listPeople ();
		}
	}

	//EMBRYO CUT
	//Call at initialisation 
	public static void updateEmbryoCutting(){
		GameObject ce=canvas.transform.Find("MenuDataset").gameObject;
		if(ce!=null){
			//Define  EMBRYO CUT Sliders
			SliderXMin=ce.transform.Find ("SliderXMin").gameObject.GetComponent<Slider> ();SliderXMin.minValue=int.MaxValue;SliderXMin.value=int.MinValue;SliderXMin.maxValue=int.MinValue;
			SliderXMax=ce.transform.Find ("SliderXMax").gameObject.GetComponent<Slider> ();SliderXMax.minValue=int.MaxValue;SliderXMax.value=int.MaxValue;SliderXMax.maxValue=int.MinValue;
			SliderYMin=ce.transform.Find ("SliderYMin").gameObject.GetComponent<Slider> ();SliderYMin.minValue=int.MaxValue;SliderYMin.value=int.MinValue;SliderYMin.maxValue=int.MinValue;
			SliderYMax=ce.transform.Find ("SliderYMax").gameObject.GetComponent<Slider> ();SliderYMax.minValue=int.MaxValue;SliderYMax.value=int.MaxValue;SliderYMax.maxValue=int.MinValue;
			SliderZMin=ce.transform.Find ("SliderZMin").gameObject.GetComponent<Slider> ();SliderZMin.minValue=int.MaxValue;SliderZMin.value=int.MinValue;SliderZMin.maxValue=int.MinValue;
			SliderZMax=ce.transform.Find ("SliderZMax").gameObject.GetComponent<Slider> ();SliderZMax.minValue=int.MaxValue;SliderZMax.value=int.MaxValue;SliderZMax.maxValue=int.MinValue;

			ce.transform.Find ("SliderFixedPlan").gameObject.SetActive (false);
			SliderFixedPlan=ce.transform.Find ("SliderFixedPlan").gameObject.GetComponent<Slider> (); SliderFixedPlan.minValue=int.MaxValue; SliderFixedPlan.value=0; SliderFixedPlan.maxValue=int.MinValue;
		}
	}
	//We update the boundaries of the sliders accord the different gravity center
	public static void updateXYZCutSliders(Vector3 Grav){
		//X
		if (Grav [0] < SliderXMin.minValue) {
			SliderXMin.minValue = (int)Mathf.Floor(Grav[0])-1;
			SliderXMax.minValue = SliderXMin.minValue;
			SliderXMin.value = SliderXMin.minValue;  //SLIDER WHERE NOT YET MOVE
		}
		if (Grav [0] > SliderXMin.maxValue) {
			SliderXMin.maxValue = (int)Mathf.Floor(Grav[0])+1;
			SliderXMax.maxValue = SliderXMin.maxValue;
			SliderXMax.value=SliderXMax.maxValue;  //SLIDER WHERE NOT YET MOVE
		}
		//Y
		if (Grav [1] < SliderYMin.minValue) {
			SliderYMin.minValue =(int)Mathf.Floor(Grav[1])-1;
			SliderYMax.minValue = SliderYMin.minValue;
			SliderYMin.value = SliderYMin.minValue;  //SLIDER WHERE NOT YET MOVE
		}
		if (Grav [1] > SliderYMin.maxValue) {
			SliderYMin.maxValue = (int)Mathf.Floor(Grav[1])+1;
			SliderYMax.maxValue = SliderYMin.maxValue;
			SliderYMax.value=SliderYMax.maxValue;  //SLIDER WHERE NOT YET MOVE
		}

		//Z
		if (Grav [2] < SliderZMin.minValue) {
			SliderZMin.minValue = (int)Mathf.Floor(Grav[2])-1;
			SliderZMax.minValue = SliderZMin.minValue;
			SliderZMin.value = SliderZMin.minValue;  //SLIDER WHERE NOT YET MOVE
		}
		
		if (Grav [2] > SliderZMin.maxValue) {
			SliderZMin.maxValue = (int)Mathf.Floor(Grav[2])+1;
			SliderZMax.maxValue = SliderZMin.maxValue ;
			SliderZMax.value=SliderZMax.maxValue;  //SLIDER WHERE NOT YET MOVE
		}
		//FOR FIXED PLAN
		for (int i = 0; i < 3; i++) {
			if (Grav [i] > SliderFixedPlan.maxValue) {
				SliderFixedPlan.maxValue = (int)Mathf.Floor (Grav [i]) + 1;
			}
			if (Grav [i] < SliderFixedPlan.minValue) {
				SliderFixedPlan.minValue = (int)Mathf.Floor(Grav[i])-1;
			}
		}
	}
	public void Xmin(){ XMin = (int)Mathf.Round (SliderXMin.value);}
	public void Xmax(){ XMax = (int)Mathf.Round (SliderXMax.value);}
	public void Ymin(){ YMin = (int)Mathf.Round (SliderYMin.value);}
	public void Ymax(){ YMax = (int)Mathf.Round (SliderYMax.value);}
	public void Zmin(){ ZMin = (int)Mathf.Round (SliderZMin.value);}
	public void Zmax(){ ZMax = (int)Mathf.Round (SliderZMax.value);}

	public void onFixedPlan(bool v){
		GameObject ce=canvas.transform.Find("MenuDataset").gameObject;
		if (ce != null) {
			ce.transform.Find ("XMin").gameObject.SetActive (!v);
			ce.transform.Find ("XMax").gameObject.SetActive (!v);
			ce.transform.Find ("YMin").gameObject.SetActive (!v);
			ce.transform.Find ("YMax").gameObject.SetActive (!v);
			ce.transform.Find ("ZMin").gameObject.SetActive (!v);
			ce.transform.Find ("ZMax").gameObject.SetActive (!v);
			ce.transform.Find ("SliderXMin").gameObject.SetActive (!v);
			ce.transform.Find ("SliderXMax").gameObject.SetActive (!v);
			ce.transform.Find ("SliderYMin").gameObject.SetActive (!v);
			ce.transform.Find ("SliderYMax").gameObject.SetActive (!v);
			ce.transform.Find ("SliderZMin").gameObject.SetActive (!v);
			ce.transform.Find ("SliderZMax").gameObject.SetActive (!v);
			ce.transform.Find ("SliderFixedPlan").gameObject.SetActive (v);
		}
	}
	////////////////////////////   CELLS
	//Create a new cell
	public static Cell createCell(int t, string id)
	{
		if (t >= Instantiation.MinTime && t <= Instantiation.MaxTime) {
			//Debug.Log("Create Cell " +id + " at " +t);
			Cell c = new Cell (t, id);
			CellT [t] [id] = c;
			return c;
		}//else Debug.Log("Time " +t + " is out of range  " + Instantiation.MinTime + " and " +  Instantiation.MaxTime);
		return null;
	}
	//Return a cell from a string define by tuple
	//Get Cell From the tuple t,id,ch
	public static Cell getCell(string cellstr,bool create){
		string[] cellstrs = cellstr.Split (',');
		string id = "";
		int t;
		//Debug.Log ("Get Cell :" + cellstrs.Count ()+"->"+cellstr);
		switch (cellstrs.Count ()) { 
			case 0: //Not possible
				return null;
				break;
			case 1: //Just name
				t = 0;
				id = cellstrs [0].Trim ();
				break;
			case 2: //Time + Name
				if (!int.TryParse (cellstrs [0].Trim (), out t))
					return null;
				id = cellstrs [1].Trim ();
				break;
			case 3: //Time,Name,Channel
				if (!int.TryParse (cellstrs [0].Trim (), out t))
					return null;
				id = cellstrs [1].Trim ();
				break;
			default:
				return null;
				break;
		}
		Cell c = getCell (t, id);
		if (c == null && create)
			c = createCell (t, id);
		//Debug.Log (" --> Found cell " + c.ID);
		return c;
	}


	
	public static Cell getCell(int t, string id)
	{
		if (t>=MinTime && t<=MaxTime)
			if (CellT [t].ContainsKey (id))
				return CellT [t] [id];
		return null;
	}

	public static Cell getCell(GameObject go,bool create)
	{
		if (go != null) return getCell(go.name,create);
		return null;
	}
	

	public void LoadObjects(GameObject goe, int t){
		//Debug.Log ("LoadObjects at " + t);
		if (goe != null) {
			Component ret = goe.GetComponent<Component> ();
			if (ret != null) {
				Transform trns = goe.transform;
				int nbCell = trns.childCount;
				for (int c = 0; c < nbCell; c++) {
					GameObject to = trns.GetChild (c).gameObject;
					to.transform.localScale = new Vector3 (1f, 1f, 1f);
					to.transform.Translate (embryoCenter.x * Instantiation.initcanvasScale, -embryoCenter.y * Instantiation.initcanvasScale, -embryoCenter.z * Instantiation.initcanvasScale);
					if(to.name.Count()>5 && to.name.Substring (0, 4) == "cell") to.name = t + "," + to.name.Substring (5) + ",0"; //OLD VERSION
					Cell ce = getCell (to, true);
					if (ce != null) {
						//Debug.Log ("Found Cell "+ce.ID+" at " + ce.t);
						GameObject go = to.gameObject;
						string channelName = getChannelString (go.name);
						ChannelObject cho = createChannel (channelName); //We have to create a sub menu for each channel
						ce.addChannel (channelName, go, cho);
					} else
						Debug.Log ("No cell " + to.name);// TODO WE HAVE TO REMOVE THIS CHILD ?? 
				}
			}
		}
	}
	
	////////////////////////////   CHANNEL
	//Active or Desactive Membrane Visualisation
	public ChannelObject createChannel(string ch){
		//Debug.Log ("Create Channel " + ch);
		if (!Instantiation.Channels.ContainsKey (ch)) { //Only if it was not previously created
			Instantiation.Channels[ch]=new ChannelObject(ch,channelDefault);
		}
		return Instantiation.Channels [ch];
	}
	//Return the channel from a tuple
	public static string getChannelString(string cellstr){
		string[] cellstrs = cellstr.Split (',');
		if (cellstrs.Count () != 3)
			return "";

		int t;
		if(!int.TryParse (cellstrs [0].Trim(), out t)) 
			return "";

		string ch = cellstrs [2].Trim();

		return ch;
	}


	////////////////////////////   TIME
	private string convertTimeToHpf(int t){
		return (Math.Round(100.0*(t * dt + spf)/3600.0)/100.0).ToString()+"hpf";
	}


	public void initializeTime(){
		
		if (MinTime != MaxTime) {
		  	//Debug.Log ("MinTime=" + MinTime + "MaxTime=" + MaxTime); 
			scrollbarTime.transform.GetComponent<Slider> ().maxValue = MaxTime;
			scrollbarTime.transform.GetComponent<Slider> ().minValue = MinTime;
			scrollbarTime.transform.GetComponent<Slider> ().value = MinTime;
			//scrolledVal = MinTime;
			//textTime.GetComponent<InputField> ().text = scrolledVal.ToString ("0");
			TimeBar.SetActive (true);
			canvas.transform.Find ("MenuClicked").gameObject.transform.Find ("Actions").gameObject.transform.Find ("AllTimes").gameObject.SetActive (true);

		} else {
			
			TimeBar.SetActive (false);
			canvas.transform.Find ("MenuClicked").gameObject.transform.Find ("Actions").gameObject.transform.Find ("AllTimes").gameObject.SetActive (false);
			GameObject MenuObjects = canvas.transform.Find ("MenuObjects").gameObject.transform.Find ("Selection").gameObject;
			MenuObjects.transform.Find ("thiscells").gameObject.transform.Find ("AllTimes").gameObject.SetActive (false);
			MenuObjects.transform.Find ("thisSelection").gameObject.transform.Find ("ThisAllTimes").gameObject.SetActive (false);
			MenuObjects.transform.Find ("thisSelection").gameObject.transform.Find ("ThisAllTimes").gameObject.SetActive (false);
			MenuObjects.transform.Find ("thisSelection").gameObject.transform.Find ("propagateThis").gameObject.SetActive (false);
			MenuObjects.transform.Find ("thisSelection").gameObject.transform.Find ("propagateThisPast").gameObject.SetActive (false);
			MenuObjects.transform.Find ("thisSelection").gameObject.transform.Find ("propagateThisFutur").gameObject.SetActive (false);

			MenuObjects.transform.Find ("allSelection").gameObject.transform.Find ("AllAllTimes").gameObject.SetActive (false);
			MenuObjects.transform.Find ("allSelection").gameObject.transform.Find ("propagateAll").gameObject.SetActive (false);
			MenuObjects.transform.Find ("allSelection").gameObject.transform.Find ("propagateAllPast").gameObject.SetActive (false);
			MenuObjects.transform.Find ("allSelection").gameObject.transform.Find ("propagateAllFutur").gameObject.SetActive (false);

			MenuObjects.transform.Find ("NonSelected").gameObject.transform.Find ("AllTimes").gameObject.SetActive (false);

			GameObject MenuInfos = canvas.transform.Find ("MenuInfos").gameObject;
			MenuInfos.transform.Find ("CellMotherID").gameObject.SetActive (false);
			MenuInfos.transform.Find ("LineMotherCell").gameObject.SetActive (false);
			MenuInfos.transform.Find ("LineCellDaughters").gameObject.SetActive (false);
			MenuInfos.transform.Find ("CellDaughtersID").gameObject.SetActive (false);
	
		}
	}

	//Centralize version
	public void scrollTime(){
		scrollbarTime.transform.GetComponent<Slider> ().value = scrolledVal;  // -> GO TO updateScrollTime
	}
	//Update Time Text From Scroll bar
	public void updateScrollTime(Single newt){
		scrolledVal = (int)newt;
		textTime.GetComponent<InputField>().text = scrolledVal.ToString ("0");
		//Debug.Log ("updateTime to " + scrolledVal);
		hpf.text = convertTimeToHpf (scrolledVal);	
		CurrentTime = scrolledVal;
	}

	//FROM RIGHT BUTTON
	public void scrollNext()
	{
		if(scrolledVal<MaxTime) scrolledVal += 1;
		//Debug.Log ("scrollNext "+CurrentTime+" scrolledVal=" + scrolledVal);
		scrollTime ();
	}
	//FROM LEFT BUTTON
	public void scrollPrevious()
	{
		if(scrolledVal>MinTime) scrolledVal -= 1;
		//Debug.Log ("scrollPrevious "+CurrentTime+" scrolledVal=" + scrolledVal);
		scrollTime ();
	}


	//Main update from outside fonction
	public static void setTime(int t){
		canvas.GetComponent<Instantiation> ().updateTextTime (t.ToString ());
	}

	//Update Time Bar from Text
	public void updateTextTime(string textVal){
		int parsedVal;
		if (int.TryParse (textVal, out parsedVal)) {
			scrolledVal = parsedVal;
		} else {
			textVal = scrolledVal.ToString ("0");
		}
		scrollTime ();
	}

	
    //Calcul Frame Rate
	//Frame rate
	public Text Framerate;
	private int m_frameCounter = 0;
	private float m_timeCounter = 0.0f;
	private float m_lastFramerate = 0.0f;
	private float m_refreshTime = 0.5f;
	private int last_fps=0;
	private void drawFrameRate(){
		//if (Time.frameCount % 300 == 0) System.GC.Collect(); //Clear Memory
		if( m_timeCounter < m_refreshTime )
		{
			m_timeCounter += Time.deltaTime;
			m_frameCounter++;
		}
		else
		{
			//This code will break if you set your m_refreshTime to 0, which makes no sense.
			m_lastFramerate = (float)m_frameCounter/m_timeCounter;
			m_frameCounter = 0;
			m_timeCounter = 0.0f;
		}
		//TODO NOT REFRESH IOF PREIVOUS SAME
		int fps = (int) Math.Round (m_lastFramerate);
		if (last_fps != fps) {
			Framerate.text = fps.ToString () + "fps ";
			if (fps > 50)
				Framerate.color = Color.green;
			else if (fps < 20)
				Framerate.color = Color.red;
			else
				Framerate.color = Color.yellow;
			last_fps=fps;
		}
	   }

	


    void Update()
	{
		drawFrameRate();
		//Debug.Log("update");
		if (canvasScale != canvas.transform.localScale.x) {
			canvasScale = canvas.transform.localScale.x;  //If screen change ....
			canvasWidth = canvas.GetComponent<RectTransform> ().sizeDelta.x;
			canvasHeight = canvas.GetComponent<RectTransform> ().sizeDelta.y;
			canvas.transform.Find ("HelpMorphoNet").GetComponent<RectTransform> ().sizeDelta = new Vector2( canvasWidth, canvasHeight);
			//Debug.Log ("Change Screen Size " + canvasScale+ " canvasWidth=" + canvasWidth + " canvasHeight=" + canvasHeight);
		}
		
		timePassed += Time.deltaTime;
		if (!SearchField.isFocused && Input.GetKey (KeyCode.RightArrow) && timePassed>=keyDelay){
			timePassed = 0f;
			//Debug.Log ("RightArrow");
			scrollNext ();
		}
		if (!SearchField.isFocused && Input.GetKey (KeyCode.LeftArrow) && timePassed >= keyDelay) {
			timePassed = 0f;
			//Debug.Log ("LeftArrow");
			scrollPrevious ();
		}
		
		if (CurrentTime >= MinTime && CurrentTime <= MaxTime) {
			//TIME CHANGEMENT
			if (CurrentTime != PreviousTime) { //Time step changes...
				//Debug.Log("Time Changement ="+CurrentTime+"!="+PreviousTime);
				if (MenuCells.activeSelf) MenuCells.transform.Find ("Selection").gameObject.GetComponent<SelectionColor> ().OnEnabled (); //Active Cells number if selection if open
				if (!cellsearched) SelectionCell.followSelectedCells(CurrentTime); //We follow in time the selected cells
				//for (int i = 0; i < SelectionCell.clickedCells.Count; i++)Debug.Log (" found clicked cell " + SelectionCell.clickedCells [i].ID + " at " + SelectionCell.clickedCells [i].t+" ->" +SelectionCell.clickedCells [i].selected.ToString());
				if(isSpaceConnection) plotSpaceConnection();
				MenuCell.describe ();
				cellsearched = false;
				if (EmbryoT [CurrentTime] != null) {
					if (PreviousTime >= MinTime && PreviousTime <= MaxTime && EmbryoT [PreviousTime] != null) //DESACTIVE PREVIOUS TIME STEP
						EmbryoT [PreviousTime].SetActive (false);
					//Active Cell Shader (otherwise a wierd pink color appear between transition)
					foreach(KeyValuePair<string,Cell> item in  Instantiation.CellT[CurrentTime]){
						Cell c = item.Value;
						//Debug.Log ("Active Shader for " + c.ID + " at " + c.t+ "->"+c.show);
						if(c.Channels!=null)
							foreach (KeyValuePair<string,Channel>itemc in c.Channels) {
								Channel ch = itemc.Value;
								ch.go.GetComponent<ObjectRender> ().Update ();
							}
					}
					EmbryoT [CurrentTime].SetActive (true);
					PreviousTime = CurrentTime; //maj previoustime

					foreach(KeyValuePair<string, ChannelObject> item in Instantiation.Channels)
						item.Value.updateTrackingVectors(); //Update Tracking Visualisation (to update track length)


					//Debug.Log ("Change time Time=" + CurrentTime);
					if (!Scenario.actionDone)  Scenario.finishAction();   //It was the result of an action
					
				}
			}
		}
    }


	//Search a cell name or cell id
	public void search(){
		string searchtext = SearchField.text.ToString();
		if (searchtext != "") { 
			List<Cell> cellsSearched = new List<Cell> ();
			Cell c = getCell(searchtext,false);
			if (c == null)
				c = getCell (searchtext + ",0",false);
			if (c == null)
				c = getCell (CurrentTime + "," + searchtext + ",0",false);
			if (c == null) { //Search By String informatiopn described in Infos
				string []searchtexts=searchtext.Split(',');
				foreach(Correspondence cor in MenuCell.Correspondences){
					if (c == null && cor.datatype == "string") {
						for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
							if (c == null) {
								Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
								foreach (var itemc in CellAtT) {
									Cell ce = itemc.Value; 
									string cell_inf = ce.getInfos (cor.id_infos);
									foreach (string s in searchtexts) {
										string ss = s.Trim ();
										if (ss!="" && cell_inf.Length >= ss.Length && cell_inf.Substring (0, ss.Length) == ss) {
											//Debug.Log ("Add Cell " + ce.ID + " at " + ce.t);
											cellsSearched.Add (ce);
										}
									}
								}
							}
						}
					}
				}
			} else {
				//Debug.Log ("Add Cell " + c.ID + " at " + c.t);
				cellsSearched.Add (c);
			}
			if(cellsSearched.Count>0){
				//First We Look if there is a cell a this current time
				bool isACellAtT = false;
				foreach (Cell ce in cellsSearched) 
					if (ce.t == CurrentTime)
						isACellAtT = true;
				if (isACellAtT) {//We only show cell at this current time
					foreach (Cell ce in cellsSearched)
						if (ce.t == CurrentTime)
							SelectionCell.AddCellSelected (ce,false);
				} else { //We Look for the min time founded
					int minTimeFounded = MaxTime;
					foreach (Cell ce in cellsSearched)
						minTimeFounded = Mathf.Min (minTimeFounded, ce.t);
					setTime(minTimeFounded); //We MOve Time point
					foreach (Cell ce in cellsSearched)
					if (ce.t == minTimeFounded)
							SelectionCell.AddCellSelected (ce,false);
					//cellsearched = true;//WHAT FOR ??
				}
				MenuCell.describe();
				

			}
		}
	}

	//Show Lineage 
	public void lineage_showCell(string p){
		//Debug.Log ("lineage_showCell " + p);
		Lineage.showCell(p);
	}
	
	//////////////////////////// DOWLOAD ALL TYPE OF DOWNLOAD WE HAVE TO PERFORM  (OBJ OR LINK)
	public class meshType{
		public string link;
		public uint version;
		public bool obj;
		public int id;
		public int t;

		public meshType(string link,uint version,bool obj,int id,int t){
			this.link=link;
			this.version=version;
			this.obj=obj;
			this.id=id;
			this.t=t;
		}
	}
	
	public static Vector3 parseCenter(string cent){
		//Debug.Log ("Found Center " + cent);
		if (cent == "") return Vector3.zero ;
		string [] cents=cent.Split (',');
		if(cents.Count()!=3) return Vector3.zero;
		float x, y, z;
		if(float.TryParse(cents [0],out x) && float.TryParse(cents [1],out y) && float.TryParse(cents [2],out z))
			return new Vector3 (x,y,z);
		return Vector3.zero;
	}

	public static int TotalNumberOfMesh = 0;
	public IEnumerator start_Download_Mesh_Type(){
		setComment("Wait during the streaming of the dataset");
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "0");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("quality", Instantiation.Quality.ToString ());
		form.AddField ("os", bundleType);
		//Debug.Log(Instantiation.urlDataset + "?hash=" + Instantiation.hash + "&download=0&id_dataset=" + Instantiation.id_dataset.ToString ()+"&quality=" + Instantiation.Quality.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		//ProgressBar.addWWW (www, "Meshes Infos");
		yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
        {
            setComment("Error : " + www.error);
        }
		else {
			//Debug.Log("DONE meshes "+www.downloadHandler.text+":");
			if (www.downloadHandler.text != "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				embryoCenter = Vector3.zero;
				TotalNumberOfMesh = N.Count;
				if (N != null && TotalNumberOfMesh > 0) {
					setComment("Found " + TotalNumberOfMesh+ " meshes");
					for (int i = 0; i < TotalNumberOfMesh; i++) { //Number of Infos(as id,infos,field 
						//Debug.Log("-->"+N[i].ToString());
						int id_mesh = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						int t=int.Parse (N [i] ["t"].ToString ().Replace ('"', ' ').Trim ());
						//int obj=int.Parse (N [i] ["isnull(obj)"].ToString ().Replace ('"', ' ').Trim ());
						string link="";
						if(bundleType=="WebGL") link=N [i] ["link"].ToString ().Replace ('"', ' ').Trim ();
						if(bundleType=="Android")  link=N [i] ["android"].ToString ().Replace ('"', ' ').Trim ();
						Vector3 centerT = parseCenter(N [i] ["center"].ToString ().Replace ('"', ' ').Trim ());
                        //Debug.Log("at="+t+"->link=" + link);
						//Debug.Log ("Found Center " + centerT.x + " :" + centerT.y + " :" + centerT.z);
						embryoCenter += centerT;
						uint version = uint.Parse (N [i] ["version"].ToString ().Replace ('"', ' ').Trim ());
						meshType me = new meshType (link, version, link.ToLower() == "null",id_mesh,t);
						if (!meshToDownload.ContainsKey (t)) meshToDownload[t] = new List<meshType> ();
						meshToDownload [t].Add (me);
					}
					embryoCenter /= TotalNumberOfMesh;
                    nbMeshesDownloaded=0;
					ProgressBar.addMeshes ();
					downloadNextMesh (0);
				} else setComment("No Mesh associated for this dataset");
			} else setComment("No Mesh associated for this dataset");
		}
		www.Dispose (); 
	}
	public static int nbMeshesDownloaded;
	public void downloadNextMesh(int nextBundle){
		setComment ("LoadingTime=" + LoadingTime);
		if(meshToDownload.ContainsKey (LoadingTime)){
			if (nextBundle < meshToDownload [LoadingTime].Count) {
				//MESSAGE DURING LAODING
				if (MaxTime != MinTime) {  //SERVERAL TIME STEP
					if (meshToDownload [LoadingTime].Count == 1)
                        setComment("Stream time point " + LoadingTime);
					else
						setComment("Load  " + Mathf.Round (100.0f * (nextBundle) / meshToDownload [LoadingTime].Count) + "% at " + LoadingTime);
				} else {
					if (meshToDownload [LoadingTime].Count == 1)
						setComment("Load this dataset");
					else
						setComment("Loading  " + Mathf.Round (100.0f * (nextBundle) / meshToDownload [LoadingTime].Count) + "%");
				}
				//Debug.Log ("downloadNextMesh at " + LoadingTime + " -> " +currentMeshDownload.obj);
				if (meshToDownload [LoadingTime] [nextBundle].obj) { //DONWLOAD OBJ FORMAT
					StartCoroutine (Donwload_OBJ (nextBundle));
				} else {// LINK
					StartCoroutine (Download_Next_Bundle (nextBundle));
				}
            } else donwloadNextLoadingMesh(); 
        }else {
            setComment("No mesh specification   at "+LoadingTime);
            donwloadNextLoadingMesh();
        }

	}
    public void donwloadNextLoadingMesh(){
        LoadingTime++;
        if (LoadingTime <= MaxTime)
            downloadNextMesh(0);
        else
        {
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
            setComment("");

        }
    }

    //Donwload a Bundle from a url
    public IEnumerator Download_Next_Bundle(int nextBundle)
    {
        nbMeshesDownloaded++;
        string url = Instantiation.urlSERVER + meshToDownload[LoadingTime][nextBundle].link;
        //Debug.Log ("url=" + url);
        uint version = meshToDownload[LoadingTime][nextBundle].version;
        //version += 3;
        while (!Caching.ready) yield return null;
        UnityWebRequest www_FBX = UnityWebRequestAssetBundle.GetAssetBundle(url, version, 0);
        //ProgressBar.addWWW (www_FBX, "Mesh at "+LoadingTime);
        yield return www_FBX.Send();
        if (www_FBX.isNetworkError)
        {
            Debug.LogError("Erreur Download " + www_FBX.error + " from " + url);
            setComment("ERROR Download dataset from " + url);
            www_FBX.Dispose();
        }
        else
        {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www_FBX);
            string[] AssetNames = bundle.GetAllAssetNames();
            //Debug.Log ("-> Load  AssetNames  " + AssetNames [0] + ":" + AssetNames.Count ());
            //setComment("LOAD "+AssetNames [0]);
            GameObject goe = (GameObject)bundle.LoadAsset(AssetNames[0], typeof(GameObject));
            LoadMesh(goe, false);
            bundle.Unload(true);
            DestroyImmediate(goe, true);
            www_FBX.Dispose();
            downloadNextMesh(nextBundle + 1);
        }
    }
	// After Download Asset Bundle or OBJ 
	public void LoadMesh(GameObject goe,bool invShiftX){
		//Debug.Log("goe.name="+goe.name);
		ArcBall.resetRotation ();

		if (EmbryoT [LoadingTime] == null) { //First Time we create the embryo
			
			EmbryoT [LoadingTime] = Instantiate (goe) as GameObject;
			EmbryoT [LoadingTime].name = "Embryo" + LoadingTime; 
			EmbryoT [LoadingTime].transform.SetParent (EmbryosGO.transform);
			LoadObjects (EmbryoT [LoadingTime], LoadingTime);
			EmbryoT [LoadingTime].SetActive (false);
			EmbryoT [LoadingTime].transform.localScale = new Vector3 (Instantiation.initcanvasScale, Instantiation.initcanvasScale, Instantiation.initcanvasScale);
			if(!invShiftX) EmbryoT [LoadingTime].transform.Translate (embryoCenter.x * Instantiation.initcanvasScale, -embryoCenter.y * Instantiation.initcanvasScale, -embryoCenter.z * Instantiation.initcanvasScale);
			else EmbryoT [LoadingTime].transform.Translate (-embryoCenter.x * Instantiation.initcanvasScale, -embryoCenter.y * Instantiation.initcanvasScale, -embryoCenter.z * Instantiation.initcanvasScale);
			MorphoNetLogo.SetActive (false);
		} else { //Already things .... me add the element as a cell
			
			GameObject Egoe = Instantiate (goe) as GameObject;
			Transform trns = Egoe.transform;
			
				while (trns.childCount > 0) {
					GameObject go = trns.GetChild (0).gameObject;
					//Debug.Log (" --> go.name=" + go.name);
					Cell ce = getCell (go.name, true);
					go.transform.SetParent (EmbryoT [LoadingTime].transform);
					//Debug.Log ("Add Cell " + ce.ID);
					string channelName = getChannelString (go.name);
					ChannelObject cho = createChannel (channelName); //We have to create a sub menu for each channel
					ce.addChannel (channelName, go, cho);

					go.transform.localScale = new Vector3 (1f, 1f, 1f);
					go.transform.Translate (embryoCenter.x * Instantiation.initcanvasScale, -embryoCenter.y * Instantiation.initcanvasScale, -embryoCenter.z * Instantiation.initcanvasScale);
				}
				DestroyImmediate (Egoe, true);
		}
		ArcBall.updateRotation ();
	}


	//Donwload OBJ a specitif time sept from database data set
	public  IEnumerator Donwload_OBJ (int nextBundle) {
		nbMeshesDownloaded++;
		meshType currentMeshDownload=meshToDownload [LoadingTime][nextBundle];
		//setComment("Download dataset at " +currentMeshDownload.t);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "2");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("quality", Instantiation.Quality.ToString ());
		form.AddField ("t", currentMeshDownload.t.ToString ());
		form.AddField ("id", currentMeshDownload.id.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {

			if(www.downloadHandler.data.Length>0){
				Debug.Log ("Load Embryo at " + currentMeshDownload.t);
				string embryoOBJ = Encoding.UTF8.GetString (Unzip(www.downloadHandler.data));

				GameObject goe = null;
				//if(isBundle<2) 
                    goe=createGameFromObj (embryoOBJ, currentMeshDownload.t); //OBJ
				//if(isBundle==2) goe=createGameFromSphere(embryoOBJ); //SPHERE
				if (goe != null) {
					LoadMesh (goe,true);
					DestroyImmediate (goe, true);
				}

			}else Debug.Log ("Nothing to download..."); 
		}
		www.Dispose (); 
		//Debug.Log ("Download DONE Embryo at " + currentMeshDownload.t+" ->"+EmbryoT [currentMeshDownload.t]);
		if(EmbryoT [currentMeshDownload.t]==null) {
			Debug.Log ("NULL Embryo at " + currentMeshDownload.t+" ->"+EmbryoT [currentMeshDownload.t]);
			ArcBall.resetRotation ();
			EmbryoT [currentMeshDownload.t] =new GameObject ();
			EmbryoT [currentMeshDownload.t].name = "Embryo" + currentMeshDownload.t; 
			EmbryoT [currentMeshDownload.t].SetActive (false);
			EmbryoT [currentMeshDownload.t].transform.SetParent (EmbryosGO.transform);
			EmbryoT [currentMeshDownload.t].transform.localScale = new Vector3 (Instantiation.initcanvasScale, Instantiation.initcanvasScale, Instantiation.initcanvasScale);
			EmbryoT [currentMeshDownload.t].transform.Translate (embryoCenter.y * Instantiation.initcanvasScale,-embryoCenter.x * Instantiation.initcanvasScale,-embryoCenter.z * Instantiation.initcanvasScale);

			ArcBall.updateRotation ();
		}

		downloadNextMesh(nextBundle + 1);
	}
	//Download all meshes in one file 
	public void onDownloadAllMeshes(){ StartCoroutine (downloadAllMeshes());}

	public IEnumerator downloadAllMeshes(){
		GameObject downloadMesh=canvas.transform.Find ("MenuDataset").gameObject.transform.Find ("downloadMesh").gameObject;
		downloadMesh.SetActive (false);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "11");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("quality", Instantiation.Quality.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			Debug.Log ("window.open(\"" + urlSERVER + Encoding.UTF8.GetString (www.downloadHandler.data) + "\")");
			Application.ExternalEval("window.open(\""+urlSERVER+Encoding.UTF8.GetString (www.downloadHandler.data)+"\")");
		}
		www.Dispose (); 
        if(userRight<2)downloadMesh.SetActive (true); //ONLY FOR CREATOR AND OWNER
	}

	////////////////////////////   COMMUN FUNCTION
	//To Decompress an embryo
	public static byte[ ] Unzip(byte[ ] data)
	{
        MemoryStream inStream = new MemoryStream(data ,0 ,data.Length);
		MemoryStream outStream = new MemoryStream() ;
		BZip2.Decompress(inStream ,outStream,true ) ;
		byte[ ] result = outStream.ToArray() ;
		inStream.Close() ;
		outStream.Close() ;
		return result ;
	}
	public static byte[ ] Zip(byte[ ] data)
	{
		MemoryStream inStream = new MemoryStream(data ,0 ,data.Length);
		MemoryStream outStream = new MemoryStream() ;
		BZip2.Compress (inStream, outStream, true,9);
		byte[ ] result = outStream.ToArray() ;
		inStream.Close() ;
		outStream.Close() ;
		return result ;
	}


	//////////////////////////// DOWLOAD ALL TYPE OF COORESPONDENCE WE HAVE TO PERFORM  (TEXT OR LINK)

	//Cre	te on the menu embryo the space or time icon to show or download infos
	public void createSpaceTime(int id_infos,string datatype,int type){
		//Debug.Log ("createSpaceTime for " + id_infos);
		GameObject me = canvas.transform.Find ("MenuDataset").gameObject;
		GameObject dwn = me.transform.Find ("download").gameObject;
		GameObject st = (GameObject)Instantiate (dwn);
		st.name = datatype + "_" + id_infos;
		st.transform.SetParent (me.transform);
		st.transform.localScale = new Vector3 (1f, 1f, 1f);
		st.transform.localPosition = new Vector3 (dwn.transform.localPosition.x,dwn.transform.localPosition.y,dwn.transform.localPosition.z);
		dwn.transform.localPosition=new Vector3 (dwn.transform.localPosition.x,dwn.transform.localPosition.y-40,dwn.transform.localPosition.z);
		if(datatype=="time") st.transform.Find ("Text").GetComponent<Text>().text = "Lineage Tree";
		else st.transform.Find ("Text").GetComponent<Text>().text = "Spatial Connections ";
		if (datatype == "time") {
			st.transform.Find ("show").GetComponent<Button> ().onClick.AddListener (() => canvas.GetComponent<Lineage> ().showLineage (st));
			st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("lineagetree", typeof(Sprite)) as Sprite;

		}
		if (datatype == "space") {
			st.transform.Find ("show").GetComponent<Button> ().onClick.AddListener (() => showSpaceConnection (st));
			st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("network", typeof(Sprite)) as Sprite;
			st.transform.Find ("show").gameObject.transform.Find ("comment").gameObject.transform.Find ("Text").GetComponent<Text> ().text = "View objects neighbour relations";
		}
			st.SetActive (true);
	}

	public bool isSpaceConnection = false;
	public void showSpaceConnection(GameObject st){
		if (isSpaceConnection) { //DESACTIVATE
			for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) 
				if(EmbryoT [t]!=null){
					if ( EmbryoT [t].transform.Find ("Space") != null)
						Destroy (EmbryoT [t].transform.Find ("Space").gameObject);
				}
		} else { //ACTIVATE
			plotSpaceConnection();
		}
		isSpaceConnection = !isSpaceConnection;
		if(!isSpaceConnection) st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("network", typeof(Sprite)) as Sprite;
		else st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("eyeclose", typeof(Sprite)) as Sprite;
	}

	public  void plotSpaceConnection(){
		if (EmbryoT [Instantiation.CurrentTime].transform.Find ("Space") == null) {
			ArcBall.resetRotation ();
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
			GameObject nv = new GameObject ();
			nv.name = "Space";
			foreach (var itemc in CellAtT) {
				Cell c = itemc.Value; 
				Channel ch = c.getFirstChannel ();
				if (ch != null && c.Neigbhors != null && c.Neigbhors.Count () > 0) {
					GameObject nvd = new GameObject ();
					nvd.name = c.ID;
					nvd.AddComponent<LineRenderer> ();
					nvd.GetComponent<LineRenderer> ().useWorldSpace = false;
					nvd.GetComponent<LineRenderer> ().SetWidth (0.1f, 0.1f);
					nvd.GetComponent<LineRenderer> ().SetVertexCount (c.Neigbhors.Count () * 2);
					int i = 0;
					Vector3 grv = new Vector3 ((ch.Gravity [0] + embryoCenter.x), (ch.Gravity [1] - embryoCenter.y), (ch.Gravity [2] - embryoCenter.z));

					foreach (Cell n in c.Neigbhors) {

						nvd.GetComponent<LineRenderer> ().SetPosition (i, grv * Instantiation.initcanvasScale);
						if (n.getFirstChannel () != null) {
							Channel chn = n.getFirstChannel ();
							if (chn.Gravity != null) {
								Vector3 grvN = new Vector3 ((chn.Gravity [0] + embryoCenter.x), (chn.Gravity [1] - embryoCenter.y), (chn.Gravity [2] - embryoCenter.z));
								nvd.GetComponent<LineRenderer> ().SetPosition (i + 1, grvN * Instantiation.initcanvasScale);
							}
						} else
							nvd.GetComponent<LineRenderer> ().SetPosition (i + 1, grv * Instantiation.initcanvasScale);
						i += 2;
					}
					// Instantiation.Default;
					if (ch.cell.selected)
						nvd.GetComponent<LineRenderer> ().sharedMaterial = Instantiation.Selected;
					else if (ch.cell.UploadedColor) {
						nvd.GetComponent<LineRenderer> ().material.color = ch.cell.UploadColor;
						//	.ch.center.GetComponent<Renderer> ().material.color =c;
					} else if (ch.cell.selection != null && ch.cell.selection.Count > 0) {
						nvd.GetComponent<LineRenderer> ().sharedMaterial = SelectionColor.getSelectedMaterial (ch.cell.selection [0]);
					} else {
						nvd.GetComponent<LineRenderer> ().sharedMaterial=Instantiation.Default;
					}
					nvd.transform.SetParent (nv.transform);
				}
			}
			nv.transform.SetParent (Instantiation.EmbryoT [Instantiation.CurrentTime].transform);
			ArcBall.updateRotation ();
		}
	}




	//see http://pastebin.com/7sLteqNw
	public GameObject createGameFromObj(string embryoOBJ,int t){
		//Lire d'abord toutes les vertex et ensuite les assign au mesh cell
		GameObject emb = new GameObject ();
		string [] lines=embryoOBJ.Trim ().Split("\n"[0]);
		//First we store all vertx
		Vector3 tempVector3=Vector3.zero;
		Dictionary<int,Vector3> allVertex = new Dictionary<int,Vector3> ();
		int iv = 0;
		for (int i = 0; i < lines.Count (); i++) {
			string line = lines [i].Trim ();
			if (line.StartsWith ("v ")) {
				try {
					while(line.IndexOf("  ")>=0) line=line.Replace("  "," "); //Problem of double space
					string []datas = line.Split (' ');
					if(float.TryParse(datas [1], out tempVector3.x) && float.TryParse(datas [2], out tempVector3.y)  && float.TryParse(datas [3], out tempVector3.z) )
						allVertex[iv]=tempVector3;
					else if(iv>0) allVertex[iv]=allVertex[iv-1];
					else allVertex[iv]=Vector3.zero;
					iv+=1;
				} catch (Exception ex) {
					Debug.Log ("PARSE ERROR (vertex) : line " + i + " error " + ex);
				}
			}

		}
		if (allVertex.Count () <= 3) return null;
		
		//Now we look for the face and their group
		string meshname = "";
		
		List<int> triangles = new List<int> ();
		//Quaternion DataBaseCellTransformation=Quaternion.Euler(new Vector3(0, 0, 0));
		//Debug.Log ("Found " + lines.Count () + " lines");
		for (int i = 0; i < lines.Count (); i++) {
			string line = lines [i].Trim ();
			string []datas = line.Split (' ');
			if (datas.Count () > 1) {
				if (line.StartsWith ("g")) {
					//Debug.Log ("Found meshname=" + meshname  +" at " + i);
					if (meshname != "" && triangles.Count()>0) {
						//Debug.Log ("triangles=" + triangles.Count());
						int minT=triangles.Min ();
						int maxT = triangles.Max ();
						//Debug.Log ("minT=" + minT + " maxT=" + maxT);
						//vertices[idF]=allVertex[fidx[fi]-1];
						List<Vector3> vertices = new List<Vector3> ();
						List<int> shiftTriangles = new List<int> ();
						foreach (int f in triangles) shiftTriangles.Add (f - minT);
						
						for (int idf = minT; idf <= maxT; idf++) {
							if(!allVertex.ContainsKey(idf)) 	Debug.LogError("Value  "+idf + " is not in vertex ... ");
							vertices.Add(allVertex[idf]);//embryoCenter
						}
						GameObject cell = CreateCellGameObject (meshname, vertices, shiftTriangles);

						cell.transform.SetParent (emb.transform);
						//Debug.Log ("cell=" + cell.transform.localScale);
						//cell.transform.rotation = DataBaseCellTransformation;
					}
					//vertices =new Dictionary<int,Vector3> ();
					triangles = new List<int> ();
					meshname = datas [1];
				}

				// triangle (face)
				if (line.StartsWith ("f")) {
					try {
						int []fidx=new int[3];
					int nbD=datas.Count();
						for(int fi=0;fi<nbD-1;fi++){
							fidx[fi]=int.Parse (datas [1+fi].Split ('/') [0]);
							triangles.Add (fidx[fi] - 1);
						}
					} catch (Exception ex) {
					Debug.Log (line);
						Debug.Log("PARSE ERROR (triangle) : line " + i + " while loading mesh : " + meshname + "\n" + ex);
						//triangles.Add (1); triangles.Add (2); triangles.Add (3);
					}
				}
			}
		}
		if (meshname != "" && triangles.Count()>0) { //We Do the last cell...
			int minT=triangles.Min ();
			int maxT = triangles.Max ();
			//Debug.Log ("minT=" + minT + " maxT=" + maxT);
			List<Vector3> vertices = new List<Vector3> ();
			List<int> shiftTriangles = new List<int> ();
			foreach (int f in triangles) shiftTriangles.Add (f - minT);
			for (int idf = minT; idf <= maxT; idf++) {
				//Debug.Log("For Face "+idf + " ->add vertices " + idf);
				//if(!triangles.Contains(idf)) Debug.Log("Value  "+idf + " is not in face ... ");
				if(!allVertex.ContainsKey(idf)) Debug.Log("Value  "+idf + " is not in vertex ... ");
				vertices.Add(allVertex[idf]);
			}
			//Debug.Log ("Lioast Vertices=" + vertices.Count ()+ " new max triangles="+shiftTriangles.Max());
			GameObject cellfinal = CreateCellGameObject (meshname, vertices, shiftTriangles);
			//Debug.Log ("cellfinal=" + cellfinal.transform.localScale);
			cellfinal.transform.SetParent (emb.transform);
			//cellfinal.transform.rotation = DataBaseCellTransformation;
		}
		

		return emb;
	}
	
	public static GameObject CreateCellGameObject(string name,List<Vector3> vertices,List<int> triangles){
		//Debug.Log ("Create Cell " + name + " with " + vertices.Count () + " vertices and " + triangles.Count () + " faces with max="+triangles.Max ());
		if (triangles.Max () != vertices.Count ()-1) Debug.LogError ("The number of faces do not correspond with the number of vertices");
		GameObject cell = new GameObject ();
		int limit = 65000;
		if (vertices.Count () > limit) { //We have to split the vertices in serveral meshes
			Mesh mesh = new Mesh ();
			mesh.Clear ();
			mesh.name = name;
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();
			cell.AddComponent<MeshFilter>().mesh = mesh;
			cell.AddComponent<MeshRenderer> ();
		} else {
			Mesh mesh = new Mesh ();
			mesh.Clear ();
			mesh.name = name;
			mesh.vertices = vertices.ToArray ();
			mesh.triangles = triangles.ToArray ();
			mesh.RecalculateNormals ();
			mesh.RecalculateBounds ();
			cell.AddComponent<MeshFilter>().mesh = mesh;
			cell.AddComponent<MeshRenderer> ();
		}
		
		cell.name = name;
		return cell;
	}
	

	public static string convertJSON(string jsonfield){
		return jsonfield.ToString ().Replace ('"', ' ').Trim ();
	}
	public static int convertJSONToInt(string jsonfield){
		return int.Parse(jsonfield.ToString ().Replace ('"', ' ').Trim ());
	}
	



	//Donwload the comments 
	public  IEnumerator downloadComments () {
		GameObject ce=canvas.transform.Find("MenuDataset").gameObject;
		if (ce != null) {
			WWWForm form = new WWWForm ();
			form.AddField ("hash", Instantiation.hash); 
			form.AddField ("download", "10");
			form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
           	UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlDataset, form);
           // Debug.Log(Instantiation.urlDataset+"?hash="+Instantiation.hash+"&download=10&id_dataset="+ Instantiation.id_dataset.ToString());
			yield return www.Send ();
			if (www.isHttpError || www.isNetworkError)
				Debug.Log ("Error : " + www.error);
			else {
				GameObject dc = ce.transform.Find ("datasetcomment").gameObject;
				GameObject linkc = ce.transform.Find ("link").gameObject;
                linkc.SetActive(false);
				if (www.downloadHandler.data.Length > 0) {
					//Debug.Log ("COMMENTS=" + www.downloadHandler.text);
					var N = JSONNode.Parse (www.downloadHandler.text);
                    string comments = N[0][0].ToString().Replace('"', ' ').Trim();
                    string link=N[0][1].ToString().Replace('"', ' ').Trim();

                    if (comments.ToLower() != "null")
                    {
                        dc.SetActive(true);
                        dc.GetComponent<Text>().text = comments;
                    }
					//if(link!="") linkc.GetComponent<Button> ().onClick.AddListener (() => onLinkClick(link));
				} else {
					dc.SetActive (false);
					linkc.SetActive (false);
					//Debug.Log ("Nothing to download..."+www.downloadHandler.text); 

				}
			}
			www.Dispose (); 
		}
	}

	public void onLinkClick(string link){
		Application.ExternalEval("window.open(\""+link+"\")");
	}
}

