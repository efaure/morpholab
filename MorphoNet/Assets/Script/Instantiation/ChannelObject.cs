﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp
{
	public class ChannelObject
	{
		public string name; //Name of the channel

		public bool mesh;//Visualize or not the main mesj

		public bool center; //Visualize or not the center
		public int centerSize;

		public bool tracking; //Visualize or not the trai,nc
		public float trackingSize;
		public int trackingLengthPath;
		public int trackingLengthFutur;
		public GameObject Tracking; //Main Tracking Object

		public GameObject menu; //Associated Game Obbject


		public ChannelObject (string name,GameObject channelDefault)
		{
			this.name = name;
			this.mesh = true;
			this.center = false;
			this.tracking = false;
			this.centerSize = 8;
			this.trackingSize =0.03F;
			this.trackingLengthPath = 0;
			this.trackingLengthFutur = 0;

			this.menu = (GameObject)Instantiation.Instantiate (channelDefault); 
			this.menu.name = name;
			GameObject meshmenu = this.menu.transform.Find ("meshs").gameObject;
			meshmenu.GetComponent<Toggle> ().onValueChanged.AddListener (delegate {showChannel ();});
			meshmenu.transform.Find ("Label").GetComponent<Text>().text = "Channel " + name;
			this.menu.transform.Find ("moreChannel").gameObject.SetActive (false); //WE DESACTIVE TRACKING AND CENTERS
			this.menu.transform.Find ("moreChannel").GetComponent<Button>().onClick.AddListener (() => showChannelMenu ());
			this.menu.transform.SetParent (channelDefault.transform.parent);
			this.menu.transform.localScale = new Vector3 (1f, 1f, 1f);
			this.menu.transform.position = channelDefault.transform.position;//new Vector3(0F,0F,0F);
			this.menu.SetActive (true);
		}

		public void showChannel(){
			this.mesh = this.menu.transform.Find ("meshs").GetComponent<Toggle> ().isOn;
			for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
				Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
				foreach (var itemc in CellAtT) { 
					Cell c = itemc.Value; 
					if (c.Channels!=null && c.Channels.ContainsKey (this.name)){
						GameObject go = c.Channels [this.name].go;
						if (go != null && go.GetComponent<ObjectRender> () != null)
							go.GetComponent<ObjectRender> ().previouslyActivate = !go.GetComponent<ObjectRender> ().previouslyActivate;
					}
				}
			}
		}


		public void showChannelMenu(){
			GameObject MenuChannel = Instantiation.canvas.GetComponent<Menus> ().MenuChannel;
			MenuChannel.transform.Find ("back").GetComponent<Button> ().onClick.RemoveAllListeners ();
			MenuChannel.transform.Find ("back").GetComponent<Button> ().onClick.AddListener (() => showChannelMenu ());
			if (MenuChannel.activeSelf)
				MenuChannel.SetActive (false);
			else {
				MenuChannel.transform.Find ("ChannelName").GetComponent<Text> ().text = "Channel " + this.name;
				//Center
				MenuChannel.transform.Find ("centers").GetComponent<Toggle> ().isOn = this.center;
				MenuChannel.transform.Find ("centers").GetComponent<Toggle> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("centers").GetComponent<Toggle> ().onValueChanged.AddListener (delegate {showCenter ();});

				MenuChannel.transform.Find ("centersSize").GetComponent<Slider> ().value = this.centerSize;
				MenuChannel.transform.Find ("centersSize").GetComponent<Slider> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("centersSize").GetComponent<Slider> ().onValueChanged.AddListener (delegate {changeCenterSize ();});

				//Tracking
				MenuChannel.transform.Find ("Tracking").GetComponent<Toggle> ().isOn = this.tracking;
				MenuChannel.transform.Find ("Tracking").GetComponent<Toggle> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("Tracking").GetComponent<Toggle> ().onValueChanged.AddListener (delegate {showTracking ();});

				MenuChannel.transform.Find ("TrackingSize").GetComponent<Slider> ().value = this.trackingSize;
				MenuChannel.transform.Find ("TrackingSize").GetComponent<Slider> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("TrackingSize").GetComponent<Slider> ().onValueChanged.AddListener (delegate {changeTrackingSize ();});


				MenuChannel.transform.Find ("TrackingLengthPath").GetComponent<Slider> ().maxValue = Instantiation.MaxTime;
				MenuChannel.transform.Find ("TrackingLengthPath").GetComponent<Slider> ().value = this.trackingLengthPath;
				MenuChannel.transform.Find ("TrackingLengthPath").GetComponent<Slider> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("TrackingLengthPath").GetComponent<Slider> ().onValueChanged.AddListener (delegate {changeTrackingLengthPath ();});

				MenuChannel.transform.Find ("TrackingLengthFutur").GetComponent<Slider> ().maxValue = Instantiation.MaxTime;
				MenuChannel.transform.Find ("TrackingLengthFutur").GetComponent<Slider> ().value = this.trackingLengthFutur;
				MenuChannel.transform.Find ("TrackingLengthFutur").GetComponent<Slider> ().onValueChanged.RemoveAllListeners ();
				MenuChannel.transform.Find ("TrackingLengthFutur").GetComponent<Slider> ().onValueChanged.AddListener (delegate {changeTrackingLengthFutur ();});

				MenuChannel.SetActive (true);
			}


		}
		//////////////////// CENTER
		//Active or Desactive Center Visualisation
		public void showCenter() {
			this.center= Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("centers").GetComponent<Toggle> ().isOn;
			if (!this.center) { //Destroy Center
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) { 
						Cell c = itemc.Value; 
						if (c.Channels.ContainsKey (this.name))
							c.Channels [this.name].destroyCenter ();
					}
				}
				System.GC.Collect ();
			} 
		}
		//Change Nuclei Size
		public void changeCenterSize(){
			this.centerSize = (int)Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("centersSize").GetComponent<Slider> ().value;
			if (this.center) {
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) { 
						Cell c = itemc.Value; 
						if (c.Channels.ContainsKey (this.name))
							c.Channels [this.name].resizeCenter ();
					}
				}
			}
		}


		//////////////////// TRACKING
		public void showTracking(){
			this.tracking= Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("Tracking").GetComponent<Toggle> ().isOn;

			if (!this.tracking) { //Destroy Tracking
				Instantiation.DestroyImmediate (this.Tracking, true);
					System.GC.Collect ();
			} else {
				if (this.Tracking == null)
					createTracking ();
			}
			//updateTrackingVectors ();
		}
		public void createTracking(){
			this.Tracking = new GameObject ();
			this.Tracking.name = this.name;
			this.Tracking.transform.SetParent(Instantiation.Tracking.transform);
			int lastT = (int)Mathf.Max (Instantiation.MinTime, Instantiation.CurrentTime - this.trackingLengthPath);
			int firsT = (int)Mathf.Min (Instantiation.MaxTime, Instantiation.CurrentTime + this.trackingLengthFutur);
			for (int t = lastT; t <= firsT; t++)
				createTrackingAt (t);
		}

		public void changeTrackingLengthPath(){ 
			this.trackingLengthPath = (int)Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("TrackingLengthPath").GetComponent<Slider> ().value; 
			updateTrackingVectors ();
		}

		public void changeTrackingLengthFutur(){
			this.trackingLengthFutur = (int)Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("TrackingLengthFutur").GetComponent<Slider> ().value; 
			updateTrackingVectors ();

		}

		public void changeTrackingSize(){
			this.trackingSize = Instantiation.canvas.GetComponent<Menus> ().MenuChannel.transform.Find ("TrackingSize").GetComponent<Slider> ().value; 
			if (this.tracking && this.Tracking != null) {
				for (int ic = 0; ic < this.Tracking.transform.childCount; ic++) {
					GameObject TrackingT = this.Tracking.transform.GetChild (ic).gameObject;
					for (int ix = 0; ix < TrackingT.transform.childCount; ix++) {
						GameObject nv = TrackingT.transform.GetChild (ix).gameObject;
						for (int iy = 0; iy < nv.transform.childCount; iy++) {
							GameObject nvd = nv.transform.GetChild (iy).gameObject;
							nvd.GetComponent<LineRenderer> ().SetWidth (this.trackingSize, this.trackingSize);
						}
					}
				}
			}

		}

		public void updateTrackingVectors(){
			if (this.tracking && this.Tracking != null) {
				int lastT = (int)Mathf.Max (Instantiation.MinTime, Instantiation.CurrentTime - this.trackingLengthPath);
				int firsT = (int)Mathf.Min (Instantiation.MaxTime, Instantiation.CurrentTime + this.trackingLengthFutur);
				for (int t = Instantiation.MinTime; t < lastT; t++) {
					if( this.Tracking.transform.Find ("tracking_" + t)!=null) 
						this.Tracking.transform.Find ("tracking_" + t).gameObject.SetActive (false);
				}
				for (int t = lastT; t <= firsT; t++) {
					if (this.Tracking.transform.Find ("tracking_" + t) == null)  createTrackingAt (t);
					else this.Tracking.transform.Find ("tracking_" + t).gameObject.SetActive (true);
				}
				for (int t = firsT + 1; t <= Instantiation.MaxTime; t++) {
					if( this.Tracking.transform.Find ("tracking_" + t)!=null) 
						this.Tracking.transform.Find ("tracking_" + t).gameObject.SetActive (false);
				}
			}
		}

		public GameObject createTrackingAt(int t){
			ArcBall.resetRotation();
			GameObject TrackingT = new GameObject ();
			TrackingT.name = "tracking_" + t;
			TrackingT.transform.SetParent (this.Tracking.transform);
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
			foreach (var itemc in CellAtT) { 
				Cell c = itemc.Value; 
				if(c.Daughters.Count>0){
					if (c.Channels.ContainsKey (this.name)) {
						Channel ch = c.Channels [this.name];
						GameObject nv = new GameObject ();
						nv.name = ch.getTuple ();
						nv.transform.SetParent(TrackingT.transform);
						ch.vector = nv;
						foreach(Cell d in c.Daughters){
							GameObject nvd = new GameObject ();
							nvd.AddComponent<LineRenderer> ();
							nvd.GetComponent<LineRenderer> ().useWorldSpace = false;
							nvd.GetComponent<LineRenderer> ().SetWidth (this.trackingSize, this.trackingSize);
							nvd.GetComponent<LineRenderer> ().SetVertexCount (2);
							nvd.GetComponent<LineRenderer> ().SetPosition (0, (ch.Gravity - Instantiation.embryoCenter) * Instantiation.canvasScale);
							nvd.GetComponent<LineRenderer> ().SetPosition (1, (d.Channels[this.name].Gravity - Instantiation.embryoCenter) * Instantiation.canvasScale);
							nvd.GetComponent<LineRenderer> ().sharedMaterial = Instantiation.Default;
							nvd.transform.SetParent(nv.transform);
						}	
					}
				}
			}
			ArcBall.updateRotation();
			return TrackingT;
		}
	}
}

