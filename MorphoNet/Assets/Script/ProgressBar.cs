﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;



public class Progress  : MonoBehaviour {
	//UnityWebRequest www ;
	Correspondence cor;
	public GameObject go;
	Image foregroundImage;
	public float value;
	public bool isCor = false;

	public Progress(int count){//Download Meshes
		isCor = false;
		this.go = (GameObject)Instantiate (ProgressBar.defaultPB,ProgressBar.defaultPB.transform.parent);
		this.go.transform.Find("Name").gameObject.GetComponent<Text>().text = "Meshes";
		this.go.name = "Meshes";
		this.go.transform.Translate (new Vector3 (0, count * 20f*Instantiation.canvasScale, 0));
		this.foregroundImage = this.go.transform.Find ("Foreground").gameObject.GetComponent<Image> ();
		this.foregroundImage.fillAmount = 0f;
		this.value = 0f;
		this.go.SetActive (true);
		this.update ();

	}
	public Progress (Correspondence cor,int count) { //Download Infos
		isCor = true;
		this.cor = cor;
		//this.www=www;
		this.go = (GameObject)Instantiate (ProgressBar.defaultPB,ProgressBar.defaultPB.transform.parent);
		this.go.transform.Find("Name").gameObject.GetComponent<Text>().text = cor.name;
		this.go.name = cor.name;
		this.go.transform.Translate (new Vector3 (0, count * 20f*Instantiation.canvasScale, 0));
		this.foregroundImage = this.go.transform.Find ("Foreground").gameObject.GetComponent<Image> ();
		this.foregroundImage.fillAmount = 0f;
		this.value = 0f;
		this.go.SetActive (true);
		this.update ();
	}

	public void shift(int s){
		this.go.transform.Translate (new Vector3 (0, -s * 20f*Instantiation.canvasScale, 0));

	}
	public void update(){
		if (!isCor) {//Download Meshes 
            this.value = 1f * Instantiation.nbMeshesDownloaded / Instantiation.TotalNumberOfMesh;
            if (Instantiation.nbMeshesDownloaded == Instantiation.TotalNumberOfMesh) Instantiation.canvas.GetComponent<Menus>().MenuShortcuts.SetActive(false);
		} else {
			if (cor.lines == null) this.value = 1f;
			else this.value = 1f * cor.startIndex / cor.lines.Length;
		}
		//Debug.Log (this.go.name + "->" + this.value .ToString ());
		this.foregroundImage.fillAmount = this.value;
	}
}


public class ProgressBar : MonoBehaviour
{

	public static GameObject defaultPB;
	//public GameObject PB;
	public static List<Progress> Progresss;
	//Main initialisation
	void Start()
	{
		//Debug.Log ("Start ProgressBar");
		init ();
	
		//PB.SetActive (true);
	}
	public static void init(){
		if (defaultPB == null) {
			defaultPB = Instantiation.canvas.transform.Find ("ProgressBar").Find ("default").gameObject;
			defaultPB.SetActive (false);
		}
		if (Progresss == null) Progresss = new List<Progress> ();
	}
		

	public static void addMeshes(){//Meshes
		init ();
		Progress p=new Progress(Progresss.Count);
		Progresss.Add (p);
	}
	public static void addCor(Correspondence cor){
		init ();
		Progress p=new Progress(cor,Progresss.Count);
		Progresss.Add (p);
	}

	public void Update(){
		foreach (Progress p in Progresss)
			p.update ();

		List<Progress> NewProgresss = new List<Progress> ();
		int shift = 0;
		foreach (Progress p in Progresss)
			if (p.value >= 1f) {
				shift += 1;
				DestroyImmediate (p.go);
			} else {
				p.shift (shift);
				NewProgresss.Add (p);
			}
		Progresss = NewProgresss;
	}
}
