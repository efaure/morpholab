﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class Confirm : MonoBehaviour {

	public static GameObject ConfirmBox;
	public static Button yes;

	public static void init (){
		if(ConfirmBox==null) ConfirmBox=Instantiation.canvas.transform.Find("ConfirmBox").gameObject;
		if (yes == null) {
			yes = ConfirmBox.transform.Find ("yes").gameObject.GetComponent<Button> ();
			ConfirmBox.transform.Find ("no").gameObject.GetComponent<Button> ().onClick.AddListener (() => Confirm.close ());
			yes.onClick.RemoveAllListeners ();
			yes.onClick.AddListener (() => Confirm.close ());
		}
	}


	public static void confirm(){
		ConfirmBox.SetActive (true);
	}

	public static void close(){
		ConfirmBox.SetActive (false);
	}


}
