﻿using UnityEngine;
using System.Collections;
using System.Text;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class PlayAPI : MonoBehaviour {

	public GameObject Canvas;
	bool listening=false;
	bool block=false;
	// Use this for initialization
	void Start () {
		this.listening=false;
		this.block=false;
	}

	public void listen(){
		listening = !listening;
		if (listening)
			block = false;
	}

	// Update is called once per frame
	void Update () {
		if (listening & !block) {
			StartCoroutine (checkNextPlay ());
		}
	}

	//WHEN PLAY MODE
	public IEnumerator checkNextPlay(){
		block = true;
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("playapi", "1");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlPlayAPI, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError) {
			Debug.Log("Error : " + www.error); 
			block = false;
		} else {
			string [] todo =www.downloadHandler.text.Split (';');
			if(todo.Length==2){
				//Debug.Log ("Found Play "+todo [0]+ "->" +todo [1]);
				string action = todo [0]; string value = todo [1];
				if (action == "dataset") {
					StartCoroutine (EmbryonSelected (int.Parse (value)));
				} else if (action == "selection") {
					StartCoroutine (queryLoadSelection (int.Parse (value)));
				} else if (action == "resetselection") {
					resetSelection (int.Parse (value));
				} else if (action == "hideselection") {
					hideShowSelection (true, int.Parse (value));
				} else if (action == "showselection") {
					hideShowSelection (false, int.Parse (value));
				} else if (action == "time") {
					ChangeTime (int.Parse (value));
				} else if (action == "rotate") {
					ArcBall.rotate (Scenario.parseQuaterion (value));
					block = false;
				} else if (action == "move") {
					ArcBall.move (Scenario.parseVector3 (value));
					block = false;
				} else if (action == "scale") {
					ArcBall.rescale (float.Parse (value));
					block = false;
				} else if (action == "createcellassphere") {//t:id:coordX,coordY,coordZ:radius
					ArcBall.resetRotation ();
					createCellAsSphere(value);
					ArcBall.updateRotation ();
					block = false;
				}else if (action == "createcellsassphere") {//Multiple line of t:id:coordX,coordY,coordZ:radius
					string [] values=value.Split("\n"[0]);
					ArcBall.resetRotation ();
					for(int i=0;i<values.Length;i++)
						createCellAsSphere(values[i]);
					ArcBall.updateRotation ();
					block = false;
				} else if (action == "movecell") {//t:id:coordX,coordY,coord:radius
					ArcBall.resetRotation ();
					moveCell (value);
					ArcBall.updateRotation ();
					block = false;
				}else if (action == "movecells") {//Multiple line of t:id:coordX,coordY,coord:radius
					string [] values=value.Split("\n"[0]);
					//Debug.Log ("Move "+values.Length+" cells" );
					ArcBall.resetRotation ();
					for(int i=0;i<values.Length;i++)
						moveCell(values[i]);
					ArcBall.updateRotation ();
					block = false;
				} else
					block = false;
			} else block = false;
		}
		www.Dispose (); 
	}


	  private void createCellAsSphere(string value){ //t:id:coordX,coordY,coordZ:radius
		
	}
	private void moveCell(string value){ //t:id:coordX,coordY,coordZ:radius
		string []tvalue=value.Split (':');
		if (tvalue.Length == 4) {
			int t_cell = int.Parse (tvalue [0]);
			if (Instantiation.EmbryoT [t_cell] != null) {
				int idcell = int.Parse (tvalue [1]);
				if(Instantiation.EmbryoT [t_cell].transform.Find ("cell_" + idcell)!=null){
					GameObject cell = Instantiation.EmbryoT [t_cell].transform.Find ("cell_" + idcell).gameObject;
					//Debug.Log ("Move Cell " + idcell+" to "+tvalue [2]);
					string[] pos = tvalue [2].Split (',');
					cell.transform.position = new Vector3 (float.Parse (pos [0]) *Instantiation.initcanvasScale, float.Parse (pos [1]) *Instantiation.initcanvasScale, float.Parse (pos [2]) *Instantiation.initcanvasScale);
					cell.transform.localScale = new Vector3 (float.Parse (tvalue [3]), float.Parse (tvalue [3]), float.Parse (tvalue [3]));
				}
			}
		}
	}

	//Choose an embryo
	public  IEnumerator EmbryonSelected(int id_dataset)
	{
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "6");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("id_dataset", id_dataset.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		 else {
			Debug.Log("EEE Found " +www.downloadHandler.text);
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N.Count ==1 ) {
				Instantiation.id_dataset = id_dataset;
				//int id_dataset=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
				Instantiation.embryo_name = N [0] [1].ToString ().Replace('"',' ').Trim();
				//string date = N [0] [2].ToString ().Replace('"',' ').Trim();
				Instantiation.MinTime=int.Parse(N [0] [3].ToString ().Replace('"',' ').Trim());
				Instantiation.MaxTime=int.Parse(N [0] [4].ToString ().Replace('"',' ').Trim());
				//int id_people=int.Parse(N [0] [5].ToString ().Replace('"',' ').Trim());
				Instantiation.isBundle=int.Parse(N [0] [6].ToString ().Replace('"',' ').Trim());
				SceneManager.LoadScene("SceneEmbryo");
				block = false;
			} 
		}
		www.Dispose (); 
		block = false;
	}


	//Hides Cells (from a selection)
	public void hideShowSelection(bool v,int id_selection){
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
			foreach (var itemc in CellAtT) { 
				Cell c = itemc.Value; 
				if (c.selection!=null && c.selection.Contains(id_selection))
					c.hide (v);
			}
		}
	}



	//Reset Selection
	public void resetSelection(int id_time_selection){
		if(id_time_selection==-1) //ALL TIMES
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
				Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
				foreach (var itemc in CellAtT) { 
					Cell c = itemc.Value; 
					if (c.selection!=null && c.selection.Count>0) c.removeAllSelection();
				}
		     }
		else
			if(id_time_selection>=Instantiation.MinTime && id_time_selection<=Instantiation.MaxTime) {
				Dictionary<string, Cell> CellAtT=Instantiation.CellT[id_time_selection];
				foreach (var itemc in CellAtT) { 
					Cell c = itemc.Value; 
					if (c.selection!=null && c.selection.Count>0) c.removeAllSelection();
				}
			}
		block = false;
	}

	//Change Time Bar
	public void ChangeTime(int t){
		if (t >= Instantiation.MinTime && t <= Instantiation.MaxTime) {
			if (Instantiation.CurrentTime != t) Instantiation.scrolledVal = t;
		}
		block = false;
	}

	//Load a Selection
	public  IEnumerator queryLoadSelection (int id_selection) {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("selection", "2");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString());
		form.AddField ("id_selection", id_selection.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlSelection, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError) Debug.Log("Error : " + www.error); 
		else {
			//Return colors ....
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N != null && N.Count > 0) {
				foreach( var key in N.Keys )
				{
					int t = int.Parse (key);
		
				}
			}
		}
		www.Dispose (); 
		block = false;
	}
		

}
