﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;

public class DevelopmentTable : MonoBehaviour {



	public void Start(){
		//StartCoroutine (downloadDevelopmentalTable(Instantiation.id_type));
	}
	//Download Developmental Table
	public static List<Dvpt>Dvpts;

	public class Dvpt
	{
		public string period;
		public string stage;
		public string developmentaltstage;
		public string description;
		public string hpf;
		public string hatch;
		public Dvpt (string period,string stage,string developmentaltstage,string description,string hpf,string hatch) //Constructeur 
		{
			this.period=period;
			this.stage=stage;
			this.developmentaltstage=developmentaltstage;
			this.description=description;
			this.hpf=hpf;
			this.hatch=hatch;
		}
	}

	public static string convertTimeToStage(int id_datasettype,int spf,int dt,int t){

		float hpf = (spf + t * dt) / 3600f;
		//Debug.Log ("hpf==" + hpf+" spf="+spf+" dt="+dt+" t="+t);
		return convertHPFtoStage(id_datasettype,hpf);
	}

	public static string convertHPFtoStage(int id_datasettype,float HPF){
		//Lookforborder 
		string stage_begin = "";
		float distance_to_begin = 10000000f;
		string stage_end = "";
		float distance_to_end = 10000000f;
		foreach (Dvpt dvpt in Dvpts) {
            float dvptHPF = 0;float.TryParse (dvpt.hpf,out dvptHPF);
			if (dvptHPF>HPF) {
				float d =dvptHPF - HPF ;
				if (d < distance_to_end) {
					distance_to_end = d;
					stage_end = dvpt.stage;
				}
			}
			if (dvptHPF<=HPF) {
				float d = HPF - dvptHPF;
				if (d < distance_to_begin) {
					distance_to_begin = d;
					stage_begin = dvpt.stage;
				}
			}
		}
		if(distance_to_begin>distance_to_end) return stage_end;
		return stage_begin;
	}
	public static float convertHPFToHatch(int id_datasettype,float HPF){

		//Lookfor min max
		float MaxHatch = 0;
		float MaxHPF = 0;
		foreach (Dvpt dvpt in Dvpts) {
            float dvpthatch = 0;
            if(float.TryParse(dvpt.hatch,out dvpthatch))
                if (dvpthatch > MaxHatch) {
                   float.TryParse(dvpt.hatch,out MaxHatch);
                    float.TryParse(dvpt.hpf,out MaxHPF);
    			}
		}
		//Debug.Log ("HPF=" + HPF + " *MaxHatch=" + MaxHatch + " / MaxHPF=" + MaxHPF);
		return HPF * MaxHatch / MaxHPF;
	}
	public static float HatchLength=9f;
	public static IEnumerator downloadDevelopmentalTable(int id_datasettype){

		if (Dvpts == null)
			Dvpts = new List<Dvpt> ();
		else
			Dvpts.Clear ();
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("developmental_table", id_datasettype.ToString());
		//Debug.Log ("Instantiation.urlDataset=" + Instantiation.urlDataset+"?hash="+Instantiation.hash+"&developmental_table="+id_datasettype.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlDataset, form);
		yield return www.Send(); 
		if (www.isHttpError || www.isNetworkError) {
			Debug.Log ("Error : " + www.error +" with "+www.responseCode); 

			//foreach (var it in www.GetResponseHeaders ())  Debug.Log ("Header" + it.Key + "->" + it.Value);
		}
		else {
			if(www.downloadHandler.text!=""){
				var N = JSONNode.Parse (www.downloadHandler.text);
				for (int i = 0; i < N.Count; i++) {

					//Debug.Log ("Add "+i + "->" + N [i].ToString ());
					string period = N [i] [0].ToString ().Replace ('"', ' ').Trim ();
					string stage = N [i] [1].ToString ().Replace ('"', ' ').Trim ();
					string developmentaltstage = N [i] [2].ToString ().Replace ('"', ' ').Trim ();
					string description = N [i] [3].ToString ().Replace ('"', ' ').Trim ();
					string hpf = N [i] [4].ToString ().Replace ('"', ' ').Trim ();
					string hatch = N [i] [5].ToString ().Replace ('"', ' ').Trim ();
					Dvpts.Add (new Dvpt (period, stage, developmentaltstage, description, hpf, hatch));
				}
			}
		}
		www.Dispose (); 

		//Now we adapt the size of the litlle boxes
		//Stage
		Instantiation.start_stage = "Stage "+convertTimeToStage (Instantiation.id_type, Instantiation.spf, Instantiation.dt,Instantiation.MinTime);
		Instantiation.end_stage = "Stage "+convertTimeToStage (Instantiation.id_type, Instantiation.spf, Instantiation.dt,Instantiation.MaxTime);


	}



}
