﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using System;


//Mutant
public class Mutant{
    public int id;
    public GameObject go;
    public int biomaterial_id;
    public int mutant_id;
    public int nbderegulations;
    public Dictionary<Cell, float> CellsMutant;
    public   Mutant(int id){  this.id = id;  }

    public void active(){
        //Debug.Log("ACTIVE");
        if (this.CellsMutant != null)
        {
            foreach (var cn in this.CellsMutant)
            {
                Cell c = cn.Key;
                c.addSelection(Mutation.geneMutate.selection);
            }
        }        
    }
}

//Main mutation classes 
public class Mutation : MonoBehaviour
{

    public static GameObject MenuMutants;
    public static GameObject MenuDeregulations;
    public static GameObject scrollBar;
    public static Mutant MutantActif;
    //List all Mutants avaible 
    public static List<Mutant> Mutants;
    public static Gene geneMutate;

    public void Start()
    {
       
    }
    public static void init()
    {
        MenuMutants = Instantiation.canvas.transform.Find("MenuMutants").gameObject;
        MenuDeregulations=Instantiation.canvas.transform.Find("MenuDeregulations").gameObject;
        scrollBar = MenuMutants.transform.Find("Scrollbar").gameObject;
        scrollBar.SetActive(false);
        MutantActif = null;
    }
    
    //Is this gene have mutant ?
    public static void havemutant(Gene g) { Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.ismutant(g)); }


    //Check if we have any mutant for this gene
    public static IEnumerator ismutant(Gene g)
    {
        g.SubGene.transform.Find("mutant").gameObject.SetActive(false);
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("havemutant", g.id);
        //Debug.Log("havemutant ?. Gene id" + g.id);
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAniseed, form);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
            Debug.Log("Error : " + www.error);
        else
        {
            if (www.downloadHandler.text == "") g.SubGene.transform.Find("mutant").gameObject.SetActive(false); //No mutant ID
            else
            {  //MUTANT ID
                g.SubGene.transform.Find("mutant").gameObject.SetActive(true);
            }
        }
        www.Dispose();
    }


     //DEREGULATIONs
    public static void listMutantsListen(Button b, Gene g) { b.onClick.AddListener(() => Mutation.listMutants(g)); }
    public static void listMutants(Gene g) { Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryMutants(g)); }



    public static IEnumerator queryMutants(Gene g)
        {
            MutantActif=null;
            geneMutate = g;
            //Debug.Log("geneMutate==" + geneMutate.name);
            Mutation.MenuMutants.SetActive(false);
            Mutation.MenuDeregulations.SetActive(false);

            Mutation.MenuMutants.transform.Find("MutationFor").gameObject.GetComponent<Text>().text = g.name;

            g.SubGene.transform.Find("mutant").gameObject.SetActive(false);
            WWWForm form = new WWWForm();
            form.AddField("hash", Instantiation.hash);
            form.AddField("listmutants", g.id);
            UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAniseed, form);
            yield return www.Send();
            if (www.isHttpError || www.isNetworkError) Debug.Log("Error : " + www.error);
            else
            {
                //Debug.Log("www.downloadHandler.text="+www.downloadHandler.text);
                if (www.downloadHandler.text == "") g.SubGene.transform.Find("mutant").gameObject.SetActive(false); //No mutant ID
                else
                { //MUTANT ID
                    g.SubGene.transform.Find("mutant").gameObject.SetActive(true);
                    Mutation.MenuMutants.SetActive(true);
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        //Delete Previous Deregulations
                        if (Mutants != null)
                        {
                            foreach (Mutant mut in Mutants) Destroy(mut.go);
                            Mutants.Clear();
                        }else    Mutants = new List<Mutant>();
                        GameObject MutantsList = Mutation.MenuMutants.transform.Find("Mutants").gameObject;
                        GameObject template = MutantsList.transform.Find("MutantTemplate").gameObject;
                        template.SetActive(false);
                        initialY = template.transform.position.y;
                        int nbTotalDeregulation = 0;
                        foreach (var key in N.Keys)
                        {
                                int id = int.Parse(key.ToString().Trim());
                                Mutant mutant = new Mutant(id);
                                mutant.go = (GameObject)Instantiate(template, MutantsList.transform);
                                mutant.go.name = "MUT_" + id.ToString();
                                 mutant.biomaterial_id  = int.Parse(N[key]["biomaterial_id"].ToString().Replace('"',' ').Trim());
                                JSONNode NJ = N[key]["names"];
                                String names = "";
                                //Debug.Log("Muta,nt "+id+ "with "+NJ.Count);
                                for (int i = 0; i < NJ.Count; i++)
                                {
                                    if (names != "") names += "\n";
                                    names += NJ[i].ToString().Replace('"',' ').Trim();
                                }
                                //Debug.Log(names);
                                mutant.nbderegulations = NJ.Count;
                                GameObject mutant_name = mutant.go.transform.Find("mutant_name").gameObject;
                                mutant_name.GetComponent<Text>().text = names;
                                //Resize the menu
                                RectTransform mutant_rect = mutant.go.transform.GetComponent<RectTransform>();
                                mutant_rect.sizeDelta= new Vector2(mutant_rect.rect.width,mutant.nbderegulations* Mutation.spaceY);

                                RectTransform mutant_name_rect = mutant_name.transform.GetComponent<RectTransform>();
                                mutant_name_rect.sizeDelta = new Vector2(mutant_name_rect.rect.width, mutant.nbderegulations * Mutation.spaceY);

                                mutant.go.SetActive(true);
                                mutant.go.transform.position = new Vector3(mutant.go.transform.position.x,initialY - nbTotalDeregulation * Mutation.spaceY * Instantiation.canvasScale, mutant.go.transform.position.z);
                                nbTotalDeregulation += mutant.nbderegulations;

                                openDeregulationListen(mutant_name.GetComponent<Button>(),mutant);

                                mutant.go.transform.Find ("selection").transform.Find ("Background").gameObject.GetComponent<Image> ().sprite = null;
                                mutant.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().color = geneMutate.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().color;
    
                                activeMutantListen("selection",mutant.go.transform.Find("selection").gameObject.GetComponent<Toggle>(), mutant);
                                activeMutantListen("selectionWT",mutant.go.transform.Find("selectionWT").gameObject.GetComponent<Toggle>(), mutant);

                                activeAniseedCardListen(mutant.go.transform.Find("genecard").gameObject.GetComponent<Button>(), mutant);
                                Mutants.Add(mutant);
                        }
                        //Resize the menu
                         RectTransform dere_rect = Mutation.MenuMutants.transform.GetComponent<RectTransform>();
                        if (nbTotalDeregulation > maxNbDraw)  dere_rect.sizeDelta = new Vector2(dere_rect.rect.width, 65f+(maxNbDraw+1)*Mutation.spaceY);
                        else dere_rect.sizeDelta = new Vector2(dere_rect.rect.width, 65f+nbTotalDeregulation  * Mutation.spaceY);
                        
                    }
                }
            }
            www.Dispose();
            Mutation.updateScrol();
           Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryNbCellsbyMutant("selection",g));
           Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryNbCellsbyMutant("selectionWT",g));
        }

    //ANISEED CARD
    public static void activeAniseedCardListen(Button b, Mutant mn) { b.onClick.AddListener(() => Mutation.activeAniseedCard(mn)); }
    public static void activeAniseedCard(Mutant mn) {  //Change show_gene by show_expression to get expression
        Application.ExternalEval("window.open(\"https://www.aniseed.cnrs.fr/aniseed/experiment/show_insitu_by_biomaterial?biomaterial_id=" + mn.biomaterial_id + "&mutant_id=" + mn.id + "\")");
    }


    //DEREGULATIONs
    public static void openDeregulationListen(Button b, Mutant mn) { b.onClick.AddListener(() => Mutation.openDeregulation(mn)); }
    public static void openDeregulation(Mutant mn) { Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Deregulate.queryDeregulations(mn)); }



    //FOR SCROLL BAR
    public static int maxNbDraw = 27;  //Commpris
    public static float initialY = 0;
    public static float spaceY = 20f;

    public static int getNbScrol(){
        int nb = 0;
        if (Mutants != null) 
            foreach (Mutant mn in Mutants) nb += mn.nbderegulations;
        return nb;
    }

    public static void updateScrol()
    {
        int nbTotalScroll=Mutation.getNbScrol();
        if (nbTotalScroll < Mutation.maxNbDraw)
        {
            scrollBar.SetActive(false);
            int nbView = 0;
            foreach (Mutant mn in Mutants)
            {
                mn.go.SetActive(true);
                mn.go.transform.position = new Vector3(mn.go.transform.position.x, Mutation.initialY - nbView * Mutation.spaceY * Instantiation.canvasScale, mn.go.transform.position.z);
                nbView += mn.nbderegulations;
            }
        }
        else
        {
            if (!scrollBar.activeSelf) scrollBar.GetComponent<Scrollbar>().value = 1;
            scrollBar.GetComponent<Scrollbar>().numberOfSteps = 1 + nbTotalScroll - maxNbDraw;
            scrollBar.GetComponent<Scrollbar>().size = 1f / (float)(1 + nbTotalScroll - maxNbDraw);
            scrollBar.SetActive(true);
            Scroll(); //Each Time we update the scroll bar (search ,selected cells ..) we reset the list from scratc
        }
    }

    public void onSroll(Single value) { Scroll(); }
    public static void Scroll()
    {
        Single value = scrollBar.GetComponent<Scrollbar>().value;
        int nbSteps = scrollBar.GetComponent<Scrollbar>().numberOfSteps;
        int decals = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
        int nbView = 0;
        foreach (Mutant mn in Mutants)
        {
            if (nbView >= decals && nbView <= decals + Mutation.maxNbDraw-mn.nbderegulations+1)
            {
                mn.go.SetActive(true);
                mn.go.transform.position = new Vector3(mn.go.transform.position.x, Mutation.initialY - (nbView - decals) * Mutation.spaceY * Instantiation.canvasScale, mn.go.transform.position.z);
            }
            else mn.go.SetActive(false);
            nbView += mn.nbderegulations;
        }
    }


    public static IEnumerator queryNbCellsbyMutant(String which, Gene g)
    {
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("nbcellsmutants", g.id.ToString());
        if (which == "selectionWT") form.AddField("wildtype", "ok");
        form.AddField("organism_id", Aniseed.organism_id.ToString());
        form.AddField("start_stage", Instantiation.start_stage);
        form.AddField("end_stage", Instantiation.end_stage);
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAniseed, form);
        //Debug.Log (Instantiation.urlAniseed + "?nbcellsmutants="+g.id.ToString()+"&organism_id=" + organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
        {
            Debug.Log("Error : " + www.error);
        }
        else
        {
            //Debug.Log ("queryNbCellsbyMutant="+www.downloadHandler.text);
            var N = JSONNode.Parse(www.downloadHandler.text);
            if (N != null)
            {

                Dictionary<int, int> CellsForMutant = new Dictionary<int, int>();
                for (int i = 0; i < N.Count; i++)
                {
                    int id_mutant = int.Parse(N[i][0].ToString().Replace('"', ' ').Trim());
                    int nbcells = int.Parse(N[i][1].ToString().Replace('"', ' ').Trim());
                    CellsForMutant[id_mutant] = nbcells;
                    //Debug.Log("Keep " + id_mutant + " with " + nbcells + " cells");
                }
                if (which == "selection")
                {
                    foreach (Mutant mn in Mutants)
                    {
                        if (CellsForMutant.ContainsKey(mn.id)) mn.go.transform.Find(which).gameObject.SetActive(true);
                        else mn.go.transform.Find(which).gameObject.SetActive(false);
                    }
                }
                else
                {
                    foreach (Mutant mn in Mutants)
                    {
                        if (CellsForMutant.ContainsKey(mn.biomaterial_id)) mn.go.transform.Find(which).gameObject.SetActive(true);
                        else mn.go.transform.Find(which).gameObject.SetActive(false);
                    }
                }
            }
        }
        www.Dispose();
    }



    //ACTIVE MUTANT
    public static void activeMutantListen(String which,Toggle b, Mutant mn) { b.onValueChanged.AddListener(delegate { Mutation.activeMutant(which,mn); }); }

    public static void activeMutant(String which, Mutant mutant)
    {
        Toggle to = mutant.go.transform.Find(which).gameObject.GetComponent<Toggle>();
        if(to.isOn) {
            if(mutant.CellsMutant==null) Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryCells(which, mutant));
            else mutant.active();
            to.onValueChanged.RemoveAllListeners();
            to.isOn = true;
            activeMutantListen(which, to, mutant);
            MutantActif = mutant;
        }
    }

  
   

    public static IEnumerator queryCells(String which,Mutant mn)
    {
        mn.go.transform.Find(which).gameObject.SetActive(false);
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("mutantcells", mn.id.ToString());
        if (which == "selectionWT")  form.AddField("wildtype", mn.biomaterial_id.ToString());
        form.AddField("organism_id", Aniseed.organism_id.ToString());
        form.AddField("start_stage", Instantiation.start_stage);
        form.AddField("end_stage", Instantiation.end_stage);
        UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAniseed, form);
        //Debug.Log (Instantiation.urlAniseed + "?mutantcells="+mn.id.ToString()+"&organism_id=" + Aniseed.organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
        yield return www.Send();
        if (www.isHttpError || www.isNetworkError)
        {
            Debug.Log("Error : " + www.error);
        }
        else
        {
            //Debug.Log(which +" -> queryCells=" + www.downloadHandler.text);
            if (www.downloadHandler.text != "[]")
            {
                mn.CellsMutant = new Dictionary<Cell, float>();
                var N = JSONNode.Parse(www.downloadHandler.text);
                //Debug.Log ("Found " + N.Count + " Stages");
                if (N.Count > 0)
                {
                    int i = 0;

                    foreach (var key in N.Keys)
                    {
                        string stage = key.ToString().Trim();
                        //Debug.Log ("stage=" + stage);
                        string[] cellnamess = N[i].ToString().Replace('[', ' ').Replace(']', ' ').Replace('"', ' ').Trim().Split(',');

                        Vector2 hpfb = Aniseed.getHPFBoundaries(stage);
                        //Debug.Log ("Found hpfb.x=" + hpfb.x + " hpfb.y=" + hpfb.y);
                        int minTime = (int)Mathf.Floor((hpfb.x * 3600.0f - Instantiation.spf) / Instantiation.dt);
                        int maxTime = (int)(Mathf.Floor((hpfb.y * 3600.0f - Instantiation.spf) / Instantiation.dt) + 1);
                        //Debug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);
                        if (minTime < Instantiation.MinTime)  minTime = Instantiation.MinTime;
                        if (maxTime > Instantiation.MaxTime)   maxTime = Instantiation.MaxTime;
                        //Debug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);

                        List<string> cellnamesConverted = new List<string>();
                        foreach (string cn in cellnamess)
                        {
                            string cnc = Aniseed.convertCellName(cn);
                            if (cnc != "" && !cellnamess.Contains(cnc))
                            {
                                //Debug.Log ("For " + g.name + " -> at Stage " + stage + "-> cell==" + cnc);
                                cellnamesConverted.Add(cnc);
                            }
                        }

                        for (int t = minTime; t <= maxTime; t++)
                        {
                            Dictionary<string, Cell> CellAtT = Instantiation.CellT[t];
                            foreach (var itemc in CellAtT)
                            {
                                Cell c = itemc.Value;
                                string cn = c.getInfos(Aniseed.idxInfoxName);
                                //Debug.Log (stage +"-> TEST==" + cn);
                                if (cellnamesConverted.Contains(cn))
                                {
                                    if (!mn.CellsMutant.ContainsKey(c))
                                    {
                                        //Debug.Log(" Add Cell "+cn+" at "+t);
                                        mn.CellsMutant[c] = 1f;
                                    }
                                }
                            }
                        }
                        i += 1;
                    }
                    mn.active();
                }
            }
        }
        www.Dispose();
        mn.go.transform.Find(which).gameObject.SetActive(true);
    }


    public void resetExpression()
    {
        //Desactive all
        foreach (Mutant mn in Mutation.Mutants)
        {
            Toggle ton = mn.go.transform.Find("selection").gameObject.GetComponent<Toggle>();
            if (ton.isOn)
            {
                ton.onValueChanged.RemoveAllListeners();
                ton.isOn = false;
                activeMutantListen("selection", ton, mn);
            }
            Toggle tonWT = mn.go.transform.Find("selectionWT").gameObject.GetComponent<Toggle>();
            if (tonWT.isOn)
            {
                tonWT.onValueChanged.RemoveAllListeners();
                tonWT.isOn = false;
                activeMutantListen("selectionWT", tonWT, mn);
            }
        }

        //Debug.Log("resetExpression for "+Mutation.geneMutate.name);
        for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
        {
            Dictionary<string, Cell> CellAtT = Instantiation.CellT[t];
            foreach (var itemc in CellAtT)
            {
                Cell ce = itemc.Value;
               ce.removeSelection(Mutation.geneMutate.selection);
            }
        }
    }
}
