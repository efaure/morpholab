﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using System;



public class Gene : MonoBehaviour{

	public GameObject go;
	public GameObject SubGene;
	public int id;
	public string name;
	public string unique_id;
	public int selection;
	public Dictionary<Cell,float> Cells;
	public bool isActive;
	public bool isDownload;
	public bool isSelected;
	public int nbCells;
	public bool isQuantitative;
	public Correspondence cor; //For genetic file, the associated correspondence
	public Gene (int id,string name,string unique_id)
	{
		this.Cells = null;
		this.id = id;
		this.name = name;
		this.unique_id = unique_id;
		this.isActive = false;
		this.isDownload = false;
		this.isSelected = true;
		this.nbCells = -1;
		this.isQuantitative = false;
		this.cor = null;
	}

	public void init(int shiftV){
		this.go = (GameObject)Instantiate (Aniseed.GeneTemplate,Aniseed.GeneTemplate.transform.parent);
		this.name=this.name;
		this.go.name = this.name;
		this.setName ();
		this.selection = shiftV + 1;
		while (this.selection > 254) this.selection -= 254;//Mathf.RoundToInt(Mathf.Repeat(shift+1, 254))+1;;

		this.SubGene = this.go.transform.Find ("SubGene").gameObject;
		SubGene.SetActive (false);

		this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField> ().text=this.selection.ToString();
		//this.go.transform.Find ("selection").gameObject.SetActive (false);
		setSelectionRendering ();

	
		this.go.transform.Find ("annotation").gameObject.SetActive (false); //FOR MASSIVE CURRATION

		Aniseed.activeListen(this.go.transform.Find ("selection").gameObject.GetComponent<Toggle> (),this);
		Aniseed.sliderListen(this.SubGene.transform.Find ("Slider").gameObject.GetComponent<Slider> (),this);

        if (id != -1)
        {
            this.SubGene.transform.Find("genecard").gameObject.SetActive(true);
            Aniseed.openLinkListen(this.SubGene.transform.Find("genecard").gameObject.GetComponent<Button>(), this);
            Mutation.listMutantsListen(this.SubGene.transform.Find("mutant").gameObject.GetComponent<Button>(), this);
        }
        else
        {
            this.SubGene.transform.Find("genecard").gameObject.SetActive(false);
            this.SubGene.transform.Find("mutant").gameObject.SetActive(false);
        }
		Aniseed.showSubGeneListen(this.go.transform.Find ("deploy").gameObject.GetComponent<Button> (),this);
		Aniseed.editSelectionListen (this.SubGene.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> (), this);

		Aniseed.changeColorBarListen (this.SubGene.transform.Find ("Quantitative").gameObject.GetComponent<Button> (), this);
		Aniseed.changeColorMapListen (this.SubGene.transform.Find ("Quantitative").gameObject.transform.Find ("execute").gameObject.GetComponent<Button> (), this);
	}


	public void showSubGene(){
		if (this.SubGene.activeSelf)
			this.SubGene.SetActive (false);
		else this.SubGene.SetActive (true);
		Aniseed.reOrganize ();
	}


	public void addExpression(Cell c,float expression){
		if (this.Cells == null) this.Cells = new Dictionary<Cell,float> ();
		this.Cells [c] = expression;
		this.nbCells = this.Cells.Count ();
		this.setName();
	}

	public void removeExpression(Cell c){
		if (this.Cells != null && this.Cells.ContainsKey (c))
			this.Cells.Remove (c);
	}

	//SELECTION
	public void editSelection(){
		int previousSelection = this.selection;
		string s = this.SubGene.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> ().text;
		//Debug.Log ("Edit " + s);
		int ns;
		if (int.TryParse (s,out ns)) {
			if (ns > 254) ns = 254;
			this.selection = ns;
		}
		this.SubGene.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> ().text=this.selection.ToString();
		if (this.isActive && this.isQuantitative) this.inActiveQuantitative ();
	
		if (this.isActive && previousSelection!=this.selection) {
			foreach (var ce in this.Cells) {
				Cell c = ce.Key;
				c.removeSelection (previousSelection);
				c.addSelection (this.selection);
			}
		}
		this.SubGene.transform.Find ("Slider").gameObject.GetComponent<Slider> ().value = this.selection;
	}

	public void slider(){
		if (this.isActive && this.isQuantitative) this.inActiveQuantitative ();
		int previousSelection = this.selection;
		this.selection = Mathf.RoundToInt(this.SubGene.transform.Find ("Slider").gameObject.GetComponent<Slider> ().value);
		this.SubGene.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> ().text=this.selection.ToString();
		this.setSelectionRendering ();
		if (this.isActive && previousSelection!=this.selection) {
			foreach (var ce in this.Cells) {
				Cell c = ce.Key;
				c.removeSelection (previousSelection);
				c.addSelection (this.selection);
			}
		}
	}


	public void setSelectionRendering(){
		this.go.transform.Find ("selection").transform.Find ("Background").gameObject.GetComponent<Image> ().sprite = null;
		this.go.transform.Find ("selection").transform.Find("Background").gameObject.GetComponent<Image> ().color = SelectionColor.getSelectedMaterial (this.selection).color;
	}


	//QUANTITATIVE
	public void inActiveQuantitative(){
		if (this.isActive && this.isQuantitative) {
			this.removeQuantitative ();
			GameObject buttonToColorize = this.SubGene.transform.Find ("Quantitative").gameObject;
			buttonToColorize.GetComponentInChildren<Text> ().text = "None";
			buttonToColorize.GetComponent<Image> ().sprite = null;
		}
		this.isQuantitative = false;
	}
	public void removeQuantitative(){
		foreach (var cn in this.Cells) {
			Cell c = cn.Key;
			c.UploadedColor = false;
			c.updateShader ();
		} 
	}
	//When we click on the button
	public void changeColorBar(){
		MenuColorBar.buttonToColorize = this.SubGene.transform.Find ("Quantitative").gameObject;
		int type=int.Parse(MenuColorBar.buttonToColorize.transform.Find ("type").gameObject.GetComponentInChildren<Text> ().text);
		float MinV = 0f;float MaxV = 1f;
		//First we have to check if 
		if (MenuCell.MenuBarColor.activeSelf)
			MenuCell.MenuBarColor.SetActive (false);
		else {
			MenuCell.MenuBarColor.SetActive (true);
			MenuColorBar.assignValues(type,int.Parse (MenuColorBar.buttonToColorize.transform.Find ("min").gameObject.GetComponentInChildren<Text> ().text), int.Parse (MenuColorBar.buttonToColorize.transform.Find ("max").gameObject.GetComponentInChildren<Text> ().text), MinV,MaxV,this.name);
		}
	}


	public void changeColorMap(){
		Debug.Log ("changeColorMap");
		GameObject buttonToColorize = this.SubGene.transform.Find ("Quantitative").gameObject;
		int typecmap=int.Parse(buttonToColorize.transform.Find ("type").gameObject.GetComponentInChildren<Text> ().text);
		int minC=int.Parse (buttonToColorize.transform.Find ("min").gameObject.GetComponentInChildren<Text> ().text);
		int maxC=int.Parse (buttonToColorize.transform.Find ("max").gameObject.GetComponentInChildren<Text> ().text);
        float minV=0;float.TryParse (buttonToColorize.transform.Find ("minV").gameObject.GetComponentInChildren<Text> ().text,out minV);
        float maxV=minV;float.TryParse (buttonToColorize.transform.Find ("maxV").gameObject.GetComponentInChildren<Text> ().text,out maxV);

		this.go.transform.Find ("selection").transform.Find ("Background").gameObject.GetComponent<Image> ().color = Color.white;
		this.go.transform.Find ("selection").transform.Find("Background").gameObject.GetComponent<Image> ().sprite=buttonToColorize.GetComponent<Image> ().sprite;
		if(typecmap>0) this.applyCmap (typecmap,minC,maxC,minV,maxV);
		else  this.cancelCmap ();
	}

	//Apply the changement
	public void applyCmap(int typecmap, int minC,int maxC,float minV,float maxV){
		if (this.isActive && !this.isQuantitative) { //REMOVE OLD SELECTION 
			foreach (var ce in this.Cells) {
				Cell c = ce.Key;
				c.removeSelection (this.selection);
			}
		}
		this.isQuantitative = true;
		if (this.isActive) {
			Debug.Log ("applyCmap from " + minV + " to " + maxV);
			foreach (var cn in this.Cells) {
				Cell c = cn.Key;
				float v = cn.Value;
				c.UploadNewColor (MenuColorBar.getColor (typecmap, v, minC, maxC, minV, maxV));
			} 
		}
	}
	//Reset Upload Color
	public void cancelCmap(){
		this.isQuantitative = false;
		if (this.isActive) removeQuantitative ();
		setSelectionRendering ();
	}



	public void setNbCells(int nb){
		this.nbCells = nb;
		this.setName ();
	}
	public void setName(){
		string n = this.name;
		if (this.nbCells >= 0) n = "(" + this.nbCells + ")" + n;
		this.go.transform.Find ("GeneName").gameObject.GetComponent<Text> ().text= n;
	}

	public void download(){
		//if(this.Cells!=null) this.setName("(" + this.Cells.Count + ") "+ this.name);
		//else this.setName("(0) "+ this.name);
		this.go.transform.Find ("selection").gameObject.SetActive (true);
		this.isDownload = true;
	}
	public void changeActive(){
		if (!this.isActive) this.active ();
		else this.inActive();
	}
	public void active(){
		if (!this.isDownload)
			Instantiation.canvas.transform.Find ("MenuGenetic").GetComponent<Aniseed> ().StartCoroutine (Aniseed.queryGeneAnissed (this));
		else{
			//Debug.Log ("Active Gene " + this.name);
			//Debug.Log ("Contains " + this.Cells.Count + " cells");
			this.isActive = true;
			if (Aniseed.isUI == 0) {
				if (this.Cells != null) {
					if (!this.isQuantitative) { //SELECTIO?
						foreach (var cn in this.Cells) {
							Cell c = cn.Key;
							c.addSelection (this.selection);
						}
					} else { //QUANTITATIVE
						this.changeColorMap ();
					}
				}
			} else {
				Aniseed.removeUI ();
				Aniseed.computeCellsUI ();
			}

		}
	}
		
	public void inActive(){
		this.isActive = false;
		if (Aniseed.isUI == 0) {
			if (!this.isQuantitative) { //SELECTIO?
				if (this.Cells != null)
					foreach (var ce in this.Cells) {
						Cell c = ce.Key;
						c.removeSelection (this.selection);
					}
			} else
				this.removeQuantitative ();
		}
		else {
			Aniseed.removeUI ();
			Aniseed.computeCellsUI ();
		}
	}


	public void select(int shiftV){
		this.shift (shiftV, 0);
		this.isSelected = true;
	}
	public void shift(int shiftV,int decals){
		if (shiftV >= decals && shiftV <= decals + Aniseed.maxNbDraw) {
			this.go.SetActive (true);
			this.go.transform.position = new Vector3 (this.go.transform.position.x,  Aniseed.initialY - (shiftV - decals) * Aniseed.spaceY * Instantiation.canvasScale, this.go.transform.position.z);
		}
		else this.go.SetActive (false);
	}
	public void unSelect(){
		this.go.SetActive (false);
		this.isSelected = false;
	}


	public void openLink(){
		//Debug.Log ("Open " + this.unique_id);
		//Change show_gene by show_expression to get expression
		Application.ExternalEval("window.open(\"https://www.aniseed.cnrs.fr/aniseed/gene/show_gene?unique_id="+this.unique_id+"\")");
	}

   
   
	public void annotate(){
		if (this.Cells == null) this.Cells = new Dictionary<Cell,float>  ();
		GameObject annotation = this.go.transform.Find ("annotation").gameObject;
		bool v = annotation.transform.Find ("annotate").gameObject.GetComponent<Toggle> ().isOn;
		if(v){ //Activate Cell 
			foreach (Cell cc in SelectionCell.clickedCells)
				if(!this.Cells.ContainsKey (cc)){
					this.Cells [cc] = 1f;
					Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(MenuCurrated.addCuration (this.cor,cc,"1"));
				}
			annotation.transform.Find ("nbTrue").gameObject.GetComponent<Text> ().text = ""+SelectionCell.clickedCells.Count.ToString()+"/"+SelectionCell.clickedCells.Count.ToString();
		}
		else{ //Inactive Cell
			//Dictionary<Cell,float> TempCells = new Dictionary<Cell,float>  ();
			foreach (Cell cc in SelectionCell.clickedCells)
				if(this.Cells.ContainsKey (cc)){
					this.Cells.Remove (cc);
					Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(MenuCurrated.addCuration (this.cor,cc,"0"));
				}
			
			annotation.transform.Find ("nbTrue").gameObject.GetComponent<Text> ().text = "0/"+SelectionCell.clickedCells.Count.ToString();
		}
			

	}

}

public class Aniseed : MonoBehaviour {


	//GENE CARD https://www.aniseed.cnrs.fr/aniseed/gene/show_gene?unique_id=Cirobu.g00006940
	//WASHU BROWSER https://www.aniseed.cnrs.fr/browser/?genome=Cirobu_KH&coordinate=KhC4:4306889-4323528&defaultContent=on
	//GENOME BROWSER https://www.aniseed.cnrs.fr/fgb2/gbrowse/ciona_intestinalis?name=KhC4:4306889..4323528


	public static GameObject scrollBar;
	public static int organism_id = 112;//Ciona intestinalis (see http://dev.aniseed.cnrs.fr/api/)

	public static Dictionary<string, Gene> Genes ; //List of Genes

	public static GameObject MenuGenetic;
   
	public static GameObject GeneTemplate;
	public static GameObject GeneList;

	public Text currenttextloaded; //TEMP Text draw for informations loading 
	public static float initialY=0;
	public static float spaceY = 20;
	public static int idxInfoxName;
	public  InputField GeneName; //For Search 

	//UNION INTRSECTION
	public static Toggle Union;
	public static Toggle Intersection;
	public static Toggle Different;

	void Start () { //When we click on Menu Genetic  the first time 
		if (Instantiation.id_type == 2 || Instantiation.id_type == 3) {
			organism_id = 112;
			idxInfoxName = MenuCell.getInfosIndex ("Name");
			if(idxInfoxName==-1) idxInfoxName = MenuCell.getInfosIndex ("Names");
			//Debug.Log ("Start Anisseed "+idxInfoxName);
			init ();
			StartCoroutine (requestAllGenes ());
			MenuGenetic.transform.Find ("genecard").gameObject.SetActive (true);
		}
	}


	public static void init(){
		//Debug.Log("INIT GENETIC");
		if (scrollBar == null) {
			MenuGenetic=Instantiation.canvas.transform.Find ("MenuGenetic").gameObject;
            scrollBar = MenuGenetic.transform.Find ("Scrollbar").gameObject;
			scrollBar.SetActive (false);
			Instantiation.canvas.GetComponent<Menus> ().buttonGenetic.SetActive(true);
			Instantiation.canvas.GetComponent<Menus> ().reOrganizeButtons (); //RESCALE BUTTONS AND MENU
			MenuGenetic.transform.Find ("genecard").gameObject.SetActive (false);
            GeneList = MenuGenetic.transform.Find ("GeneList").gameObject;
			GeneTemplate = GeneList.transform.Find ("GeneTemplate").gameObject;
			GeneTemplate.SetActive (false);

			GeneList = MenuGenetic.transform.Find ("GeneList").gameObject;
			GeneTemplate.SetActive (false);
			initialY = GeneTemplate.transform.position.y;

			Intersection = MenuGenetic.transform.Find ("intersection").gameObject.GetComponent<Toggle> ();
			Union = MenuGenetic.transform.Find ("union").gameObject.GetComponent<Toggle> ();
			Different = MenuGenetic.transform.Find ("different").gameObject.GetComponent<Toggle> ();
			onSelectIntersectionListen ();
			onSelectUnionListen ();
			activeUI (false);

            Mutation.init();
		}
	}
	public static GameObject sliderUI;
	public static GameObject selectionUI;
	public static int selectionUIV=1;
	public static List<Cell> UICells;
	public static int isUI = 0; //0 : Different, 1: Union, 2 : Intersection,
	public static void activeUI(bool v){
		if (sliderUI == null) {
			sliderUI = MenuGenetic.transform.Find ("SliderUI").gameObject;
			sliderUI.GetComponent<Slider> ().onValueChanged.AddListener (delegate { Aniseed.onScrollUI (); });
		}
		if (selectionUI == null) selectionUI = MenuGenetic.transform.Find ("selectionUI").gameObject;
		sliderUI.SetActive (v);
		selectionUI.SetActive (v);
		if (v) {
			if (isUI == 0) { //We change only from different
				selectionUI.GetComponent<Image> ().color = SelectionColor.getSelectedMaterial (selectionUIV).color;
				foreach (var gn in Genes) {
					Gene g = gn.Value;
					g.SubGene.SetActive (false);
				}
				reOrganize();
			}
		} else {//NORMAL STATE
			if (isUI != 0) reOrganize();
		}
	}

	public static void onScrollUI(){
		int previousSelection = selectionUIV;
		selectionUIV=Mathf.RoundToInt(sliderUI.GetComponent<Slider> ().value);
		if(UICells!=null)
			foreach (Cell c in UICells) {
				c.removeSelection (previousSelection);
				c.addSelection (selectionUIV);
			}
		selectionUI.GetComponent<Image> ().color = SelectionColor.getSelectedMaterial (selectionUIV).color;
	}
	public static void  removeUI(){
		if(UICells!=null) foreach (Cell c in UICells) c.removeSelection (selectionUIV);
	}
	public static void removeDifferent(){
		foreach (var gn in Genes) {
			Gene g=gn.Value;
			if (g.isActive && g.Cells!=null) {
				foreach (var cn in g.Cells) {
					Cell c = cn.Key;
					c.removeSelection (g.selection);
				}
			}
		}
	}
	//Compute all cells for union or intersecction and apply its selection value
	public static void computeCellsUI(){
		UICells = new List<Cell> ();
		if (isUI == 1) { //UNION 
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				if (g.isActive && g.Cells != null) {
					//Debug.Log ("ad gene " + g.name);
					foreach (var cn in g.Cells) {
						Cell c = cn.Key;
						if (! UICells.Contains (c))
							UICells.Add (c);
					}
				}
			}
		} else { //INTERSECTION
			List<Cell> AllCells= new List<Cell> (); //First We List All Cells in all Active Genes
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				if (g.isActive && g.Cells != null) {
					//Debug.Log ("ad gene " + g.name);
					foreach (var cn in g.Cells) {
						Cell c = cn.Key;
						if (! AllCells.Contains (c))
							AllCells.Add (c);
					}
				}
			}
			//Now we check for all cell if they are inside all Genes
			foreach(Cell ca in AllCells){
				bool isAll = true;
				foreach (var gn in Genes) {
					Gene g = gn.Value;
					if (isAll && g.isActive && g.Cells != null)
					if (!g.Cells.ContainsKey (ca))
						isAll = false;
					
				}
				if (isAll && ! UICells.Contains (ca))
					UICells.Add (ca);
			}


		}
		foreach (Cell c in UICells) c.addSelection (selectionUIV); //Apply the selection value
	}

	//Active Union
	public static void onSelectUnionListen(){ Union.onValueChanged.AddListener (delegate { Aniseed.onSelectUnion (); });}
	public static void onSelectUnion(){
		if (Union.isOn) {
			desactivateToggle (Intersection);
			desactivateToggle (Different);
			Union.onValueChanged.RemoveAllListeners ();
			onSelectIntersectionListen ();
			onSelectDifferentListen ();
			onSelectUnionListen ();
			if (isUI == 0) removeDifferent (); //We Remove all individuals Selection
			else removeUI(); ////We Remove Intersection Selection
			activeUI (true);
			isUI = 1;
			computeCellsUI ();

		} else
			Union.isOn = true;
	}

	//Active Intersection
	public static void onSelectIntersectionListen(){ Intersection.onValueChanged.AddListener (delegate { Aniseed.onSelectIntersection (); });}
	public static void onSelectIntersection(){
		if (Intersection.isOn) {
			desactivateToggle(Union);
			Intersection.onValueChanged.RemoveAllListeners();
			desactivateToggle(Different);
			onSelectUnionListen ();
			onSelectDifferentListen ();
			onSelectIntersectionListen ();
			if (isUI == 0) removeDifferent (); //We Remove all individuals Selection
			else removeUI(); ////We Remove Union Selection
			activeUI (true);
			isUI = 2;
			computeCellsUI ();
		} else
			Intersection.isOn = true;
	}

	//Active Different
	public static void onSelectDifferentListen(){ Different.onValueChanged.AddListener (delegate { Aniseed.onSelectDifferent (); });}
	public static void onSelectDifferent(){
		if (Different.isOn) {
			desactivateToggle (Union);
			desactivateToggle (Intersection);
			Different.onValueChanged.RemoveAllListeners ();
			onSelectUnionListen ();
			onSelectIntersectionListen ();
			onSelectDifferentListen ();
			removeUI(); ////We Remove Union or Intersection Selection
			activeUI (false);
			isUI = 0;
			//Reattribute all cell specification
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				if (g.isActive && g.Cells != null) {
					foreach (var cn in g.Cells) {
						Cell c = cn.Key;
						c.addSelection (g.selection);
					}
				}
			}
		} else
			Different.isOn = true;
	}

	public static void desactivateToggle(Toggle b){
		b.onValueChanged.RemoveAllListeners();
		b.isOn=false;
	}


	public static Gene getGene(string genename){
		if (Genes == null) Genes = new  Dictionary<string, Gene> ();
		if (Genes.ContainsKey (genename)) return Genes [genename];
	
		//Create a new one
		//Debug.Log (" Create Gene for " + genename + " -> ");
		Gene gg=new Gene(-1,genename,"");
		gg.init (Genes.Count);
		gg.shift (Genes.Count,0);
		gg.download ();
		Genes[genename]=gg;
		return gg;
	}


	public static void addCellGene(Cell c, string genename, float expression,Correspondence cor){
		Gene g = getGene (genename);
		g.cor = cor;
		if (expression > 0)
			g.addExpression (c, expression);
		else
			g.removeExpression (c);
	}

	public static void updateGenesNbCells(){
		foreach (var gn in Genes) {
			Gene g = gn.Value;
			if(g.Cells!=null) g.nbCells = g.Cells.Count ();
			//Debug.Log (" updateGenesNbCells for " + g.name + " -> " + g.nbCells);
			g.setName ();
		}
	}

	//When we select a gene name
	public void search(string name){
		name = name.ToLower ().Trim ();
		int nbView = 0;
		if (name == "") {
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				g.select (nbView);
				nbView += 1;
				if(g.SubGene.activeSelf) nbView += 1;
			}
		} else {
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				if (g.name.ToLower ().Contains (name)) {
					g.select (nbView);
					nbView += 1;
					if(g.SubGene.activeSelf) nbView += 1;
				} else
					g.unSelect ();
			}
		}
		updateScrol (nbView);

	}

	public void cancel(){ if (GeneName.text != "")
		GeneName.text = ""; //THIS directrly invoke search ("");
	else
		search ("");		
	}	


	//Show Sub Gene Infos 
	public static void showSubGeneListen(Button b,Gene g){ b.onClick.AddListener (delegate { Aniseed.showSubGene (g); });}
    public static void showSubGene(Gene g)
    {
        if (Aniseed.isUI == 0)
        {
            g.showSubGene();
            Mutation.havemutant(g);
        }
    }
	//ACTIVE GENE
	public static void activeListen(Toggle b,Gene g){ b.onValueChanged.AddListener (delegate { Aniseed.Active (g); });}
	public static void Active(Gene g){   g.changeActive ();  }
	//EDIT SELECTION
	public static void editSelectionListen(InputField inf,Gene g){inf.onEndEdit.AddListener (delegate { Aniseed.editSelection (g); });}
	public static void editSelection(Gene g){ 	g.editSelection (); }
	//SLIDER FOR SELECTION
	public static void sliderListen(Slider s,Gene g){s.onValueChanged.AddListener (delegate { Aniseed.slider (g); });}
	public static void slider(Gene g){ 	g.slider (); }
	//COLOR MAP FOR QUANTITATIVE EXPRESSION
	public static void  changeColorBarListen(Button b,Gene g)  {b.onClick.AddListener (() => Aniseed.changeColorBar(g)); }
	public static void changeColorBar(Gene g){g.changeColorBar (); }
	//Change Colormap
	public static void  changeColorMapListen(Button b,Gene g)  {b.onClick.AddListener (() => Aniseed.changeColorMap(g)); }
	public static void changeColorMap(Gene g){g.changeColorMap (); }
	//For Massive Gene Annotation
	public static void annotateListen(Toggle b,Gene g){ b.onValueChanged.AddListener (delegate { Aniseed.annotate (g); });}
	public static void annotate(Gene g){   g.annotate ();  }



	public void onActiveAll(bool v){
		foreach (var gn in Genes) {
			Gene g = gn.Value;
			if (g.isSelected) {
				//Debug.Log ("Active Gene " + g.name);
				if (v)
					g.go.transform.Find ("selection").gameObject.GetComponent<Toggle> ().isOn = true;//g.active ();
				else
					g.go.transform.Find ("selection").gameObject.GetComponent<Toggle> ().isOn = false;//g.inActive ();
			}
		}
	}

	//FOR SCROLL BAR
	public static int maxNbDraw=27;
	public static float sizeBar = 20f;
	public static void reOrganize(){
		int nbView = 0;
		foreach (var gn in Genes) {
			Gene g = gn.Value;
			if (g.isSelected) {
				nbView += 1;
				if (g.SubGene.activeSelf) nbView += 1;
			}
		}
		updateScrol (nbView);
	}

	public static void updateScrol(int nbTotalScroll){
		
		if (nbTotalScroll < Aniseed.maxNbDraw)
			scrollBar.SetActive (false);
		else {
			if(!scrollBar.activeSelf) scrollBar.GetComponent<Scrollbar> ().value = 1;
			scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1 + nbTotalScroll - maxNbDraw;
			scrollBar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + nbTotalScroll - maxNbDraw);
			scrollBar.SetActive (true);

		}
		Scroll (); //Each Time we update the scroll bar (search ,selected cells ..) we reset the list from scratch

	}
	public void onSroll(Single value){ Scroll (); }
	public static void Scroll(){
		Single value=scrollBar.GetComponent<Scrollbar> ().value;
		int nbSteps = scrollBar.GetComponent<Scrollbar> ().numberOfSteps;
		int decals = (int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		int nbView = 0;
		foreach (var gn in Genes) {
			Gene g = gn.Value;
			if (g.isSelected) {
				g.shift (nbView, decals);
				nbView += 1;
				if(g.SubGene.activeSelf)nbView += 1;
			}
		}
	}



	//ANISEED DB SPECIFIC REQUEST
	//Query to look for a gene namev
	public IEnumerator requestAllGenes(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("allgene", "ok");
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAniseed, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			//Debug.Log("FOUNDNNNND : " + www.downloadHandler.text);
			var N = JSONNode.Parse (www.downloadHandler.text);
			Genes = new  Dictionary<string, Gene> ();
			if (N!=null && N.Count > 0) {
				for (int i = 0; i < N.Count; i++) {
					string name = N [i] ["gene_name"].ToString ();
					name = name.Substring (1, name.Length - 2);
					string unique_id = N [i] ["unique_gene_id"].ToString ();
					unique_id = unique_id.Substring (1, unique_id.Length - 2);
					Gene g = new Gene (int.Parse (N [i] ["id"]), name,unique_id);
					g.init (i);
					//StartCoroutine (Aniseed.queryGeneAnissed (g));
					Genes[name]=g;
				}
			} 
			updateScrol (Genes.Count);
		}
		www.Dispose (); 
		StartCoroutine(Aniseed.queryNbCells());
	}


		

	public static IEnumerator queryGeneAnissed(Gene g){
		if (g.Cells == null) {
			Debug.Log ("Look for cells for " + g.name);
		
			//g.setName(g.name + " (wait ....)");
			WWWForm form = new WWWForm ();
			form.AddField ("hash", Instantiation.hash); 
			form.AddField ("gene_id", g.id.ToString ());
			form.AddField ("organism_id", organism_id.ToString ());
			form.AddField ("start_stage", Instantiation.start_stage);
			form.AddField ("end_stage", Instantiation.end_stage);
			UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlAniseed, form);
			yield return www.Send ();
			if (www.isHttpError || www.isNetworkError) {
				Debug.Log ("Error : " + www.error); 
			} else {
				//Debug.Log ("For " + g.name + " -> found cells " + www.downloadHandler.text);
				if (www.downloadHandler.text != "[]") {
					g.Cells = new Dictionary<Cell,float> ();
					var N = JSONNode.Parse (www.downloadHandler.text);
					//Debug.Log ("Found " + N.Count + " Stages");
					if (N.Count > 0) {
						int i = 0;
	
						foreach (var key in N.Keys) {
							string stage = key.ToString ().Trim ();
							//Debug.Log ("stage=" + stage);
							string[] cellnamess = N [i].ToString ().Replace ('[', ' ').Replace (']', ' ').Replace ('"', ' ').Trim ().Split (',');


							Vector2 hpfb = getHPFBoundaries (stage);
							//Debug.Log ("Found hpfb.x=" + hpfb.x + " hpfb.y=" + hpfb.y);
							int minTime = (int)Mathf.Floor ((hpfb.x * 3600.0f - Instantiation.spf) / Instantiation.dt);
							int maxTime = (int)(Mathf.Floor ((hpfb.y * 3600.0f - Instantiation.spf) / Instantiation.dt) + 1);
							//Debug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);
							if (minTime < Instantiation.MinTime)
								minTime = Instantiation.MinTime;
							if (maxTime > Instantiation.MaxTime)
								maxTime = Instantiation.MaxTime;
							//Debug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);

							List<string> cellnamesConverted = new List<string> ();
							foreach (string cn in cellnamess) {
								string cnc = convertCellName (cn);
								if (cnc != "" && !cellnamess.Contains (cnc)) {
									//Debug.Log ("For " + g.name + " -> at Stage " + stage + "-> cell==" + cnc);
									cellnamesConverted.Add (cnc);
								}
							}

							for (int t = minTime; t <= maxTime; t++) {
								Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
								foreach (var itemc in CellAtT) { 
									Cell c = itemc.Value;
									string cn = c.getInfos (idxInfoxName);
									//Debug.Log (stage +"-> TEST==" + cn);
									if (cellnamesConverted.Contains (cn)) {
										if (!g.Cells.ContainsKey (c)) {
											//Debug.Log(" Add Cell "+cn+" at "+t);
											g.Cells [c] = 1f;
										}
									}
								}
							}
							i += 1;
						}
					}
				}
				g.download ();
				g.active ();
			}
			www.Dispose (); 
		} else {
			g.active ();
			g.active ();
		}
	}


	public static IEnumerator queryNbCells(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("numberofcells", "ok");
		form.AddField ("organism_id", organism_id.ToString ());
		form.AddField ("start_stage", Instantiation.start_stage);
		form.AddField ("end_stage", Instantiation.end_stage);
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlAniseed, form);
		//Debug.Log (Instantiation.urlAniseed + "?numberofcells=ok&organism_id=" + organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
		yield return www.Send ();
		if (www.isHttpError || www.isNetworkError) {
			Debug.Log ("Error : " + www.error); 
		} else {
			//Debug.Log ("queryNbCells="+www.downloadHandler.text);
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N !=  null) {
				Dictionary<int,int> NbCells = new Dictionary<int,int> ();
				foreach (var key in N.Keys) NbCells [int.Parse (key)] = int.Parse (N [key]);
				foreach (var gn in Genes) {
					Gene g = gn.Value;
					g.setNbCells (NbCells [g.id]);
				}
				NbCells.Clear ();
				NbCells = null;
			}
		}
	}


	public static Vector2 getHPFBoundaries(string stage){
		stage = stage.Replace ("Stage ", "");
		//if(!EmbryosList.Dvpts.ContainsKey(2))StartCoroutine(EmbryosList.downloadDevelopmentalTable(2,
		if(!EmbryosList.Dvpts.ContainsKey(2))  return new Vector2 (0f, 20f);
		List<EmbryosList.Dvpt> dvptL=EmbryosList.Dvpts[2];
		int i = 0;
		foreach (EmbryosList.Dvpt dvpt in dvptL) {
			//Debug.Log ("Compare  :" + stage+"=="+dvpt.stage+":");
			if (dvpt.stage == stage) {
				//Debug.Log ("Found  :" +dvpt.stage);
                float start_HPF = 0; float.TryParse (dvpt.hpf,out start_HPF);
				float end_HPF = start_HPF+10f;
				if (i < dvptL.Count) {
					EmbryosList.Dvpt dvptAfter = dvptL [i + 1];
					//Debug.Log ("dvptAfter  :" +dvptAfter.stage+ " at "+dvptAfter.hpf);
                    float.TryParse (dvptAfter.hpf,out end_HPF);
				}
				return new Vector2 (start_HPF, end_HPF);

			}
			i += 1;
		}
		Debug.Log ("Didn't find this stage " + stage);
		return new Vector2 (0f, 20f);
	}


	//Convert A10.34* to A10.00034* 
	public static string convertCellName(string cell_name){
		string[] celspl = cell_name.ToLower().Trim().Split ('.');
		if (celspl.Count() != 2) return "";
		string ab = celspl [0];
		string nb = celspl [1];
		if (ab[0] !='a' && ab[0] != 'b') return "";
		bool start = false; // Is there a star at the end (for left cells)
		if(nb[nb.Length-1]=='*'){ nb=nb.Substring(0,nb.Length-1);start=true;}
		int nbv = 0;
		if (! int.TryParse(nb,out nbv)) return "";
		cell_name = ab + '.' + nbv.ToString ("0000"); //Get the last par of the cell name
		if(start)  return cell_name+'*';
		return cell_name+'_';
	}
	//Invser convert A10.00034*  to A10.34*
	public static string invConvertCellName(string cell_name){
		string[] celspl = cell_name.ToLower().Trim().Split ('.');
		if (celspl.Count() != 2) return "";
		string ab = celspl [0];
		string nb = celspl [1];
		if (ab[0] !='a' && ab[0] != 'b') return "";
		bool start = false; // Is there a star at the end (for left cells)
		while(nb[0]=='0') nb=nb.Substring(1); //Remove the 0
		if(nb[nb.Length-1]=='*'){ nb=nb.Substring(0,nb.Length-1);start=true;}
		if(nb[nb.Length-1]=='_'){ nb=nb.Substring(0,nb.Length-1);}
		cell_name = ab + '.' + nb; //Get the last par of the cell name
		if(start)  return cell_name+'*';
		return cell_name;
	}

	//OPEN GENE CARD
    public static void openLinkListen(Button b, Gene g) { b.onClick.AddListener(() => Aniseed.openLink(g)); }
    public static void openLink(Gene g) { g.openLink(); }





	//FILTER GENES ON SELECTED CELL
	public static void onshowSelectedCells(){MenuGenetic.GetComponent<Aniseed>().showSelectedCells();}
	public  void showSelectedCells(){
		cancel ();
		if(SelectionCell.clickedCells!=null){
			if (Instantiation.id_type == 2 || Instantiation.id_type == 3) {
				if (SelectionCell.selectedCell != null)
					StartCoroutine (querySelectedCell (SelectionCell.selectedCell));
			}
			else{ //We Check for all genes if they contains the cells
				int nbView = 0;
				if (isAnnotate) { //ANNOTATION MODE
					foreach (var gn in Genes) {
						Gene g = gn.Value;
						g.select (nbView);
						nbView += 1;
					}
					checkValues ();
				} else {  //NORMALS MODE 
					foreach (var gn in Genes) {
						Gene g = gn.Value;
						if (g.Cells != null) {
							bool isIn = false;
							foreach (Cell cc in SelectionCell.clickedCells)
								if (!isIn && g.Cells.ContainsKey (cc))
									isIn = true;
							if (isIn) {
								g.select (nbView);
								nbView += 1;
								if (g.SubGene.activeSelf)
									nbView += 1;
							} else
								g.unSelect ();
						}
					}
				}
				updateScrol (nbView);
			}
	  }
	}

	public static IEnumerator querySelectedCell(Cell c){
		string cn = c.getInfos (idxInfoxName);
		string stage = "Stage " + DevelopmentTable.convertTimeToStage (Instantiation.id_type, Instantiation.spf, Instantiation.dt, c.t);
		//Debug.Log ("Show on " + c.ID + " -> " + cn + "==" + invConvertCellName (cn) + " at " + stage);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("getGenes", invConvertCellName (cn));
		form.AddField ("organism_id", organism_id.ToString ());
		form.AddField ("stage", stage);
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlAniseed, form);
		yield return www.Send ();
		if (www.isHttpError || www.isNetworkError) {
			Debug.Log ("Error : " + www.error); 
		} else {
			//Debug.Log ("For Cell " + cn + " -> found GENES " + www.downloadHandler.text);
			if (www.downloadHandler.text != "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				Dictionary<int,int> ShowGenes = new Dictionary<int,int> ();
				if (N != null)
					for (int i = 0; i < N.Count; i++) {
						string ns = N [i].ToString ().Trim ();
						int gene_id = int.Parse (ns.Substring (3, ns.Length - 6).Trim ());
						//Debug.Log (i + "->" + N [i].ToString () + " -> " + ns.Substring (3, ns.Length - 6) + "->" + gene_id);
						ShowGenes [gene_id] = 1;
					}

				int nbView = 0;
				foreach (var gn in Genes) {
					Gene g = gn.Value;
					if (ShowGenes.ContainsKey (g.id)) {
						g.select (nbView);
						nbView += 1;
						if(g.SubGene.activeSelf) nbView += 1;
					} else
						g.unSelect ();
				}
				updateScrol (nbView);
			}
			www.Dispose (); 

		}
	}

	public static bool isAnnotate = false;
	public void massiveCurration(){
		isAnnotate = !isAnnotate;
		if (isAnnotate) {//In Curration Mode
			int nbView = 0;
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				g.go.transform.Find ("deploy").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
				g.go.transform.Find ("selection").gameObject.SetActive (false);
				g.go.transform.Find ("annotation").gameObject.SetActive (true);
				g.select (nbView);
				nbView += 1;
				g.SubGene.SetActive (false);
			}
			updateScrol (nbView);
			checkValues ();

		} else {
			int nbView = 0;
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				Aniseed.showSubGeneListen (g.go.transform.Find ("deploy").gameObject.GetComponent<Button> (), g);
				g.go.transform.Find ("annotation").gameObject.SetActive (false);
				g.go.transform.Find ("selection").gameObject.SetActive (true);
				if (g.isSelected)
					nbView += 1;
			}
			updateScrol (nbView);

		
		}
	}
	//Check the last values on all genes (for select cells)
	public void checkValues(){
		if (SelectionCell.clickedCells == null || SelectionCell.clickedCells.Count == 0) { //NO CELLS SELECTED
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				GameObject annotation = g.go.transform.Find ("annotation").gameObject;
				annotation.transform.Find ("nbTrue").gameObject.GetComponent<Text> ().text = "0";
				annotation.transform.Find ("annotate").gameObject.SetActive (false);
			}
		} else {
			foreach (var gn in Genes) {
				Gene g = gn.Value;
				GameObject annotation = g.go.transform.Find ("annotation").gameObject;
				int nbOn = 0; int nbTot = SelectionCell.clickedCells.Count;
				foreach( Cell cc in SelectionCell.clickedCells)
					if(g.Cells!=null && g.Cells.ContainsKey(cc)) nbOn++;
				annotation.transform.Find ("nbTrue").gameObject.GetComponent<Text> ().text = ""+nbOn.ToString()+"/"+nbTot.ToString();
				GameObject annotate = annotation.transform.Find ("annotate").gameObject;
				annotate.SetActive (true);
				Toggle annotateT = annotate.GetComponent<Toggle> ();
				annotateT.onValueChanged.RemoveAllListeners ();
				if (nbOn == nbTot) annotateT.isOn = true;
				else  annotateT.isOn = false;
				Aniseed.annotateListen (annotateT, g);

			}

		}
	}


}
