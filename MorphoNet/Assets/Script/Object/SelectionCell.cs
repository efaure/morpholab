﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using AssemblyCSharp;

public class SelectionCell : MonoBehaviour {

	public static Cell selectedCell; //The last selected Cell
	public static List<Cell> clickedCells; //List of cells currently selected

	public Toggle allTimes;

	void Start () {
		clickedCells=new List<Cell>(); 
		selectedCell = null;
	}

	//Clear The List of selected Cells
	public static void clearSelectedCells(){
		if (clickedCells != null) {
			if (Lineage.isLineage) Lineage.clearSelectedCells ();
			foreach(Cell c in clickedCells) c.choose(false);
			clickedCells.Clear ();
		}
	}
	//Clear the click cell
	public static void clearClickedCell(){
		if (selectedCell != null) { selectedCell = null; }
		MenuCell.describe (); 
	}


	//Follow selected Cells
	public static void followSelectedCells(int t){
		//Debug.Log ("followSelectedCells to " + t);
		if (clickedCells != null) {
			List<Cell> NewClickedCells=new List<Cell>(); 
			foreach(Cell c in clickedCells) { 
				//Debug.Log ("followSelected  " + c.ID.ToString ());
				c.choose(false);
				List<Cell> Parents=c.GetParentAt (t);
				if(Parents!=null)
					for (int k = 0; k < Parents.Count; k++) {
						//Debug.Log ("Add " + Parents [k].ID.ToString ()+" at " +Parents [k].t);
						if (!NewClickedCells.Contains (Parents [k])) {
							//Debug.Log ("ok to Add " + Parents [k].ID.ToString ());
							Parents [k].choose (true);
							NewClickedCells.Add (Parents [k]);
						}
					}
			}
			if (Lineage.isLineage) Lineage.clearSelectedCells ();
			clickedCells.Clear ();
			clickedCells = NewClickedCells;
			if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		}

	}

	//Remove a selected cell
	public static void RemoveCellSelected(Cell c,bool updateDescribe=true){
		if (Lineage.isLineage) Lineage.RemoveCellSelected (c);
		c.choose(false);
		if (clickedCells != null && clickedCells.Contains (c))
			clickedCells.Remove (c);
		if(updateDescribe)  MenuCell.describe(); //Update the description menu
		if(Aniseed.MenuGenetic!=null && Aniseed.MenuGenetic.activeSelf  && Aniseed.isAnnotate)Aniseed.onshowSelectedCells();
	}

	//Add a cell in the selection
	public static void AddCellSelected(Cell c,bool updateDescribe=true){
		if (Lineage.isLineage) Lineage.AddCellSelected (c);
		c.choose(true);
		if (clickedCells != null && !clickedCells.Contains (c))
			clickedCells.Add (c);
		if(updateDescribe) MenuCell.describe(); //Update the description menu
		if(Aniseed.MenuGenetic!=null && Aniseed.MenuGenetic.activeSelf  && Aniseed.isAnnotate)Aniseed.onshowSelectedCells();


	}


	//Return the selection number from the selected list if exist;
	public static int getSelection(){
		if (clickedCells.Count > 0) 
			if( clickedCells [0].selection!=null && clickedCells [0].selection.Count>0)
				return clickedCells [0].selection[0];
		return 0;

	}
	//Clear All selected cells
	public  void resetSelection(){
		if (Lineage.isLineage) Lineage.clearSelectedCells ();
		clearSelectedCells ();
		clearClickedCell ();
	} 
	//Select the others cells none selected
	public  void inverseSelection(){
		//2 WAYS TO DO IT FROM THE LIST OR FROM THE CELL STATUS ....
		/*List<Cell>  NewClickedCells=new List<Cell>();
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if (!clickedCells.Contains (c))
				NewClickedCells.Add (c);
			c.choose (!c.selected);
		}
		clickedCells.Clear ();
		clickedCells = NewClickedCells;*/
		if (Lineage.isLineage) Lineage.clearSelectedCells ();
		clickedCells.Clear ();
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if (c.show) {
				if (!c.selected)
					clickedCells.Add (c);
				c.choose (!c.selected);
			}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe (); 
	}

	//Add the neigbhors of the clicked cell in the list of selectd cells
	public  void showNeigbhors(){ 
		if (Lineage.isLineage) Lineage.clearSelectedCells ();
		List<Cell> temp_clickedCells = new List<Cell> ();
		for (int i = 0; i < clickedCells.Count; i++)
			temp_clickedCells.Add (clickedCells [i]);
		
		for (int i = 0; i < temp_clickedCells.Count; i++) {
			//Debug.Log ("CEll " + temp_clickedCells [i].ID);
			if(temp_clickedCells [i].Neigbhors!=null)
				foreach (Cell n in temp_clickedCells[i].Neigbhors) {
					n.choose(true);
					//Check if the cell were not previoulsy added
					bool isInside = false;
					for (int ix = 0; ix < clickedCells.Count; ix++)
						if (clickedCells [ix] == n)
							isInside = true;
					if (!isInside) clickedCells.Add (n);
				}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe(); //Update the description menu
	} 
	//Hide the selected cells
	public  void hideClickedCell(){
		if (clickedCells != null) {
			if (Lineage.isLineage) Lineage.hideSelectedCells (allTimes.isOn);
			//We Hide 
			if (allTimes.isOn) { //ALL TIMES
				for (int i = 0; i < clickedCells.Count; i++)
					clickedCells [i].hideAll (true);
			} else { //ONLY THIS TIME
				for (int i = 0; i < clickedCells.Count; i++)
					clickedCells [i].hide (true);
			}
		}
		clearSelectedCells ();
		clearClickedCell ();
		MenuCell.describe ();
	} 

	//Show all cells 
	public  void showAllCell(){
		if (Lineage.isLineage) Lineage.showSelectedCells (allTimes.isOn);
		if (allTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				showAllAt(t);
		else showAllAt(Instantiation.CurrentTime);
	} 
	//Unhide all cell
	public void showAllAt(int t){
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
		foreach (var itemc in CellAtT) { 
			Cell c = itemc.Value; 
			c.hide(false);
		}
	}
    //Select multiple Cell inside box area
    public void selectObjectsInArea(){
        RaycastHit hitInfo = new RaycastHit();
        clearSelectedCells();
        int shift = 5;
        for (int x = (int)Mathf.Round((Mathf.Min(firstPoint.x, secondPoint.x))); x <= (int)Mathf.Round(Mathf.Max(firstPoint.x, secondPoint.x)); x += shift)
            for (int y = (int)Mathf.Round((Mathf.Min(firstPoint.y, secondPoint.y))); y <= (int)Mathf.Round(Mathf.Max(firstPoint.y, secondPoint.y)); y += shift)
            {
                bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(x, y, 0f)), out hitInfo);
                if (hiting)
                {
                    string clickedname = hitInfo.transform.gameObject.name;
                    if (clickedname.Contains("_MeshPart")) clickedname = hitInfo.transform.parent.gameObject.name;
                    Cell c = Instantiation.getCell(clickedname, false);
                    if (c != null) AddCellSelected(c);
                }
            }

    }
    //To Select raycast on an area
    Vector3 firstPoint = Vector3.zero;
    Vector3 secondPoint = Vector3.zero;
    RectTransform BoxSelectionArea = null;
	void Update () {
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetMouseButtonUp(0) && firstPoint != Vector3.zero)  { //On mouse Up reinit box selection area
                Instantiation.canvas.gameObject.transform.Find("BoxSelectionArea").gameObject.SetActive(false);
                //if (Vector3.Distance(firstPoint, secondPoint) > 1) selectObjectsInArea();//NOT NECESSARY DO IT LIVE NOW
                firstPoint = Vector3.zero;
            }
            if (Input.GetMouseButton(0) && firstPoint != Vector3.zero)  { //Mouse down and a first click were selected
                secondPoint = Input.mousePosition;
                // Debug.Log("secondPoint=" + secondPoint);
                if (Vector3.Distance(firstPoint, secondPoint) > 1)
                {
                    if (BoxSelectionArea == null) BoxSelectionArea = Instantiation.canvas.gameObject.transform.Find("BoxSelectionArea").gameObject.transform.GetComponent<RectTransform>();
                    float boxX = firstPoint.x;
                    if (firstPoint.x > secondPoint.x) boxX = secondPoint.x;
                    float boxW = Mathf.Abs(secondPoint.x - firstPoint.x);
                    float boxY = firstPoint.y;
                    if (firstPoint.y > secondPoint.y) boxY = secondPoint.y;
                    float boxH = Mathf.Abs(secondPoint.y - firstPoint.y);
                    BoxSelectionArea.position = new Vector3((boxX - Instantiation.canvasWidth / 2f) * Instantiation.canvasScale, (boxY - Instantiation.canvasHeight / 2f) * Instantiation.canvasScale, -100f);
                    BoxSelectionArea.sizeDelta = new Vector2(boxW, boxH);
                    Instantiation.canvas.gameObject.transform.Find("BoxSelectionArea").gameObject.SetActive(true);
                    if (Vector3.Distance(firstPoint, secondPoint) > 1) selectObjectsInArea();//SELECT OBJECTS
                }
            }
        }


		//When String Visualisation is activate we have to check on where we are
		if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ()) {
            
			///////// ON CLICK ON A CELL
			#if UNITY_ANDROID
			if (Input.touchCount==1 && !ArcBall.moving && !ArcBall.zoomning && !ArcBall.dragging){
				Touch touch=Input.touches[0];
					if (touch.phase == TouchPhase.Ended) {
						RaycastHit hitInfo = new RaycastHit ();
						bool hiting = Physics.Raycast (Camera.main.ScreenPointToRay (touch.position), out hitInfo);

					if (hiting) {
						string clickedname = hitInfo.transform.gameObject.name;
						//Debug.Log (clickedname+" at " +(Screen.height-Input.mousePosition.y));
						if (clickedname != "Background"){
							if (clickedname.Contains ("_MeshPart")) clickedname = hitInfo.transform.parent.gameObject.name;
							//Debug.Log ("Click ="+clickedname);
							Cell c = Instantiation.getCell (clickedname,false);
							if(c !=null){
								selectedCell = c;
								//Test if we click on a already clicked cell
								if (selectedCell.selected) { //Was Previously selected
									selectedCell.choose (false);
									clickedCells.Remove (selectedCell);
									MenuCell.describe ();
								} else {
									if (!Input.GetKey (KeyCode.LeftShift))
										clearSelectedCells ();  // WE Selected only this one and remove the previous one if necessary
									AddCellSelected (selectedCell);
								}
							}
						}
					}
				}
			}
           
			#else
			
            if (Input.GetMouseButtonDown (0)) { //On Click down
				RaycastHit hitInfo = new RaycastHit ();
                bool hiting = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);

				if (hiting) {
					string clickedname = hitInfo.transform.gameObject.name;
					//Debug.Log (clickedname+" at " +(Screen.height-Input.mousePosition.y));
					if (clickedname == "Background" && Screen.height-Input.mousePosition.y>30 && Input.mousePosition.x>30 ) { //We click outside
						clearSelectedCells ();
						clearClickedCell ();
						//Debug.Log("outside");
					} else { 
						if (clickedname.Contains ("_MeshPart")) clickedname = hitInfo.transform.parent.gameObject.name;

						if (!Input.GetKey (KeyCode.LeftShift)) //SHIFT KEY IS MAINTING
							clearSelectedCells ();  // WE Selected only this one and remove the previous one if necessary
						
						Cell c = Instantiation.getCell (clickedname,false);
						if(c !=null){
							//Debug.Log("Found cell "+c.ID);
							if(c.selected){ //Already selected
								//Debug.Log("is selected cell "+c.ID);
								RemoveCellSelected(c);
								clearClickedCell();
								selectedCell = null;
							}else{
								//Debug.Log("selected cell "+c.ID);
								AddCellSelected (c);
								selectedCell = c;
							}
						}
					}
			    }
                if (BoxSelectionArea == null) BoxSelectionArea = Instantiation.canvas.gameObject.transform.Find("BoxSelectionArea").gameObject.transform.GetComponent<RectTransform>();
                firstPoint = Input.mousePosition;
                if (Input.GetKey(KeyCode.LeftShift)) firstPoint = Vector3.zero;
			}

			#endif

			if (Input.GetKey (KeyCode.X)) { //X is pressed we select all cells on over
				RaycastHit hitInfo = new RaycastHit ();
				bool hiting = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
				if (hiting && hitInfo.transform.gameObject.name != "Background") {
					string clickedname = hitInfo.transform.gameObject.name;
					Cell c = Instantiation.getCell (hitInfo.transform.gameObject,false);
					if (c != null) {
						AddCellSelected (c);
						selectedCell = c;
					}
				}
			}


			/////// TO DRAW STRING INFOS ON CELLS
			if (Instantiation.goShowString != null) { //Wait for initialisation
				if (MenuCell.NbStringOn > 0) {
					//Debug.Log ("show tring " + MenuCell.NbStringOn);
					Instantiation.goShowString.transform.GetComponent<Renderer> ().enabled = true;
					RaycastHit hitInfo = new RaycastHit ();
					bool hiting = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
					if (hiting) {
						//Debug.Log (hitInfo.transform.gameObject.name);
						if (hitInfo.transform.gameObject.name != "Background") { 
							string clickedname = hitInfo.transform.gameObject.name;
							//Debug.Log ("On  " + clickedname);
							Cell c = Instantiation.getCell (hitInfo.transform.gameObject,false);
							if (c != null) {
								Vector3 screenPoint = new Vector3 (Input.mousePosition.x + 20, Input.mousePosition.y + 20, 10f);
								Instantiation.goShowString.transform.position = Camera.main.ScreenToWorldPoint (screenPoint);
								string stringToShow = c.getShowString ();
								if(stringToShow!= Instantiation.goShowString.GetComponent<TextMesh> ().text)
									Instantiation.goShowString.GetComponent<TextMesh> ().text = stringToShow;
							} else if( Instantiation.goShowString.GetComponent<TextMesh> ().text!="")  Instantiation.goShowString.GetComponent<TextMesh> ().text = "";
						} else if( Instantiation.goShowString.GetComponent<TextMesh> ().text!="")  Instantiation.goShowString.GetComponent<TextMesh> ().text = "";
					} else if( Instantiation.goShowString.GetComponent<TextMesh> ().text!="")  Instantiation.goShowString.GetComponent<TextMesh> ().text = "";
				} else Instantiation.goShowString.transform.GetComponent<Renderer> ().enabled = false;
			}//else Debug.Log("MenuCell.NbStringOn="+MenuCell.NbStringOn + " CanvasScript.NbStringOn="+CanvasScript.NbStringOn );
		} 

	}

 


}