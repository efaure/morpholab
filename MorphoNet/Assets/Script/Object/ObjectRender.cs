﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using AssemblyCSharp;
using System.Collections.Generic;




[ExecuteInEditMode]
public class ObjectRender : MonoBehaviour {

	public Channel ch;  //Associated channel 

	private void setRendererEnabled(bool v){
		if (GetComponent<Renderer> () != null) {
			if(GetComponent<Renderer> ().enabled!=v) GetComponent<Renderer> ().enabled = v;
		}
		//else { //Child ...
			Transform trns = transform;
			for (int c = 0; c < trns.childCount; c++) trns.GetChild (c).gameObject.GetComponent<Renderer> ().enabled = v;
		//}
	}
	private void setMeshColliderEnabled(bool v){
		if (GetComponent<MeshCollider> () != null) {
			if(GetComponent<MeshCollider> ().enabled!=v) GetComponent<MeshCollider> ().enabled = v;
		}
		//else { //Child ...
			Transform trns = transform;
			for (int c = 0; c < trns.childCount; c++) trns.GetChild (c).gameObject.GetComponent<MeshCollider> ().enabled = v;
		//}
	}
	private void setRendererMaterial(Material m){
		if (GetComponent<Renderer> () != null) 	GetComponent<Renderer> ().material = m;
		//else { //Child ... (could be sub mesh, or additional component such as sphere etc..)
			Transform trns = transform;
			for (int c = 0; c < trns.childCount; c++) trns.GetChild (c).gameObject.GetComponent<Renderer> ().material = m;
		//}
	}
	public void setRendererMaterialColor(Color col){
		if (GetComponent<Renderer> () != null)
			GetComponent<Renderer> ().material.color =col;
		//else { //Child ...
			Transform trns = transform;
			for (int c = 0; c < trns.childCount; c++) trns.GetChild (c).gameObject.GetComponent<Renderer> ().material.color =col;
		//}
	}


	public bool previouslyActivate=true;
	public void activate(bool v){
		if (v != previouslyActivate) { //TO REDUCE TIME ....
			//Debug.Log ("activate " + this.ch.cell.ID+ " at "+this.ch.cell.t+" ->"+this.ch.cell.show);
			previouslyActivate = v;
			if (v) { //VISIBLE 
				if (this.ch.cho.mesh) {
					setRendererEnabled (v);
					setMeshColliderEnabled (v);
				} else {
					setRendererEnabled (!v);
					setMeshColliderEnabled (!v);
				}
		
				//NUCLEI
				if (this.ch.cho.center) {
					if (this.ch.center == null)
						this.ch.createCenter ();
					this.ch.center.GetComponent<Renderer> ().enabled = v;
					this.ch.center.GetComponent<SphereCollider> ().enabled = v;
				} else {
					if (this.ch.center != null) {
						this.ch.center.GetComponent<Renderer> ().enabled = !v;
						this.ch.center.GetComponent<SphereCollider> ().enabled = !v;
					}
				}

				//TRACKING
				if (this.ch.cho.tracking) {
					if (this.ch.vector != null) {
						for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++) {
							GameObject nvd = this.ch.vector.transform.GetChild (iy).gameObject;
							nvd.GetComponent<LineRenderer> ().enabled = v;
						}
					}
				} else {
					if (this.ch.vector != null) {
						for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++) {
							GameObject nvd = this.ch.vector.transform.GetChild (iy).gameObject;
							nvd.GetComponent<LineRenderer> ().enabled = !v;
						}
					}

				}
	
				//CELL NAME
				//TODO
			

			} else { //HIDDING
				//OBJECT
				setRendererEnabled (v);
				setMeshColliderEnabled (v);

				//NUCLEI
				if (this.ch.center != null) {
					this.ch.center.GetComponent<Renderer> ().enabled = v;
					this.ch.center.GetComponent<SphereCollider> ().enabled = v;
				}
				//TRACKING
				if (this.ch.vector != null) {
					for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++) {
						GameObject nvd = this.ch.vector.transform.GetChild (iy).gameObject;
						nvd.GetComponent<LineRenderer> ().enabled = v;
					}
				}
			}
		}
	}


	public void destroyPreviousMaterial(){
		if (GetComponent<Renderer> () != null) {
			if (GetComponent<Renderer> ().material != null)
				DestroyImmediate (GetComponent<Renderer> ().material, true);
		}
		else { //Child ...
			Transform trns = transform;
			for (int c = 0; c < trns.childCount; c++) {
				GameObject goc = trns.GetChild (c).gameObject;
				if (goc.GetComponent<Renderer> ().material != null)
					DestroyImmediate (goc.GetComponent<Renderer> ().material,true);
			}
		}
		if (this.ch.center != null) DestroyImmediate (this.ch.center.GetComponent<Renderer> ().material,true);
		if (this.ch.vector != null) {
			for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++)
				DestroyImmediate (this.ch.vector.transform.GetChild (iy).gameObject.GetComponent<LineRenderer> ().material);
		}
	}

	public void assignMaterial(Material m){
		setRendererMaterial (m);
		if (this.ch.center != null) this.ch.center.GetComponent<Renderer> ().sharedMaterial = m;
		if (this.ch.vector != null) {
			for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++)
				this.ch.vector.transform.GetChild (iy).gameObject.GetComponent<LineRenderer> ().sharedMaterial = m;
		}
	}
		

	public void assignColor(Color c){
		setRendererMaterialColor (c);
		if (this.ch.center != null) this.ch.center.GetComponent<Renderer> ().material.color =c;
		if (this.ch.vector != null) {
			for (int iy = 0; iy < this.ch.vector.transform.childCount; iy++)
				this.ch.vector.transform.GetChild (iy).gameObject.GetComponent<LineRenderer> ().material.color =c;
		}
	}


	//Ideally the planes do not need to be updated every frame, but we'll just keep the logic here for simplicity purposes.
	public void Update()
	{

	
		//Debug.Log ("Upodate " + this.ch.cell.ID+ " at "+this.ch.cell.t+" ->"+this.ch.cell.show);
		if (!ArcBall.fixedplan.isOn && (Instantiation.XMin > this.ch.Gravity [0] || Instantiation.XMax < this.ch.Gravity [0] || Instantiation.YMin > this.ch.Gravity [1] || Instantiation.YMax < this.ch.Gravity [1] || Instantiation.ZMin > this.ch.Gravity [2] || Instantiation.ZMax < this.ch.Gravity [2])) {
			activate (false);
		} else if (ArcBall.fixedplan.isOn && ArcBall.outOfPlan(this.ch.Gravity)) {
			activate (false);
		}
		else {
			if (!this.ch.cell.show)
				activate (false);
			else{
				activate (true);
			    if (this.ch.updateShader) {
					
					bool updateShader = true;
					//Debug.Log ("Update " + this.ch.cell.ID + " at " + this.ch.cell.t+" ->" +this.ch.cell.selected.ToString()+ " -> " + this.ch.cell.selection.Count);

					destroyPreviousMaterial ();

					if (this.ch.cell.selected)
						assignMaterial (Instantiation.Selected);
					else if (this.ch.cell.UploadedColor)
						assignColor (this.ch.cell.UploadColor);
					else if (this.ch.cell.selection != null) {
						if (this.ch.cell.selection.Count == 1) { //Only One  Material
							//Debug.Log ("Assign Material " + this.ch.cell.ID + " at " + this.ch.cell.t+ "->"+this.ch.cell.selection [0]);
							assignMaterial (SelectionColor.getSelectedMaterial (this.ch.cell.selection [0]));
						} else if (this.ch.cell.selection.Count > 1) { //Multiples  Material
							int s = (int)Mathf.Round (this.ch.cell.currentIxSelect / Instantiation.timeChangeShader);
							if(this.ch.cell.selection.Count>s) assignMaterial (SelectionColor.getSelectedMaterial (this.ch.cell.selection [s]));
							this.ch.cell.currentIxSelect += 1;
							if (this.ch.cell.currentIxSelect >= this.ch.cell.selection.Count * Instantiation.timeChangeShader)
								this.ch.cell.currentIxSelect = 0;
							updateShader = false;
						} else
							assignMaterial (Instantiation.Default);
					} else {
                        //Debug.Log("Assign Material at " +this.ch.cell.ID);
						assignMaterial (Instantiation.Default);
					}
					if(updateShader) this.ch.updateShader = false;
					
				}
			}
		}
	}
}

