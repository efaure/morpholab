﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
//using AssemblyCSharp;


public class ChangeColorSelection : MonoBehaviour {


	public  int R=0;
	public  Slider SliderR;
	public  InputField InputFieldR;

	public  int G=0;
	public  Slider SliderG;
	public  InputField InputFieldG;

	public  int B=0;
	public  Slider SliderB;
	public  InputField InputFieldB;

	public  int A=255;
	public  Slider SliderA;
	public  InputField InputFieldA;

	public  GameObject NewColor;


	public  Button ValidNewColor;
	public  Button CancelNewColor;

	public GameObject MenuColor;
	public GameObject Selection;

	public Dropdown ShadersSelection;

	void Start() {
		//chooseMaterial = Instantiation.Default;
	}

	public void InitColor(Material m){ //Initialise the color set on menu opening
		replaceMaterial(m);
		chooseShaders (ShadersSelection.value);
		Color c=m.color;
		changeR((int)Mathf.Round(c.r*255f));
		changeG((int)Mathf.Round(c.g*255f));
		changeB((int)Mathf.Round(c.b*255f));
		changeA((int)Mathf.Round(c.a*255f));
		UpdateColor();
	}

	public void chooseShaders(int v){
		Material chooseMaterial = null;
		switch (v) {
			case 0:  chooseMaterial = new Material(Instantiation.Default); break;
			case 1:  chooseMaterial = new Material(Instantiation.Diamond); break;
			case 2:  chooseMaterial = new Material(Instantiation.Dark); break;
			case 3:  chooseMaterial = new Material(Instantiation.Vertex); break;
			case 4:  chooseMaterial = new Material(Instantiation.Bumped); break;
			case 5:  chooseMaterial = new Material(Instantiation.Brick); break;
			case 6:  chooseMaterial = new Material(Instantiation.Stars); break;
            case 7: chooseMaterial = new Material(Instantiation.Wireframe); break; 
		}
		NewColor.GetComponent<Renderer> ().material = chooseMaterial;

		UpdateColor();
	}
	//Select the material in the dropdown button
	public void replaceMaterial(Material m){
		if (m.name.IndexOf ("StandardCell") == 0)
			ShadersSelection.value = 0;
		else if (m.name.IndexOf ("WhiteGem") == 0)
			ShadersSelection.value = 1;
		else if (m.name.IndexOf ("Rough Textured Metal") == 0)
			ShadersSelection.value = 2;
		else if (m.name.IndexOf ("Robot MatCap Plain Additive") == 0)
			ShadersSelection.value = 3;
		else if (m.name.IndexOf ("Bubble wrap pattern") == 0)
			ShadersSelection.value = 4;
		else if (m.name.IndexOf ("Lavabrick") == 0)
			ShadersSelection.value = 5;
        else if (m.name.IndexOf("Stars") == 0)
            ShadersSelection.value = 6;
        else if (m.name.IndexOf("Wireframe - ShadedUnlit") == 0)
            ShadersSelection.value = 7;
	}

	public static Material getMaterialByName(string name){
		Material chooseMaterial = null;
		if (name.IndexOf ("StandardCell") == 0)
			chooseMaterial = new Material(Instantiation.Default);
		else if (name.IndexOf ("WhiteGem") == 0)
			chooseMaterial = new Material(Instantiation.Diamond);
		else if (name.IndexOf ("Rough Textured Metal") == 0)
			chooseMaterial = new Material(Instantiation.Dark);
		else if (name.IndexOf ("Robot MatCap Plain Additive") == 0)
			chooseMaterial = new Material(Instantiation.Vertex);
		else if (name.IndexOf ("Bubble wrap pattern") == 0)
			chooseMaterial = new Material(Instantiation.Bumped);
		else if (name.IndexOf ("Lavabrick") == 0)
			chooseMaterial = new Material(Instantiation.Brick);
		else if (name.IndexOf ("Stars") == 0)
			chooseMaterial = new Material(Instantiation.Stars);
        else if (name.IndexOf("Wireframe - ShadedUnlit") == 0)
            chooseMaterial = new Material(Instantiation.Wireframe);
		return chooseMaterial;

	}

	public  void UpdateColor(){
		NewColor.GetComponent<Renderer> ().material.color = new Color((float)R/255f,(float)G/255f,(float)B/255f,(float)A/255f);
	}

	public void changeR(int v){
		if (v < 0) v = 0;
		if (v > 255) v = 255;
		R=v;
		SliderR.value = (float)v;
		InputFieldR.text=v.ToString();

	}
	public void changeB(int v){
		if (v < 0) v = 0;
		if (v > 255) v = 255;
		B=v;
		SliderB.value = (float)v;
		InputFieldB.text=v.ToString();
	}
	public void changeG(int v){
		if (v < 0) v = 0;
		if (v > 255) v = 255;
		G=v;
		SliderG.value = (float)v;
		InputFieldG.text=v.ToString();
	}
	public void changeA(int v){
		if (v < 0) v = 0;
		if (v > 255) v = 255;
		A=v;
		SliderA.value = (float)v;
		InputFieldA.text=v.ToString();
	}

	//When Input field changed
	public  void changeFieldR(){
        int v = 0;
        int.TryParse(InputFieldR.text, out v);
		changeR(v);
		UpdateColor();
	}

	public  void changeFieldB(){
        int v = 0;
        int.TryParse(InputFieldB.text, out v);
        changeB(v);
		UpdateColor();
	}

	public  void changeFieldG(){
        int v = 0;
        int.TryParse(InputFieldG.text, out v);
		changeG(v);
		UpdateColor();
	}

	public  void changeFieldA(){
        int v = 0;
        int.TryParse(InputFieldA.text, out v);
		changeA(v);
		UpdateColor();
	}


	//When Slider changed
	public  void changeSliderR(){ 
        int v = (int)Mathf.Round(SliderR.GetComponent<UnityEngine.UI.Slider>().value);
        changeR (v);
		UpdateColor();
	}
	public  void changeSliderB(){ 
		int v=(int)Mathf.Round(SliderB.GetComponent<UnityEngine.UI.Slider> ().value);
		changeB (v);
		UpdateColor();
	}
	public  void changeSliderG(){ 
		int v=(int)Mathf.Round(SliderG.GetComponent<UnityEngine.UI.Slider> ().value);
		changeG (v);
		UpdateColor();
	}
	public  void changeSliderA(){ 
		int v=(int)Mathf.Round(SliderA.GetComponent<UnityEngine.UI.Slider> ().value);
		changeA (v);
		UpdateColor();
	}





	public  void Valid(){
		Selection.GetComponent<SelectionColor> ().changeMaterialSet(NewColor.GetComponent<Renderer> ().material);
		MenuColor.SetActive (false);
	}
	public  void Cancel(){
		MenuColor.SetActive (false);

	}



}
