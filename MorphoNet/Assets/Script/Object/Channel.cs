﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace AssemblyCSharp
{
	public class Channel
	{
		public Cell cell; //Pointer back on the cell
		public ChannelObject cho; //Associated Channel GameObject Menu
		//Main Mesh for this channel
		public GameObject go;
		public bool updateShader; //To kown if we have to update the shader

		//Gravity Center
		public Vector3 Gravity; //GravityCenter
		public GameObject center; //Sphere associate to the gravity position 

		//Tracking
		public GameObject vector; //For tracking

		public Channel (GameObject go,Cell cell,ChannelObject cho)
		{
			this.cell = cell;
			this.go = go;
			this.cho = cho;
			this.updateShader = true;


			this.calculGravityCenter ();
			try{
				if (go.transform.childCount == 0) {//Default the Mesh is only one boject attached to the cell GameObject
					if (go.GetComponent<MeshCollider> () == null) 	go.AddComponent<MeshCollider> ();
				}else { //Multiples Mesh are attachaed to childs of this GameObject
					for (int ic = 0; ic < go.transform.childCount; ic++) {
						GameObject co = go.transform.GetChild (ic).gameObject;
						if(co.GetComponent<MeshCollider>()==null) co.AddComponent<MeshCollider> ();
					}
				}	
			}catch (Exception ex) {
				Debug.Log ("MeshCollider ERROR "+ ex);
			}
				if (go.GetComponent<ObjectRender> () == null) go.AddComponent<ObjectRender> ();
				if (go.GetComponent<ObjectRender> ().ch == null) go.GetComponent<ObjectRender> ().ch = this;


		}
		//Return the name of the channel (channel 0 etc...)
		public string getName(){ //We could store the name in a field but it whill increase memory...
			if(this.cell.Channels==null) return "";
			foreach (KeyValuePair<string, Channel> item in this.cell.Channels)
				if (item.Value == this)
					return item.Key;
			return "";
		}

		public string getTuple(){
			return "" + this.cell.t + "," + this.cell.ID + "," + this.getName ();
		}
		/////////////////  CENTER
		public void calculGravityCenter(){
			this.Gravity = new Vector3 (0, 0, 0);
			if (this.go.GetComponent<MeshFilter> () != null) {
				Vector3[] objectVerts = this.go.GetComponent<MeshFilter> ().mesh.vertices;
				for (int i = 0; i < objectVerts.Length; i++)
					for (int j = 0; j < 3; j++)
						Gravity [j] += objectVerts [i] [j];
				for (int j = 0; j < 3; j++)
					Gravity [j] /= objectVerts.Length;
			} else { //Multiples subb Ojbect with different filter are associaed
				//Debug.Log("Calcul Gravity for "+go.name);
				Transform trns = this.go.transform;
				for (int c = 0; c < trns.childCount; c++) {
					GameObject to = trns.GetChild (c).gameObject;
					//Debug.Log("Add Gravity for "+to.name);
					int nbG = 0;
					if (to.GetComponent<MeshFilter> () != null) {
						Vector3[] objectVerts = to.GetComponent<MeshFilter> ().mesh.vertices;
						for (int i = 0; i < objectVerts.Length; i++)
							for (int j = 0; j < 3; j++)
								Gravity [j] += objectVerts [i] [j];
						nbG += objectVerts.Length;
					}
					for (int j = 0; j < 3; j++) Gravity [j] /= nbG;
				}
			}
			Instantiation.updateXYZCutSliders (this.Gravity);
			//if(this.cell.t==0) Debug.Log ("Gravity for "+ this.cell.ID + " at " +this.cell.t+ " is " + Gravity [0] + ":" + Gravity [1] + ":" + Gravity [2]);
		}

		public void createCenter(){
			ArcBall.resetRotation();
			this.center=Instantiation.Instantiate(Instantiation.sphere);
			this.center.name=this.go.name; //Same name as the nmesh (TODO CHECK...)
			this.center.transform.SetParent (this.go.transform);
			this.center.transform.localScale = new Vector3 (this.cho.centerSize,this.cho.centerSize,this.cho.centerSize);
			this.center.transform.position = (this.Gravity - Instantiation.embryoCenter) * Instantiation.initcanvasScale;
			this.center.GetComponent<Renderer> ().sharedMaterial = Instantiation.Default;
			this.updateShader = true;
			ArcBall.updateRotation();
		}

		public void destroyCenter(){
			Instantiation.DestroyImmediate (this.center,true);
		}
		public void resizeCenter(){
			if(this.center!=null) this.center.transform.localScale = new Vector3 (this.cho.centerSize, this.cho.centerSize, this.cho.centerSize);
		}



	}
}

