﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using AssemblyCSharp;
using System;

namespace AssemblyCSharp
{
	public class Cell
	{
		public int t; //Cell Time
		public string ID; //Cell at this time point
		public List<int> selection; //List of Color Selction 
		public int currentIxSelect; //Just indice of the id selection showed (automatique iteration and loop infinite !)

		public List< Cell > Mothers; //Pointer to  Mothers
		public List< Cell > Daughters;//Pointer to  Daughters

		public bool selected; //If cell is selected true

		public bool show = true; //Do we display this cell ? 

		//Cell Description 
		public Dictionary<int,string> Infos; //Map of infos (fate,compacity,volume,surface...)
		public List< Cell > Neigbhors; //List of Neibhors  
		//Curation
		public Dictionary<int, Curation > Curations; //List of Curations for each infos 

	
		//public Color color;
		public Color UploadColor; //Color upload by the user 
		public bool UploadedColor=false; //Color upload by the user 

		public int lifePast;
		public int lifeFutur;

		public Dictionary<string, Channel> Channels; //List of Channels 


		public Cell (int t,string ID) //Constructeur 
		{
			//Debug.Log ("Create Cell " + ID + " at " + t);
			this.t = t;
			this.ID = ID;
			this.currentIxSelect = 0;
			this.selected = false;
			this.lifePast = -1;
			this.lifeFutur = -1;
			this.UploadedColor=false; 
			this.show = true;
		}
		public string getName(){
			string n="";
			if (Instantiation.MaxTime != Instantiation.MinTime)
				n = this.t.ToString () + ",";
			n+= this.ID.ToString ();
			//Channel ch = this.getFirstChannel ();
			//if (ch != null) 	n += "," + ch.getName ();
			return n;
		}

		public void Destroy(){
			if(Neigbhors!=null) Neigbhors.Clear ();Neigbhors = null;
			if(Infos!=null) Infos.Clear ();Infos = null;
		
		}
		public Channel getFirstChannel(){
			if (this.Channels == null) return null;
			foreach (KeyValuePair<string,Channel> ite in this.Channels)
				return ite.Value;
			return null;
		}
		public Channel getChannel(string ch){
			if (this.Channels == null) return null;
			if (this.Channels.ContainsKey (ch))
				return this.Channels [ch];
			return null;
		}
		public Channel addChannel(string ch,GameObject go,ChannelObject cho){
			if(this.Channels==null) this.Channels=new Dictionary<string,Channel>(); 
			if (this.Channels.ContainsKey (ch))
				return this.Channels [ch];
			this.Channels [ch] = new Channel (go,this,cho);
			return this.Channels [ch];
		}
	
		public void clearInfos(int idx){
			if (this.Infos != null && this.Infos.ContainsKey (idx))
				this.Infos.Remove (idx);
		}
		public void setInfos(int idx,string v){
			if(this.Infos==null) this.Infos = new Dictionary<int,string>();
            Infos[idx] = v;
            //if (idx == 885) Debug.Log("Cell " + this.getName() + " -> " + v);
		}


		public void addMother(Cell c){
			if(this.Mothers==null) this.Mothers =new List<Cell>();
			if (!Mothers.Contains (c))
				Mothers.Add (c);
		}
		public void addDaughter(Cell c){
			if(this.Daughters==null) this.Daughters =new List<Cell>();
			if (!Daughters.Contains (c))
				Daughters.Add (c);
			c.addMother (this);
		}
		public void addNeigbhors(Cell c){
			if(this.Neigbhors==null) this.Neigbhors =new List<Cell>();
			if (!Neigbhors.Contains (c))
				Neigbhors.Add (c);
		}
		public string getInfos(int idx){
			//Check First if any curration exist for this cell
			string curated=this.getCuration(idx);
			if (curated != null) return curated;
			//Otherwise check normal values
			if (idx >= 0 && Infos!=null && Infos.ContainsKey (idx)) {
				string[] row = Infos [idx].Trim().Split(',');
				if(row.Length>=2) return Infos [idx];
				float v;
				if (float.TryParse (Infos [idx], out v)) {
					return "" + (Mathf.Round (v * 1000) / 1000);
				}
				else  return Infos [idx];
			}
			return "";
		}
		//Return the average (during no transformation)


		public bool loadColor ( int idx){
			string curated=this.getCuration(idx);
			if (curated != null) return parseColor(curated);
			//Otherwise check normal values
			if (idx >= 0 && Infos!=null && Infos.ContainsKey (idx)) 
				return parseColor(Infos[idx]);

			return false;
		}
		private bool parseColor(string col){
			float R, B, G;
			string[] row = col.Trim().Split(',');
			if (row.Length>=3 && float.TryParse (row [0], out R) && float.TryParse (row [1], out G) && float.TryParse (row [2], out B)) {
				this.UploadNewColor (new Color (R / 255f, G / 255f, B / 255f, 1));
				return true;
			}
			return false;
		}



		public string getShowString(){
			string Description = "";
			if(this.Infos!=null)
				foreach(Correspondence cor in MenuCell.Correspondences)
					if(cor.isValueShow) 
						Description +=  this.getInfos(cor.id_infos)+"\n";
			return Description;
		}

		//Add a new curration
		public void setCuration(int idInfos,Curation cur ){
			if(Curations==null)  Curations = new Dictionary<int, Curation >();
			Curations [idInfos] = cur;
		}
		public string getCuration(int idInfos){
			if (this.Curations == null) return null;
			if (Curations != null && Curations.ContainsKey (idInfos))
				return Curations [idInfos].value;
			return  null;
		}
		public void deleteCuration(int idInfos){
			if (Curations != null && Curations.ContainsKey (idInfos))
				Curations.Remove (idInfos);
		}

		//Check if this cell is on the right side
		public bool isRight(){ 
			int nbName = MenuCell.getInfosIndex ("Name");
			string name = this.getInfos (nbName);
			if (name.Length > 1 && name [name.Length - 1] == '*')
					return true;
			
			return false;
		}
		//Check if this cell is on the left side
		public bool isLeft(){  return !this.isRight();}

	

		//Load a unpload sphere 
		public bool loadSphere(int idx, string v){
			//Debug.Log ("loadSphere "+v);
			Channel ch=this.getFirstChannel();

			if (ch != null) {
				string[] row = v.Trim ().Split (',');
				float x, y, z, r;
				if (row.Length >= 4 && float.TryParse (row [0], out x) && float.TryParse (row [1], out y) && float.TryParse (row [2], out z) && float.TryParse (row [3], out r)) {
					ArcBall.resetRotation ();
					GameObject pt = Instantiation.Instantiate (Instantiation.sphere);
					pt.name = "UPLOAD_" + idx;
					pt.transform.SetParent (ch.go.transform);
					Vector3 o = new Vector3 (x, y, z);
					o.Set (-o.y, o.x, o.z);
					//Debug.Log ("1b-> "+o);
					o.Set (o.x - Instantiation.embryoCenter.x, o.y - Instantiation.embryoCenter.y, o.z - Instantiation.embryoCenter.z);
					//Debug.Log ("2-> "+o);
					o.Set (o.x * Instantiation.initcanvasScale, o.y * Instantiation.initcanvasScale, o.z * Instantiation.initcanvasScale); //was TrackingVectors.scaleFactor
					//Debug.Log ("3-> "+o);
					pt.transform.position = o;
					pt.transform.localScale = new Vector3 (r, r, r);

					pt.GetComponent<Renderer> ().sharedMaterial = ch.go.GetComponent<Renderer> ().sharedMaterial;

					//Debug.Log ("AFTER ID " + this.ID + "==" + o + " gravity=" + ch.Gravity + " distance=" + Vector3.Distance (o, ch.Gravity));

					ArcBall.updateRotation ();
					return true;
				}
			}
			return false;

		}
		public void removeObject(int idx){
			Channel ch=this.getFirstChannel();
			if (ch != null && ch.go!=null)
				while(ch.go.transform.Find("UPLOAD_" +idx) !=null)//We can have multiple things (line or sphere ...)
					Instantiation.DestroyImmediate (ch.go.transform.Find("UPLOAD_" +idx).gameObject, true);
		}

		//Load a upload vector
		public bool loadVector(int idx,string v){
			Channel ch=this.getFirstChannel();
			if(ch!=null){
                //Debug.Log(v);
                string[] vectors = v.Trim().Split(':');
				if(vectors.Count()==2){ 
					float x1, y1, z1,r1;
					float x2, y2, z2,r2;
					string[] row1 =vectors[0].Trim().Split(',');
					string[] row2 =vectors[1].Trim().Split(',');
					if (row1.Length >= 4 && float.TryParse (row1 [0], out x1) && float.TryParse (row1 [1], out y1) && float.TryParse (row1 [2], out z1) && float.TryParse (row1 [3], out r1) &&
					    row2.Length >= 4 && float.TryParse (row2 [0], out x2) && float.TryParse (row2 [1], out y2) && float.TryParse (row2 [2], out z2) && float.TryParse (row2 [3], out r2)) {
							

						ArcBall.resetRotation ();
						GameObject vector = new GameObject ();
						vector.name = "UPLOAD_" + idx;
						vector.AddComponent<LineRenderer> ();
						vector.GetComponent<LineRenderer> ().useWorldSpace = false;
						vector.transform.SetParent (ch.go.transform);
						vector.GetComponent<LineRenderer> ().SetVertexCount (2);

						Vector3 o = new Vector3 (x1, y1, z1);
						//Debug.Log ("o=" + o);
						//o.Set (-o.y, o.x, o.z);

						o.Set (o.x - Instantiation.embryoCenter.x, o.y - Instantiation.embryoCenter.y, o.z - Instantiation.embryoCenter.z);
						o.Set (o.x * Instantiation.initcanvasScale, o.y * Instantiation.initcanvasScale, o.z * Instantiation.initcanvasScale); //was TrackingVectors.scaleFactor
						//o = (o - Instantiation.embryoCenter);
						//o.Set (-o.x / TrackingVectors.scaleFactor, o.y / TrackingVectors.scaleFactor, o.z / TrackingVectors.scaleFactor);
						vector.GetComponent<LineRenderer> ().SetPosition (0, o);

						Vector3 d = new Vector3 (x2, y2, z2);
						//Debug.Log ("d=" + d);
						//d.Set (-d.y, d.x, d.z);
						d.Set (d.x - Instantiation.embryoCenter.x, d.y - Instantiation.embryoCenter.y, d.z - Instantiation.embryoCenter.z);
						d.Set (d.x * Instantiation.initcanvasScale, d.y * Instantiation.initcanvasScale, d.z * Instantiation.initcanvasScale); //was TrackingVectors.scaleFactor

						//d = (d - Instantiation.embryoCenter);
						//d.Set (-d.x / TrackingVectors.scaleFactor, d.y / TrackingVectors.scaleFactor, d.z / TrackingVectors.scaleFactor);
						vector.GetComponent<LineRenderer> ().SetPosition (1, d);

						//WIDTH  (from 1 to 5) 
						vector.GetComponent<LineRenderer> ().SetWidth (r1 / 100f, r2 / 100f);
						vector.GetComponent<Renderer> ().sharedMaterial = ch.go.GetComponent<Renderer> ().sharedMaterial;

		
						ArcBall.updateRotation ();

						return true;
					}
				}
			}
			return false;
		}
				


		public void updateShader(){
			if(this.Channels!=null)
			foreach (KeyValuePair<string,Channel>item in this.Channels) {
				Channel ch = item.Value;
				ch.updateShader = true;
			}
		}

		//When add a selection value
		public void addSelection(int selectValue){
			if(this.selection==null) this.selection=new List<int> (); 
			if (!this.selection.Contains (selectValue)) {
				this.selection.Add (selectValue);
				this.updateShader ();
				//Lineage.addCell (this);
			}
		}

		//We remove a selection value
		public void removeSelection(int selectValue){
			if (this.selection != null) {
                if (this.selection.Contains(selectValue))
                {
                    this.selection.Remove(selectValue);
                    this.updateShader();
                }
				//Lineage.addCell (this);
			}
		}
		public void removeAllSelection(){
			if (this.selection != null) {
                if (this.selection.Count > 0)
                {
                    this.selection.Clear();
                    this.updateShader();
                }
				//Lineage.addCell (this);
			}
		}


		//When clicked on a cell it select it
		public void choose(bool v){
			//Lineage.addCell(this);
			this.selected = v;
			this.updateShader ();
		}
		public void UploadNewColor(Color col){
			this.UploadColor = col;
			this.UploadedColor = true; 
			this.updateShader ();
			//Lineage.addCell(this);
		}
		public void removeUploadColor(){
			this.UploadedColor = false; 
			this.updateShader ();
			//Lineage.addCell(this);
		}

		public void hide(bool v){
			if(v) this.choose (false); //If it was selected we remove it
			this.show = !v;
			//Lineage.addCell(this);
			//this.updateShader ();
		}
		public void hideAll(bool v){
			this.hide (v);
			if( this.Mothers!=null)
				foreach (Cell c in this.Mothers)
					c.hidePast (v);
			if( this.Daughters!=null)
				foreach (Cell c in this.Daughters)
					c.hideFutur (v);
		}
		public void hidePast(bool v){
			this.hide (v);
			if( this.Mothers!=null)
				foreach (Cell c in this.Mothers)
					c.hidePast (v);
		}
		public void hideFutur(bool v){
			this.hide (v);
			if( this.Daughters!=null)
				foreach (Cell c in this.Daughters)
					c.hideFutur (v);
		}
			

		//Return the list of parents at t
		public List<Cell> GetParentAt(int nt){
			//Debug.Log ("Get PArent of Cells " + this.ID + " at " + this.t + " to " + nt);
			if (this.t - 1 == nt) //PAST
				return this.Mothers;
			else if (this.t + 1 == nt) //FUTUR
				return this.Daughters;

			List<Cell> parent = new List<Cell> ();
			if (this.t == nt)//SAME TIME
				parent.Add (this);
			else if (this.t > nt){ //VERY PAST
				if(this.Mothers!=null)
					foreach(Cell c in this.Mothers) {
						List<Cell> parentMother = c.GetParentAt (nt);
						if(parentMother!=null)
							foreach (Cell m in parentMother)
								if (!parent.Contains (m))
									parent.Add (m);
					}
			}
			else if (this.t < nt){ //VERY FUTUR
				if(this.Daughters!=null)
					foreach(Cell c in this.Daughters) {
						List<Cell> parentDaughter = c.GetParentAt (nt);
						if(parentDaughter!=null)
							foreach (Cell d in parentDaughter)
								if (!parent.Contains (d))
									parent.Add (d);
				}
			}
			return parent;
		}

		//Return the number of time step before mother get multiple childs
		public int getPastLife(int t){
			if (this.Mothers == null) 	return t;
			if(this.Mothers.Count==1 && this.Mothers[0].Daughters.Count==1) { //Une seul Mere et une seul fille
				return this.Mothers[0].getPastLife (t+1);
			}
			return t;
		}
		//Return the number of time step before division
		public int getFuturLife(int t){
			if (this.Daughters == null) 	return t;
			if(this.Daughters.Count==1 && this.Daughters[0].Mothers.Count==1) { //Une seul Mere et une seul fille
				return this.Daughters[0].getFuturLife (t+1);
			}
			return t;
		}
		//Return all daighter and mother
		public List<Cell> getAllCellLife(){
			List<Cell> cells = new List<Cell> ();
			List<Cell> cellsPast = this.getAllCellLifePast ();
			if(cellsPast!=null) foreach (Cell d in cellsPast) if (!cells.Contains (d)) cells.Add (d);
			List<Cell> cellsFutur = this.getAllCellLifeFutur ();
			if(cellsFutur!=null) foreach (Cell d in cellsFutur) if (!cells.Contains (d)) cells.Add (d);
			if (!cells.Contains (this)) cells.Add (this);
			cellsPast.Clear ();cellsFutur.Clear ();
			return cells;
		}
		public List<Cell> getAllCellLifePast(){
			List<Cell> cells = new List<Cell> ();
			if(this.Mothers!=null)
				foreach (Cell c in this.Mothers) {
					if (!cells.Contains (c)) cells.Add (c);
					List<Cell> cellsMothers = c.getAllCellLifePast();
					if(cellsMothers!=null) 
						foreach (Cell cm in cellsMothers) 
							if (!cells.Contains (cm)) cells.Add (cm);
				}
			return cells;
		}
		public List<Cell> getAllCellLifeFutur(){
			List<Cell> cells = new List<Cell> ();
		if(this.Daughters!=null)
				foreach (Cell c in this.Daughters) {
					if (!cells.Contains (c)) cells.Add (c);
					List<Cell> cellsDaughters = c.getAllCellLifeFutur();
					if(cellsDaughters!=null)
						foreach (Cell cd in cellsDaughters) 
							if (!cells.Contains (cd)) cells.Add (cd);
				}
			return cells;
		}

		//Propagate the selection to mothers
		public void PropagateMother(int s){
			if(this.Mothers!=null)
				foreach (Cell c in this.Mothers) {
					c.addSelection (s);
					c.PropagateMother (s);
				}
		}
		//Propagate the selection to all daughters
		public void PropagateDaughters(int s){
			if(this.Daughters!=null)
				foreach (Cell c in this.Daughters) {
					c.addSelection (s);
					c.PropagateDaughters (s);
				}
		}

		public int getLifeLength(){
			return 1 + lifePast + lifeFutur;
		}

	
		public string Describe(){
			
			string Description = "";

			//Mother
			if (this.Mothers!=null && this.Mothers.Count>0)
				foreach(Cell m in this.Mothers) 
					Description += "Mother: " + m.ID + "(" + this.lifePast + ") \n";
			else
				Description += "No Mother " + "(" + this.lifePast + ") \n";

			//Daughters
			if (this.Daughters!=null ||  this.Daughters.Count == 0)
				Description += "No Daughters " + "\n";
			else {
				Description += "Daughers: ";
				foreach (Cell d in this.Daughters)
					Description += d.ID + ";";
				Description += "(" + this.lifeFutur + ") \n";
			}
			Description += "\n";
			return Description;
		}

		public string DescribeNeigbohrs(){
			if (this.Neigbhors==null || this.Neigbhors.Count == 0)
				return "";
			string Description = "";
			//Cell Neigbhors
			Description += "Neighbors:\n";
			if (this.Neigbhors.Count > 0) {
				foreach(Cell n in this.Neigbhors)
				{
					if (n != null) 
						Description +=  n.ID + "\n";
					
				}
				Description+= "\n";
			}
			Description+= "\n";

			return Description;

		}
	}
}


