﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;
using System.Collections.Generic;

public class SelectionColor : MonoBehaviour {

	public static  string [] indexcolors = new string[] {"#B88183", "#922329", "#5A0007", "#D7BFC2", "#D86A78", "#FF8A9A", "#3B000A", "#E20027", "#943A4D", "#5B4E51", "#B05B6F",  "#D83D66", "#895563", "#FF1A59", "#FFDBE5", "#CC0744", "#CB7E98", "#997D87", "#6A3A4C", "#FF2F80", "#6B002C", "#A74571", "#C6005A", "#FF5DA7", "#300018", "#B894A6", "#FF90C9", "#7C6571", "#A30059", "#DA007C", "#5B113C", "#402334", "#D157A0", "#DDB6D0", "#885578", "#962B75", "#A97399", "#D20096", "#E773CE", "#AA5199", "#E704C4", "#6B3A64", "#FFA0F2", "#6F0062", "#B903AA", "#C895C5", "#FF34FF", "#320033", "#DBD5DD", "#EEC3FF", "#BC23FF", "#671190", "#201625", "#F5E1FF", "#BC65E9", "#D790FF", "#72418F", "#4A3B53", "#9556BD", "#B4A8BD", "#7900D7", "#A079BF", "#958A9F", "#837393", "#64547B", "#3A2465", "#353339", "#BCB1E5", "#9F94F0", "#9695C5", "#0000A6", "#000035", "#636375", "#00005F", "#97979E", "#7A7BFF", "#3C3E6E", "#6367A9", "#494B5A", "#3B5DFF", "#C8D0F6", "#6D80BA", "#8FB0FF", "#0045D2", "#7A87A1", "#324E72", "#00489C", "#0060CD", "#789EC9", "#012C58", "#99ADC0", "#001325", "#DDEFFF", "#59738A", "#0086ED", "#75797C", "#BDC9D2", "#3E89BE", "#8CD0FF", "#0AA3F7", "#6B94AA", "#29607C", "#404E55", "#006FA6", "#013349", "#0AA6D8", "#658188", "#5EBCD1", "#456D75", "#0089A3", "#B5F4FF", "#02525F", "#1CE6FF", "#001C1E", "#203B3C", "#A3C8C9", "#00A6AA", "#00C6C8", "#006A66", "#518A87", "#E4FFFC", "#66E1D3", "#004D43", "#809693", "#15A08A", "#00846F", "#00C2A0", "#00FECF", "#78AFA1", "#02684E", "#C2FFED", "#47675D", "#00D891", "#004B28", "#8ADBB4", "#0CBD66", "#549E79", "#1A3A2A", "#6C8F7D", "#008941", "#63FFAC", "#1BE177", "#006C31", "#B5D6C3", "#3D4F44", "#4B8160", "#66796D", "#71BB8C", "#04F757", "#001E09", "#D2DCD5", "#00B433", "#9FB2A4", "#003109", "#A3F3AB", "#456648", "#51A058", "#83A485", "#7ED379", "#D1F7CE", "#A1C299", "#061203", "#1E6E00", "#5EFF03", "#55813B", "#3B9700", "#4FC601", "#1B4400", "#C2FF99", "#788D66", "#868E7E", "#83AB58", "#374527", "#98D058", "#C6DC99", "#A4E804", "#76912F", "#8BB400", "#34362D", "#4C6001", "#DFFB71", "#6A714A", "#222800", "#6B7900", "#3A3F00", "#BEC459", "#FEFFE6", "#A3A489", "#9FA064", "#FFFF00", "#61615A", "#FFFFFE", "#9B9700", "#CFCDAC", "#797868", "#575329", "#FFF69F", "#8D8546", "#F4D749", "#7E6405", "#1D1702", "#CCAA35", "#CCB87C", "#453C23", "#513A01", "#FFB500", "#A77500", "#D68E01", "#B79762", "#7A4900", "#372101", "#886F4C", "#A45B02", "#E7AB63", "#FAD09F", "#C0B9B2", "#938A81", "#A38469", "#D16100", "#A76F42", "#5B4534", "#5B3213", "#CA834E", "#FF913F", "#953F00", "#D0AC94", "#7D5A44", "#BE4700", "#FDE8DC", "#772600", "#A05837", "#EA8B66", "#391406", "#FF6832", "#C86240", "#29201D", "#B77B68", "#806C66", "#FFAA92", "#89412E", "#E83000", "#A88C85", "#F7C9BF", "#643127", "#E98176", "#7B4F4B", "#1E0200", "#9C6966", "#BF5650", "#BA0900", "#FF4A46", "#F4ABAA", "#000000", "#452C2C", "#C8A1A1"};
	public static int [] colororder=new int[]{76,27,189,244,5,20,151,37,180,80,101,215,206,160,113,213,25,221,61,190,22,203,100,96,111,38,94,201,195,128,71,243,120,168,34,170,237,199,90,105,36,95,182,98,216,107,70,35,84,17,155,193,89,163,50,15,197,196,85,232,14,154,166,224,249,51,246,214,236,72,104,135,147,139,198,48,106,207,126,66,1,200,161,177,41,64,152,141,186,239,172,81,10,3,121,112,132,205,194,42,54,69,143,115,229,178,162,16,68,230,238,87,118,175,97,144,114,174,234,108,60,53,13,30,21,225,210,138,58,136,102,245,231,12,220,57,59,184,171,158,252,55,39,11,125,140,187,88,157,33,4,110,130,235,192,159,2,49,153,183,242,86,185,45,52,28,211,75,145,82,156,124,164,227,62,217,218,0,73,83,129,167,208,149,8,233,133,56,142,47,92,79,103,241,74,29,240,209,223,7,250,23,18,173,188,117,9,91,131,134,169,148,228,77,165,176,219,253,202,31,67,212,119,99,251,32,109,6,123,78,222,122,204,93,127,26,247,40,254,44,150,179,65,146,181,191,137,248,43,226,19,46,24,63,116};

	public static Material  [] materials;
	public  int SelectionValue;
	public  GameObject SelectedColor;
	public  GameObject SelectedColorRender;
	public  InputField SelectionNumber;
	public  InputField remap;
	public  Slider SliderSelectionNumber;

	public GameObject MenuColor;

	public Toggle ThisAllTimes;
	public Toggle AllAllTimes;
	public Toggle MultiAllTimes;
	public Toggle NonAllTimes;


	public Button ThisSelection; 
	public Button AllSelections;
	public Button MultiSelection;
	public Button NonSelected;



	public GameObject MenuLoadSelection;

	public Toggle SelectedCellAllTimes;
	// Use this for initialization
	void Start () {
		if(materials==null)  materials=new Material[indexcolors.Length];
		if (materials [0] == null) createMaterial (0);
		MenuColor.SetActive (false);
		UpdateSelectionColor(1);
	}

	public void OnEnabled(){
		UpdateNumberCells ();
	}
	public static void createMaterial(int i){
		string hexcol = indexcolors [colororder[i]];
		Color newColor = new Color();
		ColorUtility.TryParseHtmlString (hexcol, out newColor);
		materials [i] = new Material(Instantiation.Default);
		materials [i].color = newColor;

	}
	public  void ChangeColorValue(){ //From Input Field
		float vv;
		if (float.TryParse (SelectionNumber.text, out vv)) {
			int v=(int)Mathf.Round(vv);
			UpdateSelectionColor (v);
		}
	}

	public  void onReMapValue(){ //From Input Field remap
		float vv;
		if (float.TryParse (remap.text, out vv)) {
			int v=(int)Mathf.Round(vv);
			int Previous = SelectionValue;
			UpdateSelectionColor (v);
			if (SelectionValue != Previous)
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					foreach (var itemc in Instantiation.CellT [t]) { 
						Cell c = itemc.Value; 
						if (c.selection != null && c.selection.Contains (Previous)) {
							c.removeSelection (Previous);
							c.addSelection (SelectionValue);
						}
					}
				}
		}
		UpdateThisNumberCells ();
	}

	public  void ChangeSliderValue(){ //From Slider
		int v=(int)Mathf.Round(SliderSelectionNumber.GetComponent<UnityEngine.UI.Slider> ().value);
		UpdateSelectionColor (v);
	}
		
	//When changing the selection value (from slider or input field or as selected)
	public void UpdateSelectionColor(int v){
		//v=Mathf.RoundToInt(Mathf.Repeat(v, 255))+1;
		SliderSelectionNumber.GetComponent<UnityEngine.UI.Slider> ().value = (float)v;
		SelectionNumber.text = v.ToString ();
		SelectionValue = v;
		if (materials [v - 1] == null) createMaterial (v - 1);
		SelectedColorRender.GetComponent<Renderer>().material=materials [v-1];
		SelectedColor.GetComponent<Renderer>().material=materials [v-1];
		//SelectedColor.GetComponent<Image> ().color = materials [v-1].color ;
		remap.text= v.ToString ();
		UpdateNumberCells ();
	}

	//After closing the colormenu
	//After validating a new color from Menu colors 
	public void changeMaterialSet(Material m){
		for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
			foreach (var itemc in Instantiation.CellT [t]) { 
				Cell c = itemc.Value; 
				if (c.selection!=null && c.selection.Contains (SelectionValue))
					c.updateShader ();
			}
		}
		materials [SelectionValue - 1] = m;
		UpdateSelectionColor(SelectionValue);
	}
	//After changing materials from else where (load selection) reaffect all materials
	public void resetAllMaterials(){
		for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
			foreach (var itemc in Instantiation.CellT [t]) { 
				Cell c = itemc.Value; 
				if (c.selection!=null && c.selection.Count >0)
					c.updateShader();
			}
		}
		UpdateSelectionColor (SelectionValue);
	}

	//Return the material assign to a cell
	public static Material getSelectedMaterial(int v){
        //v=Mathf.RoundToInt(Mathf.Repeat(v, 254))+1;
        while (v > indexcolors.Length) v -= indexcolors.Length;
		if (v <= 0) return Instantiation.Default;
		if(materials==null)  materials=new Material[indexcolors.Length];

		if (materials [v - 1] == null) createMaterial (v - 1);
		return materials[v-1];
	}

	public void selectAsSelected(){
		int v=SelectionCell.getSelection ();
		if (v >= 1 && v <= 255)
			UpdateSelectionColor (v);
		
	}


	public  void ChangeColorSelection(){ //Open the menu color 
		if (MenuColor.activeSelf)
			MenuColor.SetActive (false);
		else {
			MenuColor.GetComponent<ChangeColorSelection> ().InitColor (SelectedColor.GetComponent<Renderer>().material);
			MenuColor.SetActive (true);
			MenuLoadSelection.SetActive (false);
		}
	}

	//When we click on ok to applly this selection to the selected cell
	public void applySelection(){
		if (Lineage.isLineage) Lineage.applySelection (SelectionColor.getSelectedMaterial (SelectionValue).color);
		for (int i = 0; i < SelectionCell.clickedCells.Count; i++) { //For all cells selected
			//Debug.Log(" Assign selection to " +SelectionCell.clickedCells[i].ID+ " at " +SelectionCell.clickedCells[i].t);
			SelectionCell.clickedCells[i].addSelection(SelectionValue);

		}
		SelectionCell.clearSelectedCells ();
		UpdateNumberCells ();
	}




	//Clear selection for all cells with this selection number at this time step
	public void clearThis(){
		if (Lineage.isLineage) Lineage.clearThis (ThisAllTimes.isOn,SelectionValue);
		if (ThisAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				clearThisAt(t);
		else clearThisAt(Instantiation.CurrentTime);
		UpdateNumberCells ();

	}
	public void clearThisAt(int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Contains(SelectionValue))
				c.removeSelection(SelectionValue);
		}
	}

	//Clear selection for all cells with all selection number at this time step
	public void clearAll(){
		if (Lineage.isLineage) Lineage.clearAll (ThisAllTimes.isOn);
		if (AllAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				clearAllAt(t);
		else clearAllAt(Instantiation.CurrentTime);
		UpdateNumberCells ();
	}
	public void clearAllAt(int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			c.removeAllSelection ();
		}
	}

	//Clear selection for all cells with all selection number at this time step
	public void clearMulti(){
		if (Lineage.isLineage) Lineage.clearAll (MultiAllTimes.isOn);
		if (MultiAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				clearMultiAt(t);
		else clearMultiAt(Instantiation.CurrentTime);
		UpdateNumberCells ();
	}
	public void clearMultiAt(int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>=2)
			 c.removeAllSelection ();
		}
	}

	//Propagation selection
	public void propagateThisPast(){
		if (Lineage.isLineage) Lineage.propagateThis (false, SelectionValue);
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime])  { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Contains(SelectionValue))
				c.PropagateMother (SelectionValue);
		}
		UpdateNumberCells ();
	}


	public void propagateThisFutur(){
		if (Lineage.isLineage) Lineage.propagateThis (true, SelectionValue);
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Contains(SelectionValue))
				c.PropagateDaughters (SelectionValue);
		}
		UpdateNumberCells ();
	}


	public void propagateAllPast(){
		if (Lineage.isLineage) Lineage.propagateAll (false);
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime])  { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>0)
				foreach(int s in c.selection)
					c.PropagateMother (s);
		}
		UpdateNumberCells ();
	}

	public void propagateAllFutur(){
		if (Lineage.isLineage) Lineage.propagateAll (true);
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime])  { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>0)
				foreach(int s in c.selection)
					c.PropagateDaughters (s);
		}
		UpdateNumberCells ();
	}

	//Hide or Show Cells
	public void hideThis(){ applyhideThis (true); }
	public void showThis(){ applyhideThis (false); }
	public void applyhideThis(bool v){
		if (Lineage.isLineage) Lineage.applyhideThis (v, ThisAllTimes.isOn,SelectionValue);
		if (ThisAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				applyhideThisAt(v,t);
		else applyhideThisAt(v,Instantiation.CurrentTime);
	}
	public void applyhideThisAt(bool v,int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Contains(SelectionValue))
				c.hide(v);
		}
	}

	public void hideAll(){ applyhideAll (true); }
	public void showAll(){ applyhideAll (false); }
	public void applyhideAll(bool v){
		if (Lineage.isLineage) Lineage.applyhideAll (v, ThisAllTimes.isOn);
		if (AllAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				applyhideAllAt(v,t);
		else applyhideAllAt(v,Instantiation.CurrentTime);
	}
	public void applyhideAllAt(bool v,int t){
		foreach (var itemc in Instantiation.CellT [t])  { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>0) 
				c.hide(v);

		}
	}

	public void hideMulti(){ applyhideMulti (true); }
	public void showMulti(){ applyhideMulti(false); }
	public void applyhideMulti(bool v){
		if (Lineage.isLineage) Lineage.applyhideAll (v, MultiAllTimes.isOn);
		if (MultiAllTimes.isOn) 
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++)
				applyhideMultiAt(v,t);
		else applyhideMultiAt(v,Instantiation.CurrentTime);
	}
	public void applyhideMultiAt(bool v,int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>=2) 
				c.hide(v);
		}
	}



	//Clear selection for all cells with all selection number at all time step
	public void hideNonSelected(){applyHideNonSelected(true);}
	public void showNonSelected(){applyHideNonSelected(false);}

	public void applyHideNonSelected(bool v){
		if (NonAllTimes.isOn)
			for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
				applyHideNonSelectedAt (v, t);
		else
			applyHideNonSelectedAt (v, Instantiation.CurrentTime);
	}

	public void applyHideNonSelectedAt(bool v, int t){
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection==null || c.selection.Count == 0) 
				c.hide(v);
		}
	}
	//To Select all cell corresponding to this selection at this time point
	public void onSelect(){
		if (SelectionCell.clickedCells != null)SelectionCell.clickedCells=new List<Cell>(); 
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime])  { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Count > 0 && c.selection.Contains (SelectionValue) && !c.selected && !SelectionCell.clickedCells.Contains (c)) {
				c.choose(true);
				SelectionCell.clickedCells.Add (c);
			}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe (); 
	}
	public void onSelectAll(){
		if (SelectionCell.clickedCells != null)SelectionCell.clickedCells=new List<Cell>(); 
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Count > 0 && !c.selected && !SelectionCell.clickedCells.Contains (c)) {
				c.choose(true);
				SelectionCell.clickedCells.Add (c);
			}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe (); 
	}
	public void onSelectMulti(){
		if (SelectionCell.clickedCells != null)SelectionCell.clickedCells=new List<Cell>(); 
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Count >=2 && !c.selected && !SelectionCell.clickedCells.Contains (c)) {
				c.choose(true);
				SelectionCell.clickedCells.Add (c);
			}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe (); 
	}
	public void onSelectNone(){
		if (SelectionCell.clickedCells != null)SelectionCell.clickedCells=new List<Cell>(); 
		foreach (var itemc in Instantiation.CellT [Instantiation.CurrentTime]) { 
			Cell c = itemc.Value; 
			if ((c.selection == null || c.selection.Count == 0) && !c.selected && !SelectionCell.clickedCells.Contains (c)) {
				c.choose(true);
				SelectionCell.clickedCells.Add (c);
			}
		}
		if (Lineage.isLineage) Lineage.addAllSelectedCells ();
		MenuCell.describe (); 
	}

	//Update All Fields
	public void UpdateNumberCells(){
		UpdateThisNumberCells ();
		UpdateAllNumberCells ();
		UpdateMultiNumberCells ();
		UpdateNonNumberCells ();
	}
	//FOR THIS SELECTED CELLS
	public void UpdateThisNumberCells(){
		int nb = 0;
		if (ThisAllTimes.isOn) nb = CountCellsThisSelectionAll ();
		else nb = CountCellsThisSelection (Instantiation.CurrentTime);
		if (nb > 0) { 
			string nbcell = nb + " object";
			if (nb > 1) nbcell += "s";
			ThisSelection.gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text =nbcell;
			ThisSelection.gameObject.SetActive (true);
		}  else ThisSelection.gameObject.SetActive (false);

	}
	public int CountCellsThisSelectionAll(){
		int nb = 0;
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
			nb += CountCellsThisSelection (t);
		return nb;
	}

	//Just Countthe number of cells with this selection value
	public int CountCellsThisSelection(int t){
		int nb = 0;
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Contains(SelectionValue))
				nb++;
		}
		return nb;
	}

	//FOR ALLL SELECTED CELLS
	public void UpdateAllNumberCells(){
		int nb = 0;
		if (AllAllTimes.isOn) nb = CountCellsAllSelectionAll ();
		else nb = CountCellsAllSelection (Instantiation.CurrentTime);
		if (nb > 0) { 
			string nbcell =nb + " object";
			if (nb > 1) nbcell += "s";
			AllSelections.gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text =nbcell;
			AllSelections.gameObject.SetActive (true);
		}  else AllSelections.gameObject.SetActive (false);
	}
	public int CountCellsAllSelectionAll(){
		int nb = 0;
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
			nb += CountCellsAllSelection (t);
		return nb;
	}

	//Just Countthe number of cells with this selection value
	public int CountCellsAllSelection(int t){
		int nb = 0;
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>0)
				nb++;
		}
		return nb;
	}

	//FOR THIS MULTI CELLS
	public void UpdateMultiNumberCells(){
		int nb = 0;
		if (MultiAllTimes.isOn) nb = CountCellsMultiSelectionAll ();
		else nb = CountCellsMultiSelection (Instantiation.CurrentTime);
		if (nb > 0) { 
			string nbcell = "";
			nbcell = nb + " object";
			if (nb > 1) nbcell += "s";
			MultiSelection.gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text =nbcell;
			MultiSelection.gameObject.SetActive (true);
		}  else MultiSelection.gameObject.SetActive (false);
	}
	public int CountCellsMultiSelectionAll(){
		int nb = 0;
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
			nb += CountCellsMultiSelection (t);
		return nb;
	}

	//Just Countthe number of cells with this selection value
	public int CountCellsMultiSelection(int t){
		int nb = 0;
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection!=null && c.selection.Count>=2)
				nb++;
		}
		return nb;
	}


	//FOR NON SELECTED CELLS
	public void UpdateNonNumberCells(){
		int nb = 0;
		if (NonAllTimes.isOn) nb = CountCellsNonSelectionAll ();
		else nb = CountCellsNonSelection (Instantiation.CurrentTime);
		if (nb > 0) { 
			string nbcell = "";
			nbcell = nb + " object";
			if (nb > 1) nbcell += "s";
			NonSelected.gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text =nbcell;
			NonSelected.gameObject.SetActive (true);
		}  else NonSelected.gameObject.SetActive (false);
	}
	public int CountCellsNonSelectionAll(){
		int nb = 0;
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++)
			nb += CountCellsNonSelection (t);
		return nb;
	}

	//Just Countthe number of cells with this selection value
	public int CountCellsNonSelection(int t){
		int nb = 0;
		foreach (var itemc in Instantiation.CellT [t]) { 
			Cell c = itemc.Value; 
			if (c.selection==null || c.selection.Count==0)
				nb++;
		}
		return nb;
	}


	public void openLoadSelection(){
		if (MenuLoadSelection.activeSelf)
			MenuLoadSelection.SetActive (false);
		else {
			MenuColor.SetActive (false);
			MenuLoadSelection.SetActive (true);
			MenuLoadSelection.GetComponent<SelectionDB> ().listAllColormap ();

		}
	}


	//FOR SELECTED CELLS
	public void resetSelectionOnSelectedCells(){
		if (Lineage.isLineage) Lineage.resetSelectionOnSelectedCells (SelectedCellAllTimes.isOn);

		if (SelectedCellAllTimes.isOn) { //ALL TIMES
			for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				List<Cell> cells = SelectionCell.clickedCells [i].getAllCellLife ();
				foreach (Cell c in cells)
					c.removeAllSelection ();
			}
		} else { //JUST THIS TIME STEP
			for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				SelectionCell.clickedCells [i].removeAllSelection ();

			}
		}
		MenuCell.describe ();
	}
}
