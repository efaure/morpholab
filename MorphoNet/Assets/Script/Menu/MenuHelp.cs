﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MenuHelp : MonoBehaviour {



	public Text Me;
	public InputField email;
	public InputField password;
	public InputField comments;
	public Text changement;
	// Use this for initialization
	void Start () {
		Me.text = Instantiation.surnameUser + " " + Instantiation.nameUser;
	}
		
	public IEnumerator SentForm(string field,string value){
		if(value!="" && field!=""){
			WWWForm form = new WWWForm ();
			form.AddField ("hash", Instantiation.hash); 
			form.AddField ("ID", Instantiation.IDUser);
			form.AddField (field, value);
			Debug.Log(Instantiation.urlUsers+"?hash="+Instantiation.hash+"&ID="+Instantiation.IDUser+"&"+field+"="+value);
			UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlUsers, form);
			yield return www.Send(); 
			if(www.isHttpError || www.isNetworkError)
				changement.text = "Error : " + www.error; 
			else {
				string txt = System.Text.Encoding.UTF8.GetString (www.downloadHandler.data, 1, www.downloadHandler.data.Length - 1); 
				if (txt.Length>2 && txt.Substring (0, 2) == "OK"){
					if(field=="email") changement.text = "Email changed ";
					if(field=="password") changement.text = "Password changed ";
					if(field=="comments") changement.text = "Message sent ";
				}
				else {
					changement.text = "Error plase retry ? ";
					Debug.Log (txt);
				}
			}
			www.Dispose (); 
		}
	}

	public void onChangeEmail(){
		StartCoroutine (SentForm ("email", email.text));
	}
		
	public void onChangePassword(){
		StartCoroutine (SentForm ("password", password.text));
	}
		
	public void onChangeComments(){
		StartCoroutine (SentForm ("comments", comments.text));
	}


}
