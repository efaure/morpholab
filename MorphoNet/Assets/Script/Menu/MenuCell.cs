﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;
using System.Collections.Generic;
using UnityEngine.Networking;
using SimpleJSON;
using System.Linq;
using System.Text;
using System.IO;
using System;

public class CorrespondenceType: MonoBehaviour{
	public string name;
	public List<Correspondence> Correspondences;
	public GameObject go;
	public bool isDeploy;
	public CorrespondenceType(string name){
		this.isDeploy = true;
		this.name = name;
		this.Correspondences =new List<Correspondence> ();
		this.go = (GameObject)Instantiate(MenuCell.DefaultCT,MenuCell.MenuInfos.transform,true);
		this.go.name = name;
		this.go.transform.Find ("datatype").transform.GetComponent<Text> ().text = name;
		this.go.transform.Find ("Infos").gameObject.SetActive (false);
		if(name=="Quantitative") this.go.transform.Find ("compare").gameObject.SetActive (true);
		else this.go.transform.Find ("compare").gameObject.SetActive (false);
		//if(name=="Genetic")  this.go.transform.Find ("downloadAll").gameObject.SetActive (true);
		//else 
			this.go.transform.Find ("downloadAll").gameObject.SetActive (false);
		this.go.SetActive (true);
		this.go.transform.Find ("reploy").gameObject.SetActive (false);
        if (Instantiation.userRight <= 1)
        {//NO READER 
            MenuCell.deployCTListen(this.go.transform.Find("deploy").transform.GetComponent<Button>(), this);
            MenuCell.deployCTListen(this.go.transform.Find("reploy").transform.GetComponent<Button>(), this);
        }
        else
        {
           // Debug.Log("Hide Deploy for " + this.name);
            this.go.transform.Find("deploy").gameObject.SetActive(false);
            this.go.transform.Find("reploy").gameObject.SetActive(false);
        }
	}

	public void deploy(){
		if (this.isDeploy) {
			this.go.transform.Find ("reploy").gameObject.SetActive (true);
			this.go.transform.Find ("deploy").gameObject.SetActive (false);
		} else {
			this.go.transform.Find ("reploy").gameObject.SetActive (false);
			this.go.transform.Find ("deploy").gameObject.SetActive (true);
		}
		this.isDeploy = !this.isDeploy;
		foreach (Correspondence cor in this.Correspondences)
			if (cor.isActive)
				cor.inf.SetActive (this.isDeploy);
		MenuCell.reOrganize ();
	}
	public bool Contains(Correspondence cor){
		return this.Correspondences.Contains (cor);
	}
	public void addCorrespondence(Correspondence cor){
		this.Correspondences.Add (cor);
		MenuCell.Correspondences.Add(cor);
	}
	public void removeCorrespondence(Correspondence cor){
		//THIS DOES NOT WORK 
		//this.Correspondences.Remove (cor);
		//MenuCell.Correspondences.Remove(cor);
		List<Correspondence> NC=new List<Correspondence>();
		foreach(Correspondence corr in this.Correspondences)
			if (corr.id_infos != cor.id_infos)
				NC.Add (corr);
		this.Correspondences = NC;

		List<Correspondence> NNC=new List<Correspondence>();
		foreach(Correspondence corr in MenuCell.Correspondences)
			if (corr.id_infos != cor.id_infos)
				NNC.Add (corr);
		MenuCell.Correspondences = NNC;
	}

	public int SetPosition(int Height){
		//SCROLL ACTIVATION
		MenuCell.inScroll++;
		if (MenuCell.inScroll <= MenuCell.nbSCroll) {
			this.go.transform.Find ("datatype").gameObject.SetActive (false);
			if(this.name=="Quantitative") this.go.transform.Find ("compare").gameObject.SetActive (false);
			if (this.isDeploy)  this.go.transform.Find ("deploy").gameObject.SetActive (false);
			 else {
				this.go.transform.Find ("reploy").gameObject.SetActive (true);
				this.go.SetActive (false);
			}
		} else if((MenuCell.inScroll-MenuCell.nbSCroll)*22>MenuCell.MaxHeight){ //TOO LONG BOTTOM
			this.go.SetActive (false);
		}else{ //PILE POILE
			this.go.SetActive (true);
			if(this.name=="Quantitative") this.go.transform.Find ("compare").gameObject.SetActive (true);
			this.go.transform.Find ("datatype").gameObject.SetActive (true);
			if (this.isDeploy) 	this.go.transform.Find ("deploy").gameObject.SetActive (true); 	//But  We have to check if all are deploy
			else this.go.transform.Find ("reploy").gameObject.SetActive (true);
		}
			
		//Debug.Log ("SetPosition Types to " + this.name+ " at " + Height);
		this.go.transform.position=new Vector3(this.go.transform.position.x,(MenuCell.nbSCroll*22+MenuCell.shiftStart-Height)*Instantiation.canvasScale, this.go.transform.position.z);
		int sizeH=0;
		if(this.isDeploy)
			foreach (Correspondence cor in this.Correspondences) 
				if (cor.isActive) 
					sizeH += cor.SetPosition (Height + sizeH);	
			
		sizeH += 22;
		return sizeH;
	}


}


public class CorrespondenceOtherData : MonoBehaviour {
	public int id_owner;
	public int id_infos; //ID IN database
	public string name;
	public string link;
	public uint version;
	public bool txt;//Bundle version of direct download ?
	public string datatype; //string,float,etc...
	public string data_show; //For space , time groups, we change the output 
	public int type; //1 FOR PUBLIC, 2 FOR UPLOAD, 3 FOR SAVED SELECTION
	public int pos;
	public string uploadDate;
	public int id_dataset;

	public CorrespondenceOtherData(int id_dataset,int id_owner,int type,int id_infos, string name,string link,uint version,bool txt,string datatype,string uploadDate,int pos){
		this.type = type;
		this.id_infos = id_infos;
		this.link = link;
		this.version = version;
		this.txt = txt;
		this.name = name;
		this.id_owner = id_owner;
		this.id_dataset = id_dataset;
		if (datatype == "rgb" || datatype == "rgba") datatype = "color";//TEMP TO DESTROY 
		this.datatype = datatype;
		this.pos = pos;
		this.uploadDate = uploadDate;

	}

}
public class Correspondence : MonoBehaviour {
	public bool isActive;
	public int id_owner;
	public int id_infos; //ID IN database
	public string name;
	public string link;
	public uint version;
	public bool txt;//Bundle version of direct download ?
	public string datatype; //string,float,etc...
	public string data_show; //For space , time groups, we change the output 
	public int type; //1 FOR PUBLIC, 2 FOR UPLOAD, 3 FOR SAVED SELECTION
	public float MinInfos;
	public float MaxInfos;

	public int common;
	public bool isValueShow; //Do we want to see the string value 

	public GameObject inf;
	public GameObject SubInfos;
	public CorrespondenceType ct;
	public int id_dataset;
	public List<Curation> Curations;

	public Correspondence(int id_dataset,CorrespondenceType ct,int id_owner,int type,int id_infos, string name,string link,uint version,bool txt,string datatype,string uploadDate,int common){
		this.isActive = true;
        if (datatype == "time" || datatype == "group" || name.ToLower() == "name" || name.ToLower() == "names") type = 1;
        this.type = type;
		this.id_infos = id_infos;
		this.link = link;
		this.version = version;
		this.txt = txt;
		this.name = name;
		this.id_owner = id_owner;
		if (datatype == "rgb" || datatype == "rgba") datatype = "color";//TEMP TO DESTROY 
		this.datatype = datatype;
		this.MinInfos  = float.MaxValue;
		this.MaxInfos = float.MinValue;
		this.isValueShow = false;
		this.id_dataset = id_dataset;
		this.ct = ct;
		data_show = datatype;
		if (datatype == "time") data_show = "float";
		if (datatype == "space") data_show = "float";
		if (datatype == "group") data_show="string";
		this.common = common;
        //Debug.Log (this.name + " ->data_show=" + data_show + " datatype=" + datatype+" witth id "+this.id_infos);
		inf = (GameObject)Instantiate(ct.go.transform.Find("Infos").gameObject,ct.go.transform,false);

		inf.SetActive (true);
		inf.name = this.name;
		//inf.transform.Translate(0, -ct.Correspondences.Count*MenuCell.shiftY*Instantiation.canvasScale, 0, Space.World);
		inf.transform.Find ("infos").gameObject.GetComponent<Text>().text = this.name;
		inf.transform.Find ("UploadDate").gameObject.GetComponent<Text> ().text = uploadDate;
		inf.transform.Find ("UploadDate").gameObject.SetActive (false);
	
		this.unActive (); //Default everything button if turn off
		//PUBLIC CORRESPONDENCE we directly download ...
       	if ( id_dataset == Instantiation.id_dataset && type == 1 ) MenuCell.addDownloadDATA (this);
		else if (datatype=="genetic") MenuCell.addDownloadDATA (this);
		else  MenuCell.loadListen(inf.transform.Find ("load").gameObject.GetComponent<Button> (),this);
        //REMOVE || datatype == "space"	
        if (Instantiation.userRight >= 2)  inf.transform.Find("deploy").gameObject.SetActive(false); //READER CANNOT MANAGE INFOS


	}
	public int SetPosition(int Height){
		MenuCell.inScroll++;
		if (MenuCell.inScroll <= MenuCell.nbSCroll) this.inf.SetActive (false);  //OUT BEFORE
		else if ((MenuCell.inScroll - MenuCell.nbSCroll) * 22 > MenuCell.MaxHeight) this.inf.SetActive (false);  //OUT AFTER 
		else this.inf.SetActive (true);
		if (this.SubInfos.activeSelf) MenuCell.inScroll++;
		//Debu.Log ("SetPosition to " + this.name+ " at " + Height+ "->"+Instantiation.canvasScale);
		inf.transform.position=new Vector3(inf.transform.position.x, (MenuCell.nbSCroll*22+MenuCell.shiftStart-15-Height)*Instantiation.canvasScale, inf.transform.position.z);
		int shiftY = 22;
		if (this.SubInfos.activeSelf) shiftY += 22;
		return shiftY;
	}
	public void unActive(){
		if (inf != null) {
			inf.transform.Find ("load").gameObject.SetActive (true);
			//inf.transform.Find ("UploadDate").gameObject.SetActive (true);
			inf.transform.Find ("show").gameObject.SetActive (false);
			inf.transform.Find ("show").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();

			inf.transform.Find ("InfosColor").gameObject.SetActive (false);
			inf.transform.Find ("InfosColor").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();

			inf.transform.Find ("value").gameObject.SetActive (false);
			inf.transform.Find ("deploy").gameObject.SetActive (false);
			inf.transform.Find ("deploy").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();

			SubInfos = inf.transform.Find ("SubInfos").gameObject;
			SubInfos.SetActive (false);
			SubInfos.transform.Find ("download").gameObject.SetActive (false);
			SubInfos.transform.Find ("remove").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
			SubInfos.transform.Find ("cancel").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
			SubInfos.transform.Find ("onShare").gameObject.GetComponent<Toggle> ().onValueChanged.RemoveAllListeners ();

			//Curration
			if (this.Curations != null) {
				this.Curations.Clear ();
				//System.GC.Collect ();
			}
			SubInfos.transform.Find ("Curate").gameObject.SetActive (false);
			SubInfos.transform.Find ("Curate").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
		}
	}

	//After download the data, we active the buttons
	public void active(){
        Debug.Log ("Active "+this.name + " -> with id "+this.id_infos);

		//FOR ALL
		inf.transform.Find ("load").gameObject.SetActive (false);
		inf.transform.Find ("UploadDate").gameObject.SetActive (false);
        if (Instantiation.userRight < 2)  { //ONLY FOR OWNER AND CREATOR
            inf.transform.Find("deploy").gameObject.SetActive(true);
            MenuCell.showSubInfosListen(inf.transform.Find("deploy").gameObject.GetComponent<Button>(), this);
        }
		if (data_show == "color") { //COLOR
			inf.transform.Find ("value").gameObject.SetActive (true);
			inf.transform.Find ("show").gameObject.SetActive (true);
			inf.transform.Find ("show").Find("Text").gameObject.GetComponent<Text> ().text="apply";
			MenuCell.loadColorListen (inf.transform.Find ("show").gameObject.GetComponent<Button> (), this, true);
		} else if (data_show == "string") { //STRING ...
			inf.transform.Find ("value").gameObject.SetActive (true);
			MenuCell.changeValueListen (inf.transform.Find ("value").gameObject.GetComponent<Button> (), this);
			inf.transform.Find ("show").gameObject.SetActive (true);
			MenuCell.showStringListen (inf.transform.Find ("show").gameObject.GetComponent<Button> (), this, true);
		} else if (data_show == "float") { //FLOAT
			inf.transform.Find ("value").gameObject.SetActive (true);
			inf.transform.Find ("InfosColor").gameObject.SetActive (true);
			MenuCell.changeColorBarListen (inf.transform.Find ("InfosColor").gameObject.GetComponent<Button> (), this);
			MenuCell.changeColorMapListen (inf.transform.Find ("InfosColor").gameObject.transform.Find ("execute").gameObject.GetComponent<Button> (), this);
			inf.transform.Find ("InfosColor").gameObject.GetComponentInChildren<Text> ().text = "";
		} else  if (data_show == "selection") { //SELECTION
			inf.transform.Find ("value").gameObject.SetActive (true);
			inf.transform.Find ("show").gameObject.SetActive (true);
			inf.transform.Find ("show").Find("Text").gameObject.GetComponent<Text> ().text="apply";
			MenuCell.applySelectionListen (inf.transform.Find ("show").gameObject.GetComponent<Button> (), this);
		} else if (data_show == "sphere") { //SPHERE
			inf.transform.Find ("value").gameObject.SetActive (false);
			inf.transform.Find ("show").gameObject.SetActive (true);
			MenuCell.applySphereListen (inf.transform.Find ("show").gameObject.GetComponent<Button> (), this,true);
		}else if (data_show == "vector") { //VECTOR
			inf.transform.Find ("value").gameObject.SetActive (false);
			inf.transform.Find ("show").gameObject.SetActive (true);
			MenuCell.applyVectorListen (inf.transform.Find ("show").gameObject.GetComponent<Button> (), this,true);
		} else if (data_show == "dict"){ //DICTIONNARY 
			inf.transform.Find ("value").gameObject.SetActive (false);
			inf.transform.Find ("InfosColor").gameObject.SetActive (true);
			MenuCell.changeColorBarListen (inf.transform.Find ("InfosColor").gameObject.GetComponent<Button> (), this);
			MenuCell.changeColorMapListen (inf.transform.Find ("InfosColor").gameObject.transform.Find ("execute").gameObject.GetComponent<Button> (), this);
			inf.transform.Find ("InfosColor").gameObject.GetComponentInChildren<Text> ().text = "";
		}else if (data_show == "genetic"){ //DICTIONNARY 
			inf.transform.Find ("value").gameObject.SetActive (true);
			inf.transform.Find ("InfosColor").gameObject.SetActive (false);
		}
		else Debug.Log ("Non Treaded " + data_show+" :"+datatype+":" );

	
		if (this.type != 1 && data_show != "genetic") { //WE CANNOT REMOVE ORIGINAL LOADED DATA
			SubInfos.transform.Find ("remove").gameObject.SetActive (true);
			MenuCell.removeListen (SubInfos.transform.Find ("remove").gameObject.GetComponent<Button> (), this);
		}else SubInfos.transform.Find ("remove").gameObject.SetActive (false);


		if (common == 1) SubInfos.transform.Find ("onShare").gameObject.GetComponent<Toggle> ().isOn = true;
		else  	SubInfos.transform.Find ("onShare").gameObject.GetComponent<Toggle> ().isOn = false;


		if (Instantiation.userRight<=1) { //READER ACCESS CAN NOT DOWNLOAD DATA
			SubInfos.transform.Find ("download").gameObject.SetActive(true);
			MenuCell.downloadInfosListen (SubInfos.transform.Find ("download").gameObject.GetComponent<Button> (), this); 
			//Curration
			//if (data_show != "selection" && data_show != "genetic") {
				SubInfos.transform.Find ("Curate").gameObject.SetActive (true);
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(MenuCurrated.downloadCurations (this)); //Download all currations from db
				MenuCurrated.showCurationListen (SubInfos.transform.Find ("Curate").gameObject.GetComponent<Button> (), this);
			//}
		}

		if (Instantiation.userRight<=1) {//ONLY CREATOR AND OWNER CAN DELETE AND SHARE DATA
			SubInfos.transform.Find ("cancel").gameObject.SetActive (true);
			MenuCell.cancelListen (SubInfos.transform.Find ("cancel").gameObject.GetComponent<Button> (), this);
			SubInfos.transform.Find ("onShare").gameObject.SetActive(true);
			MenuCell.onShareListen (SubInfos.transform.Find ("onShare").gameObject.GetComponent<Toggle> (), this);
		} else {
			SubInfos.transform.Find ("cancel").gameObject.SetActive (false);
			SubInfos.transform.Find ("onShare").gameObject.SetActive(false);
		}
		
	}
	//We have to change the button if data type and nb cells change
	public void setButtons(int nbCells){
		int sizeSubInfos = 0; //DEFAUT
		/*if (nbCells > 1) { //MULTIPLE CELLS
			if (data_show != "float")   inf.transform.Find ("value").gameObject.SetActive (false); //NO VALUE FOR NO AVERAGE
		}
		else if (nbCells == 1) { //ONLY ONE OBJECT
			inf.transform.Find ("value").gameObject.SetActive (true); //OBJECT VALUE
		} else { //NO OBJECT SELECTED
			inf.transform.Find ("value").gameObject.SetActive (false); //NO VALUE
		}*/
		if (data_show != "selection" && data_show!="genetic"){
			if (Instantiation.userRight<=1) SubInfos.transform.Find ("Curate").gameObject.SetActive (true); //CURRATION 
		}
	}
	public void showSubInfos(){
		SubInfos.SetActive (!SubInfos.activeSelf);
		MenuCell.reOrganize ();
	}
	//Load Uploaded Color
	public  void loadColor(bool v){
		//Debug.Log ("loadColor for "+this.name+" -> "+ v);
		inf.transform.Find ("show").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
		MenuCell.loadColorListen(inf.transform.Find ("show").gameObject.GetComponent<Button> (),this,!v);
		if (v) {
			if (SelectionCell.clickedCells.Count == 0) { //ALL CELLS
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) {
						Cell c = itemc.Value; 
						c.loadColor (this.id_infos);
					}
				}
			} else { //ONLY SELECTED CELLS
					foreach (Cell c in SelectionCell.clickedCells) 
						c.loadColor (this.id_infos);
				SelectionCell.clearSelectedCells ();
			}
			inf.transform.Find ("show").gameObject.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "hide";
		} else { 
			if (SelectionCell.clickedCells.Count == 0) { //ALL CELLS
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) {
						Cell c = itemc.Value; 
						c.removeUploadColor();
					}
				}
			} else { //ONLY SELECTED CELLS
				foreach (Cell c in SelectionCell.clickedCells) 
					c.removeUploadColor();
				SelectionCell.clearSelectedCells ();
			}
			inf.transform.Find ("show").gameObject.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "apply";
		}

	}

	//When we click on the button
	public void changeColorBar(){
		MenuColorBar.buttonToColorize = inf.transform.Find ("InfosColor").gameObject;
		int type=int.Parse(MenuColorBar.buttonToColorize.transform.Find ("type").gameObject.GetComponentInChildren<Text> ().text);
		float MinV = this.MinInfos;float MaxV = this.MaxInfos;
		if (SelectionCell.clickedCells.Count > 0) { //ONLY ON SELECTED CELLS
			MinV=float.MaxValue;MaxV=float.MinValue;
			foreach (Cell c in SelectionCell.clickedCells) {
				float v;
				if (float.TryParse (c.getInfos (this.id_infos), out v)) {
					if (v > MaxV) MaxV = v;
					if (v < MinV) MinV = v;
				}
			}
		}
		//First we have to check if 
		if (MenuCell.MenuBarColor.activeSelf)
			MenuCell.MenuBarColor.SetActive (false);
		else {
			MenuCell.MenuBarColor.SetActive (true);
			MenuColorBar.assignValues(type,int.Parse (MenuColorBar.buttonToColorize.transform.Find ("min").gameObject.GetComponentInChildren<Text> ().text), int.Parse (MenuColorBar.buttonToColorize.transform.Find ("max").gameObject.GetComponentInChildren<Text> ().text), MinV,MaxV,this.name);
		}
	}

	public void changeColorMap(){
		//Debug.Log ("changeColorMap");
		GameObject buttonToColorize = inf.transform.Find ("InfosColor").gameObject;
		int typecmap=int.Parse(buttonToColorize.transform.Find ("type").gameObject.GetComponentInChildren<Text> ().text);
		int minC=int.Parse (buttonToColorize.transform.Find ("min").gameObject.GetComponentInChildren<Text> ().text);
		int maxC=int.Parse (buttonToColorize.transform.Find ("max").gameObject.GetComponentInChildren<Text> ().text);
        float minV=0; float.TryParse (buttonToColorize.transform.Find ("minV").gameObject.GetComponentInChildren<Text> ().text,out minV);
        float maxV=minV; float.TryParse (buttonToColorize.transform.Find ("maxV").gameObject.GetComponentInChildren<Text> ().text,out maxV);
		if(typecmap>0)
			applyCmap (typecmap,minC,maxC,minV,maxV);
		else 
			cancelCmap ();
	}


	//Apply the changement
	public void applyCmap(int typecmap, int minC,int maxC,float minV,float maxV){
		//Debug.Log("applyCmap from " +minV+" to "+maxV);
		if (SelectionCell.clickedCells.Count == 0) { //ALL CELLS
			for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
				Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
				foreach (var itemc in CellAtT) {
					Cell c = itemc.Value; 
					float v;
					if (float.TryParse (c.getInfos (id_infos), out v)) {
						c.UploadNewColor (MenuColorBar.getColor (typecmap, v, minC, maxC, minV, maxV));
					}
				}
			}
		} else { //ONLY SELECTED CELLS
			foreach (Cell c in SelectionCell.clickedCells) {
				float v;
				if (float.TryParse (c.getInfos (id_infos), out v)) {
					c.UploadNewColor (MenuColorBar.getColor (typecmap, v, minC, maxC, minV, maxV));
				}
			}
			SelectionCell.clearSelectedCells ();
		}
	}
	//Reset Upload Color
	public void cancelCmap(){
		for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
			Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
			foreach (var itemc in CellAtT) {
				Cell c=itemc.Value; 
				c.UploadedColor = false;
				c.updateShader();
			}
		}
	}

	//When we click on a value in the infos we want to change it
	public void changeValue(){
		if (SelectionCell.clickedCells.Count == 1) {
			GameObject nv = inf.transform.Find ("newvalue").gameObject;
			nv.SetActive (true);
			nv.GetComponent<InputField> ().Select ();
			nv.GetComponent<InputField> ().ActivateInputField ();
			nv.GetComponent<InputField> ().text = SelectionCell.clickedCells [0].getInfos(id_infos);
			//nv.GetComponent<InputField> ().onEndEdit.AddListener (delegate { curration(nbI); });
		}
	}

	//When we click on button show for s string (on cell)
	public void showString(bool v){
		isValueShow = v;
		GameObject show = inf.transform.Find ("show").gameObject;
		show.GetComponent<Button> ().onClick.RemoveAllListeners ();
		MenuCell.showStringListen (show.GetComponent<Button> (), this,!v);
		if (v) show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "hide";
		else  show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "show";
		if (v) MenuCell.NbStringOn++;
		else MenuCell.NbStringOn--;
	}
	//SPHERE
	public void applySphere(bool v){
		GameObject show = inf.transform.Find ("show").gameObject;
		show.GetComponent<Button> ().onClick.RemoveAllListeners ();
		MenuCell.applySphereListen (show.GetComponent<Button> (), this,!v);
		if (v) show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "hide";
		else  show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "show";
		if(v)
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
			Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
			foreach (var itemc in CellAtT) {
				Cell c=itemc.Value; 
				string info = c.getInfos (this.id_infos);
				if (info != "")
					c.loadSphere (id_infos, info);
			}
		}
		else
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
				Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
				foreach (var itemc in CellAtT) {
					Cell c=itemc.Value; 
					string info = c.getInfos (this.id_infos);
					if (info != "")
						c.removeObject (id_infos);
				}
			}
	}
	//VECTOR
	public void applyVector(bool v){
        //Debug.Log("applyVector " + v);
		GameObject show = inf.transform.Find ("show").gameObject;
		show.GetComponent<Button> ().onClick.RemoveAllListeners ();
		MenuCell.applyVectorListen (show.GetComponent<Button> (), this,!v);
		if (v) show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "hide";
		else  show.transform.Find ("Text").gameObject.transform.GetComponent<Text> ().text = "show";
		if(v)
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
				Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
                foreach (var itemc in CellAtT){
					Cell c=itemc.Value; 
                    //Debug.Log("applyVector on " + c.ID+ " at "+c.t);
					string info = c.getInfos (this.id_infos);
                   	if (info != "")
						c.loadVector (id_infos, info);
				}
			}
		else
			for(int t=Instantiation.MinTime;t<=Instantiation.MaxTime;t++){
				Dictionary<string, Cell> CellAtT=Instantiation.CellT[t];
				foreach (var itemc in CellAtT) {
					Cell c=itemc.Value; 
					string info = c.getInfos (this.id_infos);
					if (info != "")
						c.removeObject (id_infos);
				}
			}
	}
	//Donwload text infos file in a new window
	public IEnumerator downloadInfos(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "7");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_infos", id_infos.ToString ());
		//Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=7&id_dataset=" + Instantiation.id_dataset.ToString ()+"&id_infos=" + id_infos.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		//ProgressBar.addWWW (www, "Infos List");
		yield return www.Send(); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			if (www.downloadHandler.text != "") {
                string urlinfos = Instantiation.urlSERVER+www.downloadHandler.text;//Instantiation.urlSERVER + "Infos/" + Instantiation.id_dataset + "-" + id_infos + ".txt";
				#if !UNITY_EDITOR
				Scenario.openWindow(urlinfos);
				#else
				Debug.Log ("OPEN " + urlinfos);
				#endif
			}
		}
		www.Dispose (); 
	}


	public IEnumerator cancel(){
		this.remove ();
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "5");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_infos", id_infos.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		//Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=5&id_dataset=" + Instantiation.id_dataset.ToString ()+"&id_infos=" + id_infos.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send(); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			if (www.downloadHandler.text != "") 
				Debug.Log ("Error deletion this correspondence : " + www.downloadHandler.text);
		}
		www.Dispose ();
		Destroy (this.inf);
		MenuCell.deleteCorrespondence(this);
	}


	//CURRATION
	public void addCuration(Curation cur){
		if(this.Curations==null) this.Curations=new List<Curation> ();
		this.Curations.Add (cur);
	}
	public void deleteCuration(Curation cur){
		List<Curation> NewCurations = new List<Curation> ();
		foreach (Curation curr in this.Curations)
			if (curr.id != cur.id)
				NewCurations.Add (curr);
			else {
				cur.delete ();
			}
		this.Curations = NewCurations;

	}




	//SHARE THE CORRESPONDENCE
	public IEnumerator onShare(){
		bool v=SubInfos.transform.Find ("onShare").gameObject.GetComponent<Toggle> ().isOn;
		Debug.Log ("onShare=" + v);
		if (v) this.type = 1;
		else this.type = 2;
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "10");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_infos", id_infos.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		form.AddField ("common", v.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send(); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else 
		www.Dispose ();
	}




	//CLEAR THE DATA
	public void remove(){
		//First we have to inactivate (hide)
		if(datatype=="string") if(this.isValueShow) this.showString(false);
		if(datatype=="sphere") this.applySphere(false);
		if(datatype=="vector") this.applyVector(false);

		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) 
			foreach (var itemc in Instantiation.CellT [t]) {
				Cell c = itemc.Value; 
				c.clearInfos (this.id_infos);
			}
		this.unActive ();
		MenuCell.reOrganize ();
	}
	public int nbCellParsed;
	public int startIndex = 0;
	public string[] lines;
	public bool parse = false;
	public string parseTuple(string infoslist,string infoname){ //TYPE 1 infos load au carchement, 2 upoad, 3 selection 

        Debug.Log("Load " + this.name + " with id " + this.id_infos);
		nbCellParsed=0;
		lines = infoslist.Split ("\n" [0]);
		if (lines.Length >= 1) { //OTHER WISE WE DON'T HAVE ENOUGH INFOS..
			while (lines [startIndex] [0] == '#') {
				//Debug.Log ("Skip comment " + lines [startIndex]);
				startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
			}
			string types = "";
			if (datatype == "") {
				//Debug.Log ("Now " +lines[startIndex]);
				string[] infotypes = lines [startIndex].Split (':');
				types = infotypes [0].Trim ();
				datatype = infotypes [1];
				startIndex++;
			} else {
				types = "type";
				startIndex++;
			}
			if (types != "type")
				Debug.Log ("Error in type definition :" + lines [startIndex]);
			else  { 
				//Debug.Log("found "+datatype+" for "+id_infos);
				//Debug.Log ("Menu Cell Type " + MenuCell.InfosType.Count ());
				if (datatype == "time")  Instantiation.canvas.GetComponent<Instantiation> ().createSpaceTime (id_infos, datatype, type);
				if (datatype == "space") {// Space Relationship (Neigbhors)
					Instantiation.canvas.GetComponent<Instantiation> ().createSpaceTime (id_infos, datatype, type);
					Instantiation.canvas.transform.Find ("MenuClicked").gameObject.transform.Find ("Actions").gameObject.transform.Find ("NeighborsSelect").gameObject.SetActive (true);
				}
				if (datatype == "genetic")  Aniseed.init();
				if (datatype == "group") Tissue.init (this.id_infos,infoname);

				parse = true;
				ProgressBar.addCor(this);
			}
		}
		return datatype;
	}

	int nbLineToParse=200;
	public void parseLine(){
        //if(this.id_infos==885) Debug.Log (this.name + " -parse " + startIndex + "/" + lines.Length + " : "+lines[startIndex]);
		int nbLine = nbLineToParse;
		while(parse && nbLine>0){
			if (datatype == "time") {
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					//Debug.Log ("Lineage " + infos [0]+ " -> " +infos [1]);
					Cell c = Instantiation.getCell (infos [0], true);
					Cell d = Instantiation.getCell (infos [1], true);
					if (c != null && d != null) { //TIME CAN BE OUT OF RANGE
						if (c.t < d.t) c.addDaughter (d);
						if (c.t > d.t) d.addDaughter (c);
						nbCellParsed++;
					}
				}
			}
			if (datatype == "space") {
				string[] infos = lines [startIndex].Split (':');
				//Debug.Log ("TEST Cell " + infos.Count ()+"->"+ lines [i]);
				if (infos.Count () == 2) {
					//Debug.Log ("TEST Cell " + infos [0] + " -> " + infos [1]);
					Cell c = Instantiation.getCell (infos [0], true);
					//if (c != null) Debug.Log ("Found Cell " + infos [0]); else  Debug.Log ("Not Found Cell " + infos [0]);
					Cell n = Instantiation.getCell (infos [1], true);
					//if (n != null) Debug.Log ("Found Cell " + infos [1]); else  Debug.Log ("Not Found Cell " + infos [1]);
					if (c != null && n != null) { //TIME CAN BE OUT OF RANGE
						c.addNeigbhors (n);
						//Debug.Log ("Add Nei Cell " + infos [0] + " -> " + infos [1]);
						nbCellParsed++;
						if (c.Neigbhors != null) c.setInfos (this.id_infos, c.Neigbhors.Count.ToString ());
						else c.setInfos (this.id_infos, "0");
						int v = c.Neigbhors.Count ();
						if (v < MinInfos) MinInfos = v;
						if (v > MaxInfos) MaxInfos = v;
					}
				}
			}

			if (datatype == "genetic") {// Genetic Database 
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) { //TIME CAN BE OUT OF RANGE
						nbCellParsed++;
						float v=0; 
						if(float.TryParse (infos [1], out v)) { }
						string gene = this.name;
						Aniseed.addCellGene (c, gene, v,this);
						c.setInfos (this.id_infos,infos [1]);
					}
				}

			}



			if (datatype == "float") {// float number
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) { //TIME CAN BE OUT OF RANGE
						float v;
						if (float.TryParse (infos [1], out v)) {
							nbCellParsed++;
							c.setInfos (this.id_infos, v.ToString ()); //TODO FOR INFOS AS FLOAT WE HAVE TO PASSE IT AS FLOAT
							if (v < MinInfos) MinInfos = v;
							if (v > MaxInfos) MaxInfos = v;
						}
					}
				}
			}

			if (datatype == "string") {// string 
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) {
						nbCellParsed++;
						c.setInfos (this.id_infos, infos [1]);
					}
				}
			}

			if (datatype == "group") {// fate map 
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () >= 2) { //We can have mutliples sub grous
					//Debug.Log("Look for Cell "+infos [0]+ "and group "+infos[1]);
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) { //TIME CAN BE OUT OF RANGE
						nbCellParsed++;
						string gp = ""; 	for (int ii = 1; ii < infos.Length; ii++) {
							gp += infos [ii];
							if (ii < infos.Length - 1)
								gp +=":";
						}
						c.setInfos (this.id_infos,gp);
					}
				}
			}


			if (datatype == "selection") {//Selection 
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) { //TIME CAN BE OUT OF RANGE
						int selectionnb=-1;
                        //Debug.Log(this.id_infos+ " : "+infos[0] + " -> " + infos[1]);
						if (int.TryParse (infos [1], out selectionnb)) {
							nbCellParsed++;
							c.setInfos (this.id_infos, infos [1]);

						}
					}
				}
			}


			if (datatype == "color") {// r,g,b
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null){
						c.setInfos (this.id_infos, infos [1]);
						nbCellParsed++;
					}
				}

			}

			if (datatype == "sphere") { 
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 2) {
					//Debug.Log ("Found " + lines [i]);
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) {
						c.setInfos (this.id_infos, infos [1]);
						nbCellParsed++;
					}
				}

			}

			if (datatype == "dict") {
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () ==3) {
					Cell c = Instantiation.getCell (infos [0],true);
					Cell c2 = Instantiation.getCell (infos [1],true);
					if (c != null && c2 != null) {
						float v;
						if (float.TryParse (infos [2], out v)) {
							c.setInfos (id_infos, infos [1] + ":" + infos [2]);
							nbCellParsed++;
						}

					}
				}
			}

			if (datatype == "vector") {
				string[] infos = lines [startIndex].Split (':');
				if (infos.Count () == 3) {//Typle:x,y,z,r:x,y,z,r
					Cell c = Instantiation.getCell (infos [0],true);
					if (c != null) {
                        c.setInfos (this.id_infos, infos [1]+":"+infos[2]);
					}
				}
			}
			nbLine--;
			startIndex++;
			if (startIndex >= lines.Length) parse = false;
		}

		if(!parse){ //AT THE END
			if (datatype == "time") {
				//Now We Add Cell Life
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) {
						Cell c = itemc.Value; 
						c.lifePast = c.getPastLife (0);
						c.lifeFutur = c.getFuturLife (0);
						float v = 1 + c.lifePast + c.lifeFutur;
						if (v < MinInfos ) MinInfos = v;
						if (v > MaxInfos ) MaxInfos = v;
						c.setInfos (this.id_infos, v.ToString ());
					}
				}
			}
			if (datatype == "genetic") Aniseed.updateGenesNbCells ();
			if (datatype == "group") Tissue.setTime(Instantiation.CurrentTime);
			lines = null;
			Resources.UnloadUnusedAssets();
			System.GC.Collect();
		}
	}


}


public class MenuCell : MonoBehaviour {


	public static List<CorrespondenceType> CorrespondenceTypes;
	public static List<Correspondence> Correspondences;
	public static int shiftStart=280;
	public static int NbStringOn=-1; //Number of string to visualize

	public static GameObject DefaultCT; //CorrespondenceType Default Object
	public static GameObject MenuInfos;

	//Descrtiptoin 
	public static Text Description;
	public static Text DescriptionID;
	public static Text DescriptionMotherID;
	public static Text DescriptionDaughtersID;
	public static GameObject LineMotherCell;
	public static GameObject LineCellDaughters;
	public static GameObject MenuBarColor;
	public static GameObject NewDataType;

	//Common
	public static GameObject MenuCommon;
	//Comparison
	public static GameObject MenuCalcul;
	//ScrollBar
	public static GameObject scrollbar;

	public static void Init(){
		Correspondences = new List<Correspondence> (); 
		CorrespondenceTypes = new List<CorrespondenceType> ();
		NbStringOn = 0;
		if (MenuInfos == null) {
			if (GameObject.Find ("Canvas")) {
				MenuInfos = GameObject.Find ("Canvas").gameObject.transform.Find ("MenuInfos").gameObject;
				MenuCommon = GameObject.Find ("Canvas").gameObject.transform.Find ("MenuCommon").gameObject;
				MenuCommon.SetActive (false);

				MenuCalcul= GameObject.Find ("Canvas").gameObject.transform.Find ("MenuCalcul").gameObject;
				MenuCalcul.transform.Find ("defaultInfos").gameObject.SetActive (false);
				MenuCalcul.SetActive (false);

				scrollbar=MenuInfos.transform.Find ("Scrollbar").gameObject;
				scrollbar.GetComponent<Scrollbar> ().onValueChanged.AddListener(delegate { MenuCell.onScroll();});
				scrollbar.SetActive (false);

			}
			DefaultCT = MenuInfos.transform.Find ("CorrespondenceType").gameObject;
			DefaultCT.SetActive (false);
		}
		if (Description == null && MenuInfos != null) {
			Description = MenuInfos.transform.Find ("CellDescription").gameObject.transform.GetComponent<Text> ();
			DescriptionID = MenuInfos.transform.Find ("CellID").gameObject.transform.GetComponent<Text> ();
			LineMotherCell = MenuInfos.transform.Find ("LineMotherCell").gameObject;
			LineCellDaughters = MenuInfos.transform.Find ("LineCellDaughters").gameObject;
			DescriptionDaughtersID = MenuInfos.transform.Find ("CellDaughtersID").gameObject.transform.GetComponent<Text> ();
			DescriptionMotherID = MenuInfos.transform.Find ("CellMotherID").gameObject.transform.GetComponent<Text> ();
		}
		MenuBarColor = Instantiation.canvas.transform.Find ("MenuColorBar").gameObject;

		//Create the list to intialilze the order 
		getCorrespondenceType ("Selection");
		getCorrespondenceType ("Quantitative");
		getCorrespondenceType ("Qualitative");
		//getCorrespondenceType ("Genetic");

		//New DataType
		NewDataType=MenuInfos.transform.Find("NewDataType").gameObject;
		NewDataType.SetActive (false);

	}

	//Load data from db
	public static void loadListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.downloadDATA(cor)); }
	//Apply Sphere
	public static void applySphereListen(Button b,Correspondence cor,bool v)  {b.onClick.AddListener (() => MenuCell.applySphere(cor,v)); }
	public static void applySphere(Correspondence cor,bool v){ cor.applySphere (v); }
	//Apply Vector
	public static void applyVectorListen(Button b,Correspondence cor,bool v)  {b.onClick.AddListener (() => MenuCell.applyVector(cor,v)); }
	public static void applyVector(Correspondence cor,bool v){ cor.applyVector (v); }


	//Apply Selection
	public static void applySelectionListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.applySelection(cor)); }
	public static void applySelection(Correspondence cor){
		Dictionary<int,List <Cell>>  ListSelections= new Dictionary<int,List <Cell>> ();
		for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
			foreach (var itemc in CellAtT) {
				Cell c = itemc.Value; 
				var s = c.getInfos (cor.id_infos);
                //Debug.Log("Get Infos " + cor.id_infos + "->" + s);
				if (s != "") {
					int selectionnb=-1;
					if (int.TryParse (s, out selectionnb)) {
                        //Debug.Log("parsed ->" + selectionnb);
						if (Lineage.isLineage) { 
							if(!ListSelections.ContainsKey(selectionnb)) ListSelections[selectionnb]=new List <Cell>();
							ListSelections[selectionnb].Add(c);
						}
						c.addSelection (selectionnb);
					}
				}

			}
		}
		if (Lineage.isLineage){
			foreach (var item in ListSelections) {
				int selectionnb = item.Key;
				List <Cell> lc=item.Value;
				Lineage.applySelectionToCells (SelectionColor.getSelectedMaterial (selectionnb).color,lc);
			}
		}
	}


	//Load Color
	public static void loadColorListen(Button b,Correspondence cor,bool v)  {b.onClick.AddListener (() => MenuCell.loadColor(cor,v)); }
	public static void loadColor(Correspondence cor,bool v){ cor.loadColor (v); }
	//Change Value
	public static void  changeValueListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.changeValue(cor)); }
	public static void changeValue(Correspondence cor){ cor.changeValue (); }
	//Public when we click on a button to see the colorbar
	public static void  changeColorBarListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.changeColorBar(cor)); }
	public static void changeColorBar(Correspondence cor){cor.changeColorBar (); }

	//Change Colormap
	public static void  changeColorMapListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.changeColorMap(cor)); }
	public static void changeColorMap(Correspondence cor){cor.changeColorMap (); }

	//Show string
	public static void  showStringListen(Button b,Correspondence cor,bool v)  {b.onClick.AddListener (() => MenuCell.showString(cor,v)); }
	public static void showString(Correspondence cor,bool v){ cor.showString (v); }

	//Download Infos
	public static void  downloadInfosListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.downloadInfos(cor)); }
	public static void downloadInfos(Correspondence cor){ Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(cor.downloadInfos ()); }

	//Remove correspondence from database
	public static void cancelListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.cancel(cor)); }
	public static void cancel(Correspondence cor){ 
		Confirm.init ();
		Confirm.yes.onClick.AddListener (() => Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (cor.cancel ()));
		Confirm.confirm();
	}
	//Show Sub Infos
	public static void showSubInfosListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.showSubInfos(cor)); }
	public static void showSubInfos(Correspondence cor){ cor.showSubInfos (); }
	//Share Infos
	public static void onShareListen(Toggle b,Correspondence cor)  {b.onValueChanged.AddListener (delegate { MenuCell.onShare(cor);});}
	public static void onShare(Correspondence cor){ Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (cor.onShare ()); }


	//Remove from memory on upload
	public static void removeListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCell.remove(cor)); }
	public static void remove(Correspondence cor){ cor.remove (); }

	//Show Sub Infos
	public static void  deployCTListen(Button b,CorrespondenceType ct)  {b.onClick.AddListener (() => MenuCell.deployCT(ct)); }
	public static void deployCT(CorrespondenceType ct){ ct.deploy (); }



	//////////////////////////// DOWLOAD ALL TYPE OF COORESPONDENCE WE HAVE TO PERFORM  (TEXT OR LINK)
	public static IEnumerator start_Download_Correspondences(){
		//Debug.Log ( "Download Correspondence Type ");
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "1");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		//Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=1&id_people="+Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		//ProgressBar.addWWW (www, "Download Infos");
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			//Debug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
			if (www.downloadHandler.text!= "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				if (N != null && N.Count > 0) {
					///Debug.Log("Found " + N.Count+ " infos");
					for (int i = 0; i < N.Count; i++) { //Number of Infos(as id,infos,field 
						//Debug.Log("N->"+N[i].ToString());
						int id_infos = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						int id_owner=int.Parse (N [i] ["id_people"].ToString ().Replace ('"', ' ').Trim ());
						string infoname = N [i] ["infos"].ToString ().Replace ('"', ' ').Trim ();
						string link = N [i] ["link"].ToString ().Replace ('"', ' ').Trim ();
						uint version = uint.Parse (N [i] ["version"].ToString ().Replace ('"', ' ').Trim ());
						int type = int.Parse (N [i] ["type"].ToString ().Replace ('"', ' ').Trim ());
						string datatype=N [i] ["datatype"].ToString ().Replace ('"', ' ').Trim ();
						if (datatype == "subgroup") datatype = "group";//TEMP
						string uploaddate=N [i] ["date"].ToString ().Replace ('"', ' ').Trim ();
						int common = int.Parse (N [i] ["common"].ToString ().Replace ('"', ' ').Trim ());
						CorrespondenceType ct = getCorrespondenceType (datatype);
						Correspondence co = new Correspondence (Instantiation.id_dataset,ct,id_owner,type,id_infos,infoname,link, version, link == "null",datatype,uploaddate,common);
						ct.addCorrespondence (co);
					}
				}

			} else Debug.Log ("Nothing ...");
		}
		www.Dispose (); 
		reOrganize ();
	}



	
	public static CorrespondenceType getCorrespondenceType(string name){
		if (name == "time" || name=="float" || name=="space") name = "Quantitative";
		if (name == "string" || name=="group") name = "Qualitative";
		if (name == "selection") name = "Selection";
		if (name == "genetic") name = "Genetic";
		if (name != "Selection" && name != "Quantitative" && name != "Qualitative" && name != "Genetic")
			name = "Others";
		foreach (CorrespondenceType ct in CorrespondenceTypes) {
			if (ct.name == name)
				return ct;
		}
		//CREATE A NEW ONE
		CorrespondenceType ctn=new CorrespondenceType(name);
        CorrespondenceTypes.Add(ctn);
		return ctn;
	}

	public static Correspondence getCorrespondence(int id_infos){
		foreach (Correspondence cor in Correspondences)
			if (cor.id_infos == id_infos)
				return cor;
		return null;
	}

	public static List<Correspondence> CorrespondenceToDownload;
	public static void addDownloadDATA(Correspondence cor){
		cor.inf.transform.Find ("load").gameObject.SetActive (false);
		bool startDownload = false; 
		if (CorrespondenceToDownload == null) {
			startDownload = true;
			CorrespondenceToDownload = new List<Correspondence> ();
		}
		CorrespondenceToDownload.Add (cor);
		if (startDownload) startdownloadDATA ();
	}
	public static void downloadDATA(Correspondence cor){
		Debug.Log ("Donwload " + cor.name);
		cor.inf.transform.Find ("load").gameObject.SetActive (false);
		if(cor.txt) Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(downloadDataTXT(cor,false));
		else  Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(downloadDataBundle(cor,false));
	}

	public static void startdownloadDATA(){
		if (CorrespondenceToDownload!=null && CorrespondenceToDownload.Count > 0) {
			Correspondence cor = CorrespondenceToDownload [0];
			//Debug.Log ("Start Download " + cor.name);
			CorrespondenceToDownload.RemoveAt (0);
			if (cor.txt)
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (downloadDataTXT (cor,true));
			else
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (downloadDataBundle (cor,true));
		} else 	CorrespondenceToDownload = null;
	}




	void Update(){
		if (Correspondences != null)
			foreach (Correspondence cor in Correspondences)
				if (cor.parse)
					cor.parseLine ();
		
	}


	//Download all datas for a specific infos
	public static IEnumerator downloadDataTXT(Correspondence cor,bool continu){
		//Debug.Log("Download " + cor.name+" for "+cor.id_dataset); //+ " as " + infotype;
		//Debug.Log("Download start ");
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "2");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_infos", cor.id_infos.ToString ());
		Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=2&id_infos="+cor.id_infos+"&id_dataset=" + Instantiation.id_dataset.ToString ());
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlCorrespondence, form);
		//ProgressBar.addWWW (www,cor.name);
		yield return www.Send (); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			if (www.downloadHandler.text != "") {
                //Debug.Log(www.downloadHandler.data);
				string infoslist = Encoding.UTF8.GetString (Instantiation.Unzip (www.downloadHandler.data));
				//Debug.Log("Download done infoslist="+infoslist);
				cor.startIndex = 0;
				cor.parseTuple (infoslist,cor.name);
				cor.active ();
			} else
				Debug.Log ("Error Loading " + cor.name);
		}
		www.Dispose (); 
		//Debug.Log ("End Download " + cor.name);
		if(continu) startdownloadDATA ();

	}

	//Donwload a Bundle from a url
	public static IEnumerator downloadDataBundle(Correspondence cor,bool continu) 
	{
		//Debug.Log("Download Correspondence  from "+url+ " version="+version);
		while (!Caching.ready)
			yield return null;
		UnityWebRequest www_FBX = UnityWebRequestAssetBundle.GetAssetBundle (cor.link,cor.version,0);
		//ProgressBar.addWWW (www_FBX,cor.name);
		yield return www_FBX.Send ();
		if (www_FBX.isNetworkError) {
			Debug.LogError ("Erreur DL FBX " + www_FBX.error + " from " + cor.link);
			//setComment("ERROR Download Correspondence from " + url);
		} else {
			//Debug.Log("OK Download Correspondence  from "+url);
			AssetBundle bundle_cor = ((DownloadHandlerAssetBundle)www_FBX.downloadHandler).assetBundle;
			string[] AssetNames = bundle_cor.GetAllAssetNames ();

			//Debug.Log ("-> Load   " + AssetNames [0] + ":" + AssetNames.Count ());
			TextAsset corT = (TextAsset)bundle_cor.LoadAsset (AssetNames [0], typeof(TextAsset));
			cor.startIndex = 0;
			cor.parseTuple (corT.text,cor.name);
			Resources.UnloadUnusedAssets();
			bundle_cor.Unload(false);
			bundle_cor = null;
			cor.active ();
		}
		www_FBX.Dispose ();
		//Debug.Log ("End Download " + cor.name);
		if(continu) startdownloadDATA();

	}

	//when we delete a correspondence
	public static void deleteCorrespondence(Correspondence corToRemove){
		//Debug.Log ("TODO Delete correspondence " + corToRemove.name);
		CorrespondenceType ctToRemove = corToRemove.ct;
		corToRemove.isActive = false;
		ctToRemove.removeCorrespondence (corToRemove);
		if (ctToRemove.Correspondences.Count == 0)
			CorrespondenceTypes.Remove (ctToRemove);
		reOrganize ();
	}
	//To visualiy redistribute all the correspondence type and correspondance
	public static void reOrganize(){
        //Debug.Log("reOrganize MenuCels");
		shiftStart=Mathf.RoundToInt(Instantiation.canvasHeight/2f-79);
		if(Correspondences!=null)
			foreach (Correspondence cor in Correspondences)
			if (cor.isActive)
				cor.setButtons (SelectionCell.clickedCells.Count);

		int Height=0;
		inScroll=0;
		if (CorrespondenceTypes != null) {
			foreach (CorrespondenceType ct in CorrespondenceTypes)
				Height += ct.SetPosition (Height);
            //Debug.Log ("Height=" + Height + " -> "+CorrespondenceTypes.Count());
			if (Height > MaxHeight) {
				scrollbar.SetActive (true);
				scrollbar.GetComponent<Scrollbar> ().numberOfSteps = 2 + (Height - MaxHeight) / 22; //DEFINE SCROLLBAR LENGTH
				scrollbar.GetComponent<Scrollbar> ().size = 1f / (float)(2 + (Height - MaxHeight) / 22);
			} else {
				nbSCroll = 0;
				scrollbar.SetActive (false);
				scrollbar.GetComponent<Scrollbar> ().value = scrollbar.GetComponent<Scrollbar> ().numberOfSteps;
			}
		}
	}
	//SCROLLBAR 
	public static int MaxHeight = 600; 
	public static int nbSCroll = 0;
	public static int inScroll=0;
	public static void onScroll(){
		//Debug.Log ("On Scroll "+value+" : "+Instantiation.canvasScale+" ; "+Instantiation.initcanvasScale);
		int nbSteps = scrollbar.GetComponent<Scrollbar> ().numberOfSteps;
		float value = scrollbar.GetComponent<Scrollbar> ().value;
		nbSCroll = (int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		//Debug.Log ("nbSCroll=" + nbSCroll);
		reOrganize ();
	}

	//Given a name of feature return the index in the list
	public static int getInfosIndex(string infoname){
		foreach (Correspondence cor in Correspondences)
			if (cor.name.ToLower ().Trim() == infoname.ToLower ().Trim())
				return cor.id_infos;
		return -1;
	}
	public static string getInfosName(int idx){
		foreach (Correspondence cor in Correspondences)
			if (cor.id_infos == idx)
				return cor.name;
		return "";
	}
	//REturn the id containing the cell name ..
	 public static int getIdInfosForName(){
		int id_infos = getInfosIndex ("name");
		if (id_infos != -1) return id_infos;
		id_infos = getInfosIndex ("names");
		return id_infos;
	}


	
	
	

	


	//WE UPDATE THE SELETED CELL BOX IN SELEXION COLOR
	public void onupdateSelectionOnSelectedCells(bool v){ updateSelectionOnSelectedCells();} //When we click on all times
	public static void updateSelectionOnSelectedCells(){
		GameObject MenuObjects = Instantiation.canvas.transform.Find ("MenuObjects").gameObject;
		GameObject thiscells = MenuObjects.transform.Find ("Selection").gameObject.transform.Find ("thiscells").gameObject;
		bool alltimes = thiscells.transform.Find ("AllTimes").gameObject.transform.GetComponent<Toggle> ().isOn;
		//Count for this cell the number of selection
		List<int> NbSelection = new List<int> ();
		List<Cell> NbCells = new List<Cell> ();
		for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
			Cell sc = SelectionCell.clickedCells [i];
			if (!alltimes) {
				if (!NbCells.Contains (sc))
					NbCells.Add (sc);
				if (sc.selection != null)
					foreach (int s in sc.selection)
						if (!NbSelection.Contains (s))
							NbSelection.Add (s);
			} else {
				List<Cell> cells = sc.getAllCellLife ();
				foreach (Cell c in cells)
					if (!NbCells.Contains (c)) {
						NbCells.Add (c);
						if (c.selection != null)
							foreach (int s in c.selection)
								if (!NbSelection.Contains (s))
									NbSelection.Add (s);
					}
			}
		}
		string nb = NbCells.Count.ToString () + " picked object";
		if (NbCells.Count > 1) nb += "s";
		thiscells.transform.Find ("selectedcells").gameObject.transform.GetComponent<Text>().text=nb;
		thiscells.transform.Find ("selectionnumbber").gameObject.transform.GetComponent<Text>().text=NbSelection.Count.ToString()+" selections";

	}


	//When we click on a cell we update all ingos on the cell Menu
	public static void describe () //THIS THINGS IS ALWAYS CALL ...
	{	
		//Debug.Log ("Describe");
		if (Description != null) {
			Tissue.describe();

			updateSelectionOnSelectedCells ();

			if (SelectionCell.clickedCells.Count == 1) { //ONLY ONE OBJECT
				Cell c = SelectionCell.clickedCells [0];
				LineMotherCell.SetActive (true);LineCellDaughters.SetActive (true);
				DescriptionID.text = c.t + "\n" + c.ID;

				//MOTHERS (while only on mother and this not divide
				if (Instantiation.MinTime != Instantiation.MaxTime) {
					if (c.Mothers != null) {
						List<Cell> past = c.Mothers;
						while (past.Count == 1 && past [0].Daughters != null && past [0].Mothers != null && past [0].Daughters.Count == 1)
							past = past [0].Mothers;
						//Debug.Log (" past.Count=" + past.Count);
						if (past.Count == 0) DescriptionMotherID.text = "X";
						else if (past.Count == 1)
							DescriptionMotherID.text = past [0].t + "\n" + past [0].ID;
						else if (past.Count > 1) {
							DescriptionMotherID.text = "At "+past [0].t + "\n";
							foreach (Cell m in past)
								DescriptionMotherID.text += m.ID + " ";
						} 
					} else
						DescriptionMotherID.text = "X";
				

					//MOTHERS (while only one daughter and this has only one mother
					if (c.Daughters != null) {
						List<Cell> futur = c.Daughters;
						while (futur.Count == 1 && futur [0].Mothers != null && futur [0].Daughters != null && futur [0].Mothers.Count == 1)
							futur = futur [0].Daughters;
						//Debug.Log (" futur.Count=" + futur.Count);
						if (futur.Count == 0) DescriptionDaughtersID.text = "X";
						else if (futur.Count == 1)
							DescriptionDaughtersID.text = futur [0].t + "\n" + futur [0].ID;
						else if (futur.Count > 1) {
							DescriptionDaughtersID.text = futur [0].t + "\n";
							foreach (Cell d in futur)
								DescriptionDaughtersID.text += d.ID + " ";
						} 
					}  else DescriptionDaughtersID.text = "X";
				} else { 
					DescriptionMotherID.text = "";
					DescriptionDaughtersID.text = "X";
				}

					
				if (MenuCurrated.MenuCurration!=null && MenuCurrated.MenuCurration.activeSelf) MenuCurrated.showCuration (); //Update Curration

				//Description.text = SelectionCell.clickedCells [0].DescribeNeigbohrs (); //UPDATE LIST OF NEIGHBORS
				foreach(Correspondence cor in Correspondences){
					//Debug.Log (" cor=" + cor.name +" -> "+SelectionCell.clickedCells [0].getInfos (cor.id_infos));
					cor.inf.transform.Find ("value").transform.gameObject.GetComponent<Text> ().text = SelectionCell.clickedCells [0].getInfos (cor.id_infos);
				}
			} else { //MULTIPLE CELLS
				//CanvasScript.UpdateDescriptions ();//Update upload file text
				LineMotherCell.SetActive (false);LineCellDaughters.SetActive (false);
				DescriptionMotherID.text = "";DescriptionDaughtersID.text = "";
				if (SelectionCell.clickedCells.Count > 1) {
					DescriptionID.text = SelectionCell.clickedCells.Count + " objects";
					resetDescription ();
					//Calcul The average for this selection
					foreach(Correspondence cor in Correspondences){
						if (cor.data_show == "float") {
							float av=0;int nb = 0;
							for (int i = 0; i < SelectionCell.clickedCells.Count; i++){
								string s = SelectionCell.clickedCells [i].getInfos (cor.id_infos);
								if (s != "") { 
									//Debug.Log ("idx="+idx+ " -> Add " + s);
									av += float.Parse (s); 
									nb += 1; 
								}
							}
							if (nb > 0)
								cor.inf.transform.Find ("value").transform.gameObject.GetComponent<Text> ().text =  ""+Mathf.Round ((av / nb) * 1000) / 1000;
							else
								cor.inf.transform.Find ("value").transform.gameObject.GetComponent<Text> ().text = ""; 

						}
						
					}

					if (MenuCurrated.MenuCurration!=null && MenuCurrated.MenuCurration.activeSelf)MenuCurrated.showCuration ();  //Update Curration
				} else { //NO CELLS
					DescriptionMotherID.text = "";DescriptionDaughtersID.text = "";
					DescriptionID.text = "No Objects selected";
					resetDescription ();
					if (MenuCurrated.MenuCurration != null && MenuCurrated.MenuCurration.activeSelf) MenuCurrated.MenuCurration.SetActive (false); //Deasctive menu curration if it was open
				}
			}
		}
	//	reOrganize ();
	}
	

	//Rest the Neigbhors description and detail for each cell
	public static void resetDescription(){
		Description.text = "";
		foreach (Correspondence cor in Correspondences) {
			//Debug.Log ("cor -> " + cor.name);
			cor.inf.transform.Find ("value").transform.gameObject.GetComponent<Text> ().text = "";
		}
	}
		
		


	//UPLOAD NEW CORRESPONDANCE
	public void Upload () {
		#if UNITY_EDITOR
		string path = UnityEditor.EditorUtility.OpenFilePanel("Open text","","");
		if (!System.String.IsNullOrEmpty (path)){
			FileSelected ("file:///" + path+";"+Path.GetFileName(path));
		}
		#else
		//Debug.Log("Open external selectFile");
		Application.ExternalEval ("selectFile()");
		#endif
	}

	void FileSelected (string urlname) {
		Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(uploadData(urlname));
	}
	//Back From Javascript after click on upload
	public void onFinishLoadData(string id_upload_toLoadImmediately){
		Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(start_Download_ID_Correspondences(int.Parse(id_upload_toLoadImmediately)));
	}

	public  IEnumerator start_Download_ID_Correspondences(int id_correspondence){
		//Debug.Log ( "Download Correspondence Type ");
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "9");
		form.AddField ("id_correspondence", id_correspondence.ToString ());
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=9&id_correspondence="+id_correspondence.ToString ()+"&id_people="+Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			//Debug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
			if (www.downloadHandler.text!= "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				if (N != null && N.Count > 0) {
					Debug.Log("Found " + N.Count+ " infos");
					for (int i = 0; i < N.Count; i++) { //Number of Infos(as id,infos,field 
						//Debug.Log("N->"+N[i].ToString());
						int id_infos = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						int id_owner=int.Parse (N [i] ["id_people"].ToString ().Replace ('"', ' ').Trim ());
						string infoname = N [i] ["infos"].ToString ().Replace ('"', ' ').Trim ();
						string link = N [i] ["link"].ToString ().Replace ('"', ' ').Trim ();
						uint version = uint.Parse (N [i] ["version"].ToString ().Replace ('"', ' ').Trim ());
						int type = int.Parse (N [i] ["type"].ToString ().Replace ('"', ' ').Trim ());
						int common = int.Parse (N [i] ["type"].ToString ().Replace ('"', ' ').Trim ());
						string datatype=N [i] ["datatype"].ToString ().Replace ('"', ' ').Trim ();
						string uploaddate=N [i] ["date"].ToString ().Replace ('"', ' ').Trim ();
						CorrespondenceType ct = getCorrespondenceType (datatype);
						Correspondence co = new Correspondence (Instantiation.id_dataset,ct,id_owner,type,id_infos,infoname,link, version, link == "null",datatype,uploaddate,common);
						ct.addCorrespondence (co);
					}
				}

			} else Debug.Log ("Nothing ...");
		}
		www.Dispose (); 
		reOrganize ();
	}



	public IEnumerator uploadData (string urlname) {
		string [] urls = urlname.Split (';');
		string url=urls[0];
		string []filenames=urls[1].Split ('.');
		string filename = "";
		if (filenames.Count() == 1)
			filename = filenames [0];
		else {
			for (int i = 0; i < filenames.Count() - 1; i++) {
				filename += filenames [i];
				if (i < filenames.Count() - 2)
					filename += ".";
			}
		}

		Debug.Log (" URL is " + url);
		#if UNITY_EDITOR
		if(url.IndexOf("file:///")==-1 && url.IndexOf("file://")==0)
			url=url.Replace("file://","file:///");
		url=url.Replace("file:////","file:///");
		#endif
		Debug.Log (" URL is " + url);
		UnityWebRequest www =UnityWebRequest.Get (url);
		yield return www.Send();
		if(www.isNetworkError || www.isHttpError) Debug.Log("ERROR :" +www.error);
		else {
			
			//First We have to get the Type
			string [] textToLoad = www.downloadHandler.text.Split ("\n" [0]);
			int startIndex = 0;
			while (textToLoad [startIndex] [0] == '#') 
				startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
			string types = "";
			string[] infotypes = textToLoad [startIndex].Split (':');
			if (infotypes.Count() != 2)
				Debug.Log ("ERROR UPLOAD uncorrect format type specification " + textToLoad [startIndex]);
			else {
				string datatype = infotypes [1];
				// Show results as text
				//Debug.Log ("Found text " + www.downloadHandler.text);
				WWWForm form = new WWWForm ();
				form.AddField ("hash", Instantiation.hash); 
				form.AddField ("download", "6");
				form.AddField ("infos", filename);
				form.AddField ("type", "2");
				form.AddField ("datatype", datatype);
				form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
				form.AddField ("id_people", Instantiation.IDUser.ToString ());
				form.AddBinaryData ("field", Instantiation.Zip (www.downloadHandler.data));
				UnityWebRequest www2 = UnityWebRequest.Post (Instantiation.urlCorrespondence, form);
				yield return www2.Send (); 
				if (www2.isNetworkError || www2.isHttpError)
					Debug.Log ("ERROR :" + www2.error);
				else {
					int id_infos_upload;
					if (int.TryParse (Encoding.UTF8.GetString (www2.downloadHandler.data), out id_infos_upload)) {
						CorrespondenceType ct = getCorrespondenceType (datatype);
						string uploaddate = DateTime.Now.ToString ();
						Correspondence co = new Correspondence (Instantiation.id_dataset,ct, Instantiation.IDUser, 2, id_infos_upload, filename, "", 1, true, datatype,uploaddate,0);
						ct.addCorrespondence (co);
						reOrganize ();
					} else
						Debug.Log ("ERROR UPLOAD " + www2.downloadHandler.text);
					www2.Dispose (); 
				}
			}
		}
		www.Dispose ();

	}


	public void createNewInfos(){
		//Debug.Log ("createNewInfos->");
		NewDataType.SetActive (true);
		NewDataType.transform.Find ("Dropdown").gameObject.GetComponent<Dropdown> ().value = 0;
		NewDataType.transform.Find ("NameType").gameObject.GetComponent<InputField> ().text = "";

	}
	public void closeNewInfos(){
		NewDataType.SetActive (false);
	}

	public void uploadNewInfos(){
		string NameType = NewDataType.transform.Find ("NameType").gameObject.GetComponent<InputField> ().text;
		int v = NewDataType.transform.Find ("Dropdown").gameObject.GetComponent<Dropdown> ().value; 
		//v=1 pour Selection, 2 pour Quantitative, 3 pour Qualitative

		if (v == 0 || NameType == "")
			Debug.Log ("Upload->" + NameType + " -> " + v);
		else {
			if (v == 1) { //SELECTION
				//CREATE THE FORMAT 
				var morpho = new StringBuilder();
				morpho.Append("#Selection " + NameType + "\n");
				morpho.Append("type:selection\n");
				for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
					Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
					foreach (var itemc in CellAtT) { 
						Cell c = itemc.Value; 
						if (c.selection != null)
							foreach (int s in c.selection)
								morpho.Append(c.t + "," + c.ID + ",0:" + s + "\n");
					}
				}
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (uploadInfos (morpho.ToString(), NameType, "selection")); //SELECTION 
			}
			if (v == 2 || v == 3) {
				string datatype = "float";
				if (v == 3) 	datatype = "string";
				//CREATE THE EMPTY FORMAT 
				string morpho = "#Infos " + NameType + "\n";
				morpho += "type:" + datatype + "\n";
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (uploadInfos (morpho, NameType, datatype)); //SELECTION 
			}
			closeNewInfos ();
		}
	}
	public IEnumerator uploadInfos(string morpho,string name,string datatype){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "6");
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		form.AddField ("infos",  name);
		form.AddField ("type",  "3");
		form.AddField ("datatype", datatype);
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddBinaryData ("field", Instantiation.Zip (System.Text.Encoding.UTF8.GetBytes (morpho)));
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			int id_infos_upload;
			if (int.TryParse (Encoding.UTF8.GetString (www.downloadHandler.data), out id_infos_upload)) {
				CorrespondenceType ct = getCorrespondenceType (datatype);
				string uploaddate = DateTime.Now.ToString ();
				Correspondence co = new Correspondence (Instantiation.id_dataset,ct, Instantiation.IDUser, 2, id_infos_upload, name, "", 1, true, datatype,uploaddate,0);
				ct.addCorrespondence (co);
				reOrganize ();
			} 
		}
		www.Dispose (); 


	}


	//COMMON
	//Donwload List of common dataset
	public void downloadCommonDataset () {
		if (MenuCommon.activeSelf)
			MenuCommon.SetActive (false);
		else{
			//Debug.Log ("downloadCommonDataset");
			MenuCommon.SetActive (true);
			coOtherInfos = null; id_bridge = -1;id_bridge_other = -1;
			MenuCommon.transform.Find ("load").gameObject.SetActive (false);
			MenuCommon.transform.Find ("otherbridge").gameObject.SetActive (false);
			MenuCommon.transform.Find ("otherinfos").gameObject.SetActive (false);
			MenuCommon.transform.Find ("name").gameObject.SetActive (false);
			MenuCommon.transform.Find ("datasets").GetComponent<Dropdown> ().ClearOptions ();
			MenuCommon.transform.Find ("datasets").GetComponent<Dropdown> ().value = 0;
			StartCoroutine (listInfosFromDataset (Instantiation.id_dataset,"thisbridge"));
			StartCoroutine(start_downloadCommonDataset()); //Download all currations from db
		}
	}
	public static List<int> common_id_dataset;
	public static Dictionary<int,string> datasetNames;
	public IEnumerator start_downloadCommonDataset () {
		//Debug.Log ("start_downloadCommonDataset");
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "11");
		form.AddField ("id_dataset_type",  Instantiation.id_type.ToString());
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send();
		if(www.isNetworkError || www.isHttpError) Debug.Log("ERROR :" +www.error);
		else {
			if (www.downloadHandler.text!= "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				if (N != null && N.Count > 0) {
					//Debug.Log("Found " + N.Count+ " dataset");
					List<string> m_DropOptions = new List<string>();
					common_id_dataset= new List<int>();
					datasetNames =new  Dictionary<int,string> ();
					m_DropOptions.Add ("Choose a dataset");
					common_id_dataset.Add(-1);
					for (int i = 0; i < N.Count; i++) { //Number of Infos(as id,infos,field 
						//Debug.Log("N->"+N[i].ToString());
						if (int.Parse (N [i] ["id"]) != Instantiation.id_dataset) {
							m_DropOptions.Add (N [i] ["name"]);
							common_id_dataset.Add (int.Parse (N [i] ["id"]));
							datasetNames [int.Parse (N [i] ["id"])] = N [i] ["name"];
						}
					}
					Dropdown datasets = MenuCommon.transform.Find ("datasets").GetComponent<Dropdown> ();
					datasets.ClearOptions ();
					datasets.AddOptions (m_DropOptions);
				}

			} else Debug.Log ("Nothing ...");
		}
		www.Dispose ();
	}

	public void onSelectCommonDataset(Int32 v){
		int vv=MenuCommon.transform.Find ("datasets").GetComponent<Dropdown> ().value;
		int choose_id_dataset = common_id_dataset [vv];
		if (choose_id_dataset != -1) {
			Debug.Log ("select " + vv + " -> id=" + common_id_dataset [vv]);
			StartCoroutine (listInfosFromDataset (common_id_dataset [vv], "otherbridge"));
			StartCoroutine (listInfosFromDataset (common_id_dataset [vv], "otherinfos"));
		} else {
			MenuCommon.transform.Find ("load").gameObject.SetActive (false);
			MenuCommon.transform.Find ("otherbridge").gameObject.SetActive (false);
			MenuCommon.transform.Find ("otherinfos").gameObject.SetActive (false);
		}
	}

	public List<CorrespondenceOtherData> CorrespondenceThisBridge;
	public List<CorrespondenceOtherData> CorrespondenceOtherBridge;
	public List<CorrespondenceOtherData> CorrespondenceInfos;
	public IEnumerator listInfosFromDataset(int id_dataset,string whichDropdown){
		//Debug.Log ("listInfosFromDataset from "+id_dataset+" for "+whichDropdown);
		if (whichDropdown == "thisbridge") {
			if (CorrespondenceThisBridge == null) CorrespondenceThisBridge = new List<CorrespondenceOtherData> ();
			else CorrespondenceThisBridge.Clear ();
		}
		if (whichDropdown == "otherbridge") {
			if (CorrespondenceOtherBridge == null) CorrespondenceOtherBridge = new List<CorrespondenceOtherData> ();
			else CorrespondenceOtherBridge.Clear ();
		}
		if (whichDropdown == "otherinfos") {
			if (CorrespondenceInfos == null) CorrespondenceInfos = new List<CorrespondenceOtherData> ();
			else CorrespondenceInfos.Clear ();
		}

		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "1");
		form.AddField ("id_dataset",id_dataset.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCorrespondence, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			//Debug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
			if (www.downloadHandler.text!= "") {
				var N = JSONNode.Parse (www.downloadHandler.text);
				if (N != null && N.Count > 0) {
					Debug.Log("Found " + N.Count+ " infos");
					List<string> m_DropOptions = new List<string>();
					if (whichDropdown == "thisbridge") m_DropOptions.Add ("Choose a bridge for this dataset");
					if (whichDropdown == "otherbridge") m_DropOptions.Add ("Choose a bridge for the other dataset");
					if (whichDropdown == "otherinfos") m_DropOptions.Add ("Choose a infos");
					for (int i = 0; i < N.Count; i++) { //Number of Infos(as id,infos,field 
						//Debug.Log("N->"+N[i].ToString());
						int id_infos = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						int id_owner=int.Parse (N [i] ["id_people"].ToString ().Replace ('"', ' ').Trim ());
						string infoname = N [i] ["infos"].ToString ().Replace ('"', ' ').Trim ();
						string link = N [i] ["link"].ToString ().Replace ('"', ' ').Trim ();
						uint version = uint.Parse (N [i] ["version"].ToString ().Replace ('"', ' ').Trim ());
						int type = int.Parse (N [i] ["type"].ToString ().Replace ('"', ' ').Trim ());
						string datatype=N [i] ["datatype"].ToString ().Replace ('"', ' ').Trim ();
						string uploaddate=N [i] ["date"].ToString ().Replace ('"', ' ').Trim ();
						if ((whichDropdown == "thisbridge" || whichDropdown == "otherbridge") && datatype=="string") {
							CorrespondenceOtherData co = new CorrespondenceOtherData (id_dataset,id_owner,type,id_infos,infoname,link, version, link == "null",datatype,uploaddate,i);
							if (whichDropdown == "thisbridge") CorrespondenceThisBridge.Add (co);
							if (whichDropdown == "otherbridge") CorrespondenceOtherBridge.Add (co);
							m_DropOptions.Add (co.name);

						}
						if (whichDropdown == "otherinfos"  && (datatype=="string" || datatype=="float" || datatype=="selection")) {
							CorrespondenceOtherData co = new CorrespondenceOtherData (id_dataset,id_owner,type,id_infos,infoname,link, version, link == "null",datatype,uploaddate,i);
							m_DropOptions.Add (co.name);
							CorrespondenceInfos.Add (co);
						}

					}
					MenuCommon.transform.Find (whichDropdown).gameObject.SetActive (true);
					Dropdown dd = MenuCommon.transform.Find (whichDropdown).GetComponent<Dropdown> ();
					dd.value = 0;
					dd.ClearOptions ();
					dd.AddOptions (m_DropOptions);
				}
			} else Debug.Log ("Nothing ...");
		}
		www.Dispose ();
	}
	public int id_bridge = -1;
	public void onSelectThisBridge(Int32 v){
		int vv=MenuCommon.transform.Find ("thisbridge").GetComponent<Dropdown> ().value;
		if (vv >0 ) {
			id_bridge = CorrespondenceThisBridge[vv-1].id_infos;
			Debug.Log ("Select This Bridge " + id_bridge);
		}else id_bridge = -1;
	}

	public int id_bridge_other = -1;
	public void onSelectOtherBridge(Int32 v){
		int vv=MenuCommon.transform.Find ("otherbridge").GetComponent<Dropdown> ().value;
		if (vv >0 ) {
			id_bridge_other= CorrespondenceOtherBridge[vv-1].id_infos;
			Debug.Log ("Select Other Bridge " + id_bridge_other);
		}else id_bridge_other = -1;
	}
	public CorrespondenceOtherData coOtherInfos = null;
	public void onSelectOtherInfos(Int32 v){
		int vv=MenuCommon.transform.Find ("otherinfos").GetComponent<Dropdown> ().value;
		if (vv > 0) {
			coOtherInfos = CorrespondenceInfos [vv - 1];
			coOtherInfos.name = coOtherInfos.name + "(" + datasetNames [coOtherInfos.id_dataset] + ")";
			Debug.Log ("Select Other Infos " + coOtherInfos.name);
			MenuCommon.transform.Find ("load").gameObject.SetActive (true);
			MenuCommon.transform.Find ("name").gameObject.SetActive (true);
			MenuCommon.transform.Find ("name").GetComponent<InputField> ().text = coOtherInfos.name;
		} else {
			MenuCommon.transform.Find ("load").gameObject.SetActive (false);
			MenuCommon.transform.Find ("name").gameObject.SetActive (false);
			coOtherInfos = null;
		}
	}



	//Add an Infos 
	public void addInfosFromDataset(){
		Debug.Log ("Add Infos" + coOtherInfos.id_infos);
		CorrespondenceType ct = getCorrespondenceType (coOtherInfos.datatype);
		string newname = MenuCommon.transform.Find ("name").GetComponent<InputField> ().text;
		Correspondence co = new Correspondence (coOtherInfos.id_dataset, ct, Instantiation.IDUser,2, coOtherInfos.id_infos,newname,"", 1,true, coOtherInfos.datatype, DateTime.Now.ToString (),0);
		ct.addCorrespondence (co);
		co.inf.transform.Find ("load").gameObject.SetActive (false);//We inactivate the button because we are downlaoad them automatically
		reOrganize ();
		StartCoroutine (createCommonInfos (co));
		MenuCommon.SetActive (false);
	}

	public IEnumerator createCommonInfos(Correspondence cor){
		Debug.Log("this.id_bridge = "+id_bridge);
		Debug.Log("this.id_bridge_other = "+id_bridge_other);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "12");
		form.AddField ("id_bridge", id_bridge.ToString ());
		form.AddField ("id_bridge_other", id_bridge_other.ToString ());
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_dataset_infos", cor.id_dataset.ToString ());
		form.AddField ("id_infos", cor.id_infos.ToString ());
		form.AddField ("infos", cor.name.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		Debug.Log (Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=12&id_bridge="+id_bridge+"&id_bridge_other="+id_bridge_other+"&id_infos="+cor.id_infos+"&id_dataset=" + Instantiation.id_dataset.ToString ()+"&id_dataset_infos=" + cor.id_dataset.ToString ()+"&infos="+ cor.name+"&id_people="+Instantiation.IDUser.ToString());
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlCorrespondence, form);
		yield return www.Send (); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			if (www.downloadHandler.text != "") {
				//Debug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
				cor.id_infos = int.Parse(Encoding.UTF8.GetString (www.downloadHandler.data));
				cor.id_dataset = Instantiation.id_dataset;
				Debug.Log ("cor.id_infos =" + cor.id_infos);
			} else
				Debug.Log ("Error Loading " + cor.name);
		}
		www.Dispose (); 
		MenuCell.downloadDATA(cor);
	}


	//COMPARISON
	public Dictionary<int,GameObject>InfosToCompare;
	public void openMenuCalcul(){
		if (MenuCalcul.activeSelf)
			MenuCalcul.SetActive (false);
		else {
			cleanInfosToCompare ();
			listQualitativeInfos ();
			MenuCalcul.SetActive (true);

		}
	}

	public void cleanInfosToCompare(){
		if(InfosToCompare!=null) {
			foreach(var itc in InfosToCompare){
				int id_infos=itc.Key;
				GameObject go=itc.Value;
				Destroy(go);
			}
			InfosToCompare.Clear();
		}else InfosToCompare=new Dictionary<int,GameObject>();
		MenuCalcul.transform.Find ("function").gameObject.SetActive (false);
		MenuCalcul.transform.Find ("apply").gameObject.SetActive (false);
		MenuCalcul.transform.Find ("functionUnique").gameObject.SetActive (false);
	}

	public void listQualitativeInfos(){
		CorrespondenceType ct = getCorrespondenceType ("Quantitative");
		List<string> m_DropOptions = new List<string>();
		m_DropOptions.Add ("Choose a quantitative infos");
		foreach (Correspondence cor in ct.Correspondences) 
			if(cor.datatype=="float")
				m_DropOptions.Add (cor.name);

		Dropdown dd = MenuCalcul.transform.Find ("newinfos").GetComponent<Dropdown> ();
		dd.value = 0;
		dd.ClearOptions ();
		dd.AddOptions (m_DropOptions);
	}


	public void addQualitativeInfos(){
		int v=MenuCalcul.transform.Find ("newinfos").GetComponent<Dropdown> ().value;
		if (v > 0) {
			CorrespondenceType ct = getCorrespondenceType ("Quantitative");
			Correspondence corSelected = null;
			int vi = 0;
			foreach (Correspondence cor in ct.Correspondences)
				if (cor.datatype == "float") {
					vi += 1;
					if (vi == v)
						corSelected = cor;
				}
			GameObject go = (GameObject)Instantiate (MenuCalcul.transform.Find ("defaultInfos").gameObject,MenuCalcul.transform,false);
			go.transform.Find ("infos").gameObject.GetComponent<Text> ().text = InfosToCompare.Count()+" : "+corSelected.name;
			go.transform.Find ("datatype").gameObject.GetComponent<Text> ().text = corSelected.datatype;
			go.SetActive (true);
			go.name = corSelected.name;
			go.transform.Translate(0, -24*InfosToCompare.Count()*Instantiation.canvasScale, 0, Space.World);
			InfosToCompare [corSelected.id_infos] = go;
			if (InfosToCompare.Count () == 1) {
				MenuCalcul.transform.Find ("functionUnique").gameObject.SetActive (true);
				MenuCalcul.transform.Find ("function").gameObject.SetActive (false);
				MenuCalcul.transform.Find ("apply").gameObject.SetActive (true);
			}
			if (InfosToCompare.Count ()>=2){
				MenuCalcul.transform.Find ("functionUnique").gameObject.SetActive (false);
				MenuCalcul.transform.Find ("function").gameObject.SetActive (true);
				MenuCalcul.transform.Find ("apply").gameObject.SetActive (true);
			}
		}
	}

	public void onChangeFunctionUnique(Int32 v){
		int vv = MenuCalcul.transform.Find ("functionUnique").GetComponent<Dropdown> ().value;
		if (vv > 0) {
			MenuCalcul.transform.Find ("apply").gameObject.SetActive (true);
			CorrespondenceType ct = getCorrespondenceType ("Quantitative");
			string name = "";
			if (vv == 1) name = "Time(";
			if (vv == 2) name = "STDTime(";
			if (vv == 3) name = "SpaceNeigbhors(";
			if (vv == 4) name = "STDSpaceNeigbhors(";
			if (vv == 5) name = "Normalize(";
			foreach (var itc in InfosToCompare) {
				int id_infos = itc.Key;
				foreach (Correspondence cor in ct.Correspondences)
					if (cor.id_infos == id_infos)
						name += cor.name;
			}
			name += ")";
			MenuCalcul.transform.Find ("name").GetComponent<InputField> ().text = name;
		}
	}

	public void onChangeFunction(Int32 v){
		int vv = MenuCalcul.transform.Find ("function").GetComponent<Dropdown> ().value;
		if (vv > 0) {
			CorrespondenceType ct = getCorrespondenceType ("Quantitative");
			string name = "";
			if (vv == 1) name = "Average(";
			if (vv == 2) name = "STD(";
			int i = 0;
			foreach (var itc in InfosToCompare) {
				int id_infos = itc.Key;
				foreach (Correspondence cor in ct.Correspondences)
					if (cor.id_infos == id_infos)
						name += cor.name;
				if (i < InfosToCompare.Count () - 1)
					name += ",";
				i += 1;
			}
			name += ")";
			MenuCalcul.transform.Find ("name").GetComponent<InputField> ().text = name;
		}
	}
	public void calculCompare(){
		Debug.Log ("calculCompare on " + InfosToCompare.Count ());
		if (InfosToCompare.Count () == 1) {
			int vv = MenuCalcul.transform.Find ("functionUnique").GetComponent<Dropdown> ().value;
			if (vv > 0) {
				MenuCalcul.SetActive (false);
				CorrespondenceType ct = getCorrespondenceType ("Quantitative");
				string name = MenuCalcul.transform.Find ("name").GetComponent<InputField> ().text;
				string uploaddate = DateTime.Now.ToString ();
				Correspondence co = new Correspondence (Instantiation.id_dataset, ct, Instantiation.IDUser, 2, -1, name, "", 0, true, "float", uploaddate,0);
				StartCoroutine (createComparisonInfos (co, vv));
				ct.addCorrespondence (co);
				reOrganize ();
			}

		}
		if (InfosToCompare.Count () >= 2) { 
			int vv = MenuCalcul.transform.Find ("function").GetComponent<Dropdown> ().value;
			if (vv > 0) { 
				MenuCalcul.SetActive (false);
				CorrespondenceType ct = getCorrespondenceType ("Quantitative");
				string name = MenuCalcul.transform.Find ("name").GetComponent<InputField> ().text;
				string uploaddate = DateTime.Now.ToString ();
				Correspondence co = new Correspondence (Instantiation.id_dataset, ct, Instantiation.IDUser, 2, -1, name, "", 0, true, "float", uploaddate,0);
				StartCoroutine (createComparisonInfos (co, vv+5)); //5 -> NUMBER OF UNIQUE 
				ct.addCorrespondence (co);
				reOrganize ();
			}
		}
	}

	public IEnumerator createComparisonInfos(Correspondence cor,int vv){
		cor.inf.transform.Find ("load").gameObject.SetActive (false);
		WWWForm form = new WWWForm ();
		string dS = Instantiation.urlCorrespondence + "?hash=" + Instantiation.hash + "&download=13&function=" + vv + "&id_dataset=" + cor.id_dataset + "&infos=" + cor.name + "&id_people=" + Instantiation.IDUser;
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("download", "13");
		form.AddField ("function", vv.ToString());
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("infos", cor.name.ToString ());
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		int i = 0;
		foreach (var itc in InfosToCompare) {
			int id_infos_f = itc.Key;
			form.AddField ("id_infos_" + i.ToString (), id_infos_f.ToString ());
			dS += "&id_infos_" + i.ToString () + "=" + id_infos_f.ToString ();
			i += 1;
		}
		Debug.Log (dS);
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlCorrespondence, form);
		yield return www.Send (); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			if (www.downloadHandler.text != "") {
				Debug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
				cor.id_infos = int.Parse(Encoding.UTF8.GetString (www.downloadHandler.data));
				Debug.Log ("cor.id_infos =" + cor.id_infos);
			} else
				Debug.Log ("Error Loading " + cor.name);
		}
		www.Dispose (); 
		MenuCell.downloadDATA(cor);
	}
}
