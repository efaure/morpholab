﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.Networking;

public class FeedBack : MonoBehaviour {


    public GameObject MenuFeedBack; //This Menu
  
    public InputField comments;
    public Text changement;


		
    //When Menu Feed Back Open 
	public void OnEnable()
    {
		
		StartCoroutine(showComments(Instantiation.IDUser)); //Update the comments
    }

   

    public IEnumerator showComments(int id_user)
    {
        int interligne = 15;
        WWWForm form = new WWWForm();
        form.AddField("hash", Instantiation.hash);
        form.AddField("user", "6");
        form.AddField("id_user", id_user);
		GameObject text = MenuFeedBack.transform.Find("all_comments").gameObject.transform.Find("Comments").GetComponent<ScrollRect>().gameObject.transform.Find("Text").gameObject;
        text.SetActive(true);
        RectTransform text_rect = text.GetComponent<RectTransform>();
        text_rect.sizeDelta = new Vector2(text_rect.rect.width, interligne); //interligne de base 
        //Debug.Log (Instantiation.urlAdmin + "?hash=" + Instantiation.hash + "&user=5&id_user="+id_user);
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAdmin, form);
		yield return www.Send ();
		if(www.isHttpError || www.isNetworkError)Debug.Log("Error : " + www.error);
        else
        {
            string comments = "";
			var N = JSONNode.Parse(www.downloadHandler.text);
            if (N != null && N.Count > 0)
            {
                //Debug.Log("Found " + N.Count + " logs");
                for (int i = 0; i < N.Count; i++)
                { // : ip,date
                    string comment = N[i][0].ToString().Replace('"', ' ').Trim();
                    string date = N[i][1].ToString().Replace('"', ' ').Trim();
                    string com = date + " " + comment + "\n";
                    //Debug.Log("Taille commentaire" + com.Length);
                    int nb_ligne_use = CalculLigne(com.Length);
                    comments += com;
                    //Debug.Log("Jajoute " + (1 + nb_ligne_use) + "nb_lignes");
                    text_rect.sizeDelta = new Vector2(text_rect.rect.width, text_rect.rect.height + (1+nb_ligne_use)*interligne); // On ajoute un interligne a chaque fois  
                }
            }
            text.GetComponent<Text>().text = comments;
        }
		www.Dispose (); 
    }

    public int CalculLigne(int comlength)
    {
        int parameter = 60; // esayer de trouver la formuel exact
        int modulo = comlength / parameter;
        //Debug.Log("Modulo : " + modulo);
        if (comlength > parameter * (modulo+1))
        {
            return modulo + 1;
        }
        else
        {
            return modulo;
        }
    }
		

	public void onChangeComments()
	{
		StartCoroutine(sentComment("comments", comments.text));
	}
	public IEnumerator sentComment(string field, string value)
    {
        if (value != "" && field != "")
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", Instantiation.hash);
			form.AddField("ID", Instantiation.IDUser);
            form.AddField(field, value);
            //Debug.Log(Instantiation.urlUsers + "?hash=" + Instantiation.hash + "&ID=" + Instantiation.IDUser + "&" + field + "=" + value);
			UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlUsers, form);
			yield return www.Send ();
			if(www.isHttpError || www.isNetworkError)  changement.text = "Error : " + www.error;
            else
            {
				string txt = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data, 1, www.downloadHandler.data.Length - 1);
                changement.gameObject.SetActive(true);
                yield return new WaitForSeconds(2);
                changement.gameObject.SetActive(false);
                comments.text = ""; // on remet à 0
            }
			www.Dispose (); 
			StartCoroutine(showComments(Instantiation.IDUser)); //Update the comments
        }
    }
   

}
