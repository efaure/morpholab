﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MenuColorBar : MonoBehaviour {

	public static GameObject MenuBarColor;
	public static Dropdown MapColor;
	public static GameObject Colorbar;
	public static Slider SliderMin;
	public static Slider SliderMax;


	private static int MapColorV;
	private static int MinC;
	private static int MaxC;

	//For the Real Value
	private static Text MinVText;
	private static Text MaxVText;
	private static float MinV;
	private static float MaxV;
	public static Slider SliderMinV;
	public static Slider SliderMaxV;

	public static Text infosNameT;
	public static string infosName;
	public static GameObject buttonToColorize;
	// Use this for initialization
	void Start () {
		MapColorV = 0;
		MenuBarColor = this.transform.gameObject;
		MapColor=MenuBarColor.transform.Find ("ColorMap").gameObject.GetComponent<Dropdown> ();
		Colorbar = MenuBarColor.transform.Find ("Colorbar").gameObject;
		SliderMin = MenuBarColor.transform.Find ("SliderMin").gameObject.GetComponent<Slider> ();
		SliderMax = MenuBarColor.transform.Find ("SliderMax").gameObject.GetComponent<Slider> ();
		//FOR ReallValue
		SliderMinV = MenuBarColor.transform.Find ("SliderMinV").gameObject.GetComponent<Slider> ();
		SliderMaxV = MenuBarColor.transform.Find ("SliderMaxV").gameObject.GetComponent<Slider> ();
		MinVText=MenuBarColor.transform.Find ("MinV").gameObject.GetComponent<Text> ();
		MaxVText=MenuBarColor.transform.Find ("MaxV").gameObject.GetComponent<Text> ();
		infosNameT=MenuBarColor.transform.Find ("InfosText").gameObject.GetComponent<Text> ();

	}
	public static void assignValues(int type,int minC,int maxC,float minV,float maxV,string name){
		Debug.Log ("assignValues from " + minC + " to " + maxC+"  minV="+minV+" maxV="+maxV);
		MapColorV = type;
		MinC = minC;
		MaxC = maxC;
		MinV = minV;
		MaxV = maxV;
		infosName = name;
	}
		

	// TODO CHANGE THAT .... Update is called once per frame
	void Update () {
		if(MapColor!=null && MapColor.value!=MapColorV) MapColor.value=MapColorV;
		if(SliderMin!=null && SliderMin.value!=MinC) SliderMin.value=MinC;
		if(SliderMax!=null && SliderMax.value!=MaxC) SliderMax.value=MaxC;


		if (SliderMinV.minValue != MinV) { SliderMinV.minValue = MinV; SliderMinV.maxValue = MaxV; SliderMinV.value = MinV; }
		if (SliderMaxV.maxValue != MaxV) { SliderMaxV.minValue = MinV; SliderMaxV.maxValue = MaxV; SliderMaxV.value = MaxV; }
		if(MinVText!=null && MinVText.text!=SliderMinV.value.ToString()) MinVText.text=SliderMinV.value.ToString();
		if(MaxVText!=null && MaxVText.text!=SliderMaxV.value.ToString()) MaxVText.text=SliderMaxV.value.ToString();
		if (infosNameT.text != infosName) infosNameT.text = infosName;
	}


	public static Color getColor(int typecmap,float v, int minC,int maxC,float minV,float maxV){
		//Debug.Log ("getColor " + v + " minC=" + minC + " maxC=" + maxC + " minV=" + minV + " maxV=" + maxV);
		Texture2D texty=null;
		Sprite def =MapColor.options [typecmap].image;
		texty = def.texture;
		if (texty != null) {
			int x = minC + (int)Mathf.Round ((maxC - minC) * (v - minV) / (maxV - minV));
			if (x >= texty.width) x = texty.width-1;
			if (x < 0) x = 0;

			return texty.GetPixel (x, 0);
		}
		return new Color (0f, 0f, 0f);
	}

	public void changeSlidersMin(){
		if (SliderMin.value >= SliderMax.value) SliderMin.value = SliderMax.value - 1;
		if (SliderMin.value < 0) SliderMin.value = 0;
		MinC = (int)Mathf.Round(SliderMin.value);
		changeColorMap (MapColorV);
	}
	public void changeSlidersMax(){
		if (SliderMax.value <= SliderMin.value) SliderMax.value = SliderMin.value + 1;
		if (SliderMax.value > 2047) SliderMax.value = 2047;
		MaxC = (int)Mathf.Round(SliderMax.value);
		changeColorMap (MapColorV);
	}

	public void changeSlidersMinV(){
		if (SliderMinV.value > SliderMaxV.value) SliderMinV.value = SliderMaxV.value;
	}
	public void changeSlidersMaxV(){
		if (SliderMaxV.value < SliderMinV.value) SliderMaxV.value = SliderMinV.value ;
	}

	public void changeColorMap(int v){
		MapColorV = v;
		Colorbar.GetComponent<Image>().sprite = AssignSprite(v);
	}

	public Sprite AssignSprite(int v){
		Sprite def = MapColor.options [v].image;
		if (def == null) return null;
		return ExtractSprite(def);
	}
	public Sprite ExtractSprite(Sprite sprite){
		return Sprite.Create (sprite.texture, new Rect (SliderMin.value, 0, SliderMax.value-SliderMin.value, 30), new Vector2 (0.5f, 0.5f));
	}


	public void cancel(){ MenuBarColor.SetActive (false);}

	public string SetColorMapName(int v){ if (v == 0) return "None";return MapColor.options [v].text; }


	public void apply(){
		buttonToColorize.GetComponentInChildren<Text> ().text = SetColorMapName (MapColorV);
		buttonToColorize.GetComponent<Image> ().sprite = AssignSprite (MapColorV);
	
		buttonToColorize.transform.Find ("type").gameObject.GetComponentInChildren<Text> ().text = MapColorV.ToString();
		buttonToColorize.transform.Find ("min").gameObject.GetComponentInChildren<Text> ().text = SliderMin.value.ToString();
		buttonToColorize.transform.Find ("max").gameObject.GetComponentInChildren<Text> ().text = SliderMax.value.ToString();
		buttonToColorize.transform.Find ("minV").gameObject.GetComponentInChildren<Text> ().text = SliderMinV.value.ToString();
		buttonToColorize.transform.Find ("maxV").gameObject.GetComponentInChildren<Text> ().text = SliderMaxV.value.ToString();
		buttonToColorize.transform.Find ("execute").gameObject.GetComponentInChildren<Button> ().onClick.Invoke ();

		//RESET THE OTHER
		foreach(Correspondence cor in MenuCell.Correspondences) {
			if (cor.inf.transform.Find ("InfosColor").gameObject != buttonToColorize) {
				cor.inf.transform.Find ("InfosColor").gameObject.GetComponentInChildren<Text> ().text = SetColorMapName (0);
				//cor.inf.transform.Find ("InfosColor").gameObject.GetComponent<Image> ().sprite =MenuCell.DefaultInfos.transform.Find ("InfosColor").gameObject.GetComponent<Image> ().sprite;;
				cor.inf.transform.Find ("InfosColor").gameObject.GetComponentInChildren<Text> ().text = "";
			}
		}


		MenuBarColor.SetActive (false);
	}
}
