﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine.UI;
using System.Text;

public class Lineage : MonoBehaviour {

	//Lineage in a new winwo
	public static bool isLineage=false;

	public static List <Cell>toPlot;
	// Use this for initialization
	void Start () {
		if (toPlot == null)
			toPlot = new List<Cell> ();
		else
			toPlot.Clear ();
	}

	//Open a new window with the lineage
	public  void showLineage(GameObject st ){
		if (!isLineage) {
			Application.ExternalEval ("openLineage()");
		}
		isLineage = !isLineage;
		if(!isLineage) st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("lineagetree", typeof(Sprite)) as Sprite;
		else st.transform.Find ("show").GetComponent<Image>().sprite = Resources.Load("eyeclose", typeof(Sprite)) as Sprite;
	}
	//When we click on a cell in the lineafge frame
	public static void showCell(string p){
		Cell overcell=Instantiation.getCell(p,false); 

		if (overcell != null) {
			//Debug.Log ("Cell  found ..." + p);
			SelectionCell.clearSelectedCells ();
			SelectionCell.clearClickedCell ();
			SelectionCell.selectedCell = overcell;
			SelectionCell.AddCellSelected (SelectionCell.selectedCell);
			if (Instantiation.CurrentTime != overcell.t)
				Instantiation.setTime (overcell.t);
		} else
			Debug.Log ("Cell not found ..." + p);
	}

	public static void colorize(Cell c, Color col){
		Application.ExternalEval ("colorize(" + c.t + "," + c.ID + ","+Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255)+")"); 
	}

	public static void uncolorize(Cell c){
		Application.ExternalEval ("uncolorize(" + c.t + "," + c.ID +")"); 
	}


	public static void show(Cell c,bool v){
		if (!v) {
			//Debug.Log("hide(" + c.t + "," + c.ID + ")");
			Application.ExternalEval ("hide(" + c.t + "," + c.ID + ")"); 
		} else {
			//Debug.Log("show(" + c.t + "," + c.ID + ")");
			Application.ExternalEval ("show(" + c.t + "," + c.ID + ")");
		}
	}




	public static void AddCellSelected(Cell c){
		Lineage.colorize (c, Color.white);
	}
	public static void RemoveCellSelected(Cell c){
		if (c.UploadedColor) colorize (c,c.UploadColor);
		else if (c.selection == null || c.selection.Count == 0) 
				Lineage.uncolorize (c);
			else 
				Lineage.colorize (c, SelectionColor.getSelectedMaterial (c.selection [0]).color);
	}
	
	public static void hideSelectedCells(bool alltimes){
		if (alltimes) { //ALL TIMES
			StringBuilder listcells = new StringBuilder();
			for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				Cell c = SelectionCell.clickedCells [i];
				listcells.Append (c.t + ";" + c.ID + ":");
			}
			Application.ExternalEval ("hideCellsAllTime(0,\"" + listcells.ToString()+"\")");
		} else { //ONLY THIS TIME
				StringBuilder listcells = new StringBuilder();
				for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				Cell c = SelectionCell.clickedCells [i];
				listcells.Append (c.t + ";" + c.ID + ":");
			}
			Application.ExternalEval ("hideCells(0,\"" + listcells.ToString()+"\")");
		}
	}

	public static void showSelectedCells(bool alltimes){
		StringBuilder listcells = new StringBuilder();
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
		foreach (var itemc in CellAtT) { 
			Cell c = itemc.Value; 
			if(!c.show)listcells.Append (c.t + ";" + c.ID + ":");
		}

		if (alltimes) Application.ExternalEval ("hideCellsAllTime(1,\"" + listcells.ToString()+"\")");
		else Application.ExternalEval ("hideCells(1,\"" + listcells.ToString()+"\")");
	}


	public static void applySelection (Color col){
		StringBuilder listcells = new StringBuilder();
		for (int i = 0; i < SelectionCell.clickedCells.Count; i++) { //For all cells selected
			Cell c=SelectionCell.clickedCells[i];
			listcells.Append(c.t+";"+c.ID+":");
		}
		Application.ExternalEval ("applySelection(" + Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255) + ",\"" + listcells.ToString()+"\")"); 
	}

	public static void applySelectionToCells (Color col,List<Cell> lc){
		StringBuilder listcells = new StringBuilder();
		foreach(Cell c in lc) listcells.Append(c.t+";"+c.ID+":");
		Application.ExternalEval ("applySelection(" + Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255) + ",\"" + listcells.ToString()+"\")"); 
	}

	public static void resetSelectionOnSelectedCells(bool timeisOn){
		StringBuilder listcells = new StringBuilder();
		if (timeisOn) { //ALL TIMES
			for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				List<Cell> cells = SelectionCell.clickedCells [i].getAllCellLife ();
				foreach (Cell c in cells)
					listcells.Append(c.t+";"+c.ID+":");
			}
		} else { //JUST THIS TIME STEP
			for (int i = 0; i < SelectionCell.clickedCells.Count; i++) {
				Cell c=SelectionCell.clickedCells[i];
				listcells.Append(c.t+";"+c.ID+":");
			}
		}
		Application.ExternalEval ("resetSelectionOnSelectedCells(\""+listcells.ToString()+"\")"); 
	}
	public static void clearSelectedCells(){
		StringBuilder listcells = new StringBuilder();
		foreach (Cell c in SelectionCell.clickedCells) {
			if (c.selection != null && c.selection.Count > 0) 
				Lineage.colorize (c, SelectionColor.getSelectedMaterial (c.selection [0]).color);
			else 
				listcells.Append (c.t + ";" + c.ID + ":");

		}
		Application.ExternalEval ("resetSelectionOnSelectedCells(\"" +listcells.ToString()+"\")");
	}

	public static void addAllSelectedCells (){
		StringBuilder listcells = new StringBuilder();
		foreach (Cell c in SelectionCell.clickedCells) 
				listcells.Append (c.t + ";" + c.ID + ":");
		Application.ExternalEval ("applySelection(255,255,255,\"" + listcells.ToString()+"\")"); 
	}

	public static void clearThis (bool timeisOn,int selectValue){
		StringBuilder listcells = new StringBuilder();
		if (timeisOn){
			for (int t = Instantiation.MinTime; t <= Instantiation.MaxTime; t++) {
				Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
				foreach (var itemc in CellAtT) { 
					Cell c = itemc.Value; 
					if (c.selection != null && c.selection.Contains (selectValue))
						listcells.Append (c.t + ";" + c.ID + ":");
				}
			}
		}
		else{
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
			foreach (var itemc in CellAtT) { 
				Cell c = itemc.Value; 
				if (c.selection != null && c.selection.Contains (selectValue))
					listcells.Append (c.t + ";" + c.ID + ":");
			}
		}
		Application.ExternalEval ("resetSelectionOnSelectedCells(\"" +listcells.ToString()+"\")"); 
	}

	public static void clearAll (bool timeisOn){
		if (timeisOn)
			Application.ExternalEval ("clearAll()");
		else {
			StringBuilder listcells = new StringBuilder();
			Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
			foreach (var itemc in CellAtT) { 
				Cell c = itemc.Value; 
				if (c.selection != null && c.selection.Count>0)
					listcells.Append (c.t + ";" + c.ID + ":");
			}
			Application.ExternalEval ("resetSelectionOnSelectedCells(\"" +listcells.ToString()+"\")"); 


		}
	}

	public static void applyhideThis (bool v, bool timeisOn,int selectValue){
		StringBuilder listcells = new StringBuilder();
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
		foreach (var itemc in CellAtT) { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Contains (selectValue))
				listcells.Append (c.t + ";" + c.ID + ":");
		}
		int va = 0; if (v) va = 1;
		if (timeisOn) Application.ExternalEval ("hideAllCells(" +va+",\""+listcells.ToString()+"\")"); 
		else Application.ExternalEval ("hideCellsAllTime(" +va+",\""+listcells.ToString()+"\")"); 
	}

	
	public static void applyhideAll(bool v, bool timeisOn){
		List<int> SelectedValues = getSelectionValues ();
		foreach (int selectValue in SelectedValues)
			applyhideThis (v, timeisOn,selectValue);
	}

	public static void propagateThis (bool v, int selectValue){
		StringBuilder listcells = new StringBuilder();
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
		foreach (var itemc in CellAtT) { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Contains (selectValue)) 
				listcells.Append (c.t + ";" + c.ID + ":");
		}
		int va = 0; if (v) va = 1;
		Color col = SelectionColor.getSelectedMaterial (selectValue).color;
		Application.ExternalEval ("propagateThis(" +va+","+Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255)+",\""+listcells.ToString()+"\")"); 

	}

	public static void propagateAll (bool v){
		List<int> SelectedValues = getSelectionValues ();
		foreach (int selectValue in SelectedValues)
			propagateThis (v, selectValue);
	}

	public static List<int> getSelectionValues(){
		List<int> SelectedValues = new List<int> ();
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [Instantiation.CurrentTime];
		foreach (var itemc in CellAtT) { 
			Cell c = itemc.Value; 
			if (c.selection != null && c.selection.Count > 0) {
				int selectedValue = c.selection [0];
				if (!SelectedValues.Contains (selectedValue))
					SelectedValues.Add (selectedValue);
			}
		}
		return SelectedValues;
	}
	public static List<Cell> _listcells; 
	public static void getCellInGroup(Group gp){
		if (gp.SubGroups != null)
			foreach (Group sg in gp.SubGroups)
					getCellInGroup(sg);
		
		if (gp.Objects != null)
			foreach (GroupObjects sog in gp.Objects)
				if (!_listcells.Contains (sog.c))
						_listcells.Add (sog.c);
			
	}
	public static void colorizeGroup (Group gp){
		int selectionValue = 1;
		if (gp.SubGroups != null)
			foreach (Group sg in gp.SubGroups){
				_listcells = new List<Cell> ();
				getCellInGroup(sg);
				StringBuilder listcells = new StringBuilder();
				foreach(Cell c in _listcells)
					listcells.Append (c.t + ";" + c.ID + ":");
				Color col = SelectionColor.getSelectedMaterial (selectionValue).color;
				Application.ExternalEval ("applySelection("+Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255)+",\""+listcells.ToString()+"\")"); 
				selectionValue++;
		}

		if (gp.Objects != null)
			foreach (GroupObjects sog in gp.Objects){
				colorize(sog.c, SelectionColor.getSelectedMaterial (selectionValue).color);
				selectionValue++;
			}

	}
}
