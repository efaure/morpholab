﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Menus : MonoBehaviour {
	public GameObject MenuObjects;
	public GameObject MenuDataset;
	public GameObject MenuInfos;
	public GameObject MenuLoadSelection;
	public GameObject MenuShortcuts;
	public GameObject MenuGenetic;
	public GameObject MenuScenario;
	public GameObject MenuMovie;
	public GameObject MenuDebug;
	public GameObject MenuGroups;


	public GameObject MenuColor;
	public GameObject MenuColorBar;
	public GameObject MenuShare;
	public GameObject MenuChannel;
	public GameObject MenuAction;

	public GameObject MenuCommon;
	public GameObject MenuCalcul;
	public GameObject MenuCurration;
    public GameObject MenuMutants;
    public GameObject MenuDeregulations;
	public Text Description;

	public GameObject buttonGenetic;


	//ADMIN
	public GameObject buttonDebug;
	public GameObject buttonGroups;
	public GameObject buttonMovie;


	// Use this for initialization
	void Start () {
        hideMenus (MenuShortcuts);
		hideSubMenus ();
		MenuScenario.SetActive (true);
		MenuAction.SetActive (false);
		MenuChannel.SetActive (false);
	}

	public void organizeButtons(){
		GameObject ce=Instantiation.canvas.transform.Find("MenuDataset").gameObject;
		if (ce != null) {
			//Only the owner of the embryo can savetherotation
			GameObject sr = ce.transform.Find ("SaveRotation").gameObject;
			if (sr != null) {
				if (Instantiation.id_owner == Instantiation.IDUser) sr.SetActive (true);
				else sr.SetActive (false);
			}
			//Only the owner can share dataset
			GameObject shr = ce.transform.Find ("shareDataset").gameObject;

			if (shr != null) shr.SetActive (false); //DESACTIVE FOR EVERYONE  (NOW IN HTML)

			GameObject dow=ce.transform.Find ("download").gameObject;
			dow.SetActive (false);


			//Manage Gene DATABASE ACCESS
			if (Instantiation.id_type == 2 || Instantiation.id_type == 3)   buttonGenetic.SetActive (true); else buttonGenetic.SetActive (false);

			//PUBLIC ACCESS
			if (Instantiation.userRight>=2) {
				shr.SetActive (false);
				sr.SetActive (false);
				buttonDebug.SetActive (false);
			}

		}
		buttonGroups.SetActive (false); //Desactive at initialisation

		reOrganizeButtons ();

		#if UNITY_ANDROID
		RectTransform ct = Instantiation.canvas.GetComponent<RectTransform> ();
		Debug.Log ("W=" + ct.sizeDelta.x + " H=" + ct.sizeDelta.y);
		//8 Menu de 80 de hauteur and 1 half size for My account  (10*espace constant 40 fois plus petit 
		float space=0.025f;
		float ratioWH = 4;
		float nbM=8f+0.25f+10f*space;
		int bouttonH=Mathf.FloorToInt(ct.sizeDelta.y/nbM);

		float bouttonW = bouttonH / ratioWH;
		float sx = ct.sizeDelta.y / 2 - space * bouttonH;
		reScaleBoutton ("BoutonDataset", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);

		sx += - bouttonH  - space * bouttonH;
		reScaleBoutton ("BoutonInfos", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);

		sx += - bouttonH  - space * bouttonH;
		reScaleBoutton ("BoutonObjects", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);

        sx += - bouttonH  - space * bouttonH;
        reScaleBoutton ("BoutonMoovie", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);


        sx += - bouttonH  - space * bouttonH;
		reScaleBoutton ("BoutonGenetic", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);

        sx += - bouttonH  - space * bouttonH;
        reScaleBoutton ("BoutonGroups", bouttonH, bouttonW, -ct.sizeDelta.x / 2 + bouttonW, sx);


		reScaleMenu("MenuDataset",ct.sizeDelta.y / 700, -ct.sizeDelta.x / 2 + bouttonW, ct.sizeDelta.y / 2f);
		reScaleMenu("MenuInfos",ct.sizeDelta.y / 700, -ct.sizeDelta.x / 2 + bouttonW, ct.sizeDelta.y / 2f);
		reScaleMenu("MenuObjects",ct.sizeDelta.y / 700, -ct.sizeDelta.x / 2 + bouttonW, ct.sizeDelta.y / 2f);
		reScaleMenu("MenuGenetic",ct.sizeDelta.y / 700, -ct.sizeDelta.x / 2 + bouttonW, ct.sizeDelta.y / 2f);
		reScaleMenu("MenuMovie",ct.sizeDelta.y / 700, -ct.sizeDelta.x / 2 + bouttonW, ct.sizeDelta.y / 2f);
		
		float timeScale = Mathf.Max (ct.sizeDelta.y, ct.sizeDelta.x) / Mathf.Min (ct.sizeDelta.y, ct.sizeDelta.x);
		GameObject TimeBar = Instantiation.canvas.transform.Find ("TimeBar").gameObject;
		TimeBar.transform.localScale = new Vector3 (timeScale,timeScale, 1f);


		GameObject Search = Instantiation.canvas.transform.Find ("MenuClicked").Find ("Search").gameObject;
		GameObject Actions = Instantiation.canvas.transform.Find ("MenuClicked").Find ("Actions").gameObject;
		if(ct.sizeDelta.y<ct.sizeDelta.x){
		    Instantiation.canvas.transform.Find ("MenuClicked").transform.localScale = new Vector3 (timeScale,timeScale, 1f);
		}


		#endif

	}

    public void reOrganizeButtons()
    {
        //NOW WE MAY HAVE TO SHIFT FEW THINGS ....
        int startXButton = Mathf.RoundToInt(Instantiation.canvasHeight / 2f - 277);
        int shiftXButton = -80;

#if UNITY_ANDROID
        //DO NOTHING FOR NOW ...

#else
        //FOR WEBGL 

        //GROUPS
        if (buttonGroups.activeSelf) startXButton += shiftXButton;
        buttonGenetic.transform.position = new Vector3(buttonGenetic.transform.position.x, startXButton * Instantiation.canvasScale, buttonGenetic.transform.position.z);
        if (buttonGenetic.activeSelf) startXButton += shiftXButton;
        buttonMovie.transform.position = new Vector3(buttonMovie.transform.position.x, startXButton * Instantiation.canvasScale, buttonMovie.transform.position.z);
       // Debug.Log("buttonMovie.transform.position.z=" + buttonMovie.transform.position.z);
#endif
    }
	public void hideSubMenus(){
		MenuColor.SetActive (false);
		MenuLoadSelection.SetActive (false);
		MenuColorBar.SetActive (false);
		MenuShare.SetActive (false);
		MenuCurration.SetActive (false);
		MenuChannel.SetActive (false);
		MenuCommon.SetActive (false);
		MenuCalcul.SetActive (false);
        MenuMutants.SetActive(false);
        MenuDeregulations.SetActive(false);
	}

	public void hideMenus(GameObject nothide){
		if(MenuObjects!=nothide) MenuObjects.SetActive (false); else MenuObjects.SetActive (true);
		if(MenuDataset!=nothide) MenuDataset.SetActive (false); else MenuDataset.SetActive (true);
		if(MenuInfos!=nothide) MenuInfos.SetActive (false); else MenuInfos.SetActive (true);
		if(MenuShortcuts!=nothide) MenuShortcuts.SetActive (false); else MenuShortcuts.SetActive (true);
		if(MenuGenetic!=nothide) MenuGenetic.SetActive (false); else MenuGenetic.SetActive (true);
		if(MenuMovie!=nothide) MenuMovie.SetActive (false); else MenuMovie.SetActive (true);
		if(MenuDebug!=nothide) MenuDebug.SetActive (false); else MenuDebug.SetActive (true);
        if (MenuGroups != nothide) MenuGroups.SetActive(false); else MenuGroups.SetActive(true);
    }


	public void MenuObjectsActive(){
		hideSubMenus ();
		if (MenuObjects.activeSelf)
			MenuObjects.SetActive (false);
		else { 
			MenuObjects.transform.Find ("Selection").gameObject.GetComponent<SelectionColor> ().OnEnabled ();
			hideMenus (MenuObjects);
		}
	}
	public void MenuDatasetActive(){
		hideSubMenus ();
		if (MenuDataset.activeSelf)
			MenuDataset.SetActive (false);
		else
			hideMenus (MenuDataset);
	}

	public void MenuInfosActive(){
		hideSubMenus ();
		if (MenuInfos.activeSelf)
			MenuInfos.SetActive (false);
		else { 
			MenuCell.describe ();
			hideMenus (MenuInfos);
		}

	}
	public void MenuShortcutsActive(){
		hideSubMenus ();
		if (MenuShortcuts.activeSelf)
			MenuShortcuts.SetActive (false);
		else { 
			hideMenus (MenuShortcuts);
		}
	}


	public void MenuGeneticActive(){
		hideSubMenus ();
		if (MenuGenetic.activeSelf)
			MenuGenetic.SetActive (false);
		else { 
			hideMenus (MenuGenetic);
		}
	}

	public void MenuScenarioActive(){
		hideSubMenus ();
		if (MenuMovie.activeSelf)
			MenuMovie.SetActive (false);
		
		else 
			hideMenus(MenuMovie);
		
	}

	public void MenuDebugActive(){
		hideSubMenus ();
		if (MenuDebug.activeSelf)
		{
			MenuDebug.SetActive(false);
		}
		else
		{
			hideMenus(MenuDebug);
		}
	}

	public void MenuGroupsActive(){
		hideSubMenus ();
		if (MenuGroups.activeSelf)
		{
			MenuGroups.SetActive(false);
		}
		else
		{
			hideMenus(MenuGroups);
		}
	}

	public void MenuHelpOpen(){
		Application.ExternalEval("window.open(\"HELP/HelpGeneral.html\")");
	}

    

	private void reScaleBoutton(string name,float bouttonH,float bouttonW,float sx,float sy){
		GameObject bttn = Instantiation.canvas.transform.Find (name).gameObject;
		bttn.GetComponent<RectTransform> ().sizeDelta = new Vector2 (bouttonH, bouttonW);
		bttn.transform.localPosition = new Vector3 (sx, sy,0f);
		bttn.transform.Find ("Text").GetComponent<Text> ().fontSize = Mathf.RoundToInt(bouttonH / 5f);
	}

	private void reScaleMenu(string name,float lscale,float sx,float sy){
		GameObject menufr = Instantiation.canvas.transform.Find (name).gameObject;
		menufr.transform.localScale = new Vector3 (lscale, lscale, 1f);
		menufr.transform.localPosition = new Vector3 (sx, sy,0f);
	}



}
