﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using SimpleJSON;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;


public class Curation: MonoBehaviour{
	public int id;
	public Cell c;
	public string value;
	public int id_people;
	public string date;
	public GameObject go;
	public bool isLast;
	public Curation(GameObject goparent,int id ,Cell c,string value,int id_people,string date){
		this.id=id;
		this.c=c;
		this.value=value;
		this.id_people=id_people;
		this.date = date;
		this.isLast = false;
		this.go= (GameObject)Instantiate (MenuCurrated.currationDefault,goparent.transform);
		this.go.name = this.id.ToString ();
		//this.go.transform.SetParent (goparent.transform, false);
		this.go.transform.Find ("people").gameObject.transform.GetComponent<Text> ().text = MenuCurrated.Users [id_people];
		this.go.transform.Find ("date").gameObject.transform.GetComponent<Text> ().text = date;
		this.go.transform.Find ("object").gameObject.transform.GetComponent<Text> ().text = c.getName();
		this.go.transform.Find ("value").gameObject.transform.GetComponent<Text> ().text = value;
	
		MenuCurrated.propageateDescriptionListen (this.go.transform.Find ("propagateFutur").gameObject.GetComponent<Button> (), this, true);
		MenuCurrated.propageateDescriptionListen (this.go.transform.Find ("propagatePast").gameObject.GetComponent<Button> (), this, false);

		this.go.transform.Find ("propagateFutur").gameObject.SetActive (false);
		this.go.transform.Find ("propagatePast").gameObject.SetActive (false);

		MenuCurrated.deleteCurrationListen(this.go.transform.Find ("delete").gameObject.transform.GetComponent<Button> (),this);
		this.go.SetActive (true);
	}

	public void delete(){
		//Debug.Log (" DEestroy " + this.id);
		Destroy (this.go);
	}
	public void SetPosition(int Height){
		this.go.SetActive (true);
		this.go.transform.position=new Vector3(this.go.transform.position.x, Height*Instantiation.canvasScale, this.go.transform.position.z);
	}


	//We change color if it's the last curration
	public void active(Correspondence cor){
		//this.c.setCuration(cor.id_infos, this);
		this.go.transform.Find ("propagateFutur").gameObject.SetActive (true);
		this.go.transform.Find ("propagatePast").gameObject.SetActive (true);
		this.changeColor (new Color (255, 255, 255));
	}
	public void inActive(){
		this.go.transform.Find ("propagateFutur").gameObject.SetActive (false);
		this.go.transform.Find ("propagatePast").gameObject.SetActive (false);
		this.changeColor (new Color (125, 125, 125));
	}
	public void changeColor(Color col){
		this.go.transform.Find ("people").gameObject.transform.GetComponent<Text> ().color = col;
		this.go.transform.Find ("object").gameObject.transform.GetComponent<Text> ().color = col;
		this.go.transform.Find ("date").gameObject.transform.GetComponent<Text> ().color =col;
		this.go.transform.Find ("value").gameObject.transform.GetComponent<Text> ().color = col;
	}
}


public class MenuCurrated : MonoBehaviour {

	public static GameObject MenuCurration;
	public static Dictionary<int, string>Users;
	public static GameObject currationDefault;
	public static Correspondence activeCorrespondence; //When Menu is open, it's easier ...
	public static List<GameObject>  Correspondences; //Listo of game object for each correspondence categories
	public static Text CurationName;
	public static InputField newvalue;
	public static Text NbCell;
	public static GameObject scrollbar;
	public static void init(){
		if (currationDefault == null) {
			MenuCurration = GameObject.Find ("Canvas").gameObject.transform.Find ("MenuCurration").gameObject;
			currationDefault = MenuCurration.transform.Find ("curration").gameObject;
			currationDefault.SetActive (false);
			Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (listUsers ());
			newvalue = MenuCurration.transform.Find ("newvalue").GetComponent<InputField> ();
			CurationName=MenuCurration.transform.Find ("CurationName").GetComponent<Text> ();
			NbCell=MenuCurration.transform.Find ("NbCell").GetComponent<Text> ();
			scrollbar=MenuCurration.transform.Find ("Scrollbar").gameObject;
			scrollbar.GetComponent<Scrollbar> ().onValueChanged.AddListener(delegate { MenuCurrated.onScroll();});
			scrollbar.SetActive (false);
			MenuCurration.transform.Find ("Curate").GetComponent<Button> ().onClick.AddListener (() => MenuCurrated.curate()); 
		} 
	}
	public static GameObject getCorrespondence(Correspondence cor){
		if(Correspondences==null)Correspondences=new List<GameObject>();
		foreach (GameObject gocor in Correspondences)
			if (gocor.name == cor.id_infos.ToString ())
				return gocor;
		//Create a new one
		GameObject dflt=MenuCurration.transform.Find("correspondenceDefault").gameObject;
		GameObject newgocor=(GameObject)Instantiate(dflt);
		newgocor.transform.SetParent (MenuCurration.transform,false);
		newgocor.transform.position = new Vector3 (dflt.transform.position.x, dflt.transform.position.y, dflt.transform.position.z);
		newgocor.name = cor.id_infos.ToString ();
		Correspondences.Add (newgocor);
		return newgocor;
	}

	//List All USers
	public static IEnumerator listUsers(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("user", "2");
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAdmin, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			Users = new Dictionary<int, string> ();
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N != null && N.Count > 0) {
				//Debug.Log("Found " + N.Count+ " users");
				for (int i = 0; i < N.Count; i++) { // : id,name,surname,email,login
					int id_user = int.Parse (N [i] [0].ToString ().Replace ('"', ' ').Trim ());
					string name = N [i] [1].ToString ().Replace ('"', ' ').Trim ();
					string surname = N [i] [2].ToString ().Replace ('"', ' ').Trim ();
					Users[id_user]=surname+" "+name;
				}
			} 
		}
		www.Dispose (); 
	}


	//DOWNLOAD ALL  curration associated to a correspondence
	public static IEnumerator downloadCurations(Correspondence cor){
		//Debug.Log(" downloadCurations for "+cor.id_infos);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("curration", "2");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_correspondence", cor.id_infos.ToString ());
		UnityWebRequest www = UnityWebRequest.Post (Instantiation.urlCurration, form);
		yield return www.Send (); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			//Debug.Log ("Currated : " + wwwCurration.text); 
			if (www.downloadHandler.text != "") {
				//We create a false curration with the original value 
				var N = JSONNode.Parse (www.downloadHandler.text);
				if (N != null && N.Count > 0) {
					//Debug.Log("Found " + N.Count+ " Currated");
					for (int i = 0; i < N.Count; i++) { //id,id_object,value,id_people,date 
							
						//Debug.Log ("infos="+cor.id_infos+ " > NN=" + N [i].ToString ());
						int id = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						string id_object = N [i] ["id_object"].ToString ().Replace ('"', ' ').Trim ();
						string value = N [i] ["value"].ToString ().Replace ('"', ' ').Trim ();
						int id_people = int.Parse (N [i] ["id_people"].ToString ().Replace ('"', ' ').Trim ());
						string date = N [i] ["date"].ToString ().Replace ('"', ' ').Trim ();
						Cell c = Instantiation.getCell (id_object, false);
						if (c != null) {
							GameObject gocor = getCorrespondence (cor);
							Curation cur = new Curation (gocor, id, c, value, id_people, date);
							cor.addCuration (cur);
							setLastCuration (cor, c, cur);

							

							//Debug.Log ("Add Curration for "+cor.datatype+" cell "+c.ID+" value "+value);

						}//else  Debug.Log ("Error this cell does not exist .. " + id_object);
					}
				}
			}
		}
		www.Dispose (); 
	}

	//We remove the previous lasqt 	and set this new one
	public static void setLastCuration(Correspondence cor,Cell c,Curation cur){
		foreach(Curation curr in cor.Curations)
			if(curr.c==c) curr.isLast=false;
		cur.isLast=true;
		c.setCuration (cor.id_infos, cur);
		if (cor.datatype == "genetic") 
			Aniseed.addCellGene (c, cor.name, float.Parse(cur.value), cor);
		//Debug.Log ("For "+c.ID+" -> Last is " + cur.id);
	}

	//CREATE A NEW CURRATION Listen to curate button
	//public static void curateListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCurrated.curate(cor)); }
	public static void curate(){ 
		foreach (Cell c in SelectionCell.clickedCells) 
				Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(MenuCurrated.addCuration (activeCorrespondence,c));
		newvalue.text = "";
	}

	///Add a curation in the DB (from the input filed)
	public static IEnumerator addCuration(Correspondence cor,Cell c,string value=""){
		activeCorrespondence = cor;
		//Debug.Log (" Add A curration in the DB for " + c.getName ());
		if(value=="") value= newvalue.text;
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("curration", "1");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id_correspondence", cor.id_infos.ToString ());
		form.AddField ("id_object", c.getName());
		form.AddField ("id_people", Instantiation.IDUser);
		form.AddField ("value", value);
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCurration, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			int id_curation;
			if (int.TryParse (Encoding.UTF8.GetString (www.downloadHandler.data), out id_curation)) {
				GameObject gocor = getCorrespondence (cor);
				Curation cur = new Curation (gocor,id_curation,c, value, Instantiation.IDUser, DateTime.Now.ToString ());
				cor.addCuration (cur);
				setLastCuration (cor, c, cur);
			} else
				Debug.Log ("ERROR Curration " + www.downloadHandler.text);
		}
		www.Dispose (); 
		showCuration();
	}


	//SHOW   Menu Curration click
	public static void showCurationListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCurrated.openCuration(cor)); }
	//Show list of curration when click on menu
	public static void openCuration(Correspondence cor){ 
		//Debug.Log ("openCuration==" + cor.name+"->"+cor.id_infos);
		newvalue.text = "";
		CurationName.text = cor.name;
		if (MenuCurration.activeSelf) {//Menu Already Open, we close it only if the coorespondence ar the same
			if (activeCorrespondence.id_infos != cor.id_infos) {
				activeCorrespondence = cor;
				MenuCurration.SetActive (true);
				showCuration ();
			} else
				MenuCurration.SetActive (false);
		}else{ //Else we just open it
			activeCorrespondence = cor;
			MenuCurration.SetActive (true);
			showCuration ();
		}
	}

	public static void showCuration(){ 
		//We We only active the correspondence GameOjbect
		if (SelectionCell.clickedCells.Count == 1)
			NbCell.text = "object " + SelectionCell.clickedCells [0].getName ();
		else
			NbCell.text = SelectionCell.clickedCells.Count.ToString () + " objects";
		//Debug.Log ("activeCorrespondence="+activeCorrespondence.id_infos);
		if (Correspondences != null)
			foreach (GameObject gocir in Correspondences)
				if (gocir.name == activeCorrespondence.id_infos.ToString ()) {
					//Debug.Log ("OK FOR gocir.name=" + gocir.name);
					gocir.SetActive (true);
				} else {
					//Debug.Log ("NO FOR gocir.name=" + gocir.name);
					gocir.SetActive (false);
				}
		nbSCroll = 1;
		if (activeCorrespondence.Curations != null)
			updateShowCuration ();
		else
			scrollbar.SetActive (false);
	
	}

	public static void updateShowCuration(){
		int nbCurated = 0;
		int startY = 220;
		foreach (Curation cur in activeCorrespondence.Curations) {
			//Debug.Log ("Test c" + cur.c.getName ());
			if (SelectionCell.clickedCells.Contains (cur.c)) {
				nbCurated += 1;
				if(nbCurated>=nbSCroll && nbCurated<nbSCroll+maxNbDraw){
					cur.SetPosition (startY);
					startY -= 20;
					if (cur.isLast) cur.active (activeCorrespondence);
					else  cur.inActive ();
					//Debug.Log ("FOUND c" + cur.c.getName ()+" -> lastCur "+lastCur.id);
				}else
					cur.go.SetActive (false);
			} else
				cur.go.SetActive (false);
		}
		if (nbCurated > maxNbDraw) {
			scrollbar.SetActive (true);
			scrollbar.GetComponent<Scrollbar> ().numberOfSteps = 1 + nbCurated - maxNbDraw; //DEFINE SCROLLBAR LENGTH
			scrollbar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + nbCurated - maxNbDraw);
		} else {
			scrollbar.SetActive (false);
			nbSCroll = 1;
		}


	}
	//FOR SCROLL BAR
	public static  int maxNbDraw = 10;
	public static float sizeBar = 20f;
	public static int nbSCroll = 1;
	public static void onScroll(){
		//Debug.Log ("On Scroll "+value+" : "+Instantiation.canvasScale+" ; "+Instantiation.initcanvasScale);
		int nbSteps = scrollbar.GetComponent<Scrollbar> ().numberOfSteps;
		float value = scrollbar.GetComponent<Scrollbar> ().value;
		nbSCroll = nbSteps-(int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		updateShowCuration ();
	}
		


	//Propagate a string descriont
	public static void  propageateDescriptionListen(Button b,Curation cur,bool v)  {b.onClick.AddListener (() => MenuCurrated.propageateDescription(cur,v)); }
	public static void propageateDescription(Curation cur,bool futur){  //Futur=true, Past=False
		Cell c = cur.c;
		Debug.Log ("Propagate" + cur.c.ID+ "to "+futur);
		if (futur) { //Futur
			while (c.Daughters!=null && c.Daughters.Count == 1 && c.Daughters[0].Mothers.Count==1) { //Only One Daughter
				Cell d=c.Daughters[0];
				if (d.getCuration(activeCorrespondence.id_infos) != cur.value) {
					d.setCuration (activeCorrespondence.id_infos,  cur);
					Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (addCuration (activeCorrespondence, d, cur.value));
				}
				c = d;

			}
		} else { //PAST
			while (c.Mothers!=null && c.Mothers.Count == 1 && c.Mothers[0].Daughters.Count==1) { //Only One Mother and never divide
				Cell m=c.Mothers[0];
				if (m.getInfos (activeCorrespondence.id_infos) !=  cur.value) {
					m.setCuration (activeCorrespondence.id_infos,  cur);
					Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine (addCuration (activeCorrespondence, m, cur.value));
				}
				c = m;
			}
		}
	}

	//DELETE A curration
	public static void deleteCurrationListen(Button b,Curation cur) {b.onClick.AddListener (() => Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(deleteCurration (cur)));}
	public static IEnumerator deleteCurration(Curation cur){
		//Debug.Log ("deleteCurration for " +cur.id);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("curration", "3");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
		form.AddField ("id", cur.id.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlCurration, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)Debug.Log ("Error : " + www.error); 
		www.Dispose (); 

		//WE need to find the last curration for this cell 
		if (activeCorrespondence.Curations != null && activeCorrespondence.Curations.Count > 0) //Maybe not currations ...
			foreach (Curation curr in activeCorrespondence.Curations) //Un peu barbard tout ça ... on peut le faire que sur la dernière 
				if(curr.id!=cur.id && curr.c==cur.c)
					setLastCuration(activeCorrespondence,curr.c,curr);
		else
			cur.c.deleteCuration (activeCorrespondence.id_infos); //Remove the Curation in the cell


		activeCorrespondence.deleteCuration (cur); //Remove the curration from the correspondence
		showCuration (); 

	}
}
