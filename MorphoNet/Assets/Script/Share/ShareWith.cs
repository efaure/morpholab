﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Text;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class ShareWith : MonoBehaviour
{
	public GameObject guy;
	public GameObject MenuShare;
	public List<GameObject> Guys= new List<GameObject> ();
	public GameObject scrollBar;
	public string basename;
	public int id_base;
	public void Start(){
		guy.SetActive (false);
		scrollBar.SetActive (false);
	}
	public void listPeople(){
		if(Guys.Count==0)  StartCoroutine (queryListPeople ()); //We do it only once...
		else StartCoroutine (whoSharing ());
	}
		
	//FOR SCROLL BAR
	public  static int maxNbGuys = 15;
	public  static float sizeBar = 20f;
	public  void setScrollBar(){
		int height = Guys.Count;
		//Debug.Log ("height=" + height+" static="+maxNbGuys);
		if (height > maxNbGuys) {
			scrollBar.SetActive (true);
			//DEFINE SCROLLBAR LENGTH
			scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1 + height - maxNbGuys;
			scrollBar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + height - maxNbGuys);

		} else {
			scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1;
			scrollBar.GetComponent<Scrollbar> ().size = 1f;
			scrollBar.SetActive (false);
		}
		onSroll (0);
	}


	public void onSroll(Single value){
		int nbSteps = scrollBar.GetComponent<Scrollbar> ().numberOfSteps;
		int decals = (int)Mathf.Round(value*nbSteps);
		//Debug.Log ("value=" + value + " nbSteps=" + nbSteps + " decals=" + decals);
		int startScroll = 0;
		int shiftStart=Mathf.RoundToInt(Instantiation.canvasHeight/2f-99);
		foreach (GameObject guy in Guys) {
			if (startScroll >= decals && startScroll < decals + maxNbGuys) {
				guy.SetActive (true);
				guy.transform.position=new Vector3(guy.transform.position.x, (shiftStart-(startScroll-decals)*sizeBar)*Instantiation.canvasScale, guy.transform.position.z);
			}
			else
				guy.SetActive (false);
			startScroll += 1;
		}
	}
		

	//Check the uploaded data in the DB
	public  IEnumerator queryListPeople () {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("share", "1");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		Debug.Log (Instantiation.urlShare + "?hash=" + Instantiation.hash + "&share=1&id_people=" + Instantiation.IDUser.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlShare, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		 else {
			//First we have to remov all insert guy
			GameObject all = (GameObject)Instantiate(guy);
			all.transform.SetParent(MenuShare.transform,false);
			all.SetActive (true);
			all.transform.Find("Name").GetComponentInChildren<Text>().text = "public";
			all.transform.Find ("id").GetComponentInChildren<Text> ().text = "0";
			Guys.Add (all);
			float maxWidth = 0;

			//Return id,name,surname 
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N!=null && N.Count > 0) {
				//Debug.Log("list guys " + N.Count);

				for (int i = 0; i < N.Count; i++) { //Number of Uploaded Data
					//Debug.Log("N="+N[i].ToString());
					int id_people=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
					string name = N [i] [1].ToString ().Replace('"',' ').Trim();
					string surname = N [i] [2].ToString ().Replace('"',' ').Trim();
					string Labo = N [i] [3].ToString ().Replace('"',' ').Trim();
					string Team = N [i] [4].ToString ().Replace('"',' ').Trim();
					string Institution = N [i] [5].ToString ().Replace('"',' ').Trim();
					GameObject guys = (GameObject)Instantiate(guy);
					guys.transform.SetParent(MenuShare.transform,false);
					guys.transform.Translate(0, -(i+1)*sizeBar*Instantiation.canvasScale, 0, Space.World);
					guys.SetActive (true);
					guys.transform.Find("Name").GetComponentInChildren<Text>().text = surname + " " + name+", "+Team+", "+Labo +", "+Institution ;
					guys.transform.Find ("id").GetComponentInChildren<Text> ().text = id_people.ToString ();
					Guys.Add (guys);

					maxWidth = Math.Max(maxWidth,guys.transform.Find("Name").GetComponent<Text> ().preferredWidth);


				}
			} 
			//Debug.Log ("maxWidth=" + maxWidth+" ->"+N.Count+"/"+maxNbGuys);
			maxWidth += 55;
			//Debug.Log ("" + N.Count +">"+ maxNbGuys);
			if(N.Count>maxNbGuys)
				MenuShare.GetComponent<RectTransform>().sizeDelta = new Vector2( maxWidth, maxNbGuys*20+50);
			else 
				MenuShare.GetComponent<RectTransform>().sizeDelta = new Vector2( maxWidth, N.Count*20+100);
			setScrollBar();

		}
		www.Dispose (); 
		StartCoroutine (whoSharing ());
	}

	public  IEnumerator whoSharing() {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("share", "2");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("base", basename);
		form.AddField ("id_base", id_base.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlShare, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			
			foreach (GameObject guy in Guys) guy.GetComponent<Toggle> ().isOn=false; //Reset
			string id_people_list=www.downloadHandler.text;
			string[] id_people_lists = id_people_list.Split (';');
			for (int i = 0; i < id_people_lists.Length; i++) {
				if (id_people_lists [i] != "") {
					foreach (GameObject guy in Guys) {
						int id_people = int.Parse (guy.transform.Find ("id").GetComponentInChildren<Text> ().text);
						if (id_people.ToString () == id_people_lists [i])
							guy.GetComponent<Toggle> ().isOn = true;
					}
				
				}
			}

		}
		www.Dispose (); 
	}


	//When click on share...
	public void shareWith(){
		string ListShared = "";
		foreach (GameObject guy in Guys) {
			if (guy.GetComponent<Toggle> ().isOn) {
				int id_people = int.Parse (guy.transform.Find ("id").GetComponentInChildren<Text> ().text);
				ListShared += id_people.ToString () + ";";
			}
		}
		StartCoroutine (sharing (ListShared));
	}

	public  IEnumerator sharing(string  id_people_list) {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("share", "3");
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		form.AddField ("base", basename);
		form.AddField ("id_base", id_base.ToString());
		if(id_people_list!="") form.AddField ("id_people_list", id_people_list);
		//Debug.Log (Instantiation.urlShare + "?hash=" + Instantiation.hash + "&share=3&base="+basename+"&id_base=" + id_base.ToString ()+"&id_people_list="+id_people_list+"&id_people="+Instantiation.IDUser.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlShare, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			if (www.downloadHandler.text!= "")
				Debug.Log ("Error sharing " + www.downloadHandler.text);
			
		}
		www.Dispose (); 
		MenuShare.SetActive(false);
	}

}

