﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using SimpleJSON;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using AssemblyCSharp;

public class Scenario : MonoBehaviour {

	public GameObject MenuScenario;
	public GameObject MenuActions;
	public GameObject MenuShare;
	public GameObject defaultScenario;
	public GameObject defaultAction;
	public GameObject scrollActionGO;
	public Scrollbar scrollAction;
	public static Dictionary<int , GameObject> Scenarios=new Dictionary<int , GameObject>() ;
	public static Dictionary<int , Action > Actions;
	public static bool actionDone=false;
	public static int actionCurrent;
	public Text currenttextloaded;

	public GameObject framerate;
	public GameObject loadingtext;
	public GameObject BoutonDataset;
	public GameObject BoutonInfos;
	public GameObject BoutonObjects;
	public GameObject BoutonHelp;
	public GameObject BoutonAniseed;
	public GameObject BoutonMovie;
	public GameObject BoutonUsers;
	public GameObject BoutonDebug;
	public GameObject PreviousStep;
	public GameObject BoutonGroups;
	public GameObject NexStep;
	public GameObject MenuClicked;


	public GameObject hologramO;


	public Camera ShotCamera;
	// Use this for initialization
	void Start () {
		Scenarios=new Dictionary<int , GameObject>() ;
		actionDone=false;
		//openWindow (""); //We have to initialize it first
		defaultScenario.SetActive (false);
		//StartCoroutine (checkScenarios ());
		//NetworkServer.Listen(4444);	
		//NetworkServer.RegisterHandler(MsgType.Ready, OnPlayerReadyMessage);
		if(Instantiation.IDUser==1) hologramO.SetActive(true);else hologramO.SetActive(false);

		if (Instantiation.userRight>=2) {//PULBIC 
			MenuActions.transform.Find("save").gameObject.SetActive(false);
			MenuActions.transform.Find ("cancel").gameObject.SetActive (false);
		}
	}
	public void OnEnable(){
		StartCoroutine (checkScenarios ());
	}

	public void hideThings(bool v,bool menuws){
		framerate.SetActive (v);
		loadingtext.SetActive (v);
		BoutonDataset.SetActive (v);
		BoutonInfos.SetActive (v);
		BoutonObjects.SetActive (v);
		BoutonHelp.SetActive (v);
		BoutonAniseed.SetActive (v);
		BoutonMovie.SetActive (v);
		BoutonGroups.SetActive (v);
		PreviousStep.SetActive (v);
		NexStep.SetActive (v);
		MenuClicked.SetActive (v);

		if (v) { //Reactive
			if (menuws) {//From Record)
				MenuActions.SetActive (true);
				MenuScenario.SetActive (false);
			} else { //From Snapshot
				MenuActions.SetActive (false);
				MenuScenario.SetActive (true);
			}
		} else {
			MenuActions.SetActive (v);
			MenuScenario.SetActive (v);
		}
	}

	/// ////////////SNAPSHOT 

	//Create a screen shot and export it
	public void screenshot(){
        string filename = "user_" + Instantiation.IDUser.ToString()+".png";// + "_screenshot" + System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss") + ".png";
         StartCoroutine (TakeScreenShot (filename));
	}


	public IEnumerator TakeScreenShot(string filename)
	{
		hideThings (false,false);
       	yield return new WaitForEndOfFrame();
		var tex = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false ); // Create a texture the size of the screen, RGB24 format
		tex.ReadPixels( new Rect(0, 0, Screen.width, Screen.height), 0, 0 ); // Read screen contents into the texture
		tex.Apply();
		byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
		Destroy( tex );
       	WWWForm form = new WWWForm();
		form.AddField ("movie", 0);
		form.AddField("name", filename);
		form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		 else {
#if !UNITY_EDITOR
				openWindow(Instantiation.urlSERVER+"Snapshot/"+filename);
#endif
  
		}
		www.Dispose ();
		hideThings (true,false);
	}



	//Call Open Window in Javascript (Plugins/WebGL)
	[DllImport("__Internal")]
	public static extern void openWindow(string url);


	/// ///////////////////MOVIE
	private static bool record=false;
	private static int recordAction = 0;
	public static int id_scenario_active=-1;
	public static int id_scenario_owner=-1;
	public Toggle hologram;
	public static int nbHoloAngle=0;

	public void onRecord(){StartCoroutine (createRecord ());}
	public IEnumerator createRecord(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("movie", "1");
		form.AddField ("id_user", Instantiation.IDUser.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			if(Actions.Count>0){
				hideThings (false,true);
				actionCurrent = 0;
				currentAction = Actions [actionCurrent];
				ScenarioPlay = false;
				actionDone = true;
				stepFrame = 0;
				record = true;
				recordAction = 2; 
				frameNB = 0;
			}
		}
		www.Dispose ();
	}
	int frameNB=0;
	public IEnumerator frameRecord(){
		//currenttextloaded.text = "Record frame "+actionNB+"/"+lastAction;
		//Debug.Log (currenttextloaded.text);
		yield return new WaitForEndOfFrame();
		var tex = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false ); // Create a texture the size of the screen, RGB24 format
		tex.ReadPixels( new Rect(0, 0, Screen.width, Screen.height), 0, 0 ); // Read screen contents into the texture
		tex.Apply();
		byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
		Destroy( tex );
		WWWForm form = new WWWForm();
		form.AddField ("movie", 2);
		form.AddField("name", "movie_"+frameNB+".png");
		form.AddField ("id_user", Instantiation.IDUser.ToString());
		form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		www.Dispose ();
		frameNB++;
		recordAction = 2;

	}

	public IEnumerator frameRecordHologram(){
		//currenttextloaded.text = "Record frame "+frameNB+" Angle "+nbHoloAngle;
		//Debug.Log (currenttextloaded.text);
		//if(nbHoloAngle>0) ArcBall.rotate (Quaternion.AngleAxis (nbHoloAngle*90f, new Vector3 (0, 1f, 0))*Quaternion.AngleAxis (-90f, new Vector3 (1f, 0, 0)));
		//else  ArcBall.rotate (Quaternion.AngleAxis (-90f, new Vector3 (1f, 0, 0)));

		yield return new WaitForEndOfFrame();
		var tex = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false ); // Create a texture the size of the screen, RGB24 format
		tex.ReadPixels( new Rect(0, 0, Screen.width, Screen.height), 0, 0 ); // Read screen contents into the texture
		tex.Apply();
		byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
		Destroy( tex );
		WWWForm form = new WWWForm();
		form.AddField ("movie", 4);
		form.AddField("name", "movie_"+frameNB+"_"+nbHoloAngle+".png");
		form.AddField ("id_user", Instantiation.IDUser.ToString());
		form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError) Debug.Log("Error : " + www.error); 
		www.Dispose ();
		nbHoloAngle++;
		ArcBall.rotate (Quaternion.Euler(new Vector3(0f, nbHoloAngle*90f, 0f)));
		//ArcBall.addRotate(Quaternion.Euler(new Vector3(0f, 90f, 0f)));
		//ArcBall.addRotate (Quaternion.AngleAxis (90f, Vector3.up));

		recordAction = 0;
		if(nbHoloAngle==4){
			nbHoloAngle = 0;
			frameNB++;
			recordAction = 2;
		}
		//Perform the rotation


		Debug.Log ("END Record frame "+frameNB+" Angle "+nbHoloAngle);

	}

	public IEnumerator finishRecord(){
		loadingtext.SetActive (true);
		loadingtext.GetComponent<Text>().text = "Wait a litte for your movie...";
		Debug.Log ("Save record");
		record = false;
		recordAction = -1;
		actionDone = false;
		WWWForm form = new WWWForm();
        string outputname="Movie_"+Instantiation.IDUser.ToString()+ ".mp4";//+"_"+System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss")+".mp4";
		form.AddField ("movie", 3);
		form.AddField ("id_user", Instantiation.IDUser.ToString());
		form.AddField ("framerate", 10);
		form.AddField ("outputname", outputname);
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError) Debug.Log ("Error : " + www.error); 
		else {
			Debug.Log ("OK : " + www.downloadHandler.text);
			#if !UNITY_EDITOR
			openWindow(Instantiation.urlSERVER+"Movies/"+outputname);
			#endif

		}
		www.Dispose ();
		loadingtext.GetComponent<Text> ().text = "";
		hideThings (true,true);

	}

	public IEnumerator finishRecordHologram(){
		loadingtext.SetActive (true);
		loadingtext.GetComponent<Text>().text = "Wait a litte for your movie...";
		Debug.Log ("Save record Hologram");
		record = false;
		recordAction = -1;
		actionDone = false;
		WWWForm form = new WWWForm();
        string outputname="Movie_"+Instantiation.IDUser.ToString()+ ".mp4";//+"_"+System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss")+".mp4";
		form.AddField ("movie", 5);
		form.AddField ("id_user", Instantiation.IDUser.ToString());
		form.AddField ("framerate", 10);
		form.AddField ("outputname", outputname);//embryon3d.crbm.cnrs.fr/dev/movie.php?movie=5&id_user=1&outputname=Movie_1_04-08-2017-11-27-48.mp4
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlMovie, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error); 
		else {
			#if !UNITY_EDITOR
			openWindow(Instantiation.urlSERVER+"Movies/"+outputname);
			#endif

		}
		www.Dispose ();
		loadingtext.GetComponent<Text> ().text = "";
		hideThings (true,true);

	}
	/// ///////////////////SCENARIO 
	private static bool ScenarioPlay=false;


	//SHARE A SCENARIO (IF OWNER)
	public void onShare(){
		if (id_scenario_active >= 0 && id_scenario_owner==Instantiation.IDUser) {
			if (MenuShare.activeSelf)
				MenuShare.SetActive (false);
			else {
				MenuShare.SetActive (true);
				MenuShare.transform.position = new Vector3 (Mathf.RoundToInt(-Instantiation.canvasWidth/2f+273)*Instantiation.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
				MenuShare.GetComponent<ShareWith> ().basename = "scenario";
				MenuShare.GetComponent<ShareWith> ().id_base = id_scenario_active;
				MenuShare.GetComponent<ShareWith> ().listPeople ();
			}
		}

	}

	//SAVE A SCENARIO 
	public void onSaveScenario(){ StartCoroutine (saveScenario ());}
	public IEnumerator saveScenario(){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		if (id_scenario_active == -2) {
			id_scenario_owner = Instantiation.IDUser;
			form.AddField ("scenario", "3");
		}
		else{
			form.AddField ("scenario", "4");
			form.AddField ("id_scenario", id_scenario_active);
		}
		string namescenario = MenuActions.transform.Find ("NameScenario").GetComponent<InputField> ().text;
		if (namescenario == "")
			namescenario = "scenario";
		form.AddField ("name", namescenario);
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString());
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		//Convert All actions in String
		string ActionStr="";
		foreach (KeyValuePair<int, Action> actItem in Actions) {
			Action act = actItem.Value;
			ActionStr +=   actItem.Key + ":" + act.time + ":" + act.position + ":" + act.rotation + ":" + act.scale + ":" + act.framerate + ":" + act.interpolate + "\n";
		}
		byte [] ActionBy=Encoding.UTF8.GetBytes (ActionStr);
		form.AddBinaryData("actions",ActionBy);
		//Debug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=3&name="+MenuActions.transform.Find("NameScenario").GetComponent<InputField>().text+"&id_people=" + Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
		//currenttextloaded.text = Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=3&name=" + MenuActions.transform.Find ("NameScenario").GetComponent<InputField> ().text + "&id_people=" + Instantiation.IDUser.ToString () + "&id_dataset=" + Instantiation.id_dataset.ToString ();
		//Debug.Log (ActionStr);
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlScenario, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError) Debug.Log ("Error : " + www.error); 
		else {
			if (!int.TryParse (www.downloadHandler.text, out id_scenario_active))
				Debug.Log ("ERROR " + www.downloadHandler.text);
			else {
				//Debug.Log ("New id_scenario_active=" + id_scenario_active);

				if(!MenuActions.transform.Find ("share").gameObject.activeSelf){
					MenuShare.transform.position =new Vector3 (Mathf.RoundToInt(-Instantiation.canvasWidth/2f+273)*Instantiation.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
					MenuActions.transform.Find ("share").gameObject.SetActive (true);//We can share it once it's save...
				}

			}
			
		}
		www.Dispose ();
		OnEnable ();
	}
	//LIST ALL AVAILBLAE SCENARIOS 
	//Just remove everything from the menu of recall
	public void clearAllScenarios(){
		foreach(KeyValuePair<int, GameObject> item in Scenarios)
			Destroy(item.Value);
		Scenarios=new Dictionary<int , GameObject>() ;
	}

	//Check the uploaded data in the DB
	public  IEnumerator checkScenarios () {
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("scenario", "1");
		form.AddField ("id_dataset", Instantiation.id_dataset.ToString());
		form.AddField ("id_people", Instantiation.IDUser.ToString());
		//Debug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=1&id_people=" + Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
		//currenttextloaded.text = Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=1&id_people=" + Instantiation.IDUser.ToString () + "&id_dataset=" + Instantiation.id_dataset.ToString ();
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlScenario, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log("Error : " + www.error); 
		else {
			//Return id,filename,date,type,nbCell,minV,maxV
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N!=null && N.Count > 0) {
				//currenttextloaded.text = "Number of scenarios  " + N.Count;
				//Debug.Log("Number of scenarios  " + N.Count);
				clearAllScenarios ();
				for (int i = 0; i < N.Count; i++) { //Number of scenario : id,name,date
					int id_scenario=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
					string name = N [i] [1].ToString ().Replace('"',' ').Trim();
					//string date = N [i] [2].ToString ().Replace('"',' ').Trim();
					int id_owner=int.Parse(N [i] [3].ToString ().Replace('"',' ').Trim());
					GameObject udui = (GameObject)Instantiate(defaultScenario);
					udui.transform.SetParent(MenuScenario.transform,false);
					udui.transform.Translate(0, -Scenarios.Count*30f*Instantiation.canvasScale, 0, Space.World);
					udui.SetActive (true);
					udui.transform.Find ("name").gameObject.GetComponent<Text> ().text = name;
					udui.transform.Find ("id_owner").gameObject.GetComponent<Text> ().text = id_owner.ToString();
					showScenarioListen(udui.transform.Find ("show").gameObject.GetComponent<Button>().GetComponent<Button>(),id_scenario);
					Scenarios[id_scenario]=udui;
				}
			} 
		}
		www.Dispose ();
	}


	/*public void OnPlayerReadyMessage(NetworkMessage netMsg) {
		// TODO: create player and call PlayerIsReady()
	}
	*/

	public void initActionMenus(){
		MenuScenario.SetActive (false);
		if(Actions!=null)
			foreach(KeyValuePair<int, Action> act in Actions)
				Destroy (act.Value.go);
		scrollActionGO.SetActive (false);
		Actions=new  Dictionary<int,Action>();
		MenuActions.SetActive (true);
		defaultAction.SetActive (false);
	}
	public void showScenarioListen(Button b,int id_scenario)  { b.onClick.AddListener (() => showScenario(id_scenario)); }
	public void showScenario(int id_scenario){
		id_scenario_active = id_scenario;
		initActionMenus ();
		//Debug.Log ("Show Scenario "+id_scenario_active);
		string name = Scenarios [id_scenario_active].transform.Find ("name").gameObject.GetComponent<Text> ().text;
		id_scenario_owner = int.Parse (Scenarios [id_scenario_active].transform.Find ("id_owner").gameObject.GetComponent<Text> ().text);
		MenuActions.transform.Find ("NameScenario").gameObject.GetComponent<InputField> ().text = name;
		if (id_scenario_owner != Instantiation.IDUser) {
			//MenuActions.transform.Find ("share").gameObject.SetActive (false);
			MenuActions.transform.Find ("save").gameObject.SetActive (false);
			MenuActions.transform.Find ("cancel").gameObject.SetActive (false);
		} else if(Instantiation.IDUser!=-1) {
			MenuShare.transform.position =new Vector3 (Mathf.RoundToInt(-Instantiation.canvasWidth/2f+273)*Instantiation.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
			//MenuActions.transform.Find ("share").gameObject.SetActive (true);
			MenuActions.transform.Find ("save").gameObject.SetActive (true);
			MenuActions.transform.Find ("cancel").gameObject.SetActive (true);
		}
		StartCoroutine (listActions ());
	}

	//DELETE A SCENARIO
	public void onDeleteScenario(){ StartCoroutine (deleteScenario ());}
	public IEnumerator deleteScenario(){
		if (id_scenario_active != -2) { //Otherwise it has never been save so nothing to delete...
			WWWForm form = new WWWForm ();
			form.AddField ("hash", Instantiation.hash); 
			form.AddField ("scenario", "5");
			form.AddField ("id_scenario", id_scenario_active.ToString ());
			form.AddField ("id_dataset", Instantiation.id_dataset.ToString ());
			form.AddField ("id_people", Instantiation.IDUser.ToString ());
			//Debug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=2&id_scenario=" +id_scenario.ToString ());
			UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlScenario, form);
			yield return www.Send(); 
			if(www.isHttpError || www.isNetworkError) Debug.Log ("Error : " + www.error); 
			else {
				if(www.downloadHandler.text!="") Debug.Log ("Error : " + www.downloadHandler.text); 
			}
			www.Dispose ();
			OnEnable ();
		}
			
		backToScenario ();
	}

	//LIST ALL ACTIONS FOR A SPECIFIC SCENARIO
	public IEnumerator listActions(){
		//Debug.Log ("Play " + id_scenario_active);
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("scenario", "2");
		form.AddField ("id_scenario", id_scenario_active.ToString());
		//Debug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=2&id_scenario=" +id_scenario.ToString ());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlScenario, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError) Debug.Log("Error : " + www.error); 
		else {
			//Return id,filename,date,type,nbCell,minV,maxV
			var N = JSONNode.Parse (www.downloadHandler.text);
			if (N!=null && N.Count > 0) {
				Actions = new  Dictionary<int,Action>();
				//Debug.Log("Number of actions " + N.Count);

				for (int i = 0; i < N.Count; i++) { //Number of scenario : idx,time,position,rotation,scale,framerate,interpolate 
					//int idx=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
					int time=int.Parse(N [i] [1].ToString ().Replace('"',' ').Trim());
					string position = N [i] [2].ToString ().Replace('"',' ').Trim();
					string rotation = N [i] [3].ToString ().Replace('"',' ').Trim();
					float scale =float.Parse(N [i] [4].ToString ().Replace('"',' ').Trim());
					int fps=int.Parse(N [i] [5].ToString ().Replace('"',' ').Trim());
					int inter=int.Parse(N [i] [6].ToString ().Replace('"',' ').Trim());

					GameObject udui = (GameObject)Instantiate(defaultAction);
					udui.transform.SetParent(MenuActions.transform,false);
					udui.transform.position=new Vector3(udui.transform.position.x, defaultAction.transform.position.y-Actions.Count* 30*Instantiation.canvasScale, udui.transform.position.z);
					Action act = new Action (udui, time, scale, parseVector3(position),parseQuaterion(rotation), inter == 1, fps);
					delActionListen(udui.transform.Find ("cancel").gameObject.GetComponent<Button>(),Actions.Count);
					gotoActionListen(udui.transform.Find ("goto").gameObject.GetComponent<Button>(),Actions.Count);

					Actions[Actions.Count]=act;
					if (Actions.Count > 20)
						udui.SetActive (false);

				}
			} 
			checkScrollBar ();
		}
		www.Dispose (); 
	}


	//Check if the scroll bar is necessay
	public void checkScrollBar(){
		if (Actions.Count > 20) {//Start To scroll ...
			float heightAction=31.5f;  //heht=630 pour 20 Actions
			scrollActionGO.SetActive (true);
			scrollAction.size = 630f/(heightAction*Actions.Count); //Size of the scroll bar..
		}else scrollActionGO.SetActive (false);

	}
	//On scrollbar mouvmeent
	public void scrollBarAction(){
		int startAction = (int)Mathf.Round(scrollAction.value * (Actions.Count - 20));
		int idx = 0;
		foreach(KeyValuePair<int, Action> act in Actions){
			GameObject goA = act.Value.go;
			if (idx >= startAction && idx <startAction + 20) {
				goA.SetActive (true);
				goA.transform.position=new Vector3(goA.transform.position.x, defaultAction.transform.position.y-(idx - startAction)* 30*Instantiation.canvasScale, goA.transform.position.z);
			}else  goA.SetActive (false);
			idx++;
		}
	}
	//Weh change the scroll bare value to have this action at first...
	public void scrollTo(Action act){}


	//When we create a new scenario
	public void createScenario(){
		initActionMenus ();
		id_scenario_active = -2; //ID FOR NEW SCENARIO
		//id_scenario_owner=Instantiation.IDUser;
		MenuActions.transform.Find ("share").gameObject.SetActive (false);//We can share it once it's save...
		//MenuActions.transform.Find ("save").gameObject.SetActive (false);
		//MenuActions.transform.Find ("cancel").gameObject.SetActive (false);


	}
	//Close the menu action 
	public void backToScenario(){
		id_scenario_active = -1;
		MenuScenario.SetActive (true);
		MenuActions.SetActive (false);
	}

	public static int stepFrame=0;
	public static Action currentAction;

	//When click on play Scenario
	public void onPlay(){
		if(Actions.Count>0){
			actionCurrent = 0;
			currentAction = Actions [actionCurrent];
			ScenarioPlay = true;
			actionDone = true;
			stepFrame = 0;
		}
	}

	public void playActions(){
		actionDone = false;
		Debug.Log ("playActions "+actionCurrent+" / "+stepFrame);
		currentAction.active ();
		//Debug.Log ("Perform " + currentAction.time+" ->"+currentAction.interpolate);
		if (currentAction.interpolate && actionCurrent<Actions.Count-1) { //INTERPOLATION
			Action nextAct=Actions [actionCurrent+1];
			float t = (float)stepFrame / (float)currentAction.framerate;
			ArcBall.rescale (Vector3.Lerp (new Vector3 (currentAction.scale, currentAction.scale, currentAction.scale), new Vector3 (nextAct.scale, nextAct.scale, nextAct.scale), t));
			ArcBall.rotate (Quaternion.Slerp (currentAction.rotation, nextAct.rotation, t));
			ArcBall.move(Vector3.Lerp (currentAction.position, nextAct.position, t));
			ArcBall.update = true;
			int newTime = (int)Mathf.Round (currentAction.time + t * (nextAct.time - currentAction.time));
			//Debug.Log ("playActions "+actionCurrent+" / "+stepFrame+ " t=="+t+ "->Interpolate Time " + newTime+" Rotation=="+Instantiation.CurrentRotation.ToString());
			if (newTime >= Instantiation.MinTime && newTime <= Instantiation.MaxTime) {
				if (Instantiation.CurrentTime == newTime)
					finishAction (); //We already are at the well time point...
				else
					Instantiation.setTime (newTime);
			} else
				finishAction ();

		} else {
			ArcBall.rescale(currentAction.scale);
			ArcBall.rotate(currentAction.rotation);
			ArcBall.move(currentAction.position);
			if (currentAction.time >= Instantiation.MinTime && currentAction.time <= Instantiation.MaxTime) {
				if (Instantiation.CurrentTime == currentAction.time)
					finishAction (); //We already are at the well time point...
				else
					Instantiation.setTime (currentAction.time);  
			} else
				finishAction ();
		}

	}

	//End after aaction
	public static void finishAction(){
		if (currentAction != null) {
			
			Debug.Log ("End Action");
			if (stepFrame < currentAction.framerate)
				stepFrame++;
			else {
				currentAction.unActive ();
				stepFrame = 0;
				actionCurrent++;
			}

		
			if (Actions.ContainsKey (actionCurrent)) {
				if (record) recordAction = 0;
				currentAction = Actions [actionCurrent];
			} else {
				currentAction = null;
				ScenarioPlay = false;
			}	
		}
		actionDone = true;
	}
		
	public void Update(){
		if (actionDone) {
			if (ScenarioPlay) { //JUST PLAY THE SCENARIO
				playActions ();
			} 
			if(record) { //RECORD
				//Debug.Log ("Action" + actionCurrent + ":" + recordAction);
				if (recordAction == 0) { //Perform the recod
					recordAction = 1;
					if (hologram.isOn) {
						//if(nbHoloAngle==0) recordAction = 1;
						StartCoroutine (frameRecordHologram ());
					}
					else {
						StartCoroutine (frameRecord ());
					}
				}
				if (recordAction == 2) { //Record finish -> Next Action
					if (currentAction == null) {
						if (hologram.isOn)
							StartCoroutine (finishRecordHologram ());
						else
							StartCoroutine (finishRecord ());
					} else {
						/*if (hologram.isOn) {
							if (nbHoloAngle == 0)
								playActions ();
						} else*/
							playActions ();
					}
				}
			}

		}
		if(id_scenario_active==-2 || id_scenario_active>=0)
			if (Input.GetKeyDown (KeyCode.A) && !MenuActions.transform.Find("NameScenario").GetComponent<InputField>().isFocused) {
				addNewAction ();
		}


	}

	private int defaultFramerate=10;
	//When we click on 'a' to add a new actio,n
	public void addNewAction(){
		GameObject udui = (GameObject)Instantiate(defaultAction);
		udui.transform.SetParent(MenuActions.transform,false);
		udui.transform.position=new Vector3(udui.transform.position.x, defaultAction.transform.position.y-Actions.Count* 30*Instantiation.canvasScale, udui.transform.position.z);
		delActionListen(udui.transform.Find ("cancel").gameObject.GetComponent<Button>(),Actions.Count);
		Action act = new Action (udui,Instantiation.CurrentTime,ArcBall.CurrentScale.x ,ArcBall.CurrentPosition,ArcBall.CurrentRotation,true,defaultFramerate);
		Actions[Actions.Count]=act;
		checkScrollBar ();
	}


	//Add the listener to delete the Action 
	public void delActionListen(Button b,int id_action) {b.onClick.AddListener (() => delAction(id_action));}
	public void delAction(int id_action){
		Dictionary<int,Action> NewActions = new Dictionary<int,Action> ();
		foreach (KeyValuePair<int,Action>idA in Actions) {
			if (idA.Key != id_action) {
				Action act = idA.Value;
				act.go.transform.position=new Vector3(act.go.transform.position.x, defaultAction.transform.position.y-NewActions.Count* 30*Instantiation.canvasScale, act.go.transform.position.z);
				act.go.transform.Find ("cancel").gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
				delActionListen(act.go.transform.Find ("cancel").gameObject.GetComponent<Button>(),NewActions.Count);
				NewActions [NewActions.Count] = act;
			} else {
				Action act = idA.Value;
				Destroy (act.go);
			}
		}
		Actions = NewActions;
		checkScrollBar ();
	}

	public void gotoActionListen(Button b,int id_action) {b.onClick.AddListener (() => gotoAction(id_action));}
	public void gotoAction(int id_action){
		//currenttextloaded.text = "GOTO " + id_action;
		//Debug.Log ("GOTO " + id_action);
		if (Actions.ContainsKey (id_action)) {
			Action act = Actions [id_action];
			ArcBall.rescale(act.scale);
			ArcBall.rotate(act.rotation);
			ArcBall.move(act.position);
			if (act.time >= Instantiation.MinTime && act.time <= Instantiation.MaxTime) 
				if (Instantiation.CurrentTime != act.time)
					Instantiation.setTime(act.time);
			
		}
	}

	public static Vector3 parseVector3(string rString){
		string[] temp = rString.Substring(1,rString.Length-2).Split(',');
		float x = float.Parse(temp[0]);
		float y = float.Parse(temp[1]);
		float z = float.Parse(temp[2]);
		Vector3 rValue = new Vector3(x,y,z);
		return rValue;
	}


	public static Quaternion parseQuaterion(string rString){
		string[] temp = rString.Substring(1,rString.Length-2).Split(',');
		float x = float.Parse(temp[0]);
		float y = float.Parse(temp[1]);
		float z = float.Parse(temp[2]);
		float w = float.Parse(temp[3]);
		Quaternion rValue = new Quaternion(x,y,z,w);
		return rValue;
	}



}
