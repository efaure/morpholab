﻿using UnityEngine;
using System.Runtime.InteropServices;

public class WebGLMemoryStats : MonoBehaviour
{
    [DllImport("__Internal")]
    public static extern uint GetTotalMemorySize();

    [DllImport("__Internal")]
    public static extern uint GetTotalStackSize();

    [DllImport("__Internal")]
    public static extern uint GetStaticMemorySize();

    [DllImport("__Internal")]
    public static extern uint GetDynamicMemorySize();
}