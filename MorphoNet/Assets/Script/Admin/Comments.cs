﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Text;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Networking;

public class Comments : MonoBehaviour {

	//TODO ON EMBRYON3D CRBM CNRS
	//ALTER TABLE `comments` ADD `priority` INT NOT NULL DEFAULT '0' AFTER `done`;
	public  GameObject defaultComment;
	public  GameObject MenuDebug;
	public  Dictionary<int, GameObject> ListComments;
	public  GameObject scrollbar;

	// Use this for initialization
	void Start () {
		scrollbar.SetActive (false);
		defaultComment.SetActive(false);

	}

	public void OnEnable(){
		StartCoroutine (listComments ());
	}

	public string fillField(string n)
	{
		n = n.ToString().Replace('"', ' ').Trim();
		if (n == "-1") return "";
		return n;

	}

	//SCROLLBAR 
	public static int marging=14;
	public static int sizeComment=20;
	public static int MaxHeight = 550; 
	public static int nbSCroll = 0;
	public  void onScroll(){
		int nbSteps = scrollbar.GetComponent<Scrollbar> ().numberOfSteps;
		float value = scrollbar.GetComponent<Scrollbar> ().value;
		nbSCroll = (int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		//Debug.Log ("nbSCroll=" + nbSCroll+" value="+value);
		reOrganize ();
	}

	public void reOrganize(){
		float totalHeight = 0;
		foreach (var comms in ListComments) {
			GameObject comm = comms.Value;
			bool activation = true;
			if (totalHeight - nbSCroll*sizeComment <0 || totalHeight - nbSCroll*sizeComment > MaxHeight) activation = false; 
			comm.SetActive (activation);
			float hei = comm.transform.Find ("comment").gameObject.GetComponent<Text> ().preferredHeight;
			hei += marging;//For Marging
			if (activation)	comm.transform.position = new Vector3 (comm.transform.position.x, defaultComment.transform.position.y - (totalHeight-nbSCroll*sizeComment) * Instantiation.canvasScale, comm.transform.position.z);
			totalHeight += hei;
		}
	}


	//List All Users (only if admin users...) 
	public IEnumerator listComments(){ 
		int totalHeight = 0;
		if (Instantiation.urlAdmin!=null) { //Incase it was not well define urlSERVER
			WWWForm form = new WWWForm ();
			form.AddField ("hash", Instantiation.hash); 
			form.AddField ("user", "12");
			//Debug.Log (Instantiation.urlAdmin + "?hash=" + Instantiation.hash + "&user=12");
			UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAdmin, form);
			yield return www.Send(); 
			if (www.isHttpError || www.isNetworkError)
				Debug.Log ("Error : " + www.error);
			else {
				if (ListComments != null)
					foreach (KeyValuePair<int, GameObject> comm in ListComments)
						Destroy (comm.Value);
				ListComments = new Dictionary<int, GameObject> ();
				var N = JSONNode.Parse (www.downloadHandler.text);

				if (N != null && N.Count > 0) {
					//Debug.Log("Found " + N.Count+ " users");
					for (int i = 0; i < N.Count; i++) { // comments.id,people.surname,people.name,comments.comments,comments.date,comments.done,priority
						//Debug.Log (N [i]);
						int id_comment = int.Parse (N [i] ["id"].ToString ().Replace ('"', ' ').Trim ());
						string firstname = N [i] ["surname"].ToString ().Replace ('"', ' ').Trim ();
						string lastname = N [i] ["name"].ToString ().Replace ('"', ' ').Trim ();
						string comment = N [i] ["comments"].ToString ().Replace ('"', ' ').Trim ();
						int priority= int.Parse (N [i] ["priority"].ToString ().Replace ('"', ' ').Trim ());
						comment=comment.Replace ("\\n", "\n");

						string date = N [i] ["date"].ToString ().Replace ('"', ' ').Trim ();
						//Debug.Log ("id=" + id_comment + " -> " + comment);
						int done = int.Parse (N [i] ["done"].ToString ().Replace ('"', ' ').Trim ());
						GameObject comm = (GameObject)Instantiate (defaultComment); // on recupère certain gameobject de defaultuser et on les desactive si ce n'est pas un admin 
						comm.name="comment_"+id_comment;
						comm.transform.SetParent (MenuDebug.transform, false);
						comm.transform.Find ("name").gameObject.GetComponent<Text> ().text = firstname + " " + lastname;
						comm.transform.Find ("date").gameObject.GetComponent<Text> ().text = date;
						comm.transform.Find ("comment").gameObject.GetComponent<Text> ().text = comment;
						comm.transform.Find ("statut").gameObject.GetComponent<Text> ().text = done.ToString ();
						for (int ip = 1; ip <= 5; ip++) {
							if (ip <= priority) {
								comm.transform.Find ("priority" + ip+"_on").gameObject.SetActive (true);
								comm.transform.Find ("priority" + ip+"_off").gameObject.SetActive (false);
							} else {
								comm.transform.Find ("priority" + ip+"_on").gameObject.SetActive (false);
								comm.transform.Find ("priority" + ip+"_off").gameObject.SetActive (true);
							}
							priorityCommentsListen (comm.transform.Find ("priority" + ip+"_on").gameObject.GetComponent<Button> (), id_comment,ip);
							priorityCommentsListen (comm.transform.Find ("priority" + ip+"_off").gameObject.GetComponent<Button> (), id_comment,ip);
						}
						if (done == 0)
							comm.transform.Find ("archive").gameObject.SetActive (false);
						else {
							archiveCommentsListen (comm.transform.Find ("archive").gameObject.GetComponent<Button> (), id_comment);
						}
						int hei = Mathf.RoundToInt(comm.transform.Find ("comment").gameObject.GetComponent<Text> ().preferredHeight);
						hei += marging;//For Marging
						comm.transform.position = new Vector3 (comm.transform.position.x, defaultComment.transform.position.y - totalHeight * Instantiation.canvasScale, comm.transform.position.z);
						totalHeight += hei;
						comm.SetActive (true);
						ListComments [id_comment] = comm;
					}
				} 
			}
			www.Dispose ();
			//SCROLL BAR
			nbSCroll = 0;
			if (totalHeight > MaxHeight) {
				scrollbar.SetActive (true);
				scrollbar.GetComponent<Scrollbar> ().numberOfSteps = 1 + (totalHeight - MaxHeight) / sizeComment; //DEFINE SCROLLBAR LENGTH
				scrollbar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + (totalHeight- MaxHeight) /sizeComment);
				scrollbar.GetComponent<Scrollbar> ().value = scrollbar.GetComponent<Scrollbar> ().numberOfSteps;
			}  else scrollbar.SetActive (false);
			reOrganize();
		}
	}
	public void archiveCommentsListen(Button b,int id_comment) {
		b.onClick.AddListener (() => StartCoroutine (archiveComment (id_comment)));
	}

	public IEnumerator archiveComment(int id_comment){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("user", "13");
		form.AddField ("id_comment", id_comment.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAdmin, form);
		yield return www.Send(); 
		if(www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else ListComments[id_comment].transform.Find ("archive").gameObject.SetActive (false);
		www.Dispose ();

	}

	public void priorityCommentsListen(Button b,int id_comment,int priority) {
		b.onClick.AddListener (() => StartCoroutine (priorityComments (id_comment,priority)));
	}

	public IEnumerator priorityComments(int id_comment,int priority){
		WWWForm form = new WWWForm ();
		form.AddField ("hash", Instantiation.hash); 
		form.AddField ("user", "14");
		form.AddField ("id_comment", id_comment.ToString());
		form.AddField ("priority", priority.ToString());
		UnityWebRequest www = UnityWebRequest.Post(Instantiation.urlAdmin, form);
		yield return www.Send(); 
		if (www.isHttpError || www.isNetworkError)
			Debug.Log ("Error : " + www.error);
		else {
			for (int i = 1; i <= 5; i++) {
				if (i <= priority) {
					ListComments [id_comment].transform.Find ("priority" + i+"_on").gameObject.SetActive (true);
					ListComments [id_comment].transform.Find ("priority" + i+"_off").gameObject.SetActive (false);
				} else {
					ListComments [id_comment].transform.Find ("priority" + i+"_on").gameObject.SetActive (false);
					ListComments [id_comment].transform.Find ("priority" + i+"_off").gameObject.SetActive (true);
				}
			}
		}
		www.Dispose ();

	}
}
