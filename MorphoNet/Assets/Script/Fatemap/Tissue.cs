﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine.UI;
using System;


public class GroupObjects : MonoBehaviour{

	public GameObject go;
	public Cell c;
	private string cellName;
	public float width;
	public bool isView;
	public GroupObjects (Cell c,GameObject grpParent)
	{
		this.c = c;
		this.cellName = c.ID;
		this.isView = false;
	
		if (grpParent != null) {
			this.go = Instantiate (Tissue.defaultGroup, grpParent.transform);
			this.go.name = this.cellName;
			this.setName (this.cellName);
			Tissue.viewListenO (this.go.transform.Find ("def").Find ("is").gameObject.GetComponent<Toggle> (), this);
			Destroy (this.go.transform.Find ("def").Find ("reploy").gameObject);
			Destroy (this.go.transform.Find ("def").Find ("deploy").gameObject);
			GameObject colorize = this.go.transform.Find ("def").Find ("colorize").gameObject;
			Vector3 prevpos = this.go.transform.Find ("def").Find ("name").position;
			this.go.transform.Find ("def").Find ("name").position = new Vector3 (colorize.transform.position.x, prevpos.y, prevpos.z);
			colorize.SetActive (false);
		}
	}

	public bool describe(){
		if(this.go!=null && this.go.transform.Find ("def").Find ("is").GetComponent<Toggle> ().isOn != this.c.selected)
			this.go.transform.Find ("def").Find ("is").GetComponent<Toggle> ().isOn = this.c.selected;
		return this.c.selected;
		//return true;
	}
	public void setName(string name){
		if (this.go != null) {
			GameObject nameGO = this.go.transform.Find ("def").Find ("name").gameObject;
			nameGO.GetComponent<Text> ().text = name;
			this.width = nameGO.GetComponent<Text> ().preferredWidth;
			RectTransform ct = nameGO.GetComponent<RectTransform> ();
			ct.sizeDelta = new Vector2 (this.width, Tissue.shiftY);
		}
	}
	public void checkWidth(int decalsX){
		if (this.go != null) {
			//float w = (456.2f + (this.go.transform.position.x) / Instantiation.canvasScale);
			float w = decalsX*20f;
			//Debug.Log (this.cellName+" -> w=" + w);
			w += +this.width;
			if (w > Tissue.maxWidth)
				Tissue.maxWidth = w;
		}
	}
	public void draw(float shiftX){
		if (this.cellName==c.ID){
			if (Tissue.idXName == -1) Tissue.idXName = MenuCell.getIdInfosForName ();
			if (Tissue.idXName != -1){
				this.cellName =this.c.getInfos (Tissue.idXName);
				if (this.cellName == "") this.cellName = this.c.ID;
				this.setName (this.cellName);
			}
		}
		if(this.go != null){
			if(shiftX!=-1)this.go.transform.position = this.go.transform.parent.position - new Vector3 (-Tissue.shiftY*Instantiation.canvasScale, shiftX*Instantiation.canvasScale, 0f);
			this.go.SetActive(true);
		}
	}
	public void undraw(){
		if(this.go != null)this.go.SetActive(false);
	}
	//We we want to see this group
	public void changeView(){
		this.isView = !isView;
		this.View (this.isView);
	}
	public void View(bool v){
		//Debug.Log (" View " + this.cellName+ "-> "+v);
		if(this.go != null) this.go.transform.Find ("def").Find ("is").gameObject.GetComponent<Toggle> ().isOn = v;
		if (v) SelectionCell.AddCellSelected (this.c,false);
		else SelectionCell.RemoveCellSelected (this.c,false);
	}
	public void shift(float shiftX){
		//Debug.Log ("Shift  " + this.name+ " at " +shiftX);
		if(this.go != null) this.go.transform.position = this.go.transform.position - new Vector3 (0, shiftX*Instantiation.canvasScale, 0f);
	}
	public int checkScroll(int n,int decals){
		//string sc = ""; for (int i = 0; i < n - decals; i++) sc += this.cellName;
		//this.go.transform.Find ("def").Find ("name").gameObject.GetComponent<Text> ().text = sc;
		if (n - decals >= 0 && n - decals < Tissue.maxNbDraw) {
			if (this.go != null && !this.go.activeSelf) {
				if(this.go != null) this.go.SetActive (true);
				Tissue.nbSCroll += 1;
			}
		}else if (this.go.activeSelf) {
				if(this.go != null) this.go.SetActive (false);
				Tissue.nbSCroll += 1;
			}
		return n + 1;
	}

	public void setColor(int selectionValue){
		this.c.addSelection(selectionValue);
	}

	public void clear(){
		Destroy (this.go);
	}
}

public class Group : MonoBehaviour{
	public string name;
	public  List < GroupObjects > Objects=null;
	public  List < Group > SubGroups=null;
	public GameObject go;
	public Group parent;
	public bool isdeploy;
	public bool isView;
	public float width;

	public bool active; //When we change time instead of reinitialize everything we keep the active group
	public Group (string name,GameObject grpParent)
	{
		//Debug.Log ("Create group " + name +" "+Tissue.defaultGroup.name);
		this.isdeploy=false;
		this.isView = false;
		this.name = name;
		this.active = true;
	
		this.go = Instantiate (Tissue.defaultGroup, grpParent.transform);
		this.go.name = this.name;
		GameObject nameGO = this.go.transform.Find ("def").Find ("name").gameObject;
		nameGO.GetComponent<Text> ().text = this.name;
		this.width = nameGO.GetComponent<Text> ().preferredWidth;
		RectTransform ct = nameGO.GetComponent<RectTransform> ();
		ct.sizeDelta = new Vector2 (this.width, Tissue.shiftY);
		nameGO.GetComponent<Text> ().fontStyle = FontStyle.Italic;
		Tissue.viewListen(this.go.transform.Find ("def").Find ("is").gameObject.GetComponent<Toggle> (),this);
		Tissue.deployListen(this.go.transform.Find ("def").Find ("deploy").gameObject.GetComponent<Button> (),this);
		Tissue.reployListen(this.go.transform.Find ("def").Find ("reploy").gameObject.GetComponent<Button> (),this);
		Tissue.colorizeListen(this.go.transform.Find("def").Find("colorize").gameObject.GetComponent<Button> (),this);

		this.go.transform.Find ("def").Find ("reploy").gameObject.SetActive (false);
	}


	public void addCell (Cell c){
		if (this.Objects == null) this.Objects = new List<GroupObjects> ();
		this.Objects.Add (new GroupObjects (c,this.go));
	}
	public Group getGroup(string []names,int s){ 
		if (names.Length == s) {  //We are at the end
			this.active = true;
			return this;
		}
		//Debug.Log ("names.Length==" + names.Length + " s=" + s + " -> " + names [s]);

		if (this.SubGroups == null) this.SubGroups = new  List < Group > ();

		foreach (Group sg in this.SubGroups)
			if (sg.name == names [s]) {
				this.active = true;
				return sg.getGroup (names, s + 1);
			}
		Group gp = new Group (names [s],this.go);
		gp.parent = this;
		//Debug.Log ("Create group " + gp.name+ " with parent " + gp.parent.name);

		this.SubGroups.Add (gp);
		return gp.getGroup(names, s + 1);
	}
	public void show(int s){
		string st = "";
		for (int i = 0; i < s; i++) st += "-";
		st += "->" + this.name;
		if (this.Objects == null) st += " contains no cells";
		else st += " contains " + this.Objects.Count + " cells";
		Debug.Log (st);
		if(this.SubGroups!=null)
			foreach (Group sg in this.SubGroups)
				sg.show (s + 1);
	}
	public void checkWidth(int decalsX){
		if(this.go!=null){
			//float w = (456.2f+(this.go.transform.position.x ) / Instantiation.canvasScale);
			float w = decalsX*20f;
			//Debug.Log (this.name+" -> w=" + w+" decalsX="+(Instantiation.canvasWidth/2)+ " witdht="+this.width);
			w += +this.width;
			if (w > Tissue.maxWidth) Tissue.maxWidth = w;
			if (this.isdeploy) {
				if (this.SubGroups != null)
					foreach (Group sg in this.SubGroups)
						sg.checkWidth (decalsX+1);
				if (this.Objects != null)
					foreach (GroupObjects sog in this.Objects)
						sog.checkWidth (decalsX+1);
					
			}
		}
	}
	public void draw(float shiftX){
		if (this.go != null) {
			if (shiftX != -1)
				this.go.transform.position = this.go.transform.parent.position - new Vector3 (-Tissue.shiftY * Instantiation.canvasScale, shiftX * Instantiation.canvasScale, 0f);
			//else this.go.transform.localPosition = new Vector3 (0f, 0f, 0f);
			this.go.SetActive (true);
		}
	}
	public void undraw(){
		if (this.go != null) 
			this.go.SetActive(false);
	}


	public void deploy(){
		if (this.go != null) {
			this.go.transform.Find ("def").Find ("reploy").gameObject.SetActive (true);
			this.go.transform.Find ("def").Find ("deploy").gameObject.SetActive (false);
	
			isdeploy = true;
			int isg = 1;
			if (this.SubGroups != null)
				foreach (Group sg in this.SubGroups) {
					sg.draw (Tissue.shift * isg);
					isg += 1;
				}
			if (this.Objects != null)
				foreach (GroupObjects sog in this.Objects) {
					sog.draw (Tissue.shift * isg);
					isg += 1;
				}
		
			this.shiftNext (isg - 1);
		}

	}
	public void shift(float shiftX){
		//Debug.Log ("Shift  " + this.name+ " at " +shiftX);
		if(this.go != null) this.go.transform.position = this.go.transform.position - new Vector3 (0, shiftX*Instantiation.canvasScale, 0f);
	}

	//We have to shift the next SubGroup
	public void shiftNext(int nbShift){
		//Debug.Log ("Shift Next " + this.name);
		if (this.name != Tissue.mainGroupName) {
			//Debug.Log ("Parent " + this.parent.name+" ->"+ this.parent.SubGroups.Count);
			bool isShift = false;
			foreach (Group sg in this.parent.SubGroups) {
				if (sg.name == this.name) isShift = true;
				else if (isShift) {
					sg.shift (nbShift*Tissue.shift);
				}
			}
			//We also have to shift the potentiel objects
			if(this.parent.Objects!=null)
				foreach (GroupObjects sog in this.parent.Objects) 
					sog.shift (nbShift*Tissue.shift);
			
			this.parent.shiftNext (nbShift);
		}
	}

	public void reploy(){
		if (this.go != null) {
			this.go.transform.Find ("def").Find ("reploy").gameObject.SetActive (false);
			this.go.transform.Find ("def").Find ("deploy").gameObject.SetActive (true);

			isdeploy = false;
			int isg = 1;
			if (this.SubGroups != null) {
				foreach (Group sg in this.SubGroups) {
					sg.undraw ();
					isg += 1;
					if (sg.isdeploy)
						sg.reploy ();
				}
			}

			if (this.Objects != null) {
				foreach (GroupObjects sog in this.Objects) {
					sog.undraw ();
					isg += 1;
				}
			}
			this.shiftNext (-isg + 1);
		}
	}


	//We we want to see this group
	public void View(bool v){
		//Debug.Log (" View Group " + this.name+ "-> "+v);
		if(this.go != null) this.go.transform.Find ("def").Find ("is").gameObject.GetComponent<Toggle> ().isOn =v;
		if (this.SubGroups != null) {
			foreach (Group sg in this.SubGroups) {
				sg.View (v);
			}
		}
		if (this.Objects != null) {
			foreach (GroupObjects sog in this.Objects) {
				sog.View (v);
			}
		}

	}
	//We we want to see this group
	public void changeView(){
		//Debug.Log ("changeView=" + this.isView);
		this.isView = !this.isView;
		this.View (this.isView);
	}
	//Return the total deploy size
	public int getHeight(){
		int height = 1;
		if (this.isdeploy) {
			if (this.SubGroups != null) {
				foreach (Group sg in this.SubGroups)
					height += sg.getHeight ();
			}
			if (this.Objects != null) {
				//Debug.Log ("Objec topen " + this.name + " contains " + this.Objects.Count);
				height += this.Objects.Count;
			}
		}
		return height;
	}
	public int checkScroll(int n,int decals){
		if (n - decals >= 0 && n - decals < Tissue.maxNbDraw) {
			if (this.go != null && !this.go.transform.Find("def").gameObject.activeSelf) {
				this.go.transform.Find("def").gameObject.SetActive (true);
				Tissue.nbSCroll += 1;
			}
		}else if (this.go != null && this.go.transform.Find("def").gameObject.activeSelf) {
			this.go.transform.Find("def").gameObject.SetActive (false);
			Tissue.nbSCroll += 1;
		}

		n += 1;
		if (this.isdeploy) {
			if (this.SubGroups != null)
				foreach (Group sg in this.SubGroups)
					n=sg.checkScroll (n,decals);
			if (this.Objects != null)
				foreach (GroupObjects sog in this.Objects)
					n=sog.checkScroll (n,decals);
		}
		return n;
	}

	public bool describe(){
		//Debug.Log ("Describe " + this.name);
		if (this.Objects == null && this.SubGroups == null && this.go != null) this.go.transform.Find ("def").Find ("deploy").gameObject.SetActive (false);
		if (this.isdeploy) this.deploy ();
		bool allOn=true;
		if (this.SubGroups != null)
			foreach (Group sg in this.SubGroups)
				if (!sg.describe ())
					allOn = false;
		if (this.Objects != null)
			foreach (GroupObjects sog in this.Objects)
				if(!sog.describe ())
					allOn = false;
		if (this.Objects == null && this.SubGroups == null) allOn = false;
		//Debug.Log (" Describe " + this.name+ "-> "+allOn);
		if(this.go!=null) this.go.transform.Find ("def").Find ("is").gameObject.GetComponent<Toggle> ().isOn =allOn;
		this.isView = allOn;
		return allOn;
	}
	//Choose sequential color for all this subgroups element
	public void colorize(){
		if (Lineage.isLineage) Lineage.colorizeGroup (this);
		int selectionValue = 1;
		if (this.SubGroups != null)
			foreach (Group sg in this.SubGroups){
				sg.setColor (selectionValue);
				selectionValue++;
			}

		if (this.Objects != null)
			foreach (GroupObjects sog in this.Objects){
				sog.setColor (selectionValue);
				selectionValue++;
			}
	}
	public void setColor(int selection){
		if (this.SubGroups != null)
			foreach (Group sg in this.SubGroups)
				sg.setColor (selection);
		if (this.Objects != null)
			foreach (GroupObjects sog in this.Objects)
				sog.setColor (selection);
	}
	//Remove unactive groups
	public void purgeGroups(){
		if (!this.active) {
			this.clear ();
		}
		else {
			if (this.SubGroups != null) {
				List<Group> TempSubGroups = new List<Group> ();
				foreach (Group sg in this.SubGroups) {
					sg.purgeGroups ();
					if (sg.active)
						TempSubGroups.Add (sg);
				}
				if (TempSubGroups.Count  > 0)
					this.SubGroups = TempSubGroups;
				else {
					this.SubGroups.Clear ();
					this.SubGroups = null;
				}
			}
		}
	}

	//Remove all childs ...
	public void clear(){
		//Debug.Log ("Clear GRP");
		Destroy (this.go);
		if (this.SubGroups != null) {
			foreach (Group sg in this.SubGroups)
				sg.clear ();
			this.SubGroups.Clear ();
		}
		this.SubGroups = null;
		if (this.Objects != null) {
			foreach (GroupObjects sog in this.Objects)
				sog.clear ();
			this.Objects.Clear ();
		}
		this.Objects = null;
	}
	//Remove only cells childs ...
	public void clearCells(){
		this.active = false;
		if (this.SubGroups != null) {
			foreach (Group sg in this.SubGroups)
				sg.clearCells ();
		}
		if (this.Objects != null) {
			foreach (GroupObjects sog in this.Objects)
				sog.clear();
			this.Objects.Clear ();
		}
	}

}

public class Tissue : MonoBehaviour {
	public static int CurrentTime = -1;
	public static Dictionary<int , string > Tissues;
	public static Dropdown TissueDropDown;
	public static GameObject MenuGroups;
	public static GameObject defaultGroup;
	public static int drawingTime=-2;
	public static int idXName = -1;
	public static GameObject scrollBar;

	public static int shift=20;
	public static int shiftY=20;
	//public Toggle LeftTissue;
	//public Toggle RightTissue;

	public static Group grp;
	public static Dictionary<int,string> nbFates;

	public static string mainGroupName = " Visualize Objects Group By";
	void Start () {
		//if (MenuGroups == null) MenuGroups=GameObject.Find ("Canvas").gameObject.transform.Find ("MenuGroups").gameObject;
	}


	public static void init(int nbInfo,string infoname){
		//Tissue.nbFate = nbInfo;
		if (nbFates == null) nbFates =new Dictionary<int,string>();
		if (MenuGroups == null) {
			MenuGroups = GameObject.Find ("Canvas").gameObject.transform.Find ("MenuGroups").gameObject;
			defaultGroup = MenuGroups.transform.Find ("default").gameObject;
			defaultGroup.SetActive (false);
			Instantiation.canvas.GetComponent<Menus> ().buttonGroups.SetActive (true);
			Instantiation.canvas.GetComponent<Menus> ().reOrganizeButtons ();
			if (scrollBar == null) scrollBar = MenuGroups.transform.Find ("Scrollbar").gameObject;
			scrollBar.SetActive (false);
			grp = new Group (mainGroupName, MenuGroups);
			deploy (grp);
			grp.go.transform.Find ("def").Find ("is").gameObject.SetActive (false);
			grp.go.transform.Find ("def").Find ("deploy").gameObject.SetActive (false);
			grp.go.transform.Find ("def").Find ("colorize").gameObject.SetActive (false);
			grp.go.transform.Find ("def").Find ("reploy").gameObject.SetActive (false);
			grp.go.transform.Find ("def").Find ("name").transform.Translate (new Vector3 (-40*Instantiation.canvasScale, 0, 0));
		}
		nbFates [nbInfo] = infoname;
		drawingTime = -1;
	}

	public void OnEnable(){
		Tissue.describe ();
	}
	//Set a specific time point
	public static void setTime(int t){
		//Debug.Log ("SET TIME at " + t);
		grp.clearCells();
		grp.active = true;
		foreach (var nbFate in nbFates)
			grp.getGroup (nbFate.Value.Split (':'), 0).active = true;
			
		Tissue.CurrentTime = t;
		Dictionary<string, Cell> CellAtT = Instantiation.CellT [t];
		foreach (var itemc in CellAtT) {
			Cell c = itemc.Value;
			foreach (var nbFate in nbFates) {
				string infos = c.getInfos (nbFate.Key);
				if (infos != "") {
					infos = nbFate.Value + ":" + infos;
					//Debug.Log ("Add cell with " + infos);
					addCell (infos.Split (':'), c);
				}
			}
		}
		//Now We Purge the unactive groups
		grp.purgeGroups();
		grp.draw(-1);
		Tissue.updateWidth ();
		Tissue.updateHeight ();
		//grp.show(1); //Diplay group on debug 
	}
	//Add a cell (and creaet subgroup if necessary)
	public static void addCell(string []names, Cell c){
		Group sg = grp.getGroup (names, 0);
		//Debug.Log (" Found " + sg.name+ " for " +c.ID+" at " +c.t);
		if(c!=null) sg.addCell (c);
	}

		
	//DEPLOY A GROUP
	public static void deployListen(Button b,Group gp)  {b.onClick.AddListener (() => Tissue.deploy(gp)); }
	public static void deploy(Group gp){ gp.deploy (); Tissue.updateHeight (); Tissue.updateWidth (); Tissue.nbSCroll = 1;}
	//REPLOY A GROYP
	public static void reployListen(Button b,Group gp)  {b.onClick.AddListener (() => Tissue.reploy(gp)); }
	public static void reploy(Group gp){  gp.reploy (); Tissue.updateHeight ();Tissue.updateWidth (); Tissue.nbSCroll = 1;}
	//VIEW
	public static void viewListen(Toggle b,Group gp){ b.onValueChanged.AddListener (delegate { Tissue.View (gp); });}
	public static void View(Group gp){  if(!blockToogle)  { 
			blockToogle = true;
			gp.changeView (); 	
			MenuCell.describe ();
			blockToogle = false;
		}
	}
	public static void viewListenO(Toggle b,GroupObjects gop){ b.onValueChanged.AddListener (delegate { Tissue.ViewO (gop); });}
	public static void ViewO(GroupObjects gop){gop.changeView (); }

	//FOR SCROLL BAR
	public static  int maxNbDraw = 34;
	public static float sizeBar = 20f;
	public static void updateHeight(){
		int height = grp.getHeight ();
		//Debug.Log ("height=" + height);
		if (height > maxNbDraw) {
			scrollBar.SetActive (true);
			//DEFINE SCROLLBAR LENGTH
			scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1 + height - maxNbDraw;
			scrollBar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + height - maxNbDraw);
			grp.checkScroll (0,0);
		} else {
			scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1;
			scrollBar.GetComponent<Scrollbar> ().size = 1f;
			scrollBar.SetActive (false);
			grp.go.transform.position = new Vector3 (grp.go.transform.position.x, -0.1460054f, 0f);//RESET POSITION
			nbSCroll = 1;
		}
	}


	public static int nbSCroll = 1;
	public void onSroll(Single value){
		//Debug.Log ("On Scroll "+value+" : "+Instantiation.canvasScale+" ; "+Instantiation.initcanvasScale);
		int nbSteps = scrollBar.GetComponent<Scrollbar> ().numberOfSteps;
		int decals = (int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		//Debug.Log ("value=" + value+" gp="+grp.go.transform.position.y+" decal="+decals);
		//grp.go.transform.localPosition = new Vector3 (0f, 0f, 0f);
		grp.go.transform.localPosition = new Vector3 (12f, decals * shiftY, 0f);
		//Debug.Log ("gp="+grp.go.transform.localPosition.y);
		nbSCroll = 0;
		grp.checkScroll (0,decals);
	}

	void Update(){
		if (scrollBar != null &&  nbSCroll>0) onSroll (scrollBar.GetComponent<Scrollbar> ().value);
	}
	//FOR WIDTH LARGER
	public static float heightGroups=675;
	public static float maxWidth = 0;
	public static void updateWidth(){
		Tissue.maxWidth = 0;
		grp.checkWidth(0);
		RectTransform ct = MenuGroups.GetComponent<RectTransform> ();
		if(maxWidth>150)
			ct.sizeDelta = new Vector2 (maxWidth+80, heightGroups);
		else 
			ct.sizeDelta = new Vector2 (230, heightGroups);
		
	}

	//Just view reorganizeation
	public static bool blockToogle=false; //When we update the toggle values from outise we want to block the listening operation
	public static void describe(){
		if (Tissue.nbFates != null && MenuGroups.activeSelf) {
			Tissue.blockToogle = true;
			if (Instantiation.CurrentTime != Tissue.CurrentTime)
				Tissue.setTime (Instantiation.CurrentTime);
			//Debug.Log ("DESCRIBE");
			Tissue.grp.describe ();
			Tissue.blockToogle = false;
		}
	}

	//RANDOM COLORIZE
	public static void colorizeListen(Button b,Group gp)  {b.onClick.AddListener (() => Tissue.colorize(gp)); }
	public static void colorize(Group gp){ gp.colorize (); }

}
