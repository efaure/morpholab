2O                         POINT   *1     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;
struct Globals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    float4 glstate_lightmodel_ambient;
    float4 _MainTex_ST;
    float4 _NormalMap_ST;
    float4 _SmoothMap_ST;
    float4 _MetalMap_ST;
    float4 _RetroMap_ST;
    float4 _MainColor;
    float _Smoothness;
    float _Wrap;
    float _BumpDepth;
    float _Metallicity;
    float _Retro;
    float4 _LightColor0;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(0) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    sampler sampler_LightTexture0 [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_NormalMap [[ sampler (2) ]],
    sampler sampler_SmoothMap [[ sampler (3) ]],
    sampler sampler_MetalMap [[ sampler (4) ]],
    sampler sampler_RetroMap [[ sampler (5) ]],
    sampler sampler_RSRM [[ sampler (6) ]],
    texture2d<float, access::sample > _LightTexture0 [[ texture (0) ]] ,
    texture2d<float, access::sample > _MainTex [[ texture (1) ]] ,
    texture2d<float, access::sample > _SmoothMap [[ texture (2) ]] ,
    texture2d<float, access::sample > _MetalMap [[ texture (3) ]] ,
    texture2d<float, access::sample > _RetroMap [[ texture (4) ]] ,
    texture2d<float, access::sample > _NormalMap [[ texture (5) ]] ,
    texture2d<float, access::sample > _RSRM [[ texture (6) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    float4 u_xlat1;
    float3 u_xlat2;
    bool3 u_xlatb2;
    float3 u_xlat3;
    bool u_xlatb3;
    float3 u_xlat4;
    int u_xlati4;
    float3 u_xlat5;
    float3 u_xlat6;
    float3 u_xlat7;
    float u_xlat8;
    float3 u_xlat9;
    float3 u_xlat10;
    int u_xlati12;
    float u_xlat16;
    float u_xlat17;
    float2 u_xlat18;
    float2 u_xlat20;
    float u_xlat24;
    float u_xlat25;
    float u_xlat26;
    float u_xlat27;
    u_xlat0.x = dot(Globals._WorldSpaceLightPos0.xyz, Globals._WorldSpaceLightPos0.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * Globals._WorldSpaceLightPos0.xyz;
    u_xlat24 = (-Globals._WorldSpaceLightPos0.w) + 1.0;
    u_xlat0.xyz = float3(u_xlat24) * u_xlat0.xyz;
    u_xlat1.xyz = (-input.TEXCOORD1.xyz) + Globals._WorldSpaceLightPos0.xyz;
    u_xlat24 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat25 = rsqrt(u_xlat24);
    u_xlat24 = u_xlat24 * -0.100000001;
    u_xlat24 = exp2(u_xlat24);
    u_xlat24 = u_xlat24 * Globals._WorldSpaceLightPos0.w + 1.0;
    u_xlat24 = u_xlat24 + (-Globals._WorldSpaceLightPos0.w);
    u_xlat1.xyz = float3(u_xlat25) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat1.xyz * Globals._WorldSpaceLightPos0.www + u_xlat0.xyz;
    u_xlat1.xy = input.TEXCOORD0.xy * Globals._NormalMap_ST.xy + Globals._NormalMap_ST.zw;
    u_xlat1.xy = _NormalMap.sample(sampler_NormalMap, u_xlat1.xy).yw;
    u_xlat1.xy = u_xlat1.yx * float2(2.0, 2.0) + float2(-1.0, -1.0);
    u_xlat9.xyz = u_xlat1.yyy * input.TEXCOORD4.xyz;
    u_xlat1.xyz = u_xlat1.xxx * input.TEXCOORD3.xyz + u_xlat9.xyz;
    u_xlat25 = Globals._BumpDepth * 8.0;
    u_xlat25 = 8.0 / u_xlat25;
    u_xlat1.xyz = float3(u_xlat25) * input.TEXCOORD2.xyz + u_xlat1.xyz;
    u_xlat25 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat25 = rsqrt(u_xlat25);
    u_xlat1.xyz = float3(u_xlat25) * u_xlat1.xyz;
    u_xlat2.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat10.x = abs(u_xlat2.x) * -0.0187292993 + 0.0742610022;
    u_xlat10.x = u_xlat10.x * abs(u_xlat2.x) + -0.212114394;
    u_xlat10.x = u_xlat10.x * abs(u_xlat2.x) + 1.57072878;
    u_xlat18.x = -abs(u_xlat2.x) + 1.0;
    u_xlat18.x = sqrt(u_xlat18.x);
    u_xlat26 = u_xlat18.x * u_xlat10.x;
    u_xlat26 = u_xlat26 * -2.0 + 3.14159274;
    u_xlatb3 = u_xlat2.x<(-u_xlat2.x);
    u_xlat26 = u_xlatb3 ? u_xlat26 : float(0.0);
    u_xlat10.x = u_xlat10.x * u_xlat18.x + u_xlat26;
    u_xlat10.x = u_xlat10.x * 0.649999976;
    u_xlat10.x = log2(u_xlat10.x);
    u_xlat10.x = u_xlat10.x * 16.0;
    u_xlat10.x = exp2(u_xlat10.x);
    u_xlat10.x = u_xlat10.x + 1.0;
    u_xlat10.x = float(1.0) / u_xlat10.x;
    u_xlat3.xyz = (-input.TEXCOORD1.xyz) + Globals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat18.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat18.x = rsqrt(u_xlat18.x);
    u_xlat4.xyz = u_xlat3.xyz * u_xlat18.xxx + u_xlat0.xyz;
    u_xlat3.xyz = u_xlat18.xxx * u_xlat3.xyz;
    u_xlat18.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat18.x = rsqrt(u_xlat18.x);
    u_xlat4.xyz = u_xlat18.xxx * u_xlat4.xyz;
    u_xlat18.x = dot(u_xlat1.xyz, u_xlat4.xyz);
    u_xlat18.x = clamp(u_xlat18.x, 0.0f, 1.0f);
    u_xlat18.x = log2(u_xlat18.x);
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * 0.5 + 0.5;
    u_xlat0.x = log2(u_xlat0.x);
    u_xlat0.x = u_xlat0.x * 0.850000024;
    u_xlat8 = exp2(u_xlat0.x);
    u_xlat4.xy = input.TEXCOORD0.xy * Globals._SmoothMap_ST.xy + Globals._SmoothMap_ST.zw;
    u_xlat16 = _SmoothMap.sample(sampler_SmoothMap, u_xlat4.xy).x;
    u_xlat4.xy = float2(u_xlat16) * float2(Globals._Smoothness) + float2(-2.0, -1.0);
    u_xlat16 = u_xlat16 * Globals._Smoothness;
    u_xlat26 = u_xlat4.x * 0.847996891;
    u_xlat4.x = (-u_xlat4.y) * 0.0900000036 + 1.0;
    u_xlat26 = exp2(u_xlat26);
    u_xlat26 = u_xlat26 * 4.0 + 1.5;
    u_xlat27 = u_xlat8 * u_xlat26;
    u_xlat18.x = u_xlat18.x * u_xlat27;
    u_xlat18.x = exp2(u_xlat18.x);
    u_xlat27 = u_xlat8 + 1.0;
    u_xlat26 = u_xlat26 * u_xlat27;
    u_xlat26 = log2(u_xlat26);
    u_xlat18.x = u_xlat26 * u_xlat18.x;
    u_xlat10.x = u_xlat18.x * u_xlat10.x;
    u_xlat10.x = u_xlat16 * u_xlat10.x;
    u_xlat10.x = u_xlat10.x * 0.693147182;
    u_xlat18.x = float(1.0) / u_xlat16;
    u_xlat16 = u_xlat16 * 0.166666672 + 1.0;
    u_xlat10.x = u_xlat18.x * u_xlat10.x;
    u_xlat10.x = u_xlat10.x * 0.5;
    u_xlat18.x = (-u_xlat8) + 1.0;
    u_xlat18.x = log2(u_xlat18.x);
    u_xlat20.xy = input.TEXCOORD0.xy * Globals._RetroMap_ST.xy + Globals._RetroMap_ST.zw;
    u_xlat26 = _RetroMap.sample(sampler_RetroMap, u_xlat20.xy).x;
    u_xlat26 = u_xlat26 * Globals._Retro;
    u_xlat27 = u_xlat26 * u_xlat26;
    u_xlat20.xy = float2(u_xlat27) * float2(16.0, 4.0);
    u_xlat18.x = u_xlat18.x * u_xlat20.x;
    u_xlat18.x = exp2(u_xlat18.x);
    u_xlat27 = max(u_xlat26, 0.0);
    u_xlat18.x = u_xlat18.x * u_xlat27;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat17 = max(u_xlat1.x, 0.0);
    u_xlat1.x = u_xlat1.x + u_xlat1.x;
    u_xlat1.x = u_xlat1.y * (-u_xlat1.x) + u_xlat3.y;
    u_xlat1.x = u_xlat1.x * 0.5 + 0.5;
    u_xlat4.y = (-u_xlat1.x) + 1.0;
    u_xlat3.xyz = _RSRM.sample(sampler_RSRM, u_xlat4.xy).xyz;
    u_xlat1.x = (-u_xlat17) + 1.0;
    u_xlat17 = log2(u_xlat1.x);
    u_xlat16 = u_xlat16 * u_xlat17;
    u_xlat17 = u_xlat26 * 0.5 + 2.0;
    u_xlat17 = u_xlat16 * u_xlat17;
    u_xlat16 = exp2(u_xlat16);
    u_xlat17 = exp2(u_xlat17);
    u_xlat18.x = u_xlat17 * u_xlat18.x;
    u_xlat18.x = u_xlat20.y * u_xlat18.x;
    u_xlat27 = exp2((-u_xlat1.x));
    u_xlat1.x = u_xlat1.x * u_xlat1.x + (-u_xlat27);
    u_xlati4 = int((0.0<u_xlat26) ? 0xFFFFFFFFu : 0u);
    u_xlati12 = int((u_xlat26<0.0) ? 0xFFFFFFFFu : 0u);
    u_xlat26 = min(abs(u_xlat26), 2.0);
    u_xlati4 = (-u_xlati4) + u_xlati12;
    u_xlat4.x = float(u_xlati4);
    u_xlat4.x = u_xlat4.x * 0.5 + 0.5;
    u_xlat1.x = u_xlat4.x * u_xlat1.x + u_xlat27;
    u_xlat1.x = u_xlat26 * u_xlat1.x;
    u_xlat0.x = u_xlat0.x * u_xlat20.x;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat0.x = u_xlat0.x * 4.0;
    u_xlat8 = u_xlat8 * u_xlat8 + (-u_xlat0.x);
    u_xlat0.x = u_xlat4.x * u_xlat8 + u_xlat0.x;
    u_xlat0.x = u_xlat1.x * u_xlat0.x + u_xlat18.x;
    u_xlat1.w = 1.0;
    u_xlat1.xyw = _RSRM.sample(sampler_RSRM, u_xlat1.wy).xyz;
    u_xlat4.xyz = Globals.glstate_lightmodel_ambient.xyz + Globals.glstate_lightmodel_ambient.xyz;
    u_xlat1.xyw = u_xlat1.xyw * u_xlat4.xyz;
    u_xlat3.xyz = u_xlat3.xyz * u_xlat4.xyz;
    u_xlat18.xy = input.TEXCOORD0.xy * Globals._MetalMap_ST.xy + Globals._MetalMap_ST.zw;
    u_xlat8 = _MetalMap.sample(sampler_MetalMap, u_xlat18.xy).x;
    u_xlat18.x = u_xlat8 * Globals._Metallicity;
    u_xlat4.xyz = u_xlat18.xxx * Globals.glstate_lightmodel_ambient.xyz;
    u_xlat1.xyw = u_xlat1.xyw * float3(1.5, 1.5, 1.5) + u_xlat4.xyz;
    u_xlat3.xyz = u_xlat3.xyz * float3(1.5, 1.5, 1.5) + u_xlat4.xyz;
    u_xlat4.xyz = float3(u_xlat17) * u_xlat1.xyw;
    u_xlat4.xyz = u_xlat4.xyz * u_xlat20.yyy + u_xlat0.xxx;
    u_xlat4.xyz = max(u_xlat4.xyz, float3(0.0, 0.0, 0.0));
    u_xlat0.x = dot(input.TEXCOORD5.xyz, input.TEXCOORD5.xyz);
    u_xlat0.x = _LightTexture0.sample(sampler_LightTexture0, u_xlat0.xx).w;
    u_xlat17 = u_xlat0.x * 0.800000012 + 0.200000003;
    u_xlat4.xyz = float3(u_xlat17) * u_xlat4.xyz;
    u_xlat4.xyz = float3(u_xlat24) * u_xlat4.xyz;
    u_xlat5.xy = input.TEXCOORD0.xy * Globals._MainTex_ST.xy + Globals._MainTex_ST.zw;
    u_xlat5.xyz = _MainTex.sample(sampler_MainTex, u_xlat5.xy).xyz;
    u_xlat6.xyz = u_xlat5.xyz * Globals._MainColor.xyz;
    u_xlat17 = dot(u_xlat6.xyz, float3(0.300000012, 0.589999974, 0.109999999));
    u_xlat6.xyz = u_xlat18.xxx * (-u_xlat6.xyz) + u_xlat6.xyz;
    u_xlat5.xyz = u_xlat5.xyz * Globals._MainColor.xyz + (-float3(u_xlat17));
    u_xlat5.xyz = u_xlat18.xxx * u_xlat5.xyz + float3(u_xlat17);
    u_xlat4.xyz = u_xlat4.xyz * u_xlat5.xyz;
    u_xlat17 = (-u_xlat8) * Globals._Metallicity + 1.0;
    u_xlat8 = u_xlat8 * Globals._Metallicity + 2.0;
    u_xlat16 = u_xlat16 * u_xlat17 + u_xlat18.x;
    u_xlat7.xyz = u_xlat3.xyz * u_xlat5.xyz;
    u_xlat3.xyz = u_xlat3.xyz * u_xlat6.xyz;
    u_xlat7.xyz = float3(u_xlat16) * u_xlat7.xyz;
    u_xlat7.xyz = u_xlat7.xyz + u_xlat7.xyz;
    u_xlat4.xyz = u_xlat4.xyz * Globals._LightColor0.xyz + u_xlat7.xyz;
    u_xlat17 = u_xlat24 * u_xlat0.x;
    u_xlat7.xyz = u_xlat5.xyz * float3(u_xlat17);
    u_xlat7.xyz = u_xlat7.xyz * Globals._LightColor0.xyz;
    u_xlat7.xyz = float3(u_xlat8) * u_xlat7.xyz;
    u_xlat5.xyz = u_xlat5.xyz * u_xlat7.xyz;
    u_xlat10.xyz = u_xlat10.xxx * u_xlat5.xyz + u_xlat4.xyz;
    u_xlat8 = (-u_xlat0.x) + 1.0;
    u_xlat17 = max(Globals._Wrap, -0.25);
    u_xlat17 = min(u_xlat17, 1.0);
    u_xlat0.x = u_xlat17 * u_xlat8 + u_xlat0.x;
    u_xlat0.x = u_xlat24 * u_xlat0.x;
    u_xlat0.xyw = u_xlat6.xyz * u_xlat0.xxx;
    u_xlat4.xyz = Globals._LightColor0.xyz * Globals._LightColor0.xyz;
    u_xlat0.xyw = u_xlat0.xyw * u_xlat4.xyz;
    u_xlat0.xyw = u_xlat6.xyz * u_xlat0.xyw;
    u_xlat1.xyw = u_xlat1.xyw * u_xlat6.xyz;
    u_xlat1.xyw = u_xlat1.xyw + u_xlat1.xyw;
    u_xlat1.xyw = u_xlat3.xyz * float3(u_xlat16) + u_xlat1.xyw;
    u_xlat16 = (-u_xlat17) + 1.0;
    u_xlat16 = u_xlat2.x * u_xlat16 + u_xlat17;
    u_xlat17 = u_xlat17 * 2.0 + 1.0;
    u_xlat16 = max(u_xlat16, 0.0);
    u_xlat16 = log2(u_xlat16);
    u_xlat16 = u_xlat16 * u_xlat17;
    u_xlat16 = exp2(u_xlat16);
    u_xlat0.xyz = float3(u_xlat16) * u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz * float3(2.0, 2.0, 2.0) + u_xlat1.xyw;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat10.xyz;
    u_xlat0.xyz = max(u_xlat0.xyz, float3(0.0, 0.0, 0.0));
    u_xlat1.xyz = min(u_xlat0.xyz, float3(2.0, 2.0, 2.0));
    u_xlat0.xyz = min(u_xlat0.xyz, float3(1.0, 1.0, 1.0));
    u_xlatb2.xyz = (float3(1.0, 1.0, 1.0)<u_xlat1.xyz);
    u_xlat1.xyz = max(u_xlat1.xyz, float3(1.0, 1.0, 1.0));
    u_xlat1.xyz = float3(1.0, 1.0, 1.0) / u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat0.xyz * u_xlat0.xyz;
    u_xlat3.xyz = u_xlat1.xyz * float3(0.0208350997, 0.0208350997, 0.0208350997) + float3(-0.0851330012, -0.0851330012, -0.0851330012);
    u_xlat3.xyz = u_xlat1.xyz * u_xlat3.xyz + float3(0.180141002, 0.180141002, 0.180141002);
    u_xlat3.xyz = u_xlat1.xyz * u_xlat3.xyz + float3(-0.330299497, -0.330299497, -0.330299497);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat3.xyz + float3(0.999866009, 0.999866009, 0.999866009);
    u_xlat3.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat3.xyz = u_xlat3.xyz * float3(-2.0, -2.0, -2.0) + float3(1.57079637, 1.57079637, 1.57079637);
    u_xlat2.xyz = select(float3(0.0, 0.0, 0.0), u_xlat3.xyz, bool3(u_xlatb2.xyz));
    output.SV_Target0.xyz = u_xlat0.xyz * u_xlat1.xyz + u_xlat2.xyz;
    output.SV_Target0.w = 1.0;
    return output;
}
                            Globals �         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        glstate_lightmodel_ambient                           _MainTex_ST                   0      _NormalMap_ST                     @      _SmoothMap_ST                     P      _MetalMap_ST                  `      _RetroMap_ST                  p   
   _MainColor                    �      _Smoothness                   �      _Wrap                     �   
   _BumpDepth                    �      _Metallicity                  �      _Retro                    �      _LightColor0                  �         _LightTexture0                _MainTex         
   _SmoothMap           	   _MetalMap            	   _RetroMap            
   _NormalMap              _RSRM               Globals            