

# <span style="color:#26B260">WARNING</span>

This git is fixed and correspond to the publication : Leggio, B., Laussu, J., Carlier, A. et al. MorphoNet: an interactive online morphological browser to explore complex multi-scale data. Nat Commun 10, 2812 (2019). https://doi.org/10.1038/s41467-019-10668-1

The currect version of MorphoNet is splitted now in several Git:
- The 3D Viewer : https://gitlab.inria.fr/MorphoNet/morphonet_unity
- The Python API : https://gitlab.inria.fr/MorphoNet/morphonet_api
- The ImageJ API : https://gitlab.inria.fr/MorphoNet/morphonet_imagej





# MorphoNet
MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching.

MorphoNet offers a comprehensive palette of interactions to explore the structure, dynamics and variability of biological shapes and its connection to genetic expressions.

By handling a broad range of natural or simulated morphological data, it fills a gap which has until now limited the quantitative understanding of morphodynamics and its genetic underpinnings by contributing to the creation of ever-growing morphological atlases.



## Getting Started

This package is composed of :
- [MorphoNet](MorphoNet)  : the source code to re-build MorphoNet using Unity 2018.2
- [MorphoNet-V-3.1.8](MorphoNet-V-3.1.8)  : the code based on the server side
- [DB](DB) : The database structure of MorphoNet and the Taxonomy
- [API](API) :  3 different API connectors 
- [Converters](Converters) : several converters to OBJ format which is used in MorphoNet
- [HELP](HELP) : Help pages 
- [Tutorials](Tutorials) : 3 tutorials to use MorphoNet in different ways
- [LICENSE.md](LICENSE.md) : This project is licensed under the CeCILL License - see this file for details
- [InstallMorphoNet.md](InstallMorphoNet.md)  : The instruction file to install a new instance of MorphoNet on a new Server
- [README.md](README.md)  : this README file



### Installing

You can install MorphoNet on a [Debian 9.5.0](https://www.debian.org) server using  the Installation instruction provided in the [InstallMorphoNet.md](InstallMorphoNet.md) file.
You will need several package such as :  an apache server with a mysql database , and several libraries in python.

We use package manager apt-get and pip to install dependencies:

```
apt-get install apache2 php mysql-server libapache2-mod-php php-mysql 
apt-get install phpmyadmin 
apt-get install python-pip python-dev python-mysqldb
apt-get install libsm6 libxrender-dev ffmpeg
pip install opencv-python
```



## Built With

* [Unity 2018.2](https://unity3d.com/fr/get-unity/download/archive) - The game engine framework used



## Authors

* **Bruno Leggio**  [1,2,3]
* **Julien Laussu**  [1]
* **Axel Carlier**  [4]
* **Christophe Godin**  [2,3]
* **Patrick Lemaire**  [1,3]
* **Emmanuel Faure**  [1,2,3,4,5]

### with theses affiliations
* [1] Centre de Recherche de Biologie cellulaire de Montpellier, CRBM, CNRS, Université de Montpellier, France.
* [2] Laboratoire Reproduction et Développement des Plantes, Univ Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRA, Inria, F-69342, Lyon, France.
* [3] Institut de Biologie Computationnelle, IBC, Université de Montpellier, France.
* [4] Informatics Research Institute of Toulouse (IRIT), CNRS, INPT, ENSEEIHT, University of Toulouse I and III, France.
* [5] Laboratoire d'Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM), LIRMM, Univ Montpellier, CNRS, Montpellier, France.

*Correspondence should be addressed to Emmanuel Faure (emmanuel.faure@lirmm.fr)


## License

This project is licensed under the CeCILL License - see the [LICENSE.md](LICENSE.md) file for details







