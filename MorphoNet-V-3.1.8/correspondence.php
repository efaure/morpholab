<?php


include('uni-functions.php');

$now=new DateTime();
if ($connected){
	if(isset($_POST["download"])){ //Query on the  Upload
		$download=intval($_POST["download"]);
		if(!isset($_POST["id_dataset"])) echo "ERROR dataset is not define";
		else{
			$id_dataset=' id_dataset='.$_POST["id_dataset"];

		 	if($download==1){//List Infos from the dataset (lineage, could be Selection etc...)
			 	
			 	//First Get Guy Status on this dataset
			 	$id_people=$_POST["id_people"];
			 	$isAdmin=getAdmin($id_people);
			 
			 	$rows=[];
			 	$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE (id_people='.$_POST["id_people"]. ' OR  common=1) AND '.$id_dataset; //DEFAULT QUERY
			 	if($isAdmin){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE '.$id_dataset;}
			 	else{ //Check if this dataset is in  Groups Status
				 	//First we look if this guy as specific right on this dataset
				 	//HOW ==0 READER
				 	//HOW ==1 AS OWNER

					$how=query_first('select how from sharing where base="dataset" and id_base='.$_POST["id_dataset"].' AND id_who='.$id_people); //SPECIFIC RIGHT IN 
				 	if($how==""){ //NO SPECIFIC RIGHT IN FOR THE USER  WE LOOK IN GROUP
					 	$how=-1;
					 	$result = mysqli_query($link,'select how,id_group from sharing where base="dataset" and id_base='.$_POST["id_dataset"].' AND id_group!="NULL"');	
			     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
				 		else  
				 			while($r = mysqli_fetch_assoc($result))  {
				 				$status=query_first('select status from people_group where id_people='.$id_people.' and id_group='.$r['id_group']);
				 				if($status=="member" && $how!=1)$how=0;
				 				if($status=="manager")$how=1;
				 				
				 		   }
				 	}
					if($how==0){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE (id_people='.$_POST["id_people"]. ' OR  common=1) AND '.$id_dataset;} //READER
					if($how==1){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE '.$id_dataset;} //MANAGER
			 	}
			 	
			 		 		
		 		if($query!=""){
			 		$result = mysqli_query($link,$query);	
		     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
			 		else  
			 			while($r = mysqli_fetch_assoc($result))  {
			 				$rows[] = $r;
	
			 		}
			 	}
		 		
		 		print  jsonRemoveUnicodeSequences($rows);
		 	}
		 	if($download==2 && isset($_POST['id_infos'])){//Get Infos from the dataset (lineage,etcc)
		 		echo query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_infos'].' AND '.$id_dataset);
		 	}
		 	/*if($download==4 && isset($_POST['type'])){//List all infos for a sepecific user
		 		$rows = array();
		 		$result = mysqli_query($link,'SELECT id FROM correspondence WHERE id_dataset='.$_POST["id_dataset"].' AND id_people='.$_POST["id_people"].$type);
		 	}*/
		 	if($download==5){//DELETE A CORRESPONDANCE
		 		echo query('DELETE FROM share WHERE base="correspondence" AND id_base='.$_POST['id_infos'].' '.$id_people);
		 		echo query('DELETE FROM correspondence WHERE id='.$_POST['id_infos'].' AND '.$id_dataset.$id_people);
		 	}

		 	if($download==6){//CREATE A NEW CORRESPONDANCE
	 			$file=$_FILES['field'];
	 			echo query_id('INSERT INTO correspondence (id_dataset,id_people,infos,type,field,date,datatype) VALUES ('.$_POST['id_dataset'].','.$_POST['id_people'].',"'.$_POST['infos'].'",'.$_POST['type'].',"'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'","'.$now->format('Y-m-d h:s:i').'","'.$_POST['datatype'].'")');
	 		}

	 		if($download==8){//CREATE A NEW CORRESPONDANCE (FROM JAVASCRIPT UPLOAD)
		 		$data=explode("\n",$_POST['data']);
		 		$startIndex = 0;
		 		while ($data [$startIndex] [0] == '#')  $startIndex++; //We skip comments
		 		$types = "";
		 		$infotypes = explode(":",$data [$startIndex]); 
		 		$datatype = $infotypes [1];
		 						
	 			$field=bzcompress($_POST['data']);
	 			echo query_id('INSERT INTO correspondence (id_dataset,id_people,infos,type,field,date,datatype) VALUES ('.$_POST['id_dataset'].','.$_POST['id_people'].',"'.$_POST['infos'].'",'.$_POST['type'].',"'.mysqli_escape_string($link,$field).'","'.$now->format('Y-m-d h:s:i').'","'.$datatype.'")');
	 		}
	 		
	 		if($download==9){//List Infos from the dataset (lineage, could be Selection etc...)
		 		$result = mysqli_query($link,'SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE id='.$_POST["id_correspondence"]. ' AND id_people='.$_POST["id_people"]. ' AND '.$id_dataset);
		     	if (!$result) echo UTF('Error : ' . mysqli_error($link));
		 		else  while($r = mysqli_fetch_assoc($result))  $rows[] = $r;
		 		print  jsonRemoveUnicodeSequences($rows);
		 	}

	 		if($download==7){//Download a CORRESPONDANCE field
		 		//GET INFOS
		 		$field=query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_infos'].' AND '.$id_dataset);
		 		$ufield=bzdecompress($field);
		 		unset($sfield);
		 		
		 		$date = date_create();
	 			$ts=date_timestamp_get($date);
	 			system("mkdir /var/www/TEMP/".$ts);
	 			$filename="TEMP/".$ts."/".$_POST['id_dataset'].'-'.$_POST['id_infos'].'.txt';
		 			
		 		$fp = fopen("/var/www/TEMP/".$ts."/index.php", 'w');
	 			fwrite($fp, "<?php header('Location:http://".$_SERVER["SERVER_NAME"]."'); ?>");
	 			fclose($fp);
		 		
		 		$tab = explode("\n", $ufield);
 				unset($ufield);
 				$fp = fopen("/var/www/".$filename, 'w');
 				$Infos=array();
 				foreach ($tab as &$value) if(trim($value)!="") {
	 				if($value[0]=="#") fwrite($fp, $value."\n");
	 				else{
		 				$tabcid = explode(":", $value);
		 				if(count($tabcid)==2 && $tabcid[0]!="type"){
			 				$c=getCell($tabcid[0]);
			 				if(!array_key_exists($c["t"],$Infos)) $Infos[$c["t"]]=array();
			 				$Infos[$c["t"]][$c["id"]]=$tabcid[1];	
			 			} else fwrite($fp, $value."\n");
			 		}
		 		}
		 		//NOW WE GET THE CURRATED VALUE
		 		$result = mysqli_query($link,'select id_object,value from curration where id_correspondence='.$_POST['id_infos'].' AND '.$id_dataset. ' ORDER BY id');	
	     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
		 		else  
		 			while($r = mysqli_fetch_assoc($result))  {
			 			$c=getCell($r['id_object']);
			 			if(!array_key_exists($c["t"],$Infos)) $Infos[$c["t"]]=array();
		 				$Infos[$c["t"]][$c["id"]]=$r['value'];		 				
		 		   }
		 		
		 		ksort($Infos);
		 		foreach ($Infos as $t => $InfosT){
			 		ksort($InfosT);
			 		foreach ($InfosT as $cid => $value)
		 			 	fwrite($fp, $t.",".$cid.":".$value."\n");
			 	}
			 	fclose($fp);
		 		
		 		echo $filename;


	 		}
	 		//SHARE 
	 		if($download==10){//Active or Inactive Common  
	 			echo query('UPDATE  correspondence SET common='.$_POST['common'].' WHERE id='.$_POST['id_infos'].' AND '.$id_dataset);
	 		}

	 		if($download==11){//Download Common Dataset 
		
 				$rows = array();
				$result = mysqli_query($link,'SELECT id,name FROM dataset WHERE archive=0 and id_dataset_type='.$_POST['id_dataset_type'].' AND id_people='.$_POST["id_people"].' and bundle>=-1 order by id');
				
	     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
		 		else {
					while($r = mysqli_fetch_assoc($result)) $rows[] = $r;
		 		}
		 		
		 		//LOOK IN THE SHARE DATASET
	            $result = mysqli_query($link,'SELECT id_base,id_people FROM share WHERE base="dataset" and id_people!='.$_POST["id_people"].' and ( id_who='.$_POST["id_people"].' or id_who=0)  order by id_base');
	            if (!$result) echo UTF('Error : ' . mysqli_error($link));
		 		else {
					while($r = mysqli_fetch_row($result)) {
						$result2 = mysqli_query($link,'SELECT id,name  FROM dataset WHERE archive=0 and id='.$r[0].' and id_dataset_type='.$_POST['id_dataset_type'].' and bundle>=-1 and id_people='.$r[1]);
						while($r2 = mysqli_fetch_assoc($result2)) {
							$rows[] = $r2;
						}	
					}
		 		}
		 		
		 		print  jsonRemoveUnicodeSequences($rows);
	 		}
	 		
	 		if($download==12){//Download a CORRESPONDANCE field from an other dataset
		 		//print_r($_POST);echo "</br>";
		 		$id_bridge=$_POST['id_bridge'];
		 		$id_bridge_other=$_POST['id_bridge_other'];
		 		if($id_bridge_other==-1 || $id_bridge==-1)
		 			echo query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_infos'].' AND '.$id_dataset);
	 			else{
		 			
			 		$type="";
			 		$sfield=query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_infos'].' AND id_dataset='.$_POST['id_dataset_infos']);
			 		$ufield=bzdecompress($sfield);
			 		unset($sfield);
			 		$tab = explode("\n", $ufield);
			 		unset($ufield);
			 		$CID2Infos =array();
			 		foreach ($tab as &$value) {
		 				$tabcid = explode(":", $value);
			 			if(count($tabcid)==2){
				 			if($tabcid[0]=="type")$type=$tabcid[1];
				 			else $CID2Infos[$tabcid[0]]=floatval($tabcid[1]);
			 			}
			 		}
			 		unset($tab);
			 		//print_r($CID2Infos); 	echo "->CID2Infos</br></br>";
			 		
			 		
			 		$sfield=query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_bridge_other'].' AND id_dataset='.$_POST['id_dataset_infos']);
			 		$ufield=bzdecompress($sfield);
			 		unset($sfield);
			 		$tab = explode("\n", $ufield);
			 		unset($ufield);
			 		$Name2Infos=array();
			 		$NbBy=array();
			 		foreach ($tab as &$value) {
			 			$tabcid = explode(":", $value);
			 			if(count($tabcid)==2 && $tabcid[0]!="type"){
				 			$Name2=$tabcid[1];
				 			$CID2=$tabcid[0];
				 			if(array_key_exists($CID2,$CID2Infos)){
					 			if(!array_key_exists($Name2,$Name2Infos)){
						 			$Name2Infos[$Name2]=$CID2Infos[$CID2];
						 			$NbBy[$Name2]=1;
						 		}else{
							 		$Name2Infos[$Name2]+=$CID2Infos[$CID2];
						 			$NbBy[$Name2]++;
						 		}
				 				//if(!in_array($CID2Infos[$CID2],$Name2Infos[$Name2])) array_push($Name2Infos[$Name2],$CID2Infos[$CID2]);
				 			}
					 	}
			 		}
			 		//AVERAGE
			 		foreach ($Name2Infos as $Name2 => $value)
				 		$Name2Infos[$Name2]/=$NbBy[$Name2];
				 	unset($CID2Infos);
			 		unset($tab);
			 		unset($NbBy);
			 		//print_r($Name2Infos);	echo "->Name2Infos</br></br>";
			 	
			 		$sfield=query_first('SELECT field FROM correspondence WHERE id='.$_POST['id_bridge'].' AND '.$id_dataset);
		 			$ufield=bzdecompress($sfield);
		 			unset($sfield);
		 			$tab = explode("\n", $ufield);
		 			unset($ufield);
		 			$cidName=array();
		 			$NbBy=array();
		 			
		 			foreach ($tab as &$value) {
			 			$tabcid = explode(":", $value);
			 			if(count($tabcid)==2 && $tabcid[0]!="type"){
				 			$cid=$tabcid[0];
				 			$name=$tabcid[1];
				 			if(array_key_exists($name,$Name2Infos)){
					 			if(array_key_exists($cid,$cidName)){
					 				$cidName[$cid]+=$Name2Infos[$name];
					 				$NbBy[$cid]++;
				 				}else{
					 				$cidName[$cid]=$Name2Infos[$name];
					 				$NbBy[$cid]=1;
				 				}
					 		}		
				 		}
			 		}
			 		unset($tab);
			 		$field="type:$type\n";
			 		foreach ($cidName as $cidName => $value){
				 		$v=$value/$NbBy[$cidName];
				 		$field=$field."$cidName:$v\n";
				 	}
				 	now=new DateTime();
			 		$id_people=$_POST["id_people"];
			 		$infos=$_POST["infos"];
			 		$id_dataset=$_POST["id_dataset"];
			 		//echo "type=$type";
			 		echo query_id('INSERT INTO correspondence (id_dataset,infos,field,date,id_people,type,datatype) VALUES ('.$id_dataset.',"'.$infos.'","'.mysqli_escape_string($link,bzcompress($field)).'","'.$now->format('Y-m-d h:s:i').'",'.$id_people.',2,"'.$type.'")'); 
			 		
			 		
		 		}
	 		}
	 		if($download==13){//Download a CORRESPONDANCE field from an other dataset
		 		#Convert in python request
		 		$function=$_POST['function'];
		 		
		 		$cmd='python correspondence.py '.$function.' '.$_POST["id_people"].' '.$_POST["id_dataset"].' "'.$infos=$_POST["infos"].'"';
		 		
		 		$i=0;
			 	while(isset($_POST['id_infos_'.$i])) { $cmd=$cmd.' '.$_POST['id_infos_'.$i]; $i++;}
		 		os.system($cmd);
	 		}

	 		//COMMON
	 		if($download==14){//Active or Inactive Common  
	 			echo query('UPDATE  correspondence SET type='.$_POST['type'].' WHERE id='.$_POST['id_infos'].' AND '.$id_dataset);
	 		}
		}

	}
	mysqli_close($link);
}


?>
