<?php
	
session_start();


include 'functions.php';
$base=$_POST["base"];
$id_base=$_POST["id_base"];

$id_group_admin=0;
//HOW ==0 READER
//HOW ==1 AS OWNER

//WE FIRST HAVE TO CHECK THAT WE ARE ADMIN OF THIS DATASET
if ($connected){
	$id_group_admin=query_first('SELECT id FROM groups WHERE name="admin"');
	
	$is_admin=0;
	$id_owner=query_first('SELECT id_people FROM dataset where id='.$id_base);
	if($id_owner!=$_SESSION['ID']){ //Not Owner so we look if this dataset were share as admin
		$id_admin=query_first('SELECT id FROM sharing where base="'.$base.'" AND id_base='.$id_base.' AND how=1');
		if($id_admin>0)  $is_admin=1;
	} else $is_admin=1;

					
			 		

	

	if(isset($_POST['group'])){ //SHARE WITH GROUP
		$id_group=$_POST['group'];
		$how=$_POST['how'];
		$now=new DateTime();
		echo query_id('INSERT INTO sharing (base,id_base,id_people,id_who,id_group,how,date) VALUES ("'.$base.'",'.$id_base.','.$_SESSION['ID'].',NULL,'.$id_group.','.$how.',"'.$now->format('Y-m-d h:s:i').'")');
	}
	if(isset($_POST['user'])){ //SHARE WITH user
		$id_user=$_POST['user'];
		$how=$_POST['how'];
		$now=new DateTime();
		echo query_id('INSERT INTO sharing (base,id_base,id_people,id_group,id_who,how,date) VALUES ("'.$base.'",'.$id_base.','.$_SESSION['ID'].',NULL,'.$id_user.','.$how.',"'.$now->format('Y-m-d h:s:i').'")');
	}

	if(isset($_POST['unshare'])){ //UN SHARE
		$id_unshare=$_POST['unshare'];
		echo "id_unshare=$id_unshare";
		query('DELETE FROM sharing where base="'.$base.'" AND id_base='.$id_base.' AND id='.$id_unshare); 
	}

	$shared=query_array('SELECT id,id_who,id_group,how FROM sharing where base="'.$base.'" AND id_base='.$id_base);
	function getShared($shared,$type,$id_g,$how){
		foreach($shared as $id_share_t => $share){
			if($share[$type]!="" && $share[$type]==$id_g && $share['how']==$how) {
				//echo "FOUND ".$id_g." on ".$type." with how=".$how." -> ".$id_share_t."===".$share[$type]."</br>";
				return $id_share_t;
			}
		}
		return -1;
	}

	if(isset($_POST["view"])){ //FOR VIEW ALL CURRENT SHARING 

		$groups = query_array('SELECT id,name,id_owner,date,status,privacy FROM groups WHERE id!='.$id_group_admin);
		$peoples = query_array('SELECT id,name,surname,id_Institution,id_Laboratory,id_Team FROM people WHERE exist=1 AND id>0 ORDER by name,surname');
		$Institutions = query_array('SELECT id,name FROM Institution');
		$Laboratorys = query_array('SELECT id,name FROM Laboratory');
		$Teams = query_array('SELECT id,name FROM Team');

		//PUBLIC
		?>
		<div id="public" style=" display: table; width: 100%">
    		<span style="display: table;font-size:20px;color:grey;width: 300px;margin-left: 0 auto; ">Make this dataset public 
    		<label class="switch">   
    		<?php 
    			$ispublic=getShared($shared,"id_who",0,0);
    			echo '<input id="share_0_0" type="checkbox" onchange="shareUser('.$ispublic.',0,0)" '; 
	           if($ispublic!=-1) echo ' checked ';
	           echo '> ';
	          ?> <span class="slider round"></span> </label>
	          </span>
		 	<div>
    	<?php
		//GROUP
		?>
		<div id="groups" style=" display: table; width: 100%;margin-top:30px;">
       		<span style="display: table;font-size:20px;color:grey;width: 300px;margin-left: 0 auto; ">Share with a group</span>
     		<div style=" width: 600px;margin: 0 auto;margin-top:20px">
			    <table id="listgroups">
			        <tr>
			          <td class="tdtitle"> Owner</td>
			          <td class="tdtitle"> Reader</td>
			          <td class="tdtitle"> Name</td>
			          <td class="tdtitle"> Private </td>
			          <td class="tdtitle"> Create by </td>
			          <td class="tdtitle"> Creation date  </td>
			        </tr>
			        <?php
			        foreach ($groups as $id_group_t => $group){
			          $isGroupManager=0;
			          $id_grpadmin=query_first('SELECT id FROM people_group WHERE id_people='.$id_owner.' AND id_group='.$id_group_t.' AND status="manager"');
			          if($id_grpadmin!="") $isGroupManager=1;
			          $id_share_owner=getShared($shared,'id_group',$id_group_t,1); 
			          $id_share_reader=getShared($shared,'id_group',$id_group_t,0); 

			          echo "<tr>";

			        	//OWNER
		               echo '<td><div id="esharegroup_1_'.$id_group_t.'" >';
			           echo '<label class="switch">   <input id="sharegroup_1_'.$id_group_t.'" type="checkbox" onchange="shareGroup('.$id_share_owner.','.$id_group_t.',1)" '; 
			           if($id_share_owner!=-1) echo ' checked ';
			           echo '> <span class="slider round"></span> </label>'; 
			           echo '</div> </td>';

		              //READER
		               echo '<td><div id="esharegroup_0_'.$id_group_t.'" '; if($id_share_owner!=-1) echo ' style="display:none" '; echo '>';
			           echo '<label class="switch">   <input id="sharegroup_0_'.$id_group_t.'" type="checkbox" onchange="shareGroup('.$id_share_reader.','.$id_group_t.',0)"'; 
			           if($id_share_reader!=-1) echo ' checked ';
			           echo '> <span class="slider round"></span> </label>'; 
			           echo '</div></td>';


			          echo "<td>".$group['name']."</td>"; 
			          //PRIVATE
			          echo "<td>";
			          $isPrivate=0;  if($group['privacy']=="private") $isPrivate=1;
			          if( $isPrivate ) echo '<image src="images/valid.png" width="20px"  style="margin-left:20px"/>'; 
			          echo "</td>"; 

			          echo "<td>".$peoples[$group['id_owner']]["name"].' '.$peoples[$group['id_owner']]["surname"]."</td>"; 
			          echo "<td>".$group['date']."</td>";  
			          echo "</tr>";
			        }
			      ?>
			      </table>
    		</div>
    	</div>

    	<?php
    	//USERS
    	?>

       <div id="users" style=" display: table; width: 100%;margin-top:30px">
    		<span style="display: table;font-size:20px;color:grey;width: 300px;margin-left: 0 auto; ">Share with a user</span>
     		<div style=" width: 600px;margin: 0 auto;margin-top:20px">
			    <table id="listusers">
        		<tr>
       			  <td class='tdtitle'> Owner</td>
           		  <td class='tdtitle'> Reader </td>
		          <td class="tdtitle"> Name </td>
		          <td class="tdtitle"> SurName </td>
		          <td class="tdtitle"> Institution </td>
		          <td class="tdtitle"> Laboratory </td>
		          <td class="tdtitle"> Team </td>
        	    </tr>

		        <?php
		        foreach ($peoples as $id_people => $people){
		          // if($id_people!=$_SESSION['ID']) {
		           	echo "<tr>";
		          	 $id_share_owner=getShared($shared,'id_who',$id_people,1); 
		          	 $id_share_reader=getShared($shared,'id_who',$id_people,0); 

		            //OWNER
		            if($id_people!=$id_owner) {
		               echo '<td><div id="eshare_1_'.$id_people.'" >';
			           echo '<label class="switch">   <input id="share_1_'.$id_people.'" type="checkbox" onchange="shareUser('.$id_share_owner.','.$id_people.',1)" '; 
			           if($id_share_owner!=-1)  echo ' checked ';
			           echo '> <span class="slider round"></span> </label>'; 
			           echo '</div></td>';

		            //READER
		             echo '<td><div id="eshare_0_'.$id_people.'" '; if($id_share_owner!=-1) echo ' style="display:none" '; echo '>';
			           echo '<label class="switch">   <input id="share_0_'.$id_people.'" type="checkbox" onchange="shareUser('.$id_share_reader.','.$id_people.',0)" ';
			           if($id_share_reader!=-1) echo ' checked ';
			           echo '> <span class="slider round"></span> </label>'; 
			           echo '</div></td>';
			        }else{ echo "<td class='tdtitle' style='margin-left:5px' colspan=2> Creator </td>";}

		            echo "<td>".$people['name']."</td>"; 
		            echo "<td>".$people['surname']."</td>"; 
		            echo "<td>"; if(array_key_exists($people['id_Institution'], $Institutions)) echo $Institutions[$people['id_Institution']]; echo "</td>"; 
		            echo "<td>"; if(array_key_exists($people['id_Laboratory'], $Laboratorys)) echo $Laboratorys[$people['id_Laboratory']]; echo "</td>"; 
		            echo "<td>"; if(array_key_exists($people['id_Team'], $Teams)) echo $Teams[$people['id_Team']]; echo "</td>";   
		            echo "</tr>";
		       // }
		        }
		      ?>
      </table>
    </div>
    </div>


    	<script type="text/javascript">
			function shareGroup(id_unshare,id_group,how){
				 unhow=1; if(how==1)unhow=0; 
				 if(id_unshare==-1){//SHARE 
				 	$.post("sharewith.php", {
	      				base:"<?php echo $base; ?>",
	      				id_base:<?php echo $id_base; ?>,
	      				how:how,
	      				group:id_group
	            	}, function(data,status){
	            		if(how==1) $('#esharegroup_'+unhow+'_'+id_group).hide();
	            		$('#sharegroup_'+how+'_'+id_group).removeAttr("onchange");
	            		$('#sharegroup_'+how+'_'+id_group).attr("onchange", "shareGroup("+data+","+id_group+","+how+")");
	            	} );
				 }
				 else {//UNSHARE 
					if(how==1) $('#esharegroup_'+unhow+'_'+id_group).show();
					$('#sharegroup_'+how+'_'+id_group).removeAttr("onchange");
	            	$('#sharegroup_'+how+'_'+id_group).attr("onchange", "shareGroup(-1,"+id_group+","+how+")");
	            	unShare(id_unshare);
	            }
			}
			
			function shareUser(id_unshare,id_user,how){
				unhow=1; if(how==1)unhow=0; 
				if(id_unshare==-1){
					 $.post("sharewith.php", {
	      				base:"<?php echo $base; ?>",
	      				id_base:<?php echo $id_base; ?>,
	      				how:how,
	      				user:id_user
	            	}, function(data,status){
		            	//alert('#share_'+how+'_'+id_user);
	            		$('#share_'+how+'_'+id_user).removeAttr("onchange");
	            		$('#share_'+how+'_'+id_user).attr("onchange", "shareUser("+data+","+id_user+","+how+")");
	            		if(how==1) $('#eshare_'+unhow+'_'+id_user).hide();
	            	} );
				}
				else {
					//alert('unshare');
					$('#share_'+how+'_'+id_user).removeAttr("onchange");
	            	$('#share_'+how+'_'+id_user).attr("onchange", "shareUser(-1,"+id_user+","+how+")");
	            	unShare(id_unshare);
	            	if(how==1) $('#eshare_'+unhow+'_'+id_user).show();
	            }
			}
			function unShare(id_unshare){
				 $.post("sharewith.php", {
      				base:"<?php echo $base; ?>",
      				id_base:<?php echo $id_base; ?>,
      				unshare:id_unshare
            	}, function(data,status){ } );
			}
		</script>
	<?php 

	}
	mysqli_close($link);
}

?>
