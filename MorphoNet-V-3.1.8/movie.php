<?php
	
include 'uni-functions.php';

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

if ($connected){
	if(isset($_POST["movie"])){ 
		$movie=intval($_POST["movie"]);
		
		if($movie==0){//SNAPSHOT
			$file=$_FILES['fileUpload'];
			copy($file['tmp_name'],'Snapshot/'.$_POST['name']);    		
		}

		if($movie==1){//CREATE A MOVIE 
			$path="Movies/Movie_".$_POST["id_user"];
			deleteDirectory($path);
			mkdir("Movies");
			mkdir($path); 		
		}

		if($movie==2){//RECORD A FRAME
			$path="Movies/Movie_".$_POST["id_user"];
			$file=$_FILES['fileUpload'];
			copy($file['tmp_name'],$path.'/'.$_POST['name']);    		
		}
		if($movie==3){//END RECORD 
			$outputname=$_POST["outputname"];
			$framerate=$_POST["framerate"];
			$path="Movies/Movie_".$_POST["id_user"];
			if (file_exists($outputname)) unlink($outputname);
			shell_exec("python createMovie.py ".$path." Movies/".$outputname);
			$outputnameavi=str_replace('.mp4','.avi',$outputname);
			if (!file_exists("Movies/".$outputname)){
				if (file_exists("Movies/".$outputnameavi)) {
					shell_exec('ffmpeg -i Movies/'.$outputnameavi.' Movies/'.$outputname);
					shell_exec('rm -f Movies/'.$outputnameavi);
				}
			}
			deleteDirectory($path);
			  		
		}

		if($movie==4){//RECORD A FRAME HOLOGRAM
			$path="Movies/Movie_".$_POST["id_user"];
			$file=$_FILES['fileUpload'];
			copy($file['tmp_name'],$path.'/'.$_POST['name']);    		
		}	
	}
	mysqli_close($link);
}


?>
