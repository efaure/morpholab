<?php

include 'functions.php';

session_start();

$log=-1;

if(!isset($_SESSION['ID']))  header('Location:'.get_server());
$id_people=$_SESSION['ID'];

$id_dataset=-1;
$error="";
$channel=0;
$quality=0;
$currentTime=-1;
$minTime=0;
$maxTime=0; 
$id_mesh=-1;
$extraction="";
//FIRST WE CHECK IF THEREIS NO WAITING CREATED DATASET
$is_existingdataset="";

$uploadPath="UploadDATASET/";

$istep=0; 
//0 NEW DATASET NAME
//1 EXISTING DATASET
//2 MESH
//3 DESCRIPTION
//4 FINISH ...

//BUNDLE STATUS
$bundleCreation=-2; //UNDER CREATION
$bundleUpload=-3; //UNDER UPLOAD 
$bundleConversion=-4; //UNDER Conversion 
$bundleError=-5; //UNDER ERROR 


if(isset($_POST['recap'])){
	if($_POST['dele']!=""){
		$id_dataset=$_POST['dele'];
		$main_path=$uploadPath."DATASET_".$id_dataset."/";
		if(is_dir($main_path)) system("rm -rf ".$uploadPath."DATASET_".$id_dataset); 
		query('DELETE FROM dataset WHERE bundle='.$bundleCreation.' AND id_people='.$id_people.' AND id='.$id_dataset);		
		if(isset($_SESSION['id_dataset'])) unset($_SESSION['id_dataset']);
	}
	else {
		$_SESSION['id_dataset']=$_POST['recap'];
		$id_dataset=$_SESSION['id_dataset'];
		$_SESSION['datasetname']=query_first('SELECT name FROM dataset WHERE id_people='.$id_people.' AND id='.$id_dataset);
		$istep=2;
	}
}
else{
	if(!isset($_SESSION['id_dataset'])){ //DATASET NET YET DEFINE 
		$id_dataset_temp=query_first('SELECT id FROM dataset WHERE id_people='.$id_people.' AND bundle='.$bundleCreation.' LIMIT 1');
		if($id_dataset_temp!=""){//WAITING DATASET
			$id_dataset=$id_dataset_temp;
			$is_existingdataset=query_first('SELECT name FROM dataset WHERE id_people='.$id_people.' AND id='.$id_dataset);
			
			$istep=1;
		}
	}else{
		$id_dataset=$_SESSION['id_dataset'];
		if(!isset($_SESSION['datasetname'])) $_SESSION['datasetname']=query_first('SELECT name FROM dataset WHERE id_people='.$id_people.' AND id='.$id_dataset);
		$istep=2;
	}
}


if(isset($_POST['name'])){
	$nbDataset=query_first('SELECT count(id) FROM dataset WHERE LOWER(name)="'.strtolower($_POST['name']).'"');	
	if($nbDataset>0) $error="This dataset name already exist";
	else{
		//WE CREATE A NEW DATASET
		if(isset($_POST["minTime"]) && isset($_POST["maxTime"])){ //TEMPORAL ?
		 	$minTime=intval($_POST["minTime"]);
		 	$maxTime=intval($_POST["maxTime"]);
		 	$currentTime=$minTime;
		}
		
		$now=new DateTime();
		$id_dataset_type=1; #GENERIC
		$id_dataset=query_id('INSERT INTO dataset (id_people,date,name,minTime,maxTime,bundle,id_dataset_type) VALUES ('.$id_people.',"'.$now->format('Y-m-d h:s:i').'","'.$_POST["name"].'",'.$minTime.','.$maxTime.','.$bundleCreation.','.$id_dataset_type.')'); 
		$_SESSION['id_dataset']=$id_dataset;
		$_SESSION['datasetname']=$_POST['name'];
		$istep=2;
	}
}



if(isset($_FILES['mesh'])){ //MESH UPLOAD
	$id_dataset=$_SESSION["id_dataset"];
	//WE CHECK IF IT ZIP OR OBJ
	$file=$_FILES['mesh'];
	$path_parts=pathinfo($file["name"]);
	$main_path=$uploadPath."DATASET_".$id_dataset."/";
	if(is_dir($main_path)) system("rm -rf ".$uploadPath."DATASET_".$id_dataset); 
	mkdir($main_path);
	
	$mesh_path=$main_path."MESH/";
	if(!is_dir($mesh_path)) mkdir($mesh_path);
		 
	if(strcmp($path_parts['extension'],"obj")==0){ //MOVE UPLOAD FILE
		move_uploaded_file($file['tmp_name'],$mesh_path.$file["name"]);
		$extraction="Found ".$file["name"];
		$istep=3;
	}else  if(strcmp($path_parts['extension'],"zip")==0){ //We unzip the file and check 
		//print_r($file);
		//echo "FOUND ZIP ".$file['tmp_name'];
		$zip = new ZipArchive;
		$res = $zip->open($file['tmp_name']);
		if ($res === TRUE) {
			for ($i = 0; $i < $zip->numFiles; $i++) {
				$filename = $zip->getNameIndex($i);
				$extraction=$extraction."</br>Extract ".$filename;
 			}
			$zip->extractTo($mesh_path);
			$zip->close();
			$istep=3;
		} else {
			$error="Error extracting the zip file";
			$istep=2;
		}
	}else {
		$error="This file is not in obj or zip format";	
		$istep=2;
	}
}


if(isset($_FILES['desc'])){ //DESCRIPTION UPLOAD
	$file=$_FILES['desc'];
	if($file['name']!=""){//NO DESCIPTION FILE
		$path_parts=pathinfo($file["name"]);
		$main_path=$uploadPath."DATASET_".$id_dataset."/";
		$desc_path=$main_path."DESC/";
		if(!is_dir($desc_path)) mkdir($desc_path);
	
		if(strcmp($path_parts['extension'],"txt")==0){ //MOVE UPLOAD FILE
			//echo "move_uploaded_file TXT";
			move_uploaded_file($file['tmp_name'],$desc_path.$file["name"]);
			$extraction="Found ".$file["name"];
			$istep=4;
		}else  if(strcmp($path_parts['extension'],"zip")==0){ //We unzip the file and check 
			#echo "FOUND ZIP";
			$zip = new ZipArchive;
			$res = $zip->open($file['tmp_name']);
			if ($res === TRUE) {
				for ($i = 0; $i < $zip->numFiles; $i++) {
					$filename = $zip->getNameIndex($i);
					$extraction=$extraction."</br>Extract ".$filename;
	 			}
				$zip->extractTo($desc_path);
				$zip->close();
				$istep=4;
			} else {
				$error="Error extracting the zip file";
				$istep=3;
			}
		}else {
			$error="This file is not in txt or zip format";	
			$istep=3;
		}
	}
	else $istep=4;
}

if($istep==4){
	query('UPDATE dataset set bundle='.$bundleUpload.' WHERE id='.$id_dataset.' and id_people='.$id_people);
	unset($_SESSION['id_dataset']);
	unset($_SESSION['datasetname']);
	system('nohup python '.$uploadPath.'uploadNewDataset.py '.$id_people.' '.$id_dataset.'  > '.$uploadPath.'UploadNewDataset-'.$id_people.'-'.$id_dataset.'.log &');
	
}
?>
		
		
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script src="dist/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="dist/themes/default/style.min.css" />
	 <script src="dist/jstree.min.js"></script>
	 <link rel="stylesheet" href="css/morphonet.css">
  </head>
   <body>
  		<?php include "headbar.php"; ?> 
  		 <div  style="width: 100%;text-align: center;color:grey;">
  		 	<div style="margin:100px"> <span style="font-size:30px;color:grey;">Add a new dataset</span> </div>
  		 	<?php if($istep==0) { //NAME DATASET  ?>  
	  		 	<form action="newdataset.php" method="post" enctype="multipart/form-data" style="text-align:center;"  onsubmit="return validateName()">
					<div style="text-align:left;padding:10px;width:400px;margin-right:auto;margin-left:auto;margin-top:50px">
						<span style="text-align:right;"><b>Enter the name</b></span> 
						<input style="margin-left:10px" type="text" name="name" id="name">
						<input type="submit" value="Create"  style="width:100px;height:25px;text-align:center;margin-left:10px">
						<?php if($error!="") { ?>  </br><span style="margin-left:50px;color:red"><?php echo $error;?> </span>  <?php } ?> 
				     </div>
		 		</form>	
  		 	<?php } else if($istep==1) { //EXISTING DATASET  ?>  
	  		 	<form action="newdataset.php" method="post" enctype="multipart/form-data" style="text-align:center;" >
					<div style="text-align:left;padding:10px;width:400px;margin-right:auto;margin-left:auto;margin-top:50px">
						<input style="margin-left:10px;display:none" type="text" name="recap" id="recap" value="<?php echo $id_dataset;?>">
						<input style="margin-left:10px;display:none" type="text" name="dele" id="dele" value="">
						<span style="text-align:right;"><b>You already create a dataset called "<?php echo $is_existingdataset;?>" </b></span> </br>
						Do you want to  <input type="submit" value="Continue"  style="width:80px;height:25px;text-align:center;">
						or <input type="submit" value="Delete"  style="width:80px;height:25px;text-align:center;"  onclick="del(<?php echo $id_dataset;?>)"> it ?
				     </div>
		 		</form>	 	
		 	<?php } else if($istep==2) { //UPLOAD DATASET MESH.   ?> 
				<form action="newdataset.php" method="post" enctype="multipart/form-data"  onsubmit="return validateMesh()">
						<div style="text-align:center;"> <span style="text-align:left;font-size:20px"><b>Submit your mesh data for <?php echo $_SESSION['datasetname'];?> </b></br>
							 obj format / zip with multiples obj  
						</br></br></div>
						<input type="file" name="mesh" id="mesh"/>
						<input type="submit" value="Submit"  style="width:100px;height:25px;text-align:center;margin-left:30px;margin-top:30px">
						<?php if($error!="") { ?>  </br><span style="margin-left:50px;color:red"><?php echo $error;?> </span>  <?php } ?> 
			 	</form>
			 	<div id="wait" style="position:absolute;width:100%;height:100%;top:0px;background-color:rgba(255, 255, 255, 0.5);z-index:1;display:none">
		 	
		 	<?php } else if($istep==3) { //UPLOAD DESCRIPTION MESH.   ?> 
		 		</br></br><span style="margin:30px;color:green"><?php echo $extraction;?> </span>  </br></br>
	 			<form action="newdataset.php" method="post" enctype="multipart/form-data" style="text-align:center;" >
					</b></br>
					<div style="text-align:center;"> <span style="text-align:left;font-size:20px"><b>Submit your description data for  <?php echo $_SESSION['datasetname'];?> </b></br>
							 txt format / zip with multiples txt  
						</br></br></div>
					 <input type="file" name="desc" id="desc"//>
					 <input type="submit" value="Submit"  style="width:100px;height:25px;text-align:center;margin-left:30px;margin-top:30px">
					 <input type="submit" value="Skip"  style="width:100px;height:25px;text-align:center;margin-left:30px;margin-top:30px">
					 <?php if($error!="") { ?>  </br><span style="margin-left:50px;color:red"><?php echo $error;?> </span>  <?php } ?> 
		 		</form>
		 		<div id="wait" style="position:absolute;width:100%;height:100%;top:0px;background-color:rgba(255, 255, 255, 0.5);z-index:1;display:none">
			</div>
		 	 <?php } else if($istep==4) { //FINISH  DESCRIPTION MESH.   ?> 
		 		<div style="text-align:center;"> <span style="text-align:left;font-size:20px"><b>Your can now visualize your dataset <?php echo $_SESSION['datasetname'];?></br></br></div>
					 
		 		</div>
		 	 <?php } ?> 
	 	</div>

	    <?php include "footer.php"; ?> 
		
		<script type="text/javascript">
			

			function validateName() {
				var name=$('#name').val();
				if(name==""){alert("You must give a name."); return false;}
				return true;
			}
			
			
			function validateMesh() {
					var desc=$('#mesh').val();
					if(desc==""){alert("You must select a file  "); return false;}
					$("#wait").show();
					return true;
			}
			
			function validateDesc() {
					var desc=$('#desc').val();
					alert(desc);
					if(desc==""){alert("You must select a file  or skip this step"); return false;}
					$("#wait").show();
					return false;
			}
			
			function skipdesc() {
					return true;
			}
			
			function del(id_dataset) {
				$('#dele').val(id_dataset);
			}
				
				
			</script>
	</body>
</html>