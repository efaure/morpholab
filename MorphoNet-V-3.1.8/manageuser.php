<?php
session_start();


foreach($_GET as $k => $v) $_POST[$k]=$v;


include 'functions.php';
$now=new DateTime();
$id_owner=$_SESSION['ID'];
$id_people=-1; //POEPLE TO EDIT

$isAdmin=0;

if ($connected){
  $isAdmin=getAdmin($_SESSION['ID']);
 
  $Institutions = query_array('SELECT id,name FROM Institution');
  $Laboratorys = query_array('SELECT id,name FROM Laboratory');
  $Teams = query_array('SELECT id,name FROM Team');

  if(isset($_POST['createuser'])){ //CREATE A NEW GROUP
     $id_Institution=0;
     if($_POST['Institution']!=""){
       foreach($Institutions as $id => $Institution)   if(strtolower($Institution)==strtolower($_POST['Institution']))$id_Institution=$id;
       if($id_Institution==0) {
         $id_Institution=query_id('INSERT INTO Institution (name) VALUES("'.$_POST['Institution'].'")');
         $Institutions = query_array('SELECT id,name FROM Institution');
       }
      }
     $id_Laboratory=0;
     if($_POST['Laboratory']!=""){
      foreach($Laboratorys as $id => $Laboratory)   if(strtolower($Laboratory)==strtolower($_POST['Laboratory']))$id_Laboratory=$id;
      if($id_Laboratory==0) {
         $id_Laboratory=query_id('INSERT INTO Laboratory (name) VALUES("'.$_POST['Laboratory'].'")');
         $Laboratorys = query_array('SELECT id,name FROM Laboratory');
       }
     }
     $id_Team=0;
     if($_POST['Team']!=""){
      foreach($Teams as $id => $Team)   if(strtolower($Team)==strtolower($_POST['Team']))$id_Team=$id;
      if($id_Team==0) {
        $id_Team=query_id('INSERT INTO Team (name) VALUES("'.$_POST['Team'].'")');
        $Teams = query_array('SELECT id,name FROM Team');
      }
     }
     query('INSERT INTO people (name,surname,email,login,password,date,exist,tel,adress,fonction,id_Institution,id_Laboratory,id_Team) VALUES ("'.$_POST['name'].'","'.$_POST['surname'].'","'.$_POST['email'].'","'.$_POST['login'].'","'.anti_injection_login_senha($_POST['password']).'","'.$now->format('Y-m-d h:s:i').'",1,"'.$_POST['tel'].'","'.$_POST['adress'].'","'.$_POST['fonction'].'",'.$id_Institution.','.$id_Laboratory.','.$id_Team.')');
  }
  if(isset($_POST['deleteuser'])){ //GARBAGE USER
       if($isAdmin || $_POST['deleteuser']==$id_owner) query('UPDATE people set exist=0 WHERE id='.$_POST['deleteuser']);
  }
  if(isset($_POST['activeuser'])){ //ACTIVE USER
       if($isAdmin || $_POST['activeuser']==$id_owner) query('UPDATE people set exist=1 WHERE id='.$_POST['activeuser']);
  }

  if(isset($_POST['edituser'])){ //EDIT USER
      if($isAdmin || $_POST['edituser']==$id_owner)  $id_people=$_POST['edituser'];
  }
  if($isAdmin && isset($_POST['removeuser'])){ //COMPELETE USER DELATION
     query('DELETE FROM people  WHERE id='.$_POST['removeuser']);
     query('DELETE FROM people_group WHERE id_people'.$_POST['removeuser']);
  }


  $peoples = query_array('SELECT id,name,surname,email,login,date,exist,id_right,tel,adress,fonction,id_Institution,id_Laboratory,id_Team,quota FROM people WHERE exist>=0 AND exist<=1 ORDER by name,surname');

  if(isset($_POST['modifyuser']) && ($isAdmin || $_POST['modifyuser']==$id_owner)){ //MODIFY USER
       $id_people=$_POST['modifyuser'];
       $people=$peoples[$id_people];
       if($_POST['name']!="" && $_POST['name']!=$people['name']) query('UPDATE people set name="'.$_POST['name'].'" WHERE id='.$id_people);
       if($_POST['surname']!="" && $_POST['surname']!=$people['surname']) query('UPDATE people set surname="'.$_POST['surname'].'" WHERE id='.$id_people);
       if($_POST['email']!="" && $_POST['email']!=$people['email']) query('UPDATE people set email="'.$_POST['email'].'" WHERE id='.$id_people);
       if($_POST['login']!="" && $_POST['login']!=$people['login']) query('UPDATE people set login="'.$_POST['login'].'" WHERE id='.$id_people);
       if($_POST['password']!="" ) query('UPDATE people set password="'.anti_injection_login_senha($_POST['password']).'" WHERE id='.$id_people);
       if($_POST['tel']!="" && $_POST['tel']!=$people['tel']) query('UPDATE people set tel="'.$_POST['tel'].'" WHERE id='.$id_people);
       if($_POST['adress']!="" && $_POST['adress']!=$people['adress']) query('UPDATE people set adress="'.$_POST['adress'].'" WHERE id='.$id_people);
       if($_POST['fonction']!="" && $_POST['fonction']!=$people['fonction']) query('UPDATE people set fonction="'.$_POST['fonction'].'" WHERE id='.$id_people);
       if($_POST['surname']!="" && $_POST['surname']!=$people['surname']) query('UPDATE people set surname="'.$_POST['surname'].'" WHERE id='.$id_people);

        if($_POST['Institution']!=""){
            foreach($Institutions as $id => $Institution)   if(strtolower($Institution)==strtolower($_POST['Institution']))$id_Institution=$id;
            if($id_Institution==0) {
              $id_Institution=query_id('INSERT INTO Institution (name) VALUES("'.$_POST['Institution'].'")');
              $Institutions = query_array('SELECT id,name FROM Institution');
            }
            if($id_Institution!=$people['id_Institution']) query('UPDATE people set id_Institution="'.$id_Institution.'" WHERE id='.$id_people);
        } 

        if($_POST['Laboratory']!=""){
            foreach($Laboratorys as $id => $Laboratory)   if(strtolower($Laboratory)==strtolower($_POST['Laboratory']))$id_Laboratory=$id;
            if($id_Laboratory==0) {
              $id_Laboratory=query_id('INSERT INTO Laboratory (name) VALUES("'.$_POST['Laboratory'].'")');
              $Laboratorys = query_array('SELECT id,name FROM Laboratory');
            }
            if($id_Laboratory!=$people['id_Laboratory']) query('UPDATE people set id_Laboratory="'.$id_Laboratory.'" WHERE id='.$id_people);
        } 

        if($_POST['Team']!=""){
            foreach($Teams as $id => $Team)   if(strtolower($Team)==strtolower($_POST['Team']))$id_Team=$id;
            if($id_Team==0) {
              $id_Team=query_id('INSERT INTO Team (name) VALUES("'.$_POST['Team'].'")');
              $Team = query_array('SELECT id,name FROM Team');
            }
            if($id_Team!=$people['id_Team']) query('UPDATE people set id_Team="'.$id_Team.'" WHERE id='.$id_people);
        } 

		 if(isset($_POST['Quota']) && $_POST['Quota']!="" && $_POST['Quota']!=$people['Quota']) query('UPDATE people set quota="'.$_POST['Quota'].'" WHERE id='.$id_people);
		 
        $id_people=-1;
         $peoples = query_array('SELECT id,name,surname,email,login,date,exist,id_right,tel,adress,fonction,id_Institution,id_Laboratory,id_Team FROM people ORDER by name,surname');
  }

   //DOWNLOAD GROUPS
    $member = array(); foreach($peoples as $idp => $people) $member[$idp]=array();
    $manager = array(); foreach($peoples as $idp => $people) $manager[$idp]=array();
    $groups = query_array('SELECT id,name,id_owner,date,privacy FROM groups WHERE status>=0');
    foreach ($groups as $id_group => $group) { //CHECK STARTUS
      $result = my_query('SELECT id,id_people,status FROM people_group WHERE id_group='.$id_group);
      if ($result) {
        while($r = mysqli_fetch_assoc($result)) {
          $idp=$r['id_people'];
          if($r['status']=="member")  array_push($member[$idp],$id_group);
          if($r['status']=="manager") array_push($manager[$idp],$id_group);
        }
      }
    }
  

  
} else echo "PROBLEM CONNECTION</br>";

function showList($table){
    $i=0;
    foreach($table as $elt) {
      echo '"'.$elt.'"'; 
      $i+=1;
      if($i<sizeof($table)) echo ",";

     }
  }

function showGroup($table){
   global $groups;
    $i=0;
    foreach($table as $idg) {
      echo $groups[$idg]['name']; 
      $i+=1;
      if($i<sizeof($table)) echo ",";
     }
  }


?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
  </head>

  <body>
    <?php include "headbar.php"; ?>

    <div  style="width: 100%;text-align: center;">
      <span style="font-size:30px;color:grey;">Users Manager </span>
      <input type=image width="75px" height="75px"  src="images/newuser.png" style="vertical-align:middle;margin-left:20px"  title="Create a new user " onclick="showNewUser()"/>
    </div>

  
    
    <!-- EDIT A USER -->
    <?php if($id_people!=-1){ 
      $people=$peoples[$id_people];
      ?>
    <div id="edituserdiv" style=" display: table; width: 100%;">
       <div style="width: 200px;margin: 0 auto;margin-top:20px">
         <form action="manageuser.php" method="post" enctype="multipart/form-data"  >
            <input id="modifyuser" name="modifyuser" style="display: none;" value="<?php echo $id_people; ?>" />
            <table >
                <tr>  <td> Name </td><td><input id="name" name="name" width="200px" height="30px" value="<?php echo $people['name']; ?>"/> </td> </tr>
                <tr>  <td> Surname </td><td><input id="surname" name="surname" width="200px" height="30px" value="<?php echo $people['surname']; ?>"/> </td> </tr>
                <tr>  <td> email </td><td><input id="email" name="email" width="200px" height="30px" value="<?php echo $people['email']; ?>" /> </td> </tr>
                <tr>  <td> login </td><td><input id="login" name="login" width="200px" height="30px" value="<?php echo $people['login']; ?>" /> </td> </tr>
                <tr>  <td> password </td><td><input id="password" name="password" type="password" width="200px" height="30px" /> </td> </tr>
                <tr>  <td> tel </td><td><input id="tel" name="tel" width="200px" height="30px" value="<?php echo $people['tel']; ?>"/> </td> </tr>
                <tr>  <td> adress </td><td><input id="adress" name="adress" width="200px" height="30px" value="<?php echo $people['adress']; ?>" /> </td> </tr>
                <tr>  <td> fonction </td><td><input id="fonction" name="fonction" width="200px" height="30px" value="<?php echo $people['fonction']; ?>"/> </td> </tr>
                <tr>  <td> Institution </td><td><input id="Institution" name="Institution" width="200px" height="30px" value="<?php 
                    if(array_key_exists($people['id_Institution'], $Institutions)) echo $Institutions[$people['id_Institution']];
                 ?>" /> </td> </tr>
                <tr>  <td> Laboratory </td><td><input id="Laboratory" name="Laboratory" width="200px" height="30px" value="<?php 
                if(array_key_exists($people['id_Laboratory'], $Laboratorys)) echo $Laboratorys[$people['id_Laboratory']]; 
                ?>"/> </td> </tr>
                <tr>  <td> Team </td><td><input id="Team" name="Team" width="200px" height="30px" value="<?php 
                if(array_key_exists($people['id_Team'], $Teams)) echo $Teams[$people['id_Team']]; 
                ?>"/> </td> </tr>
                  <tr>  <td> Quota </td><td><?php 
	                   $quota=getQuotaByPeople($id_people);  if($isAdmin){?>  <input id="Quota" name="Quota" width="200px" height="30px" value="<?php  echo $quota; ?>"/> <?php }
	                   else echo formatSizeUnits($quota);  ?> 
	              </td> </tr> 
                
            </table>
            <input  type=submit value="modify" class="myButton" style="margin-top:10px;margin-left:20px;width:200px;" />
            <input  type=button value="cancel" class="myButton" style="margin-top:10px;margin-left:20px;width:200px;"  onclick="cancel()"/>
          </form>
       </div>
     </div>
    <?php } else { ?>

    <!-- CREATE NEW USER -->
    <div style=" display: table; width: 100%;">
      <div id="newuserdiv" style="display: none;width: 200px;margin: 0 auto;margin-top:20px">
       <span  style= "font-size:25px;color:grey;" >Create a new user</span>
       <form action="manageuser.php" method="post" enctype="multipart/form-data"  onsubmit="return createUser()">
        <input id="createuser" name="createuser" style="display: none;" />
         <table >
          <tr>  <td> Name </td><td><input id="name" name="name" width="200px" height="30px" value=""/> </td> </tr>
          <tr>  <td> Surname </td><td><input id="surname" name="surname" width="200px" height="30px" value=""/> </td> </tr>
          <tr>  <td> email </td><td><input id="email" name="email" width="200px" height="30px" value="" /> </td> </tr>
          <tr>  <td> login </td><td><input id="login" name="login" width="200px" height="30px" value="" /> </td> </tr>
          <tr>  <td> password </td><td><input id="password" name="password" type="password" width="200px" height="30px" /> </td> </tr>
          <tr>  <td> tel </td><td><input id="tel" name="tel" width="200px" height="30px" value=""/> </td> </tr>
          <tr>  <td> adress </td><td><input id="adress" name="adress" width="200px" height="30px" value="" /> </td> </tr>
          <tr>  <td> fonction </td><td><input id="fonction" name="fonction" width="200px" height="30px" value=""/> </td> </tr>
          <tr>  <td> Institution </td><td><input id="Institution" name="Institution" width="200px" height="30px" value="" /> </td> </tr>
          <tr>  <td> Laboratory </td><td><input id="Laboratory" name="Laboratory" width="200px" height="30px" value=""/> </td> </tr>
          <tr>  <td> Team </td><td><input id="Team" name="Team" width="200px" height="30px" value=""/> </td> </tr>
           <?php if($isAdmin){?> <tr>  <td> Quota </td><td><input id="Quota" name="Quota" width="200px" height="30px" value="1073741824"/> </td> </tr> <?php } ?>
        </table>
          <input  type=submit value="create" class="myButton" style="margin-top:10px;margin-left:20px;width:200px;"/>
        </form>
      </div>
     </div>
   


    <!-- LIST NEW USER -->
    <div id="users" style=" display: table; margin-right: auto;  margin-left: auto; margin-top:50px; margin-bottom:auto; width: 100%;">
      <table id="listusers" style="width: 100%;margin-top:20px">
        <tr>
          <!-- <td> id</td> -->
         <?php if($isAdmin){?> <td class="tdtitle"> Group Member </td>
          <td class="tdtitle"> Group Manager </td>  
          <td class="tdtitle"> Use  </td> 
          <td class="tdtitle"> Quota </td> <?php } ?>
          <td class="tdtitle" > Actions </td>
          <td class="tdtitle"> Name </td>
          <td class="tdtitle"> Surname </td>
          <td class="tdtitle"> email </td>
          <td class="tdtitle"> login </td>
          <td class="tdtitle"> tel </td>
          <td class="tdtitle"> adress </td>
          <td class="tdtitle"> fonction </td>
          <td class="tdtitle" > Institution </td>
          <td class="tdtitle"> Laboratory </td>
          <td class="tdtitle"> Team </td>
          <td class="tdtitle"> date </td>
        </tr>
      
        <?php
		$total_use=0;
        foreach ($peoples as $id_people_t => $people){
          echo "<tr>";
         // echo "<td>".$people['id']."</td>"; 
        if($isAdmin){
             echo "<td>";  showGroup($member[$id_people_t]);  echo "</td>";
             echo "<td>";  showGroup($manager[$id_people_t]);  echo "</td>";
         
           $use=getSizeByPeople($id_people_t);$total_use+=$use;
          echo "<td>". formatSizeUnits($use)."</td>";
          echo "<td>". formatSizeUnits(getQuotaByPeople($id_people_t))."</td>";
        }
          echo "<td>";
          if($isAdmin || $id_owner==$id_people_t){
              if($people['exist']==1)  {
                echo '<input type=image width="25px" height="25px"   style="vertical-align:middle;" src="images/garbage.png" onclick="deleteuser('.$id_people_t .')"/>';
                echo '<input type=image width="25px" height="25px"  style="vertical-align:middle;"  src="images/edituser.png" onclick="edituser('.$id_people_t .')"/>';
              }
              else {
                echo '<input type=image width="25px" height="25px"  style="vertical-align:middle;" src="images/recycle.png" onclick="activeuser('.$id_people_t .')"/>';
                if($isAdmin) echo '<input type=image width="25px" height="25px"  style="vertical-align:middle;" src="images/delete.png" onclick="removeuser('.$id_people_t .')"/>';
              }
           }
          echo "</td>"; 
          echo "<td>".$people['name']."</td>"; 
          echo "<td>".$people['surname']."</td>"; 
          echo "<td>".$people['email']."</td>";
          echo "<td>".$people['login']."</td>"; 
          echo "<td>".$people['tel']."</td>"; 
          echo "<td>".$people['adress']."</td>";
          echo "<td>".$people['fonction']."</td>"; 
          echo "<td>"; if(array_key_exists($people['id_Institution'], $Institutions)) echo $Institutions[$people['id_Institution']]; echo "</td>"; 
          echo "<td>"; if(array_key_exists($people['id_Laboratory'], $Laboratorys)) echo $Laboratorys[$people['id_Laboratory']]; echo "</td>"; 
          echo "<td>"; if(array_key_exists($people['id_Team'], $Teams)) echo $Teams[$people['id_Team']]; echo "</td>";   
          echo "<td>".$people['date']."</td>"; 
          echo "</tr>";
      }
      ?>
      </table>
      
      <?php if($isAdmin){?>   <span class="tdtitle"> Total use : <?php echo formatSizeUnits($total_use);?> </span><?php } ?>
    </div>

     <?php } ?>
      <?php include "footer.php"; ?> 
     <script>
      function showNewUser(){  $("#newuserdiv").show(); $("#users").hide(); }
    
      function activeuser(id_people_t){  window.open('manageuser.php?activeuser='+id_people_t,'_self',false); }
      function edituser(id_people_t){  window.open('manageuser.php?edituser='+id_people_t,'_self',false); }
      function deleteuser(id_people_t){  window.open('manageuser.php?deleteuser='+id_people_t,'_self',false); }
      function removeuser(id_people_t){  window.open('manageuser.php?removeuser='+id_people_t,'_self',false); }

      function cancel(){  window.open('manageuser.php','_self',false); }
       function createUser(){
        if($("#name").val()=="") { alert("You must specify a name");return false;}
        if($("#surname").val()=="") { alert("You must specify a surname");return false;}
        if($("#email").val()=="") { alert("You must specify an email");return false;}
        if($("#login").val()=="") { alert("You must specify a login");return false;}
        if($("#password").val()=="") { alert("You must specify a password");return false;}
        if($("#Institution").val()=="") { alert("You must specify an Institution");return false;}
        if($("#Laboratory").val()=="") { alert("You must specify a Laboratory");return false;}
        if($("#Team").val()=="") { alert("You must specify a Team");return false;}
        
        return true;
       }
       
       //AUTOCOMPLETE INSITUTION,LAB,TEAM
       
       var Institutions = [   <?php  showList($Institutions ); ?> ];
       var Laboratorys = [   <?php  showList($Laboratorys ); ?> ];
       var Teams = [   <?php  showList($Teams ); ?> ];

       //Remplies les valeurs après auto completion pour les produits
      function autocomplete_Institution(fieldname){
        $("#"+fieldname).autocomplete({  source: Institutions  });
      }
      autocomplete_Institution("Institution");

      function autocomplete_Laboratory(fieldname){
        $("#"+fieldname).autocomplete({  source: Laboratorys  });
      }
      autocomplete_Laboratory("Laboratory");

      function autocomplete_Team(fieldname){
        $("#"+fieldname).autocomplete({  source: Teams  });
      }
      autocomplete_Team("Team");

      
    
    </script>
    
  </body>
</html>

<?
mysqli_close($link);


?>