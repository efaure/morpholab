<footer>
	<div style="width:100%;margin-top:50px;color:#7678ff;">
		<a id="emailLnk" style="text-decoration:none;color:#7678ff;" href="license.html" target="_blank">License</a>
		<span style="margin-left:30px;margin-right:30px"> | </span>
		<a id="emailLnk" style="text-decoration:none;color:#7678ff;" href="funding.html" target="_blank" >Funding</a>
		<span style="margin-left:30px;margin-right:30px"> | </span>
		<a id="emailLnk" style="text-decoration:none;color:#7678ff;" href="mailto:morphonet@crbm.cnrs.fr" >Contact</a>
		</br>
		<img width="30px" height="30px"  src="images/cnrs.png" style="margin-left: auto; margin-right: auto;margin-top:10px; " />
		</br>
		<span > Copyright 2018 © Leggio, Laussu, Carlier, Godin, Lemaire, Faure </span>
		
	</div>
	 <div id="feedback" style="position:absolute;top:100px;right:50px;background-color:#DBF0FC;border:1px solid grey;display:none">
       <input type="image" width="20px" height="20px"  title="close this window" src="images/delete.png" style="margin:2px;position: relative; float:right" onclick="cancelfeedback()"/>
        <textarea id="comments" name="comments" rows="10" cols="50" placeholder="Send us some feedback !"></textarea>
       <input type="image" width="35px" height="35px"  title="Manage Categories on MorphoNet" src="images/send-button.png" style="margin-left:25px;position: relative; float:bottom" onclick="send()"/>
     </div>
</footer>


