<?php
session_start();


include 'functions.php';
$log=-1;



//GET DATASET INFOS
$id_dataset=$_POST['id_dataset'];
$result = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_dataset_type,rotation,translation,scale,spf,dt FROM dataset WHERE  id='.$id_dataset);

while($r = mysqli_fetch_assoc($result))  $param = $r;
if(isset($_POST['quality'])) $param['quality']=$_POST['quality'];
else $param['quality']=0;
                        

if(isset($_POST['right'])) $param['u_right']=$_POST['right'];
else $param['u_right']=3;
        
$param['u_ID']=0;
$param['u_name']="Public";
$param['u_surname']="";
if(isset($_SESSION['ID']))  {
	$param['u_ID']=$_SESSION['ID'];
	$param['u_name']=$_SESSION['name'];
	$param['u_surname']=$_SESSION['surname'];
}

$version="V-3.1.8";

?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <link rel="shortcut icon" href="<?php echo $version; ?>/TemplateData/favicon.ico">
    <link rel="stylesheet" href="<?php echo $version; ?>/TemplateData/style.css">
    <meta name="MorphoNet" content="MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching." />
    <link rel="stylesheet" href="css/morphonet.css">
    <script src="<?php echo $version; ?>/TemplateData/UnityProgress.js"></script>  
    <script src="<?php echo $version; ?>/Build/UnityLoader.js"></script>
    <script>
      var gameInstance = UnityLoader.instantiate("gameContainer", "<?php echo $version; ?>/Build/MorphoNet.json", {onProgress: UnityProgress});
    </script>
  </head>
  <body style="background-color:white;">
    <?php include "headbar.php"; ?>


    <input type="text" id="cell" style="visibility:hidden" value="">
    <input type="text" id="time" style="visibility:hidden" value="">
    <input type="button" id="showCell" style="visibility:hidden" onclick="showSelectedCell()" value="showCell">
    <input type="file" id="selectFile"  style="visibility:hidden"  value="">


     <div class="webgl-content" >
      <div id="gameContainer" style="width: 1007px; height: 718px"></div>
    </div>


    <script type="text/javascript" src="jquery.min.js"></script>
     <script>
      var lineage;
      function sendParameters( )
      {
        gameInstance.SendMessage('Canvas', 'getParameters', '<?php echo jsonRemoveUnicodeSequences($param);?>');
      }
      function openLineage(){
        lineage=window.open("lineage.php?id_dataset=<?php echo $id_dataset; ?>",'lineage','width=800,height=800');
        //lineage.focus();
      }
     
      function clearAll(){ lineage.clearAll(); }
      function hide(t,id){ lineage.hide(t,id); }
      function show(t,id){ lineage.show(t,id); }
      function hideCellsAllTime(v,listcells){ lineage.hideCellsAllTime(v,listcells); }
      function colorize(t,id,r,g,b){ lineage.colorize(t,id,r,g,b); }
      function uncolorize(t,id){ lineage.uncolorize(t,id); }
      function applySelection(r,g,b,listcells){ lineage.applySelection(r,g,b,listcells); }
      function resetSelectionOnSelectedCells(listcells){ lineage.resetSelectionOnSelectedCells(listcells); }
      function hideCells(v,listcells){ lineage.hideCells(v,listcells); }
      function propagateThis(v,r,g,b,listcells){ lineage.propagateThis(v,r,g,b,listcells); }
      

      
      function showSelectedCell(){
        var id=$("#cell").val();
        var t=$("#time").val();
        //alert('('+t+','+id+')');
        gameInstance.SendMessage('Canvas', 'lineage_showCell', t+','+id+',0');
      }

      function selectFile(){
        //alert("Select File");
        $("#selectFile").click();
      }
      $('#selectFile').change( function(event) {
        //alert("Change !!");
        var tmppath = URL.createObjectURL(event.target.files[0]);
        var file=$('#selectFile').val();
        var fileNameIndex = file.lastIndexOf("\\") + 1;
        var filename = file.substr(fileNameIndex);
        //gameInstance.SendMessage('Canvas', 'lineage_showCell', '('+t+','+id+')');
        $.get(tmppath, function(data,status){  //Get The File
          $.post("correspondence.php", {
              download:8,
              data:data,
              id_dataset:<?php echo $id_dataset;?>,
              id_people:<?php echo $param['u_ID'];?>,
              infos:filename,
              type:2
            }, function(data,status){ 
                //alert("ID created "+data);
                gameInstance.SendMessage('Canvas', 'onFinishLoadData', data);

            } );
        } );
    });


    </script>
    <div style="margin-top:900px">
     <?php include "footer.php"; ?> 
   </div>
  </body>
</html>