<?php

include 'functions.php';

session_start();

$log=-1;


if(!isset($_GET['id_dataset']))  header('Location:'.get_server());
$id_dataset=$_GET['id_dataset'];

if(isset($_GET['name'])){
	echo mysqli_query($link,'UPDATE dataset SET  name="'.$_GET['name'].'", minTime='.$_GET['minTime'].', maxTime='.$_GET['maxTime'].', id_dataset_type='.$_GET['typefield'].', spf='.$_GET['spf'].',dt='.$_GET['dt'].' WHERE id_people='.$_SESSION["ID"].' and id='.$id_dataset);
	header('Location:'.get_server());
}
$result = mysqli_query($link,'SELECT name,minTime,maxTime,id_dataset_type,spf,dt FROM dataset WHERE id_people='.$_SESSION["ID"].' and id='.$id_dataset);
					
if (!$result)  header('Location:'.get_server());
else { $r = mysqli_fetch_assoc($result);	}
	 		
?>
		
		
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script src="dist/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="dist/themes/default/style.min.css" />
	 <script src="dist/jstree.min.js"></script>
  </head>
  <style>
	  .postdata {
		font-size: 100%;
	    background-color:white;
	    border:1px solid grey;
	    height:28px;
	    color:grey;
		-moz-border-radius:2px; -khtml-border-radius:2px; -webkit-border-radius:2px;    border-radius:2px;
	}
	
	.postdata:hover {
		border:1px solid #292e37;
	}
	
	
	.boutton {
		cursor:pointer;	
		color:white;
		background-color:grey; 
		border:1px solid grey; 
		text-transform: uppercase;
	-moz-border-radius:3px; -khtml-border-radius:3px; -webkit-border-radius:3px;	border-radius:3px;
	}


	.boutton:hover {
		border:1px solid #999999; 
	}


	.error_comment{
		text-decoration:none;
		color: red;
		padding:1%;
		font-size: 10px;
		text-transform: uppercase;
	}


	body {
	    margin: 0;
	    padding: 0;
	    background-color:white;
	}
	
	
	
	.btnTYPE {
	  font-family: Arial;
	  color: #ffffff;
	  background: #3498db;
	  padding: 9px 21px 10px 20px;
	  text-decoration: none;
	}
	
	.btnTYPE:hover {
	  background: #3cb0fd;
	  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
	  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
	  text-decoration: none;
	}


	#menu-vertical, #menu-vertical ul {
	  padding:0;
	  margin:0;
	  list-style:none;
	  width: 180px; /* seule ligne rajoutée */
	}
	#menu-vertical li {
		position: relative;
		background:#3400ff; /* juste pour cacher le texte de la page en-dessous */
		height:20px;
		margin-bottom:5px;
		padding:10px;
		text-fill-color:white;
	}
	#menu-vertical li:hover {
	 background:#5d60fc;
	}
	#menu-vertical a {
	  display:block;
	  text-decoration: none;
	  color: #000;
	}
	#menu-vertical ul {
	  position: absolute;
	  left:-999em;
	}
	#menu-vertical li:hover ul {
	  top: 0;
	  left: 180px;
	}


	</style>
  <body>
  		<?php require "newdataset_header.php";?>
		<form action="modifydataset.php" method="get" enctype="multipart/form-data" style="text-align:center;"  onsubmit="return validateForm()">
			<div style="text-align:left;padding:10px;width:400px;margin-right:auto;margin-left:auto;border:1px solid black">
				<input id="id_dataset" name="id_dataset" type="text" style="visibility:hidden" value="<?php echo $id_dataset; ?>" >
				<span style="text-align:right;"><b>Choose your Type</b></span> <img width="20px" height="20px"  src="images/parameters.png" style="float:right" onclick="deployTypeParam()" /></br>
				 <span id="datatype" ></span><div id="main_type" >  </div><input id="typefield" name="typefield" type="text" style="visibility:hidden" value="<?php echo $r['id_dataset_type']; ?>" ></br>
				 
				<span style="text-align:right;"><b>Name</b></span>  <input style="margin-left:10px" type="text" name="name" id="name" value="<?php echo $r['name'];?>"></br></br>
				<span style="text-align:right;"><b>SPF</b></span>  <input style="margin-left:10px;width:50px" type="text" name="spf" id="spf" value="<?php echo $r['spf'];?>"  >
				<span style="text-align:right;"><b>dt</b></span>  <input style="margin-left:10px;width:50px" type="text" name="dt" id="dt" value="<?php echo $r['dt'];?>"  ></br></br>
				 <span style="text-align:right;"><b>Temporal</b></span>
				   <input id="check_temporal" type="checkbox" onclick="temporal()" <?php if($r['minTime']!=$r['maxTime']) echo " checked ";?> >
						<div id="temporal" <?php if($r['minTime']==$r['maxTime']) echo ' style="display:none" ';?> > 
							Min Time  <input type="text" name="minTime" id="minTime" value="<?php echo $r['minTime']; ?>" style="width:50px">
							Max Time  <input type="text" name="maxTime" id="maxTime" value="<?php echo $r['maxTime']; ?>" style="width:50px">
						</div>
				</br>
		   
				<input type="submit" value="Modify"  style="width:100px;height:25px;text-align:center;margin-left:300px">
				<input type="button" value="Cancel"  onclick="cancelEmbryo()" style="width:100px;height:25px;text-align:center;margin-left:300px">
				<input type="button" value="delete"  onclick="deleteEmbryo(<?php echo $id_dataset;?>)" style="width:100px;height:25px;text-align:center;margin-left:300px">
				
		     </div>
	 	</form>
	
	 	
	 		
	 				
		<div id="wait" style="position:absolute;width:100%;height:100%;top:0px;background-color:rgba(255, 255, 255, 0.5);z-index:1;visibility:hidden">
			<div style="background-color:rgba(255, 255, 255,1);font-size: 200%;margin:auto;margin-top:400px;border:1px solid red;text-align:center;width:350px;padding:20px;padding-top:10px;padding-bottom:10px;"><b>Wait during process </b></div >
		</div>
	


	    <footer>
			<img width="100px" height="100px"  src="images/cnrs.png" style="margin-top:100px" />
		</footer>
		
		<script type="text/javascript">
			
			var param=0;
			function validateForm() {
				var id_type=$('#typefield').val();
				if(id_type=="" || id_type=="0"){alert("You must chosse the data type."); return false;}
				var name=$('#name').val();
				if(name==""){alert("You must give a name."); return false;}
				if($('#check_temporal').is(':checked')){
					var minTime=$('#minTime').val();
					if(Math.floor(minTime) == minTime && $.isNumeric(minTime) && minTime>=0){ }else { alert("Min time should be a positif or null integer"); return false;}
					var maxTime=$('#maxTime').val();
					if(Math.floor(maxTime) == maxTime && $.isNumeric(maxTime) && maxTime>=minTime){ }else { alert("Max time should an integer bigger than minTime"); return false;}

				}
				$("#wait").css('visibility', 'visible');
				return true;
				
			}
			
			function temporal(){
				if($('#check_temporal').is(':checked')){
					$('#temporal').show();
				}else $('#temporal').hide();
			}
				
			//TYPE
			function changeType(id_child){
				$("#typefield").val(id_child);
				
			}
			function removeType(id_child){
				$.post("querydataset.php", { todo: "removeType",id_parent:id_child,"param":param}, function(data,status){
					if(data.substring(0,1)!="<") alert(data);
					else{
						$('#main_type').html(data);
						$('#type').jstree();
					}
				} );
				
			}
			function addType(id_parent){
				var value=prompt("Enter the new type");
				$.post("querydataset.php", { todo: "createtype",id_parent:id_parent,value:value,"param":param}, function(data,status){
					$('#main_type').html(data);
					$('#type').jstree();
				} );
			}
			function deployType(){
				$.post("querydataset.php", { todo: "deploytype","param":param}, function(data,status){
					$('#main_type').html(data);
					$('#type').jstree();
				} );
			}
			deployType();
		
			function deployTypeParam(){
				if(param==0) param=1; else param=0;
				$("#typefield").val(0);
				deployType();
			}
		
		
			function disconnect(){
				window.location = "index.php?todo=disconnect";
			};
				

			function deleteEmbryo(id_dataset){
				if(confirm("Are you sure you want to delete this dataset ???"))
				{
					window.location = "index.php?delete="+id_dataset;
				}
			}
			function cancelEmbryo(){
				window.location = "index.php";
			}
								
				
				
				
				
			</script>
			
	</body>
</html>