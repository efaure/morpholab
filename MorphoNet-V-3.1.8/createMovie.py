import sys
import os
import cv2

path=sys.argv[1]
outputname=sys.argv[2]

MaxFrame=0
ims={}
for f in os.listdir(path):
    filename, file_extension = os.path.splitext(f)
    if file_extension=='.png':
    	print 'Read '+path+'/'+f
    	frame=int(f.replace('movie_','').replace('.png',''))
    	MaxFrame=max(MaxFrame,frame)
    	im=cv2.imread(path+'/'+f)
    	ims[frame]=im
    	height , width , layers =  im.shape

framerate=30
fourcc =  cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
outputname=outputname.replace('.mp4','.avi')
video = cv2.VideoWriter(outputname,fourcc,framerate,(width,height))
if not video.isOpened():
	print 'Error cannot create '+outputname
	quit()


for frame in range(MaxFrame+1):
	video.write(ims[frame])

cv2.destroyAllWindows()
video.release()

os.system('ffmpeg -i '+outputname+'  '+outputname.replace('.avi','.mp4'))
os.system('rm -f '+outputname)