<?php
	
include 'uni-functions.php';

if ($connected){
	if(isset($_POST["share"])){ //Query on the  Upload
		$share=intval($_POST["share"]);
	 	if($share==1){//Get list of guys execpt me dataset LIST
	 		
	 		
	 		$Institution=[];
			$result = mysqli_query($link,'SELECT id,name FROM Institution');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Institution[$r[0]]=$r[1];

            $Laboratory=[];
			$result = mysqli_query($link,'SELECT id,name FROM Laboratory');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Laboratory[$r[0]]=$r[1];

            $Team=[];
			$result = mysqli_query($link,'SELECT id,name FROM Team');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Team[$r[0]]=$r[1];

            $Log=[];
			$result = mysqli_query($link,'SELECT id_people,COUNT(ID) FROM log GROUP BY  id_people');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Log[$r[0]]=$r[1];
           
          	$people=[];
            $result = mysqli_query($link,'SELECT id,name,surname,id_Institution,id_Laboratory,id_Team FROM people WHERE id!='.$_POST["id_people"].' AND id!=0 AND exist=1 ORDER BY name');
            if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_assoc($result)) {
            	$newr=[];
            	$newr[0]=$r['id'];
            	$newr[1]=$r['name'];
            	$newr[2]=$r['surname'];
            	$newr[3]="";
            	if($r['id_Laboratory']!=null) $newr[3]=$Laboratory[$r['id_Laboratory']];
            	$newr[4]="";
            	if($r['id_Team']!=null) $newr[4]=$Team[$r['id_Team']];
            	$newr[5]="";
            	if($r['id_Institution']!=null) $newr[5]=$Institution[$r['id_Institution']];
            	$people[]=$newr;
            }
           			
					
            echo   jsonRemoveUnicodeSequences($people);
            
            
	 	}
	 	if($share==2){//List people who alrdeay have this share 
	 		$exist = "";
			$result = mysqli_query($link,'SELECT id_who FROM share WHERE base="'.$_POST["base"].'" and id_base='.$_POST["id_base"].' and id_people='.$_POST["id_people"]);
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$exist=$exist.$r[0].';';
				}
	 		}
	 		echo $exist;
	 	}

	 	if($share==3){//Insert a new share
	 		$id_people_list=array();
	 		if(isset($_POST["id_people_list"])) $id_people_list=explode(";",substr($_POST["id_people_list"],0,-1));
	 		//First we list all people which already have access to this ressource
	 		if($_POST["base"]=="correspondence"){
	 			$isPublic=0;
	 			foreach ($id_people_list as &$i) 
	 				if($i=="0") $isPublic=1;
	 			if($isPublic)
	 				echo query("UPDATE correspondence SET type=1 WHERE id=".$_POST["id_base"]);
	 			else 
	 				echo query("UPDATE correspondence SET type=2 WHERE id=".$_POST["id_base"]);
	 		}
	 		$exist = array();
			$result = mysqli_query($link,'SELECT id_who FROM share WHERE base="'.$_POST["base"].'" and id_base='.$_POST["id_base"].' and id_people='.$_POST["id_people"]);
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$exist[] = $r[0];
				}
	 		}
	 		//Compare List
	 		$delete=array();
	 		foreach ($exist as &$e) {
	 			$conserve=False;
	 			foreach ($id_people_list as &$i) {
	 				if($i==$e) $conserve=True;
	 			}
	 			if(!$conserve)$delete[]=$e;
	 		}
	 		
	 		$insert=array();
	 		foreach ($id_people_list as &$i) {
	 			$ins=True;
	 			foreach ($exist as &$e) {
	 				if($i==$e) $ins=False;
	 			}
	 			if($ins)$insert[]=$i;
	 		}
	 		
	 		//Perform the deletion
	 		foreach ($delete as &$d) {
	 			echo query('DELETE FROM share WHERE base="'.$_POST["base"].'" and id_base='.$_POST["id_base"].' and id_people='.$_POST["id_people"].' and id_who='.$d);
	 		}

	 		//Perform the insertion
	 		
	 		foreach ($insert as &$i) {
	 			$now=new DateTime();
	 			echo query('INSERT INTO share (base,id_base,id_people,id_who,date) VALUES ("'.$_POST["base"].'",'.$_POST["id_base"].','.$_POST["id_people"].','.$i.',"'.$now->format('Y-m-d h:s:i').'")');
	 		}
	 		
	 		
	 		
	 	}
	 	
	}
	mysqli_close($link);
}

?>
