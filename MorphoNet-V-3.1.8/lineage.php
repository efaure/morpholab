<?php

include 'functions.php';

$_POST['infos']="Lineage";


class cell{
    public $t;
    public $id;
    public $ds;
    public $ms;
    public $nbds; //Total numbber of cells at the end
   
    public function __construct($t,$id) {
     $this->t = $t;
     $this->id = $id;
     $this->ms=array();
     $this->ds=array();
     $this->nbds=0;
    }

    public function addDaughter($d){
       array_push($this->ds,$d);
    }

    public function addMother($m){
       array_push($this->ms,$m);
    }
}

$Cells=array(); #LIST OF ALL CELLS


function getCelll($t,$id){
    global $Cells;
    if(!isset($Cells[$t][$id])){
        $c=new cell($t,$id); //CREATE A NEW CELL
        $Cells[$t][$id]=$c; //ADD IT TO THE LIST


    }else{
        $c=$Cells[$t][$id];
 
    }
    return $c;
}


function trace($c,$padding){
    global $Cells;
    $draw=' onmouseover="show_popup(\'('.$c->t.','.$c->id.')\')"  onclick="showSelectedCell('.$c->t.','.$c->id.')" ';//onmouseout="show_popup(\'\')" ';
    echo '<div id="cell_'.$c->t.'_'.$c->id.'"  class="cell" style="width:'.($c->nbds).'px;padding-left:'.$padding.'px" >';
    echo '<div id="circle_'.$c->t.'_'.$c->id.'" class="circle"  '.$draw.'></div>';
    if(sizeof($c->ds)>0) echo '<hr id="hr_'.$c->t.'_'.$c->id.'" class="vertical"  />';
    
        #title="('.$c->t.','.$c->id.')"/>';
        
    if(sizeof($c->ds)==1){
         foreach($c->ds as $ids => $cs){
            trace(getCelll($c->t+1,$cs),0);
        }
    }
     if(sizeof($c->ds)>=2){ //DIVISION

        echo '<hr id="hor_'.$c->t.'_'.$c->id.'" class="horizontal" style="width:'.(($c->nbds-1)/2).'px"/>';
        $nbMax=0;
        foreach($c->ds as $ids => $cs) $nbMax=max($nbMax,$Cells[$c->t+1][$cs]->nbds);
        foreach($c->ds as $ids => $cs){
            $dcs=$Cells[$c->t+1][$cs];
            $marge=($nbMax-$dcs->nbds)/2;
            echo '<div id="div_'.$dcs->t.'_'.$dcs->id.'" class="division" style="width:'.($nbMax).'px;" >';//
            trace($dcs,$marge);
            echo '</div>';
        }
        
    }
    echo "</div>";
}

$track_width=1;


if ($connected){
	$id_dataset=-1;
    if(isset($_POST['id_dataset'])) $id_dataset=$_POST['id_dataset'];
    if(isset($_GET['id_dataset'])) $id_dataset=$_GET['id_dataset'];

    $minTime=intval(query_first('SELECT minTime FROM dataset WHERE id='.$id_dataset));
    $maxTime=intval(query_first('SELECT maxTime FROM dataset WHERE id='.$id_dataset));
    
    for ($t = $minTime; $t <= $maxTime; $t++) $Cells[$t]=array();
    $infos=$_POST['infos'];
    $zippedlinage=query_first('SELECT field FROM correspondence WHERE id_dataset='.$id_dataset.' AND  datatype="time" LIMIT 1');
	
    $lineage=bzdecompress($zippedlinage);
    //$fp = fopen('/data/MorphoNetDev/Snapshot/tewt.txt', 'w'); fwrite($fp, $lineage); fclose($fp);

    $lineages = explode("\n", $lineage);
    foreach ($lineages as &$value) {
        if(strlen($value)>0 && $value[0]!="#"){
            $tab = explode(":", $value);//TIME CELL,  ID CELL , Channel : TIME Daughter, ID DAUGHER , Channel
            if(sizeof($tab)==2 && $tab[0]!="type"){
	            #MOTHER
	            $info = explode(",", $tab[0]);
	            $t=0;$idc=-1;
	            if(sizeof($info)==1) $idc=intval($info[0]); //CELL
	            if(sizeof($info)>=2) { $t=intval($info[0]); $idc=intval($info[1]); } //TIME, CELL,(CHANNEL)
	            if($idc!=-1) $c=getCelll($t,$idc);
	            
	            //DAUGHTER
	            $info = explode(",", $tab[1]);
	            $td=$t+1;$idd=-1;
	            if(sizeof($info)==1) $idd=intval($info[0]); //CELL
	            if(sizeof($info)>=2) { $td=intval($info[0]); $idd=intval($info[1]); } //TIME, CELL ,(CHANNEL)
	            if($idd!=-1)  $cd=getCelll($td,$idd);
	            
	            
	            if($idd!=-1 && $idd!=-1){
		            if($td>$t){
		            	$c->addDaughter($cd->id);
						$cd->addMother($c->id);
					}
					if($td<$t){
		            	$c->addMother($cd->id);
						$cd->addDaughter($c->id);
					}
				}
	            //echo "Found Cell ".$idc." at ".$t.' with daughter '.$idd.'</br>';
            }
        }
    }

    //Calcul number of final Cells
    for ($t = $maxTime; $t >= $minTime; $t--){
	   // echo "At $t  </br>";
        foreach($Cells[$t] as $id => $c){
            if(sizeof($c->ds)==0) {
	            $c->nbds=1; //No More Cell  Last Width
	            //echo "Last Cell ".$c->id."</br>";
	        }
            else if(sizeof($c->ds)==1) { //One Cell Same size as previous
                 foreach($c->ds as $ids => $idcs){
                    $cs=$Cells[$t+1][$idcs];
                    $c->nbds=$cs->nbds;
                }
            }
            else { //Multiples Cells
                $c->nbds=0;
                foreach($c->ds as $ids => $idcs){
                    $cs=$Cells[$t+1][$idcs];
                    $c->nbds=max($c->nbds,$cs->nbds);
                }
                $c->nbds=1+$c->nbds*sizeof($c->ds);
               //echo "Cell $c->t as +$c->nbds</br>";
            }
        }
    } 
    //Calcul the total width
    $TotalSize=0;
    foreach($Cells[$minTime] as $id => $c)   $TotalSize+=$c->nbds;
    //echo "TotalSize=$TotalSize</br>";
    mysqli_close($link);
} else echo "DATABASE connection error...";

?>
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
  </head>
  <style>

    .title {
        font-size: 15px;
        color:black;
    }

    .comment {
        font-size: 10px;
        color:black;
    }

    .mainTable{
        margin:0px;
        border: 0;
    }
    .mainTable tr{
        border: 1px solid black;
    }
    .mainTable td{
        border: 1px solid black;
    }

    .smallTable{
        margin:0px;
        border: 0;
    }
    .smallTable tr{
        border: 1px solid grey;
    }
    .smallTable td{
        border: 1px solid grey;
    }
    
    .image{
        margin:auto;
        padding:2px;
    }

    .cell{
         border:0;
         float:left;
         padding:auto;
         margin:auto;
         vertical-align:middle;

    }
    .division{
         border:0;
         float:left;
         vertical-align:middle;
    }

    hr.vertical
    {
        vertical-align:middle;
        width: 0px;
        height: 10px; /* or height in PX */
        padding:0px;
        margin:auto;
        border: 0px;
        border-left:1px solid grey;/*1px solid #8c8b8b;*/
    } 
    hr.vertical:hover{
        border-left:1px solid white;/*1px solid #8c8b8b;*/

    }
    hr.horizontal
    {
        vertical-align:middle;
        height: 0px; /* or height in PX */
        padding:0px;
        margin:auto;
        border: 0px;
        border-top:1px solid grey;/*1px solid #8c8b8b;*/
    } 

    .circle {
        vertical-align:middle;
        width: 10px;
        height: 10px; 
        padding:0px;
        margin:auto;
        border: 0px;
        background:grey;
        border-radius: 50%;
        border:1px solid grey;/*1px solid #8c8b8b;*/
    }
    .circle:hover{
        border:1px solid white;/*1px solid #8c8b8b;*/
    }

    </style>

    <body style="margin: 0;  padding: 0; text-align: center; background-color:#67C3F7;"> 
        <div style=" display: table; margin: auto; padding:20px; ">
                <span style="font-size:35px;color:grey;">MorphoNet</span>
                <input type="range" id="width_screen" value="0" max="10" min="0" step="1" style="width:100px;" onchange="resize_width_screen()">

        </div>

        <div style=" display: table; margin: auto; padding:20px; ">
                <div id="lineage" style="width:<?php echo $TotalSize; ?>px">
                <?php 
                    //trace($Cells[$minTime][6],0); //TO TEST 
                    foreach($Cells[$minTime] as $id => $c) trace($c,0);
                ?> 
            </div>

        </div> 
         <div id="popup" style="position:absolute;top:-10px;left:0px;width:100px;height:30px;"><span id="popup_content" style="padding-top:5px;font-size: 25px;"></span></div>
        <script type="text/javascript" src="jquery.min.js"></script>
        <script type="text/javascript">
                function resize_width_screen(){
                    var ws = $("#width_screen").val();
                    ws=ws*2+1; //0->1, 1->3, 2->5 etc...
                   // alert(valof);
                    <?php

                    function retrace($c,$padding){
                        global $Cells;

                        echo '$("#cell_'.$c->t.'_'.$c->id.'").css("width",'.$c->nbds.'*ws);';//.chr(13);
                         
                        if(sizeof($c->ds)==1){
                             foreach($c->ds as $ids => $cs){
                                retrace(getCelll($c->t+1,$cs),0);
                            }
                        }
                         if(sizeof($c->ds)>=2){ //DIVISION
                            echo '$("#hor_'.$c->t.'_'.$c->id.'").css("width",'.(($c->nbds-1)/2).'*ws);';//.chr(13);
                            $nbMax=0;
                            foreach($c->ds as $ids => $cs) $nbMax=max($nbMax,$Cells[$c->t+1][$cs]->nbds);
                            foreach($c->ds as $ids => $cs){
                                $dcs=$Cells[$c->t+1][$cs];
                                $marge=($nbMax-$dcs->nbds)/2;
                                echo '$("#div_'.$dcs->t.'_'.$dcs->id.'").css("width",'.$nbMax.'*ws);';//.chr(13);
                                retrace($dcs,$marge);
                           
                            }
                            
                        }
                    }

                    //retrace($Cells[$minTime][6],0);//TO TEST 
                    foreach($Cells[$minTime] as $id => $c) retrace($c,0);

                   echo '$("#lineage").css("width",'.$TotalSize.'*ws);'.chr(13);
                ?>
                }

                $(document).mousemove(function(e) {
                        window.x = e.pageX;
                        window.y = e.pageY;
                });
				
				
                function show_popup(str) {
                    $("#popup_content").html(str);
                    $("#popup").fadeIn("fast");
                    $("#popup").css("top", y);
                    $("#popup").css("left", x);
                }
                function hide(t,id){
                    $("#circle_"+t+"_"+id).css("opacity", "0");
                    $("#hr_"+t+"_"+id).css("opacity", "0");
                    if ($("#hor_"+t+"_"+id).length) $("#hor_"+t+"_"+id).css("opacity", "0");
                }
                function show(t,id){
                     $("#circle_"+t+"_"+id).css("opacity", "1");
                     $("#hr_"+t+"_"+id).css("opacity", "1");
                     if ( $("#hor_"+t+"_"+id).length ) $("#hor_"+t+"_"+id).css("opacity", "1");
                }
               
                function colorize(t,id,r,g,b){
                    $("#circle_"+t+"_"+id).css("background", "rgb("+r+","+g+","+b+")");
                }
                 function uncolorize(t,id){ 
                    $("#circle_"+t+"_"+id).css("background", "grey");
                 }
				 
                function showSelectedCell(t,id){
                    ecell = window.opener.document.getElementById("cell");
                    ecell = $(ecell);
                    ecell.val(id);

                    etime = window.opener.document.getElementById("time");
                    etime = $(etime);
                    etime.val(t);

                    eshowCell = window.opener.document.getElementById("showCell");
                    eshowCell = $(eshowCell);
                    eshowCell.click();
                }
                //MENU SELECTION
                 function clearFutur(cellid){
	                var idcell=celltid.attr('id');
					if(typeof idcell !== typeof undefined && idcell !== false){
						if(idcell.substring(0,5)=="cell_"){
							var cell=idcell.split("_");
							uncolorize(cell[1],cell[2]);
						}
						celltid.children().each( function(){
							var babyid=$(this).attr('id');
							if(babyid.substring(0,5)=="cell_" || babyid.substring(0,4)=="div_")
								clearFutur(babyid);
								
						} );
					}
                }
                function clearAll(){   clearFutur($("#lineage"));  }
                function applySelection(r,g,b,listcells){
                    var tabCells=listcells.split(":");
                    for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2)
                        	colorize(cell[0],cell[1],r,g,b);
                    }
                }
                function resetSelectionOnSelectedCells(listcells){
                    var tabCells=listcells.split(":");
                    for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2)
	                        uncolorize(cell[0],cell[1]);
                    }
                }
                function hideCells(v,listcells){
                    var tabCells=listcells.split(":");
                    for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2){
	                        if(v==1) show(cell[0],cell[1]);
							else hide(cell[0],cell[1]);
						}
                    }
                }
	            function hideFutur(v,t,id){
		            var celltid=$("#cell_"+t+"_"+id);
	                var idcell=celltid.attr('id');
					if(typeof idcell !== typeof undefined && idcell !== false){
						if(idcell.substring(0,5)=="cell_"){
							var cell=idcell.split("_");
							 if(v==1) show(cell[1],cell[2]); else hide(cell[1],cell[2]);
						}
						celltid.children().each( function(){
							var babyid=$(this).attr('id');
							if(babyid.substring(0,5)=="cell_" || babyid.substring(0,4)=="div_"){
								var cell=idcell.split("_");
								hideFutur(v,cell[1],cell[2]);
							}
								
						} );
					}
                }
                function hidePast(v,t,id){
		            if(v==1) show(t,id); else hide(t,id);
	                var parent=$("#cell_"+t+"_"+id).parent();
                   	var parentid = parent.attr('id');
                   	if(typeof parentid !== typeof undefined && parentid !== false && parentid!="lineage"){
                   	   	 var cellparent=parent.attr('id').split("_");
                       	 hidePast(v,cellparent[1],cellparent[2]);
                   	}
                }
                
                function hideCellsAllTime(v,listcells){
	                var tabCells=listcells.split(":");
                    for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2){
	                        hideFutur(v,cell[0],cell[1]);
							hidePast(v,cell[0],cell[1]);
						}
                    }  
                }
                
                
				function colorizeNext(r,g,b,celltid){
					var idcell=celltid.attr('id');
					if(typeof idcell !== typeof undefined && idcell !== false){
						if(idcell.substring(0,5)=="cell_"){
							var cell=idcell.split("_");
							colorize(cell[1],cell[2],r,g,b);
						}
						celltid.children().each( function(){
							var babyid=$(this).attr('id');
							//alert("BABY "+babyid)
							if(babyid.substring(0,5)=="cell_" || babyid.substring(0,4)=="div_")
								colorizeNext(r,g,b,$(this));
						} );
					}
					
					
				}
                function propageFutur(r,g,b,tabCells){
	                 for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2){
	                       	 t=cell[0]; id=cell[1];
	                       	 colorizeNext(r,g,b,$("#cell_"+t+"_"+id));
                       }
                    }
                }
                 function propagePast(r,g,b,tabCells){
	                for (var i = 0; i < tabCells.length; i++) {
                        var cell=tabCells[i].split(";");
                        if(cell.length==2){
                       	 t=cell[0]; id=cell[1];
                       	// if(id=="34"){
	                      // 	 alert("propagePast "+id+ " at "+t);
                       	 var parent=$("#cell_"+t+"_"+id).parent();
                       	 var parentid = parent.attr('id');
                       	 while(typeof parentid !== typeof undefined && parentid !== false && parentid!="lineage"){
                       	   	 //alert(parent.attr('id'));
	                       	 var cellparent=parent.attr('id').split("_");
	                       	 colorize(cellparent[1],cellparent[2],r,g,b);
	                       	 parent=parent.parent();
	                       	 parentid = parent.attr('id');
                       //	 }
                       	 }
					   	 
					   	 // alert($("#circle_"+t+"_"+id).parent());
                       }
                    }
	                
                }
                function propagateThis(v,r,g,b,listcells){
	                var tabCells=listcells.split(":");
	                if(v==1)propageFutur(r,g,b,tabCells);
	                else propagePast(r,g,b,tabCells);
                }
        </script> 
    </body>
</html>