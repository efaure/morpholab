<?php
	
include 'uni-functions.php';

if ($connected){
	if(isset($_POST["colormap"])){ 
		$colormap=intval($_POST["colormap"]);
		if($colormap==0){ //List all colormap for this user
			$rows = array();
			$result = mysqli_query($link,'SELECT id,name,date,id_people FROM colormap WHERE id_people='.$_POST["id_people"]);
     		if (!$result) echoUTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$rows[] = $r;
				}
	 		}
	 		//LOOK IN THE SHARE COLORMAP
            $result = mysqli_query($link,'SELECT id_base,id_people FROM share WHERE base="colormap" and id_people!='.$_POST["id_people"].' and ( id_who='.$_POST["id_people"].' or id_who=0)');
            if (!$result) echoUTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$result2 = mysqli_query($link,'SELECT id,name,date,id_people  FROM colormap WHERE id='.$r[0].' and id_people='.$r[1]);
					while($r2 = mysqli_fetch_row($result2)) {
						$rows[] = $r2;
					}	
				}
				print  jsonRemoveUnicodeSequences($rows);
	 		}
		}
	 	if($colormap==1){//Save a colormap
	 		$now=new DateTime();
			$file=$_FILES["colormap_txt"];
	 		echo query('INSERT INTO colormap (id_people,name,date,colors) VALUES ('.$_POST["id_people"].',"'.$_POST["name"].'","'.$now->format('Y-m-d h:s:i').'","'.addslashes(file_get_contents($file['tmp_name'])).'")');
	 	}
	 	if($colormap==2){//Get the colors for one colormap by time 
	 		echo query_first('SELECT colors FROM colormap WHERE id='.$_POST["id_colormap"]);
	 	}
	 	if($colormap==3){//Delete a selction
	 		echo query('DELETE FROM colormap WHERE id='.$_POST["id_colormap"]);
	 		echo query('DELETE FROM share WHERE base="colormap" and id_base='.$_POST["id_colormap"].' and id_people='.$_POST["id_people"]);
	 	}
	}
	mysqli_close($link);
}

?>
