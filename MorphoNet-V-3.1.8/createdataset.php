<?php

include 'functions.php';

session_start();

$log=-1;


if(!isset($_SESSION['ID']))  header('Location:'.get_server());

$id_people=$_SESSION['ID'];
$now=new DateTime();

$minTime=0; if(isset($_POST["minTime"])) $minTime=$_POST["minTime"];
$maxTime=0; if(isset($_POST["maxTime"])) $minTime=$_POST["maxTime"];

$bundle=-1;
 if(!isset($_POST["typefield"])) echo "MISS TYPE</br>";
$id_dataset_type=$_POST["typefield"];

$id_dataset=107;

$id_dataset=query_id('INSERT INTO dataset (id_people,date,name,minTime,maxTime,bundle,id_dataset_type) VALUES ('.$id_people.',"'.$now->format('Y-m-d h:s:i').'","'.$_POST["name"].'",'.$minTime.','.$maxTime.','.$bundle.','.$id_dataset_type.')');  


$main_path="/var/www/UploadDATASET/DATASET_".$id_dataset."/";
echo "</br>CREATE ".$main_path."</br>";
mkdir($main_path);

$mesh_path=$main_path."MESH/";
echo "</br>CREATE ".$mesh_path."</br>";
mkdir($mesh_path);

$desc_path=$main_path."DESC/";
echo "</br>CREATE ".$desc_path."</br>";
mkdir($desc_path);

$bundle_path="/var/www/Bundle/WebGL/DATASET_".$id_dataset."/";
echo "</br>CREATE ".$bundle_path."</br>";
mkdir($bundle_path);

//FOR CONVERTER
$bundle_dir="/var/www/ConvertOBJToAssetBundle/Assets/Resources/Bundle/";
$obj_dir="/var/www/ConvertOBJToAssetBundle/Assets/Resources/Obj/";



$t=0;
$channel=0;
$quality=0;


function extractUpload($filename,$pathto,$ext){
	echo "</br>extractUpload ".$filename."</br>";
	$file_parts = pathinfo($filename);
	print_r($file_parts);
	if($file_parts['extension']=="zip") {
		  echo "</br>UNZIP ".$filename."</br>";
		   $zip = new ZipArchive;
		   if ($zip->open($filename) === true) {
		    for($i = 0; $i < $zip->numFiles; $i++) {
		        $tfilename = $zip->getNameIndex($i);
		        $fileinfo = pathinfo($tfilename);
		        echo "TEST ".$tfilename."</br>";
		        if($fileinfo['extension']==$ext){
			        echo ("</br>zip://".$filename."#".$tfilename." ".$pathto.$fileinfo['basename'].'</br>');
		        	copy("zip://".$filename."#".$tfilename, $pathto.$fileinfo['basename']);
		      	}    
		    }               
			$zip->close();                   
		   }
		unlink($filename);
	}
	if($file_parts['extension']==$ext) {
		echo "MOVE ".$filename." TO " .$pathto.$file_parts["basename"]."</br>";
		rename($filename,$pathto.$file_parts["basename"]);
	}
}

//UNZIP FILES
foreach($_FILES as $key => $file){
	echo "</br>".$key."->".$file["name"]."</br>";
	print_r($file);
	$filename=$main_path.$file["name"];
	move_uploaded_file($file['tmp_name'],$filename);
	if(substr($key,0,4)=="mesh")extractUpload($filename,$mesh_path,"obj");
	if(substr($key,0,4)=="desc")extractUpload($filename,$desc_path,"txt");
		
}

//DESCRIPTION
query('DELETE FROM  correspondence WHERE id_dataset='.$id_dataset); //TEMP

$files = scandir($desc_path);	
foreach($files as $key => $filename){
	$fileinfo = pathinfo($filename);
	if($fileinfo['extension']=="txt"){
		$infos=str_replace(".txt","",$filename);
		$field = file_get_contents($desc_path.$filename);
		$now=new DateTime();
		$id_corres=query_id('INSERT INTO correspondence (id_dataset,infos,field,date,id_people,type) VALUES ('.$id_dataset.',"'.$infos.'","'.mysqli_escape_string($link,bzcompress($field)).'","'.$now->format('Y-m-d h:s:i').'",'.$id_people.',1)'); 
		echo "</br>".$filename."->".$id_corres."</br>";
	}	
}



//MESH	
query('DELETE FROM  mesh WHERE id_dataset='.$id_dataset); //TEMP


function uploadMesh($id_dataset,$t,$channel,$quality,$objfile,$link){
	$now=new DateTime();
	$obj = file_get_contents($objfile);
	$objA=explode("\n",$obj);
	$X=0; $Y=0; $Z=0; $nb=0;
	for($i=0;$i<count($objA);$i++){
		$line=$objA[$i];
		if(strlen($line)>1 && substr($line,0,1)=="v" && substr($line,1,1)==" "){
			//echo $line.':'.strlen($line)."</br>";
			$tab=explode(" ",$line);
			$X+=intval($tab[1]);
			$Y+=intval($tab[2]);
			$Z+=intval($tab[3]);
			$nb+=1;
		}
	}
	$X/=$nb;
	$Y/=$nb;
	$Z/=$nb;
	echo "</br>-> X=$X Y=$Y Z=$Z<br>";
	$id_mesh=query_id('INSERT INTO mesh (id_dataset,t,channel,quality,obj,link,date,center) VALUES ('.$id_dataset.','.$t.',"'.$channel.'",'.$quality.',"'.mysqli_escape_string($link,bzcompress($obj)).'","'.$link.'","'.$now->format('Y-m-d h:s:i').'","'.round($X,2).','.round($Y,2).','.round($Z,2).'")'); 
	echo "</br>Process ".$objfile." -> " .$id_mesh." </br>";
}


//CONVERT OBJ IN BUNDLE
$files = scandir($mesh_path);	
foreach($files as $key => $filename){
	$fileinfo = pathinfo($filename);
	if($fileinfo['extension']=="obj"){
		echo "COPY ".$mesh_path.$filename." TO ".$obj_dir.$filename."</br>";
		copy($mesh_path.$filename,$obj_dir.$filename);
	}
}
echo "</br>LAUNCH UNITY CONVERTER</br>";
system("xvfb-run --auto-servernum --server-args='-screen 0 640x480x24:32' /var/www/unity-editor-5.1.0f3/Editor/Unity -projectPath /var/www/ConvertOBJToAssetBundle   -batchmode -executeMethod myConvert.Convert.ConvertOBJTobundle  -logFile /var/www/UploadDATASET/UnityLog-".$id_dataset.".txt -quit  -nographics");


//RECOPY ALL BUNDLE
$files = scandir($bundle_dir);	
foreach($files as $key => $filename){
	$fileinfo = pathinfo($filename);
	echo "MOVE ".$bundle_dir.$filename." TO ".$bundle_path.$filename."</br>";
	rename($bundle_dir.$filename,$bundle_path.$filename);
}

//REMOVE ALL OBJ
$files = scandir($obj_dir);	
foreach($files as $key => $filename){
	echo "DELETE ".$obj_dir.$filename."</br>";
	unlink($obj_dir.$filename);
}


//NOW WE UPLOAD ALL OBJ 
$files = scandir($mesh_path);	
foreach($files as $key => $filename){
	$fileinfo = pathinfo($filename);
	if($fileinfo['extension']=="obj"){
		$name=strtolower(str_replace(".obj","",$filename));
		$link=str_replace("/var/www/","",$bundle_path).$name;
		uploadMesh($id_dataset,$t,$channel,$quality,$filename,$link);
	}	
}




function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

deleteDirectory($main_path);


 //NOW WE UPLOAD OBJ (IN ONE)
	/*$files = scandir($main_path);
	$nbL=0;
	$objAll='';
	$temp_maxFace=0;
	$maxFace=0;
	
	foreach($files as $key => $filename){
		$fileinfo = pathinfo($filename);
		if($fileinfo['extension']=="obj"){
			$name=str_replace(".obj","",$filename);
			$handle = fopen($main_path.$filename, "r");
			if ($handle) {
				echo  "</br>Process ".$name."</br>";
				$objAll=$objAll.'g 0,'+$name+',0\n';
				$maxFace=$temp_maxFace;
			    while (($line = fgets($handle)) !== false) {
				   	 if($line[0]=='f'){
						$faces=explode(" ",trim($line));
						$objAll=$objAll.'f ';
						foreach($faces as $i=>$face){
							if($i>0) $fa=intval($face);
							$temp_maxFace=max($temp_maxFace,$fa+$maxFace);
							$objAll=$objAll.strval($fa+$maxFace).' ';
						}
						$objAll=$objAll."\n";
					  }
					 else $objAll=$objAll.$line;
					 $nbL+=1;
			    }
				fclose($handle);
			} else echo "ERROR cannot read ".$filename;
		}
	}
	//print($objAll);
	file_put_contents($main_path."test.obj",$objAll);
	*/
?>
