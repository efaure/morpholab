<?php


include 'functions.php';

session_start();



$log=-1; //ACCUEIL
$errorConnection="";
if(isset($_GET['todo']) && $_GET['todo']=="disconnect") {
	$_SESSION = array();
	session_unset ();
	session_destroy();
	$log=3;
}


function addConnection($id_people){
	$IP="unkonwn";
	$now=new DateTime();
    if(isset($_SERVER["REMOTE_ADDR"]) )$IP=$_SERVER["REMOTE_ADDR"];
    query('INSERT INTO log (id_people,IP,date) VALUES ('.$id_people.',"'.$IP.'","'.$now->format('Y-m-d h:s:i').'") ');
}


if ($connected){
	if(isset($_SESSION['ID'])) $log=2; //CONNECTED
	else if(isset($_POST["login"]) && isset($_POST["pass"])){
		$login = anti_injection_login($_POST["login"]); //I use that function to protect against SQL injection
		$pass = anti_injection_login($_POST["pass"]);
		$SQL = "SELECT id,password,name,surname,id_right FROM people WHERE login = '" . $login . "' and password=MD5('".$pass."') and exist=1;";
        $result_id = @mysqli_query($link,$SQL) or die("DATABASE ERROR!");
        $total = mysqli_num_rows($result_id);
        if($total) {
            $datas = @mysqli_fetch_array($result_id);
            $idright = $datas["id_right"];
            $SQL_right = "SELECT name,dataset,unity,u_manage,git FROM people_right WHERE id = ". $idright.";";
            $result_right = @mysqli_query($link,$SQL_right) or die("DATABASE ERROR!");
            $datas_right = @mysqli_fetch_array($result_right);
           	session_start();
           	$_SESSION = array();
            $_SESSION['ID']=$datas["id"];
			$_SESSION['name']=$datas["name"];
			$_SESSION['surname']=$datas["surname"];
			$_SESSION['level']=$datas_right["name"];
			$_SESSION['dataset']=$datas_right["dataset"];	
			$_SESSION['unity']=$datas_right["unity"];	
			$_SESSION['u_manage']=$datas_right["u_manage"];	
			$_SESSION['git']=$datas_right["git"];	
	
            //We Make an insert in the log
            $now=new DateTime();
            $IP="unkonwn";
            if(isset($_SERVER["REMOTE_ADDR"]) )$IP=$_SERVER["REMOTE_ADDR"];
            query('INSERT INTO log (id_people,IP,date) VALUES ('.$datas["id"].',"'.$IP.'","'.$now->format('Y-m-d h:s:i').'") ');
            $log=2; //CONNECTED
        } else {
	        addConnection(-1);
	        $errorConnection="Login unknown";
	    }
    }   
   if($log!=2){ //NOT CONNECTED
	   if((isset($_POST['todo']) && $_POST['todo']=="connect") || (isset($_GET['todo']) && $_GET['todo']=="connect") ){
			$log=1;	//CONNECT
			$_SESSION = array();
			session_unset ();
			session_destroy();
	   }
  }
} else $errorConnection="database connection error....";



if($log==2){  header('Location:listdatasets.php'); } //CONNECTED
?>
		
		
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
     <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
     <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
     <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
	<meta name="Description" content="MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching." />
    <meta name="google" content="MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching." />
  </head>


  <body>
	    
  		 <div style=" display: table; margin-right: auto;  margin-left: auto; margin-top:10px; margin-bottom:auto;">
		  	 <?php include "headbar.php"; ?> 
		   	 <?php if($log==-1 || $log==3){  //ACCUEIL ?>
		  		 	<div>
						<div style="width:800px;font-size:18px;color:grey;margin:auto;text-align: justify">
						<p> MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching. </p>
						<p> MorphoNet offers a comprehensive palette of interactions to explore the structure, dynamics and variability of biological shapes and its connection to genetic expressions.  </p>
						<p> By handling a broad range of natural or simulated morphological data, it fills a gap which has until now limited the quantitative understanding of morphodynamics and its genetic underpinnings by contributing to the creation of ever-growing morphological atlases. </p>
						</div>
						<video src="Video_Presentation_MN.mov" controls width="600" autoplay></video>
					</div>
			<?php } else if($log==1){  //CONNECTION ?>
			    
			    <form action="index.php" method="post">
				    	<input  class="postdata" type="hidden" name="todo" id="todo" value="connect" >
						<table border="0" style="margin-top:200px;" >
								<tr style="background-color:white" >
									<td ><input type="text" class="postdata" id="login"  name="login"  placeholder="login" > </td>
									<td><input  class="postdata" type="password" name="pass" placeholder="Password"></td>
									<td> <input class="boutton" style="width:50px;height:30px;margin-left:20px;font-size:20px;background-color:#61BAE9;color:black" type="submit" value="go"> </td>
								</tr>
								
						</table>
			    </form>  
			<?php } else  if($log==2){  header('Location:listdatasets.php'); } //CONNECTED ?>
			 <?php if($errorConnection!="") { ?> <p class="error_comment" style="font-size:16px"><?php echo $errorConnection; ?> </p>  <?php } ?>
			 <div style="margin-top:50px;margin-bottom:150px"> 	<span style="color:grey;">Optimized for </span> <img style="vertical-align:middle" type="image" width="35px"  src="images/firefox.png" > </div>
  		 </div>

		  <?php include "footer.php"; ?> 
	</body>
</html>