<?php


include 'uni-functions.php';
if ($connected){
	if(isset($_POST["user"])){
		$user=intval($_POST["user"]);
		if($user==1){ // NEW USER
			
		 	$id_Institution=0;
		 	if(isset($_POST['new_insitution']))
		 		$id_Institution=query_id('INSERT INTO Institution(name) VALUES ("'.addslashes($_POST["new_insitution"]).'")');
		 	else $id_Institution=$_POST["id_Institution"];

		 	$id_Laboratory=0;
		 	if(isset($_POST['new_laboratory']))
		 		$id_Laboratory=query_id('INSERT INTO Laboratory(name) VALUES ("'.addslashes($_POST["new_laboratory"]).'")');
		 	else $id_Laboratory=$_POST["id_Laboratory"];

		 	$id_Team=0;
		 	if(isset($_POST['new_team']))
		 		$id_Team=query_id('INSERT INTO Team(name) VALUES ("'.addslashes($_POST["new_team"]).'")');
		 	else $id_Team=$_POST["id_Team"];


		 	if(isset($_POST['id_user'])){ //MODIFICATION 
				$pass="";
		 		if(isset($_POST['password']))$pass=",password=".'"'.anti_injection_login_senha($_POST["password"]).'" ';
				echo query('UPDATE people SET name="'.addslashes($_POST["lastname"]).'", surname="'.addslashes($_POST["firstname"]).'", email="'.addslashes($_POST["email"]).'", id_right="'.addslashes($_POST["id_right"]).'", tel="'.addslashes($_POST["tel"]).'", adress="'.addslashes($_POST["adress"]).'", fonction="'.addslashes($_POST["fonction"]).'" '.$pass.',id_Institution='.$id_Institution.',id_Laboratory='.$id_Laboratory.',id_Team='.$id_Team.' WHERE id='.$_POST['id_user']);
			}else{ //CREATION
				$now=new DateTime();
				$pass = anti_injection_login_senha($_POST["password"]);
				echo query('INSERT INTO people (name,surname,email,login,password,date,exist,id_right,tel,adress,fonction,id_Institution,id_Laboratory,id_Team) VALUES ("'.addslashes($_POST["lastname"]).'","'.addslashes($_POST["firstname"]).'","'.addslashes($_POST["email"]).'","'.addslashes($_POST["login"]).'","'.$pass.'","'.$now->format('Y-m-d h:s:i').'",1,'.$_POST["id_right"].',"'.addslashes($_POST["tel"]).'","'.addslashes($_POST["adress"]).'","'.addslashes($_POST["fonction"]).'",'.$id_Institution.','.$id_Laboratory.','.$id_Team.')');
			}

		}
		if($user==2){ //LIST USERS
			$Institution=[];
			$result = mysqli_query($link,'SELECT id,name FROM Institution');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Institution[$r[0]]=$r[1];

            $Laboratory=[];
			$result = mysqli_query($link,'SELECT id,name FROM Laboratory');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Laboratory[$r[0]]=$r[1];

            $Team=[];
			$result = mysqli_query($link,'SELECT id,name FROM Team');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Team[$r[0]]=$r[1];

            $Log=[];
			$result = mysqli_query($link,'SELECT id_people,COUNT(ID) FROM log GROUP BY  id_people');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_row($result)) $Log[$r[0]]=$r[1];
           
          	$people=[];
            $result = mysqli_query($link,'SELECT id,name,surname,email,login,id_Institution,id_Laboratory,id_Team FROM people WHERE exist=1 AND id>0');
            if (!$result) echo UTF('Error : ' . mysqli_error($link));
            else   while($r = mysqli_fetch_assoc($result)) {
            	$newr=[];
            	$newr['id']=$r['id'];
            	$newr['name']=$r['name'];
            	$newr['surname']=$r['surname'];
            	$newr['email']=$r['email'];
            	$newr['login']=$r['login'];
            	$newr['Institution']="";
            	if(isset($r['id_Institution']) && $r['id_Institution']!=null && isset($Institution[$r['id_Institution']])) $newr['Institution']=$Institution[$r['id_Institution']];
            	$newr['Laboratory']="";
            	if(isset($r['id_Laboratory']) &&  $r['id_Laboratory']!=null && isset($Laboratory[$r['id_Laboratory']])) $newr['Laboratory']=$Laboratory[$r['id_Laboratory']];
            	$newr['Team']="";
            	if(isset($r['id_Team']) && $r['id_Team']!=null && isset($Team[$r['id_Team']])) $newr['Team']=$Team[$r['id_Team']];
            	if(isset($r['id']) && isset($Log[$r['id']]) &&  $Log[$r['id']]!=null) $newr['log']=$Log[$r['id']];
            	else $newr['log']=0;
            	$people[]=$newr;
            }
            echo   jsonRemoveUnicodeSequences($people);
		}	
		if($user==21){ //LIST A SPECIFIC USERS
			echo query_json_field('SELECT id,name,surname,email,login,id_right,tel,adress,fonction,id_Institution,id_Laboratory,id_Team FROM people WHERE exist=1 AND id='.$_POST['id_user_courant']);
		}	
		 if($user==3){ //MODIFY OTHER USERS
		 	$pass="";
		 	if(isset($_POST['password']))$pass=",password=".anti_injection_login_senha($_POST["password"]).'" ';
			echo query('UPDATE people SET name="'.addslashes($_POST["lastname"]).'", surname="'.addslashes($_POST["firstname"]).'", email="'.addslashes($_POST["email"]).'", login="'.addslashes($_POST["login"]).'" '.$pass.',id_right='.$_POST["id_right"].',id_Institution='.$_POST["id_Institution"].',id_Laboratory='.$_POST["id_Laboratory"].',id_Team='.$_POST["id_Team"].' WHERE id='.$_POST['id_user']);
		 }
		 if($user==4){ //DELETE USERS
			echo query('UPDATE people SET exist=0 WHERE id='.$_POST["id_user"]);
		 }
		 if($user==5){ //SHOW LOGS
			echo query_json('SELECT IP,date FROM log WHERE id_people='.$_POST["id_user"]. " ORDER  by date DESC");
		 }
		 if($user==6){ //SHOW COMMENTS
			echo query_json('SELECT comments,date FROM comments WHERE id_user='.$_POST["id_user"].' ORDER by date DESC ');
		 }
		 if($user==7){ //GET ALL LEVELS
			echo query_json_field('SELECT id,name FROM people_right');
		 }
		 if($user==9){ //GET ALL INSTITUTION
			echo query_json_field('SELECT id,name FROM Institution');
		 }
		 if($user==10){ //GET ALL LABORATORY
			echo query_json_field('SELECT id,name FROM Laboratory');
		 }
		 if($user==11){ //GET ALL TEAM
			echo query_json_field('SELECT id,name FROM Team');
		 }	

		 if($user==12){ //GET ALL COMMENTS
			echo query_json_field('SELECT comments.id,people.surname,people.name,comments.comments,comments.date,comments.done,comments.priority FROM comments,people WHERE people.id=comments.id_user ORDER BY comments.done DESC,comments.priority DESC, comments.date ');
		 }	
		 if($user==13){ //ARCHIVE COMMENTS
			echo query('UPDATE comments SET done=0 WHERE id='.$_POST["id_comment"]);
		 }
		 if($user==14){ //PRIORITY COMMENTS
			echo query('UPDATE comments SET priority='.$_POST["priority"].' WHERE id='.$_POST["id_comment"]);
		}  
	}
	mysqli_close($link);
}
?>

