<?php

include 'functions.php';

session_start();

$log=-1;

if(!isset($_SESSION['ID']))  header('Location:'.get_server());

function type($id_parent,$param){
	$result = mysqli_query($link,'select id,type from dataset_type where id_parent='.$id_parent);
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else {
		echo '<ul>';
			while($r = mysqli_fetch_assoc($result)){
				echo '<li>';
				if($param) {
					echo $r['type'];
					echo '<img src="images/delete.png" with="13px" height="13px" style="margin-left:10px" onclick="removeType('.$r['id'].')"/>';
				}else echo '<a href="#" onclick="changeType('.$r['id'].')" >'.$r['type'].'</a>';
				type($r['id'],$param);
				echo '</li>';
			}
			if($param) echo '<li data-jstree=\'{"icon":"images/adds.png"}\' onclick="addType('.$id_parent.')">new</li>';
			echo "</ul>";
		
	}
}


$todo=$_POST['todo'];
if($todo=="type"){
	$id_parent=$_POST['id_parent'];
	$result = mysqli_query($link,'select id,type from dataset_type where id_parent='.$id_parent);
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else {
		$row=[];
		while($r = mysqli_fetch_assoc($result)) $row[]=$r;
		echo json_encode($row);
	}
}
if($todo=="createtype"){
	$id_parent=$_POST['id_parent'];
	$result = mysqli_query($link,'INSERT INTO dataset_type  (id_parent,type) VALUES ('.$id_parent.',"'.$_POST['value'].'")');
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else $todo="deploytype";
}

		

if($todo=="removeType"){
	$id_parent=$_POST['id_parent'];
	$result = mysqli_query($link,'select id from dataset_type where id_parent='.$id_parent);
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else {
		if(mysqli_num_rows($result)>0) echo "Cannot remove this type, ,please first remove childs";
		else {
			$result2 = mysqli_query($link,'select id from dataset where id_dataset_type='.$id_parent);
			if (!$result2) echo UTF('Error : ' . mysqli_error($link));
			else {
				
				if(mysqli_num_rows($result2)>0) echo "Cannot remove this type, it still has some dataset..";
				else   {
					mysqli_query($link,'DELETE FROM dataset_type WHERE id='.$id_parent);
					$todo="deploytype";
				}
			}
		}
	}
	
}

if($todo=="deploytype"){
	echo '<div id="type">';
	$param=intval($_POST['param']);
	if($param==1) type(0,true);
	else type(0,false);
	echo '</div>';
}

?>