<?php

session_start();

include 'functions.php';
$now=new DateTime();


$show=1;
$isAdmin=0;
if ($connected){
  $isAdmin=getAdmin($_SESSION['ID']);

  if(isset($_POST['recycle'])){
    echo query('UPDATE comments SET done='.$_POST['done'].' WHERE id='.$_POST["recycle"]);
    $show=0;
  }

  if(isset($_POST['priority'])){
    echo query('UPDATE comments SET priority='.$_POST['priority'].' WHERE id='.$_POST["id_feedback"]);
    $show=0;
  }

 
  $comments=query_array('SELECT comments.id,people.surname,people.name,comments.comments,comments.date,comments.done,comments.priority FROM comments,people WHERE people.id=comments.id_user ORDER BY comments.done DESC, comments.date DESC');
  
} else echo "PROBLEM CONNECTION</br>";


if($show){
?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
  </head>

  <body>
    <?php include "headbar.php"; ?>

    <div  style="width: 100%;text-align: center;">
      <div style=" width: 800px;margin: 0 auto;margin-top:20px">
      <span style="font-size:30px;color:grey;">Feedbacks</span>
     <table id="Feedbacks">
        <tr>
          <!-- <td class="tdtitle"> ID</td> -->
          <td class="tdtitle" width="130px"> Date </td>
          <td class="tdtitle" width="130px"> From</td>
          <td class="tdtitle"> Status </td>
          <td class="tdtitle" width="100px"> Priority</td>
          <td class="tdtitle"> Feedback </td>
        </tr>
        <?php
        $previsousstatus=1;
        foreach ($comments as $id_feedback => $feedback){
          //JUST TO HAVE A TITLE  FOR ARCHVIED
          if($feedback['done']!=$previsousstatus) echo '<tr style="margin-top:135px;background-color:white"> <td class="tdtitle" colspan="5" style="font-size:30px;color:grey;">Archived</td></tr>';
          $previsousstatus=$feedback['done'];
          
          echo "<tr>";

          //echo "<td>".$id_feedback."</td>"; //ID
          echo "<td>".$feedback['date']."</td>"; //Date
          echo "<td>".$feedback['surname'].' '.$feedback['name']."</td>"; //From
          echo "<td>";//STATUS
          if($feedback['done']==0){ ?>  <input type="image" width="25px" height="25px"  title="recycle" src="images/recycle.png" onclick="recycle(<?php echo $id_feedback;?>,1)"/> 
         <?php  } else { ?>  <input type="image" width="25px" height="25px"  title="recycle" src="images/garbage.png" onclick="recycle(<?php echo $id_feedback;?>,0)"/>  <?php }
          echo "</td>"; //STATUS
          echo "<td>";
          for($i=0;$i<$feedback['priority'];$i++){ ?>
             <input type="image" width="15px" height="15px"  id="priority_<?php echo $id_feedback.'_'.$i;?>" title="remove priority" src="images/star-on.png" onclick="priority(<?php echo $id_feedback.','.$i;?>,0)"/>  
          <?php  }
           for($i=$feedback['priority'];$i<5;$i++){ ?>
             <input type="image" width="15px" height="15px"  id="priority_<?php echo $id_feedback.'_'.$i;?>" title="add priority" src="images/star-off.png" onclick="priority(<?php echo $id_feedback.','.$i;?>,1)"/>  
          <?php  }
        
          echo "</td>"; //PRIORITY
          echo "<td>".$feedback['comments']."</td>"; //FEEDBACK

          echo "</tr>";
        }
       ?>
      </table>
   </div>
     </div>

    
     <script>
      function recycle(id_feedback,done){
        $.post("debug.php", { recycle:id_feedback,done:done}, function(data,status){ window.location="debug.php";});
      }

      function priority(id_feedback,priority,done){
        priority=priority+1;
        $.post("debug.php", { priority:priority,id_feedback:id_feedback,done:done}, function(data,status){ 
           window.location="debug.php";
        });
      }
 
  
    </script>
    <?php include "footer.php"; ?> 
  </body>
</html>

<?php
}
mysqli_close($link);


?>