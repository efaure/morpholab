<?php


include 'uni-functions.php';
if ($connected){
		if(isset($_POST["gene"])){
			echo query_json_field ("SELECT id,gene_model,gene_name,unique_gene_id FROM anissed_all_genes WHERE gene_model like '%".strtolower($_POST["gene"])."%' or gene_name like '%".strtolower($_POST["gene"])."%' or unique_gene_id like '%".strtolower($_POST["gene"])."%' LIMIT 20");
		}
		else if(isset($_POST["allgene"])){
			echo query_json_field ("SELECT id,gene_model,gene_name,unique_gene_id FROM anissed_all_genes order by gene_name");
		}
		else if(isset($_POST["numberofcells"])){
			$organism_id=$_POST["organism_id"];
			$start_stage=$_POST["start_stage"];
			if($start_stage=="Stage 5")$start_stage="Stage 5a";
			if($start_stage=="Stage 6")$start_stage="Stage 6a";
			$end_stage=$_POST["end_stage"];
			if($end_stage=="Stage 5")$end_stage="Stage 5b";
			if($end_stage=="Stage 6")$end_stage="Stage 6b";
			$stages=[];
			$isin=false;
			
			$result = mysqli_query($link,'SELECT id,stage FROM anissed_all_stages');
			$stage="";
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_assoc($result)) {
					if($r['stage']==$start_stage)$isin=true;
					if($isin) {
						if($stage=="")$stage="AND ( stage=".$r['id']; 
						else $stage=$stage." OR stage=".$r['id']; 
					}
					//Prlbème Des fois le stage est 6 et non 6a ou 6b
					if($r['stage']==$end_stage)$isin=false;
				}
	 		}
			if($stage!="")$stage=$stage.")";
			
			$NbCells=array();
			$result = mysqli_query($link,"SELECT id FROM anissed_all_genes order by gene_name");
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_assoc($result)) {
					$NbCells[$r['id']]=query_first('SELECT count(cell) FROM anissed_cells_by_gene_by_stage WHERE gene_id='.$r['id'].' '.$stage);
				}
	 		}
	 		echo json_encode($NbCells);
		}

		else if(isset($_POST["gene_id"])){
			
			
			$gene_id=$_POST["gene_id"];
			$organism_id=$_POST["organism_id"];
			$start_stage=$_POST["start_stage"];
			if($start_stage=="Stage 5")$start_stage="Stage 5a";
			if($start_stage=="Stage 6")$start_stage="Stage 6a";
			$end_stage=$_POST["end_stage"];
			if($end_stage=="Stage 5")$end_stage="Stage 5b";
			if($end_stage=="Stage 6")$end_stage="Stage 6b";
			
			$stages=[];
			$isin=false;
			$result = mysqli_query($link,'SELECT id,stage FROM anissed_all_stages');
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_assoc($result)) {
					if($r['stage']==$start_stage)$isin=true;
					if($isin) $stages[$r['id']]=$r['stage']; 
					//Prlbème Des fois le stage est 6 et non 6a ou 6b
					if($r['stage']==$end_stage)$isin=false;

				}
	 		}
			
			$CellsByStage=array();
			foreach ($stages as $idstage => $stage){
				//echo $stage.'\n';
				$CellsByStage[$stage]=array();
				$result = mysqli_query($link,'SELECT cell FROM anissed_cells_by_gene_by_stage WHERE gene_id='.$gene_id.' AND stage='.$idstage);
				if ($result)  {
					while($r = mysqli_fetch_assoc($result)) {
						$cell=$r['cell'];
						if(!array_key_exists($cell,$CellsByStage[$stage]))
	    						array_push($CellsByStage[$stage],$cell);
	    			}
					

				}

			}
			echo json_encode($CellsByStage);	
			
		}
		else if(isset($_POST["getGenes"])){
			$cell=$_POST["getGenes"];
			$organism_id=$_POST["organism_id"];
			$stage=$_POST["stage"];
			
			$stageid=query_first('SELECT id FROM anissed_all_stages where stage="'.$stage.'"');
		
			echo query_json('select gene_id from anissed_cells_by_gene_by_stage where cell="'.$cell.'" and stage='.$stageid);
			
		}
		
		
	
	mysqli_close($link);
}

?>
