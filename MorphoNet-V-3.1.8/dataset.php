<?php
	
include 'uni-functions.php';

function getBestQuality($id_dataset)
{
	return query_first('SELECT max(quality) FROM mesh WHERE id_dataset='.$id_dataset);
}
$now=new DateTime();
if ($connected){
	if(isset($_POST["download"])){ //Query on the  Upload
		$download=intval($_POST["download"]);
		if($download==0){//download mesh type
			//echo ('SELECT id,t,link,isnull(obj),version FROM mesh WHERE id_dataset='.$_POST["id_dataset"].' AND quality='.$_POST["quality"].' ORDER BY t,id');
	 		if(!isset($_POST["os"]) || $_POST["os"]=="WebGL") 
			echo query_json_field('SELECT id,t,link,isnull(obj),version,center FROM mesh WHERE id_dataset='.$_POST["id_dataset"].' AND quality='.$_POST["quality"].' ORDER BY t,id ');
	 		if(isset($_POST["os"]) && $_POST["os"]=="Android") echo query_json_field('SELECT id,t,android,isnull(obj),version,center FROM mesh WHERE id_dataset='.$_POST["id_dataset"].' AND quality='.$_POST["quality"].' ORDER BY t,id ');
	 	}

	 	if($download==1){//Get personal dataset LIST (from Embryo Lsit only use in ANDROID version)
	 		
	 		$id_people=$_POST["id_people"];
	 		
	 		$rows = array();
		
					
			//CREATOR DATASET
			$result = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_dataset_type,rotation,translation,scale,spf,dt FROM dataset WHERE archive=0 and id_people='.$id_people.' and bundle>=-1 order by id');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$r['quality']=getBestQuality($r[0]);
					$rows[$r[0]] = $r;
				}
	 		}
	 		//LOOK IN MY GROUPS FOR THE SHARED DATASET
	 		$mygroups = query_array('SELECT id,id_group,status FROM people_group WHERE id_people='.$id_people);
	 		foreach ($mygroups  as $id_t => $group){
	 			$id_group=$group['id_group'];
	 			$groupsharing = query_array('SELECT id,id_base,how FROM sharing WHERE base="dataset" and id_group='.$id_group);
            	foreach($groupsharing as $idgs => $grpshare){
            		if(!array_key_exists($grpshare['id_base'],$rows)) {
            			$result2 = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_dataset_type,rotation,translation,scale,spf,dt  FROM dataset WHERE archive=0 and id='.$grpshare['id_base']);
            			while($r2 = mysqli_fetch_row($result2)) {
							$r2['quality']=getBestQuality($r2[0]);
							$rows[$r2[0]] = $r2;
						}	
            		}
            	}
	 		}
	 			//LOOK IN THE SHARE DATASET WITH ME (OR PUBLIC)
            $usersharing = query_array('SELECT id,id_base,how,id_who FROM sharing WHERE base="dataset" and ( id_who='.$id_people.' or id_who=0) ');
            foreach($usersharing as $idus => $usershare){
            		if(!array_key_exists($usershare['id_base'],$rows)) {
            			$result2 = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_dataset_type,date  FROM dataset WHERE archive=0 and id='.$usershare['id_base']);
            			while($r2 = mysqli_fetch_assoc($result2)) {
							$r2['quality']=getBestQuality($r2[0]);
							$rows[$r2[0]] = $r2;
						}	
            		}		
	 		}

	 		$jrows = array();
	 		foreach($rows as $ids => $row) $jrows[]=$row;
		 	
	 		echo jsonRemoveUnicodeSequences($jrows);
	 		
	 		


	 	}
	 	if($download==2){//Get dataset by time 
	 		$quality=0;
	 		if(isset($_POST['quality'])) {
		 		$quality=$_POST['quality'];
		 		if($quality=="-1")$quality=0;
		 	}
	 		echo query_first('SELECT obj FROM mesh WHERE id_dataset='.$_POST["id_dataset"].' AND t='.$_POST["t"].' AND quality='.$quality.' AND id='.$_POST['id']);
	 	}

	 	
	 	//print_r($_POST);
	 	if($download==7){//Download all Infos
		 	
	 		
		 	$result = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people FROM dataset WHERE id='.$_POST["id_dataset"].' ');
		 	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($dataset = mysqli_fetch_row($result)) {
					$infos['id']=$dataset[0];
					$infos['name']=$dataset[1];
					$infos['created']=$dataset[2];
					$infos['minTime']=$dataset[3];
					$infos['maxTime']=$dataset[4];	
				}
	 		}
	 		
	 		$fp = fopen('Infos/'.$infos['name'].'-infos.json', 'w');

	 		
		 	$allinfos['infos'] =$infos;
		 	$result = mysqli_query($link,'SELECT id,infos,type FROM correspondence WHERE id_dataset='.$_POST["id_dataset"]);
		 	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				
				while($r = mysqli_fetch_row($result)) {
					//fwrite($fp,$r[0].' '.$r[1].chr(10));
					$field=query_first('SELECT field FROM correspondence WHERE id='.$r[0].' AND id_dataset='.$_POST["id_dataset"]);
					//fwrite($fp,bzdecompress($field).chr(10));
					$allinfos[$r[1]]=bzdecompress($field);
				}
	 		}
	 		fwrite($fp, json_encode($allinfos));
	 		fclose($fp);
	 	}


	 	if($download==8){//Get DATASETYPE 
	 		echo query_json('SELECT id,type,id_parent,developmental_table FROM dataset_type ORDER BY id_parent');
	 	}

	 	if($download==9){//save current rotation and translation  
	 		echo query('UPDATE dataset SET rotation="'.mysqli_escape_string($link,$_POST["rotation"]).'",translation="'.mysqli_escape_string($link,$_POST["translation"]).'" ,scale="'.mysqli_escape_string($link,$_POST["scale"]).'"  WHERE id='.$_POST["id_dataset"]);
	 	}

	 	if($download==10){//save current rotation and translation  
	 		echo query_json('SELECT comments,link from dataset WHERE id='.$_POST["id_dataset"]);
	 	}
	 	if($download==11){//Download All Mehses in one file ...  
	 		$id_dataset=$_POST["id_dataset"];
	 		$quality=$_POST["quality"];
	 		$name=query_first('SELECT name FROM dataset WHERE id='.$id_dataset);
	 		
	 		$names=$name;
	 		if(strpos($names,'(')>0){
		 		$tab=explode('(',$names);
		 		$names=$tab[0];
		 	}
		 	$names=trim($names);
		 	$names=str_replace(" ","_",$names);
	 		$names=$names."_".$id_dataset;
	 		
	 
	 		$date = date_create();
	 		$ts=date_timestamp_get($date);
	 		system("mkdir /var/www/TEMP/".$ts);
	 		$fp = fopen("/var/www/TEMP/".$ts."/index.php", 'w');
	 		fwrite($fp, "<?php header('Location:http://".$_SERVER["SERVER_NAME"]."'); ?>");
	 		fclose($fp);
	 			
	 		
	 		if(file_exists("/var/www/OBJGZ/".$names."_q".$quality.".tar.gz")){
		 		$filename="TEMP/".$ts."/".$names."_q".$quality.".tar.gz";
	 			system("cp /var/www/OBJGZ/".$names."_q".$quality.".tar.gz /var/www/TEMP/".$ts."/");
	 		}else
	 		if(file_exists("/var/www/OBJGZ/".$names.".tar.gz")){
		 		$filename="TEMP/".$ts."/".$names.".tar.gz";
	 			system("cp /var/www/OBJGZ/".$names.".tar.gz /var/www/TEMP/".$ts."/");
	 		}
	 		echo $filename;
	 	}
	}

	else if(isset($_POST["developmental_table"])){
		$id_datasettype=$_POST["developmental_table"];
		echo query_json("SELECT period,stage,developmentaltstage,description,hpf,hatch FROM developmental_table WHERE id_datasettype=".$id_datasettype);

	}
	mysqli_close($link);
}


?>
