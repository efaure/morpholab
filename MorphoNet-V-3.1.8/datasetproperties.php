<?php

session_start();


include 'functions.php';

if(!isset($_SESSION['ID'])) header('Location:'.get_server());
if(!isset($_POST['id_dataset'])) header('Location:'.get_server());

$isAdmin=getAdmin($_SESSION["ID"]);

if ($connected){
	$id_dataset=$_POST['id_dataset'];
	$dataset = query_array('SELECT id,id_NCBI,type,name,comments,spf,dt,minTime,maxTime FROM dataset WHERE id='.$id_dataset);
	$dataset=$dataset[$id_dataset];
} else  header('Location:'.get_server());


?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
       
    <script src="jquery/jquery-2.1.0.min.js"></script>

      <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script src="Taxonomy/patternfly-bootstrap-treeview-master/src/js/bootstrap-treeview.js"></script>
    <link rel="stylesheet" href="Taxonomy/patternfly-bootstrap-treeview-master/src/css/bootstrap-treeview.css" >
   
    <script type="text/javascript" src="NCBI.js"></script>

    <link rel="stylesheet" href="css/morphonet.css" >

    <meta name="MorphoNet" content="MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching." />. 

  </head>

  <body style="text-align:left;">
    <?php include "headbar.php"; ?>
    <div style="width:80%;margin:0 auto;background-color:#DBF0FC;padding:20px">

		<div style="text-align: center;width:80%;margin:auto;margin-top:10px;margin-bottom:40px;">
			<span style="font-size:30px;color:grey;width:80%;margin:auto;">Dataset Properties</span>
		</div>
    	<div  style="text-align: center;width:100%;margin:auto;margin-top:10px;margin-bottom:40px;background-color:#DBF0FC;"  >
			<span style="font-size:25px;color:black;margin:10px;display: inline-block;vertical-align: middle;line-height: normal;"><?php echo $dataset['name']; if($dataset['minTime']!=$dataset['maxTime']){$diff=1+intval($dataset['maxTime'])-intval($dataset['minTime']);   echo ' ('.$diff.' time points)';} ?></span>
		</div>

		<div style="text-align: center;width:80%;margin:auto;margin-top:10px;height:40px">
			<span id="message" style="font-size:20px;color:red;width:80%;margin:auto;display:none">message</span>
		</div>

    			
		<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px">Name </span>
		<input type="text" id="name" style="width:400px" value="<?php echo $dataset['name'];?>" >
		<input type="image" src="images/valid.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="30px" onclick="changeName()" title="valid the new name of the dataset">
		
		</br></br>
		
		
		<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px">Description</span>
		<textarea rows="4" cols="150" id="description" style="vertical-align:middle"><?php echo $dataset['comments']; ?></textarea>
		<input type="image" src="images/valid.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="30px" onclick="changeDescription()" title="valid the description of the dataset">
		
		</br></br>
		
		
		<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px">Links</span>
		<input type="image" src="images/add.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="30px" onclick="$('#nlink').show()" title="add a new link to the dataset">
		
		<div id="nlink" style="padding:10px;margin-left:50px;display:none">
			Description <input type="text" id="desc" style="width:500px;margin-left:5px"></br>
			Link <input type="text" id="link" style="width:500px;margin-left:50px">
			<input type="image" src="images/valid.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="30px" onclick="addLink()" title="valid the new link ">
		</div>
		
		<div id="links">
		<?php $links = query_array('SELECT id,link,description FROM links WHERE id_dataset='.$id_dataset);
		foreach ($links as $key => $value){ ?>
			<div id="link_<?php echo $key;?>">
							<br><input type="image" src="images/delete.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="20px" onclick="delLink(<?php echo $key;?>)" title="delete this new link ">
			<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:10px">Description :</span><? echo $value['description']; ?></br> 
			<input type="image" src="images/web-link.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="20px" >
			<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:10px">Link :</span><?php echo $value['link']; ?></br>
			</div>
		<?php } ?>
		</div>
		
		</br></br>
		
		<?php if($dataset['minTime']!=$dataset['maxTime']){ ?>
			<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px">Start Time </span>
			<input type="text" id="spf" value="<?php echo $dataset['spf']; ?>" size="4">
			<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:0px">seconds, and delta T </span>
			<input type="text" id="dt" value="<?php echo $dataset['dt']; ?>" size="4">
			<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:0px">seconds </span>
			<input type="image" src="images/valid.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="30px" onclick="changeTime()" title="change the time properties of this dataset">
		<?php  } ?>
		</br></br>
		
		</br></br><span style="font-size:20px;color:grey">Delete this dataset</span><input type="image" src="images/delete.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="20px" onclick="deleteDataset()" title="delete this dataset"></br></br>


		<div> 
			</br></br><span style="font-size:20px;color:grey">Type : </span>
			<span id="type" style="font-size:20px;color:blue;" ><?php
				$type="";
				if($dataset['type']==0) $type="Observed dataset";
				if($dataset['type']==1) $type="Simulated dataset";
				if($dataset['type']==2) $type="Drawing dataset";
				echo $type;
				?></span>	
		
			<input type="image" src="images/microscope.png" alt="microscope" width="30" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:100px" onclick="changeType(0)" title="change type to Observed dataset" >
			<input type="image"src="images/simulated.png" alt="writing" width="30" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px"  onclick="changeType(1)"  title="change type to Simulated dataset">
			<input type="image" src="images/drawing.png" alt="drawing" width="30" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" onclick="changeType(2)"  title="change type to Drawing dataset">
			
			
		</div>			

	
		</br></br><span style="font-size:20px;color:grey">Categorie : </span>
		<span id="categorie" style="font-size:20px;color:blue" ></span>

	
	<div style="width:80%;margin:auto;margin-top:20px">
          <div style="margin-bottom: 10px">
             <input type="input"  id="input-search" placeholder="Type to search in the tree..." style="height:30px;width:200px" value="" >
              <input type="image" width="30px" height="30px" style="vertical-align:bottom;margin-left:10px;margin-right:10px" src="images/search.png" onclick="updateTree()" title="search this text in the taxonomy">
              
             
            
              <span id="waitsearch" style="font-size:20px;width:100%;color:grey;margin:50px;margin-left:50px;display:none">search ... </span>
              <input type="input"  id="nb-search" placeholder="Number of search" style="height:30px;width:50px;margin-left:100px" value="10" >
             <span style="font-size:20px;width:100%;color:grey;margin:5px;margin-left:5px"> answers </span>
             <input type="image" width="30px" height="30px" style="vertical-align:bottom;margin-left:10px;margin-right:10px" src="images/oload.png" onclick="resetTree()" title="reset this search">
         	
         </div>

       
          <div id="treesearch" class="" ></div>
          <div id="treeview" class=""></div>
    </div>

    </div>
    <script type="text/javascript">
    	start_Browser();//START NCBI Taonymu browser
		id_dataset=<?php echo $id_dataset;?>;

		function get_Categorie(id_NCBI){
           	$.post("managedataset.php", {
        		id_dataset:id_dataset,
    			getCategorie:id_NCBI
			}, function(data,status){ $('#categorie').html(data); });
        }
        get_Categorie(<?php echo $dataset['id_NCBI'];?>); //On load

 		function change_Categorie(cat){
            if(confirm('Are you sure to change for this categorie "'+cat.text+'" ?')){
                 $.post("managedataset.php", {
                		id_dataset:id_dataset,
            			changeCategorie:cat.id
      				}, function(data,status){
      					message('Categorie changed');
      					$('#categorie').html(data);
            		});
             }else cat.state.selected=false;
             	
        }

 		function message(messs){
 			$('#message').html(messs);
 			$('#message').show();
 		}
 		function changeName(){
	 		name=$('#name').val();
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				changeName:name
			}, function(data,status){message('Name changed');} );
 		}
 		
 		function changeDescription(){
	 		description=$('#description').val();
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				changeDescription:description
			}, function(data,status){message('Description changed');} );
 		}
 		
 		function addLink(){
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				addLink:$('#desc').val(),
  				link:$('#link').val()
			}, function(data,status){
				message('Link added ');
				$('#links').append('<div id="link_'+data+'"><br>'+
					'<input type="image" src="images/delete.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="20px" onclick="delLink('+data+')">'+
					'<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:10px">Description :</span>'+$('#desc').val()+'</br>'+ 
					'<input type="image" src="images/web-link.png" style="display: inline-block;vertical-align: middle;line-height: normal;margin-left:10px" width="20px" >'+
					'<span style="font-size:20px;color:grey;display: inline-block;vertical-align: middle;line-height: normal;margin-right:10px;margin-left:10px">Link :</span>'+$('#link').val()+'</br>');
				} );
	 		$('#nlink').hide();
 		}
 		
 		function delLink(id_link){
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				delLink:id_link
			}, function(data,status){
				message('Link deleted');
				$('#link_'+id_link).remove();
			} );
 		}
 		
 		function deleteDataset(){
	 		if(confirm("Are you sure you want to delete this dataset ???"))
	 		{
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				deleted:1
			}, function(data,status){window.location="listdatasets.php"} );
			}
 		}
 		function changeTime(){
	 		spf=$('#spf').val();
	 		dt=$('#dt').val();
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				spf:spf,
  				dt:dt
			}, function(data,status){message('Time changed');} );
	 	}

	 	function changeType(type){
	 		$.post("managedataset.php", {
  				id_dataset:id_dataset,
  				changeType:type
			}, function(data,status){
				var strtype="";
				if(type==0) strtype="Observed dataset";
				if(type==1) strtype="Simulated dataset";
				if(type==2) strtype="Drawing dataset";
				$("#type").html(strtype);
			}
			);
 		}

    </script>
  </body>
</html>
