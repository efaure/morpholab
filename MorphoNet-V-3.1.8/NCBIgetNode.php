<?php
session_start();

include 'functions.php';


$nbsearch=10;
if(isset($_POST['nbsearch']))$nbsearch=$_POST['nbsearch'];



if(isset($_POST['id_root'])){
  $id_root=$_POST['id_root'];
  $OFFSET="";
  if(isset($_POST['nb_nodes'])) $OFFSET=" OFFSET ".$_POST['nb_nodes'];
  $query='SELECT NCBI_childs.id_child,NCBI_tree.nb_childs,NCBI_tree.name FROM NCBI_childs JOIN NCBI_tree ON NCBI_tree.id=NCBI_childs.id_child  where NCBI_childs.id_parent='.$id_root.' LIMIT '.$nbsearch.' '.$OFFSET;
  $result=@mysqli_query($link,$query) or die("ERROR : DATABASE CONNECTION FAILED!");
  $ss="";
  if (!$result) echo ('Error : ' . mysqli_error($link));
  else {
    while($r = mysqli_fetch_row($result)) {
      if($r[0]!=$id_root){
        $ss=$ss.$r[0].':'.$r[1].':'.$r[2].chr(10);
      }
    }
  }
  echo $ss;
}


else if(isset($_POST['search'])){
  $TreeParent=array();
  $TreeNbChilds=array();
  $TreeName=array();

  function addHistory($id_child){
      //echo "add history for ".$id_child.chr(10);
      global $TreeParent,$TreeName,$TreeNbChilds;;
      $query='SELECT id_parent,nb_childs,name FROM NCBI_tree WHERE id='.$id_child;
      $result = @mysqli_query($link,$query) or die("ERROR : DATABASE CONNECTION FAILED!");
      if (!$result) echo ('Error : ' . mysqli_error($link));
      else {
        while($r = mysqli_fetch_row($result)) {
          
          if (!array_key_exists($id_child,$TreeParent)) {
              //echo "add in ".$id_parent.chr(10);
              $id_parent=intval($r[0]);
              $nb_childs=intval($r[1]);
              $name=$r[2];
              $TreeParent[$id_child]=$id_parent;
              $TreeNbChilds[$id_child]=$nb_childs;
              $TreeName[$id_child]=$name;
              addHistory($id_parent);
              
          }//else echo  " -- already in ".$id_parent.chr(10);
          
        }
      }
  }

  $query='SELECT id,id_parent,nb_childs,name FROM NCBI_tree where name like "%'.$_POST['search'].'%" LIMIT '.$nbsearch;
  $result =@mysqli_query($link,$query) or die("ERROR : DATABASE CONNECTION FAILED!");
  if (!$result) echo ('Error : ' . mysqli_error($link));
  else {
    while($r = mysqli_fetch_row($result)) {
      $id=intval($r[0]);
      $id_parent=intval($r[1]);
      $nb_childs=intval($r[2]);
      $TreeParent[$id]=$id_parent;
      $TreeName[$id]=$r[3];
      $TreeNbChilds[$id]=$nb_childs;
      addHistory($id_parent);
      //$ss=$ss.$r[0].':'.$r[1].':'.$r[2].chr(10);
    }
  }

  $ss="";
  foreach ($TreeParent as $id => $value) {
      $ss=$ss.$id.':'.$TreeParent[$id].':'.$TreeNbChilds[$id].':'.$TreeName[$id].chr(10);
  }
  echo $ss;

}

?>

