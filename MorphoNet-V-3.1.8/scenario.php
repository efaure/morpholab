<?php
	
include 'uni-functions.php';

if ($connected){
	if(isset($_POST["scenario"])){ //Query on the  Upload
		$scenario=intval($_POST["scenario"]);
		if($scenario==1){//Get scenario LIST
			$rows = array();
			$result = mysqli_query($link,'SELECT id,name,date,id_people from scenario WHERE  id_people='.$_POST["id_people"].' and id_dataset='.$_POST["id_dataset"]);
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$rows[] = $r;
				}
	 		}
	 		//LOOK IN THE SHARE DATASET
            $result = mysqli_query($link,'SELECT id_base,id_people FROM share WHERE base="scenario" and id_people!='.$_POST["id_people"].' and ( id_who='.$_POST["id_people"].' or id_who=0)');
            if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$result2 = mysqli_query($link,'SELECT id,name,date,id_people from scenario WHERE id='.$r[0].' and id_people='.$r[1].' and id_dataset='.$_POST["id_dataset"]);
					while($r2 = mysqli_fetch_row($result2)) {
						$rows[] = $r2;
					}	
				}
	 		}
	 		print  jsonRemoveUnicodeSequences($rows);
		}
		if($scenario==2){// Get Actions
	 			echo query_json('SELECT idx,time,position,rotation,scale,framerate,interpolate from action  WHERE id_scenario='.$_POST["id_scenario"].' order by idx');
	 	}

	 	if($scenario==3){//Create a new scenario
	 			$now=new DateTime();
	 			$id_scenario=query_id('INSERT INTO scenario (name,id_dataset,id_people,date) VALUES ("'.$_POST["name"].'",'.$_POST["id_dataset"].','.$_POST["id_people"].',"'.$now->format('Y-m-d h:s:i').'")');
	 			$file=$_FILES['actions'];
	 			$lines=explode("\n",file_get_contents($file['tmp_name']));
	 			for($i=0;$i<count($lines);$i++){
	 				if(strlen($lines[$i])>10){
	 					$action=explode(":",$lines[$i]);
	 					echo query('INSERT INTO action (id_scenario,idx,time,position,rotation,scale,framerate,interpolate) VALUES ('.$id_scenario.','.$action[0].','.$action[1].',"'.$action[2].'","'.$action[3].'","'.$action[4].'",'.$action[5].','.strtolower($action[6]).')');
	 				}
	 			}
	 			echo $id_scenario;
	 	}

	 	if($scenario==4){//Update a scenario
	 			$id_scenario=$_POST["id_scenario"];
	 			echo query('UPDATE scenario SET name="'.$_POST["name"].'"  WHERE ID='.$id_scenario);
	 			echo query('DELETE FROM action WHERE id_scenario='.$id_scenario);
	 			$file=$_FILES['actions'];
	 			$lines=explode("\n",file_get_contents($file['tmp_name']));
	 			for($i=0;$i<count($lines);$i++){
	 				if(strlen($lines[$i])>10){
	 					$action=explode(":",$lines[$i]);
	 					echo query('INSERT INTO action (id_scenario,idx,time,position,rotation,scale,framerate,interpolate) VALUES ('.$id_scenario.','.$action[0].','.$action[1].',"'.$action[2].'","'.$action[3].'","'.$action[4].'",'.$action[5].','.strtolower($action[6]).')');
	 				}
	 			}
	 			echo $id_scenario;
	 	}
	 	if($scenario==5){//Delete a scenario
	 		echo query('DELETE FROM scenario WHERE id='.$_POST["id_scenario"]);
	 		echo query('DELETE FROM action WHERE id_scenario='.$_POST["id_scenario"]);
	 		echo query('DELETE FROM share WHERE base="scenario" and id_base='.$_POST["id_scenario"].' and id_people='.$_POST["id_people"]);
	
	 	}

	}
	mysqli_close($link);
}

?>
