<?php


include 'functions.php';
$now=new DateTime();


if ($connected){
	$id_dataset=$_POST['id_dataset'];

	
	if(isset($_POST['changeCategorie'])){
		$id_NCBI=$_POST['changeCategorie'];
		query('UPDATE dataset SET  id_NCBI='.$id_NCBI.' WHERE  id='.$id_dataset);
		$_POST['getCategorie']=$id_NCBI;
	}
	if(isset($_POST['getCategorie'])){
		///CATEGORIES : Look For Parent
		$id_NCBI=$_POST['getCategorie'];
		$parent=[];
		$n_id_NCBI=$id_NCBI;
		while($n_id_NCBI!=1){
			$result = mysqli_query($link,'SELECT id,id_parent,name FROM NCBI_tree where id='.$n_id_NCBI);
		  	if (!$result) echo ('Error : ' . mysqli_error());
		  	else {
		    	while($r = mysqli_fetch_row($result)) {
		      		array_push($parent,$r); 
		      		$n_id_NCBI=$r[1];
		      	}
		    }
		  }
		$r[0]='1'; $r[1]='1'; $r[2]='Root'; 
		array_push($parent,$r); 
		for($i=count($parent)-1;$i>=0;$i--) {
				echo $parent[$i][2];
				if($i>0) echo " : ";
		}
	}


	if(isset($_POST['changeName'])){
		$name=$_POST['changeName'];
		query('UPDATE dataset SET  name="'.$name.'" WHERE  id='.$id_dataset);
	}
	
	if(isset($_POST['changeDescription'])){
		$comments=$_POST['changeDescription'];
		query('UPDATE dataset SET  comments="'.$comments.'" WHERE  id='.$id_dataset);
	}
	
	
	if(isset($_POST['addLink'])){
		$description=$_POST['addLink'];
		$link=$_POST['link'];
		query('INSERT INTO links (id_dataset,description,link)  VALUES ('.$id_dataset.',"'.mysqli_escape_string($link,$description).'","'.mysqli_escape_string($link,$link).'")');
	}
	
	if(isset($_POST['delLink'])){
		$id_link=$_POST['delLink'];
		query('DELETE FROM  links WHERE id_dataset='.$id_dataset.' AND id='.$id_link);
	}
	if(isset($_POST['spf'])){
		$spf=$_POST['spf'];
		$dt=$_POST['dt'];
		query('UPDATE dataset SET  spf='.$spf.', dt='.$dt.' WHERE  id='.$id_dataset);
	}
	
	
	
	if(isset($_POST['deleted'])){
		query('DELETE FROM  correspondence WHERE id_dataset='.$id_dataset);
		query('DELETE FROM  mesh WHERE id_dataset='.$id_dataset);
		query('DELETE FROM  sharing WHERE base="dataset" and id_base='.$id_dataset);
		query('DELETE FROM  dataset WHERE id='.$id_dataset);
		
		$bundlePath="/var/www/Bundle/WebGL/DATASET_".$id_dataset;
		if(is_dir($bundlePath)) system("rm -rf ".$bundlePath); 
		
		$bundlePath="/var/www/Bundle/Android/DATASET_".$id_dataset;
		if(is_dir($bundlePath)) system("rm -rf ".$bundlePath); 
	}
	
 }






mysqli_close($link);
?>
