 <link rel="stylesheet" href="css/menu.css">
 
<div style=" display: table; margin-right: auto;  margin-left: auto; margin-top:10px; margin-bottom:auto;">
    <input type="image" width="150px"   src="images/logo.png" onclick="index()" />
   
   <?php if(isset($version)) { ?> <input type="image" width="30px" height="30px"  src="images/fullscreen.png"  title="Full Screen Mode"  style="margin-left:50px;margin-top:50px;position: relative; float:right" onclick="gameInstance.SetFullscreen(1)"/> <?php } ?>

  
    <?php if(isset($_SESSION['ID'])) { //DECONNECTION ?> 
    	 <input type="image" width="30px" height="30px" title="disconnect from MorphoNet"   src="images/shut-down-icon.png" style="margin-left:50px;margin-top:50px;position: relative; float:right"  onclick="disconnect()"/>
    <?php } else { //CONNECTION  ?>
    	<input type="image" width="30px" height="30px" title="connect to MorphoNet"   src="images/man-user.png" style="margin-left:50px;margin-top:50px;position: relative; float:right"  onclick="connect()"/>
     <?php }  ?>
     
      
	<input type="image" width="30px" height="30px"  title="open MorphoNet Help page" src="images/faq.png" style="margin-left:50px;margin-top:50px;position: relative; float:right" onclick="showHelp()"/>
    
    
    <?php if(isset($_SESSION['ID'])) {  $isAdmin=getAdmin($_SESSION['ID']); ?>  <input type="image" width="30px" height="30px"  title="send us some feedback !" src="images/chat.png" style="margin-left:50px;margin-top:50px;position: relative; float:right" onclick="feedback()"/>
		 <ul id="menu-demo2" style="margin-top:50px;position: relative; float:right">
			<li style="background-color: white;border : 0px solid white;"> <input type="image" width="35px" height="35px"  title="Settings" src="images/parameters.png" style="margin-left:50px;" />
				<ul>
					<li><input type="image" width="30px" height="30px"  title="add a new dataset" src="images/morphoplus.png"  style="padding:10px" onclick="newdataset()"/> </li>
					<li>  <input type="image" width="35px" height="35px"  title="Manage Users on MorphoNet" src="images/man-user.png" style="padding:10px"  onclick="users()"/> </li>
					<li> <input type="image" width="35px" height="35px"  title="Manage Groups on MorphoNet" src="images/users-group.png"  style="padding:10px"  onclick="groups()"/> </li>
					<?php if($isAdmin) { ?> <li>  <input type="image" width="30px" height="30px"  title="view all feedbacks" src="images/debug.png"style="padding:10px" onclick="debug()"/> </li><?php } ?> 
				</ul>
			</li>
	     </ul>
	 <?php } ?>
     
    <input type="image" width="35px" height="35px"  title="show the list of available datasets" src="images/list-with-dots.png" style="margin-left:50px;margin-top:50px;position: relative; float:right"  onclick="backToDataset()"/>

</div>


<script type="text/javascript">
    function index(){ window.location="index.php";   }
    function showHelp(){ window.open("HELP/HelpGeneral.html");  }
    function disconnect(){  window.location = "index.php?todo=disconnect";  };
    function connect(){  window.location = "index.php?todo=connect";  };
    function backToDataset(){  window.location = "listdatasets.php";}
    function debug(){  window.location = "debug.php";}
    function users(){window.location = "manageuser.php"; };
    function groups(){window.location = "managegroup.php"; };
    function api(){window.location = "HELP/HelpAPI.php"; };
    function cancelfeedback(){ $("#feedback").hide(); }
    
	 <?php if(isset($version)) { ?>
	 	function feedback(){  window.open("feedback.php","feedback","menubar=no, status=no,location=no, scrollbars=no, menubar=no, width=400, height=200");}
	 <?php  }else if(isset( $_SESSION['ID'])){ ?> 
	 	 function feedback(){  $("#feedback").show();  }
	     function send(){
	        comments=$('#comments').val();
	        if(comments!=""){
	            $.post("changeUsers.php", { comments: comments,hash:"ascidie12:",ID:<?php echo $_SESSION['ID'];?> }, function(data,status){$("#feedback").hide();alert('Thanks !');} );
	        }else $("#feedback").hide();
		}
	 <?php  } ?> 
	function newdataset(){window.location = "newdataset.php"; };
</script>