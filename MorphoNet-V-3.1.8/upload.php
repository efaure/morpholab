<?php
include 'uni-functions.php';

if ($connected){
	if(isset($_POST["upload"])){ //Query on the  Upload
		$upload=intval($_POST["upload"]);
		
	 	if($upload==1){//New Upload
	 		$now=new DateTime();
	 		echo query_id('INSERT INTO upload (id_people, id_dataset, filename, date, type,zip,nbCell,minV,maxV) VALUES ('.$_POST["id_people"].','.$_POST["id_dataset"].',"'.$_POST["filename"].'","'.$now->format('Y-m-d h:s:i').'",'.$_POST["type"].',1,"'.$_POST["nbCell"].'","'.$_POST["minV"].'","'.$_POST["maxV"].'")');
	 	}
	 	else if($upload==2){
	 		$file=$_FILES['data'];
	 		echo query('INSERT INTO upload_time (id_upload,t,field) VALUES ('.$_POST["id_upload"].','.$_POST["time"].',"'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'")');
	 	}
	 	else if($upload==3){//Get Upload LIST
			$rows = array();
			$result = mysqli_query($link,'SELECT id,filename,date,type,nbCell,minV,maxV,id_people,zip from upload WHERE  id_people='.$_POST["id_people"].' and id_dataset='.$_POST["id_dataset"]);
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$rows[] = $r;
				}
	 		}
	 		//LOOK IN THE SHARE DATASET
            $result = mysqli_query($link,'SELECT id_base,id_people FROM share WHERE base="upload" and id_people!='.$_POST["id_people"].' and ( id_who='.$_POST["id_people"].' or id_who=0)');
           if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$result2 = mysqli_query($link,'SELECT id,filename,date,type,nbCell,minV,maxV,id_people,zip from upload WHERE id='.$r[0].' and id_people='.$r[1].' and id_dataset='.$_POST["id_dataset"]);
					while($r2 = mysqli_fetch_row($result2)) {
						$rows[] = $r2;
					}	
				}
				print  jsonRemoveUnicodeSequences($rows);
	 		}
		}

	 	else if($upload==4){// Delete dataset 
	 		 echo query('DELETE FROM upload  WHERE id='.$_POST["id_upload"]);
		     echo query('DELETE FROM upload_time WHERE id_upload='.$_POST["id_upload"]);
		     echo query('DELETE FROM share WHERE base="upload" and id_base='.$_POST["id_upload"]);
		}
		else if($upload==5){//Get Upload Dataset time step + step
			echo query_first('SELECT field from upload_time  WHERE id_upload='.$_POST["id_upload"].' AND t='.$_POST["t"]);
	 	}
	}
	mysqli_close($link);
}

?>
