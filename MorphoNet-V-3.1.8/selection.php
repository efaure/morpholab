<?php
	
include 'uni-functions.php';

if ($connected){
	if(isset($_POST["selection"])){ //Query on the  Upload
		$selection=intval($_POST["selection"]);
		if($selection==0){ //List all selections for this user
			$rows = array();
			$result = mysqli_query($link,'SELECT id,name,date,id_people FROM selection WHERE id_people='.$_POST["id_people"].' and id_dataset='.$_POST["id_dataset"]);
     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$rows[] = $r;
				}
	 		}
	 		//LOOK IN THE SHARE SELECTION
            $result = mysqli_query($link,'SELECT id_base,id_people FROM share WHERE base="selection" and id_people!='.$_POST["id_people"].' and ( id_who='.$_POST["id_people"].' or id_who=0)');
            if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					$result2 = mysqli_query($link,'SELECT id,name,date,id_people  FROM selection WHERE id='.$r[0].' and id_people='.$r[1].' and id_dataset='.$_POST["id_dataset"]);
					while($r2 = mysqli_fetch_row($result2)) {
						$rows[] = $r2;
					}	
				}
				print  jsonRemoveUnicodeSequences($rows);
	 		}
		}
	 	if($selection==1){//Save a selection
	 		$now=new DateTime();
	 		$file=$_FILES['selection_txt'];
	 		echo query('INSERT INTO selection (id_people,name,date,cells,id_dataset) VALUES ('.$_POST["id_people"].',"'.$_POST["name"].'","'.$now->format('Y-m-d h:s:i').'","'.addslashes(file_get_contents($file['tmp_name'])).'",'.$_POST["id_dataset"].')');
	 	}
	 	if($selection==2){//Get the cells for one selection by time 
	 		echo query_first('SELECT cells FROM selection WHERE id='.$_POST["id_selection"]);
	 	}
	 	if($selection==3){//Delete a selction
	 		echo query('DELETE FROM selection WHERE id='.$_POST["id_selection"]);
	 		echo query('DELETE FROM share WHERE base="selection" and id_base='.$_POST["id_selection"].' and id_people='.$_POST["id_people"]);
	 	}
	}
	mysqli_close($link);
}

?>
