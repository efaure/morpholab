<?php

include 'functions.php';

session_start();


$isAdmin=getAdmin($_SESSION["ID"]);

if(isset($_POST['editname'])){
	$name=$_POST['editname'];
	$id=$_POST['id'];
	query('UPDATE dataset_type set type="'.$name.'" WHERE id='.$id);
	
}
if(isset($_POST['deletec'])){
	$id=$_POST['deletec'];
	query('DELETE FROM dataset_type  WHERE id='.$id);
	
}
if(isset($_POST['create'])){
	$id_parent=$_POST['create'];
	$name=$_POST['name'];
	query('INSERT INTO  dataset_type (type,id_parent) VALUES ("'.$name.'",'.$id_parent.')');
	
}

else{ //FOR NORMAL ACCES
function type($id_parent,$param){
	$result = mysqli_query($link,'select id,type from dataset_type where id_parent='.$id_parent);
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else {
		echo '<ul>';
			while($r = mysqli_fetch_assoc($result)){
				echo '<li>';
				if($param) {
					echo $r['type'];
					echo '<img src="images/delete.png" with="13px" height="13px" style="margin-left:10px" onclick="removeType('.$r['id'].')"/>';
				}else echo '<a href="#" onclick="changeType('.$r['id'].')" >'.$r['type'].'</a>';
				type($r['id'],$param);
				echo '</li>';
			}
			if($param) echo '<li data-jstree=\'{"icon":"images/adds.png"}\' onclick="addType('.$id_parent.')">new</li>';
			echo "</ul>";
	}
}

	
?>
		
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
    
  </head>

  <body>
    <?php include "headbar.php"; ?>


    <div style="width:600px;text-align: center;margin:auto">
      <span style="font-size:30px;color:grey;margin-bottom:20px">Manage dataset organization</span>
       <input type="image" width="20px" src="images/add.png" style="margin-left:10px" onclick="addi(0)">
       <div id="cat_0"></div>
	       

     <?php
	     //SHOW LIST OF CATEGOTIES
		$types = query_array('SELECT id,type,id_parent,developmental_table FROM dataset_type ORDER BY type ');
		//COunt Nbb of dataset for each type
		$result=mysqli_query($link,'SELECT id_dataset_type,count(id) FROM dataset GROUP BY id_dataset_type');
		if (!$result) echo UTF('Error : ' . mysqli_error($link));
 		else {
			while($r = mysqli_fetch_assoc($result)) {
				$types[$r['id_dataset_type']]['nb']=$r['count(id)'];
			}
 		}
 		//Now Recursivlity we added the number 
 		function addNb($type){
	 		global $types;
	 		$n=0;
	 		if(isset($type['nb']))
	 		$n=$type['nb'];
	 		foreach ($types as $key => $value)
				if($value['id_parent']==$type['id']) 
					$n+=addNb($value);
			$types[$type['id']]['nb']=$n;
			return $n;
		}
 		foreach ($types as $key => $value)
			if($value['id_parent']=='0') 
				addNb($value);

 		//print_r($types);
		
		function drawType($dataset,$types,$type){
			global $isAdmin;
			echo '<div id="cat_'.$type['id'].'" style=" text-align: left; margin-left:20px; margin-top:10px" >';
			//echo '<input type="radio" name="categorie" value="'.$type['id'].'" ';
			//if($dataset['id_dataset_type']==$type['id']) echo " checked ";
			//echo ' onclick="changeCategorie('.$type['id'].')">';
			if($isAdmin){ echo '<input type="text" id="name_'.$type['id'].'" style="color:grey;font-size:20px" onchange="editName('.$type['id'].')" value="'.$type['type'].'" >';}
			else { echo '<span  style="color:grey;font-size:20px;" >'.$type['type'].'</span>';}
			if($type['nb']==0) echo '<input type="image" width="20px" style="margin-left:10px" src="images/delete.png" onclick="deletec('.$type['id'].')">';
			echo '<input type="image" width="20px" src="images/add.png" style="margin-left:10px" onclick="addi('.$type['id'].')">';
			
			echo '<span> ( '.$type['nb'].' datasets )</span>';
			
			
			foreach ($types as $key => $value)
				if($value['id_parent']==$type['id']) 
					drawType($dataset,$types,$value);
			echo '</div>';
			
		}
						 					
		
		//We draw the type starting from parent 0
		foreach ($types as $key => $value)
			if($value['id_parent']=='0') 
				drawType($dataset,$types,$value);
		echo '<div  id="new"></div>';
		

	?>
    </div>
	
	 	
	 		
	 	
	  <?php include "footer.php"; ?> 
		
		<script type="text/javascript">
			function editName(idc){
				name=$("#name_"+idc).val();
				$.post("managecategories.php", { editname: name,id:idc}, function(data,status){  } );
				
			}
			function deletec(idc){
				if(confirm("Are you sure ?"))
				$.post("managecategories.php", { deletec:idc}, function(data,status){ window.location = "managecategories.php"; } );
			}
			function addi(idc){
				$('#new').remove();
				$('#cat_'+idc).append('<div  id="new" style=" text-align: left; margin-left:20px; margin-top:0px" >'
				+'<input type="text" id="new_name_'+idc+'" style="margin-left:0px;color:grey;font-size:20px" >'
				+'<input type="image" width="25px" src="images/valid.png" style="margin-left:10px" onclick="valid('+idc+')">'
				+'</div>');
			}
			function valid(idc){
				newname=$('#new_name_'+idc).val();
				$.post("managecategories.php", { create:idc,name:newname}, function(data,status){  window.location = "managecategories.php";} );
				
			}
				
		</script>
	</body>
</html>

<?php } ?>