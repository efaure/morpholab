<?php

include 'functions.php';

session_start();

?>
		
		
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
     <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
     <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
     <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
  </head>
  <body style="background-color:#DBF0FC;">
	    <div id="feedback" style="width:100%;height:100%;background-color:#DBF0FC">
				<textarea id="comments" name="comments" style="width:90%;" rows="11" placeholder="Send us some feedback !"></textarea>
				<input type="image" width="35px" height="35px"  title="Manage Categories on MorphoNet" src="images/send-button.png" style="margin-left:25px;position: relative; float:bottom" onclick="send()"/>
     </div>
			<script type="text/javascript">
			function send(){
				comments=$('#comments').val();
				if(comments!=""){
					$.post("changeUsers.php", { comments: comments,hash:"34morpho:",ID:<?php echo $_SESSION['ID'];?> }, function(data,status){alert('Thanks !');window.close()} );
				}
    		}
			</script>
	</body>
</html>