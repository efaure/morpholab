<?php

if (!function_exists('mymkdir')){
function mymkdir($path){ if(!is_dir($path))  mkdir($path);}
}

if (!function_exists('jsonRemoveUnicodeSequences')) {
function jsonRemoveUnicodeSequences($struct)
{
	return preg_replace_callback(
				"/\\\\u([a-f0-9]{4})/",
				function ($m) {
					return iconv('UCS-4LE', 'UTF-8', pack('V', hexdec('U' . $m[1])));
				},
				json_encode($struct)
	);
}
}

if (!function_exists('UTF'))  {
function UTF($struct) {
	return preg_replace_callback(
				"/\\\\u([a-f0-9]{4})/",
				function ($m) {
					return iconv('UCS-4LE', 'UTF-8', pack('V', hexdec('U' . $m[1])));
				},
				$struct
	);
}
}

if (!function_exists('get_server'))  {
function get_server(){
	//return $_SERVER['HTTP_HOST'];
	return "index.php";
}
}

if (!function_exists('anti_injection_login_senha'))  {
function anti_injection_login_senha($sql, $formUse = true)
	{
	#$sql = preg_replace_callback("/(from|select|insert|delete|where|drop table|show tables|,|'|#|\*|--|\\\\)/i","",$sql);
	$sql = trim($sql);
	$sql = strip_tags($sql);
	if(!$formUse || !get_magic_quotes_gpc())
	  $sql = addslashes($sql);
	  $sql = md5(trim($sql));
	return $sql;
	}
	}
	
	
if (!function_exists('anti_injection_login'))  {
function anti_injection_login($sql, $formUse = true)
{
	#$sql = preg_replace_callback("/(from|select|insert|delete|where|drop table|show tables|,|'|#|\*|--|\\\\)/i","",$sql);
	$sql = trim($sql);
	$sql = strip_tags($sql);
	if(!$formUse || !get_magic_quotes_gpc())
	  $sql = addslashes($sql);
	return $sql;
}
}


if (!function_exists('getSizeByDataset'))  {
function getSizeByDataset($id_dataset){
	return query_first('SELECT sum(size) FROM mesh WHERE id_dataset='.$id_dataset);
}
}


if (!function_exists('getSizeByPeople'))  {
function getSizeByPeople($id_people){
	global $link;
	$size=0;
	$result = mysqli_query($link,'SELECT id FROM dataset where id_people='.$id_people);
  	if (!$result) echo ('Error : ' . mysqli_error($link));
  	else {
    	while($r = mysqli_fetch_row($result)) {
	    	$size+=getSizeByDataset($r[0]); 			      	
      	}
    }
    return $size;
}
}


if (!function_exists('getQuotaByPeople'))  {
function getQuotaByPeople($id_people){
	return query_first('SELECT quota from people where id='.$id_people);
}	
}


if (!function_exists('checkQuota'))  {
function checkQuota($id_people){	    
	$quota=getQuotaByPeople($id_people);
	$size=getSizeByPeople($id_people);
	if($size>$quota){ echo "You exceed your quota of ".formatSizeUnits($quota);return false;}
	return true;
}
}

			
			
if (!function_exists('formatSizeUnits'))  {		
function formatSizeUnits($bytes)
 {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}
}


if (!function_exists('my_query'))  {
function my_query($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	return $result;
}
}


if (!function_exists('query'))  {
function query($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	return "";
}
}


if (!function_exists('query_array'))  {
function query_array($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	$rows = array();
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	else {
		while($r = mysqli_fetch_assoc($result)) {
			if(sizeof($r)==2) $rows[$r['id']] = $r['name'];
			else $rows[$r['id']]=$r;
		}
	}
	return $rows;
}
}


if (!function_exists('query_id'))  {
function query_id($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	else return mysqli_insert_id($link);
}
}


if (!function_exists('query_first')) {
function query_first($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	else {
		$row = mysqli_fetch_row($result);
		return $row[0];
	}
	return "";
}
}

if (!function_exists('query_json'))  {
function query_json($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' . mysqli_error($link));
	else {
		$rows = array();
		while($r = mysqli_fetch_row($result)) {
			$rows[] = $r;
		}
		return  jsonRemoveUnicodeSequences($rows);
	}
	return "";
}
}


if (!function_exists('query_json_field'))  {
function query_json_field($mquery){
	global $link;
	$result = mysqli_query($link,$mquery);
	if (!$result) return UTF('Error : ' .mysqli_error($link));
	else {
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		return  jsonRemoveUnicodeSequences($rows);
	}
	return "";
}
}


if (!function_exists('countQuery'))  {
function countQuery($mquery){
	global $link;
	return mysqli_num_rows(mysqli_query($link,$mquery));
}
}


if (!function_exists('getAdmin'))  {
function getAdmin($idtest){ //CHECK IF CURRENT USER IS ADMIN
	$isAdmin=0;
	$id_group_admin=query_first('SELECT id FROM groups WHERE name="admin"');
	$id_admin=query_first('SELECT id FROM people_group WHERE id_people='.$idtest.' AND id_group='.$id_group_admin.' AND status="manager"');
	if($id_admin!="") $isAdmin=1;
	return $isAdmin;
}
}



#Return a object Cell from a string
if (!function_exists('getCell'))  {
function getCell($cellst){
	$c=array("t"  => 0,"id"  => "-1", "ch"  => 0);
	$tab = explode(",", $cellst);
	if(count($tab)==1)$c["id"]=$cellst; //ID ONLY
	if(count($tab)==2){//T,ID 
		$c["t"]=intval($tab[0]); 
		$c["id"]=$tab[1]; 
	}	
	if(count($tab)==3){//T,ID,CH
		$c["t"]=intval($tab[0]); 
		$c["id"]=$tab[1];
		$c["ch"]=$tab[2]; 
	}
	return $c;	
}
}


if (!function_exists('getDatasetById'))  {
function getDatasetById($id_people,$id_dataset){
	global $link;
	
	$rows = array();
				
	//CREATOR DATASET
	$result = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type FROM dataset WHERE archive=0 and id_people='.$id_people.' and bundle>=-1 AND id='.$id_dataset);
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
		else {
		while($r = mysqli_fetch_row($result)) {
			if(!array_key_exists($r[0],$rows))  $rows[$r[0]] = $r;

		}
		}
		//LOOK IN MY GROUPS FOR THE SHARED DATASET
		$mygroups = query_array('SELECT id,id_group,status FROM people_group WHERE id_people='.$id_people);
		foreach ($mygroups  as $id_t => $group){
			$id_group=$group['id_group'];
			$groupsharing = query_array('SELECT id,id_base,how FROM sharing WHERE base="dataset" and id_group='.$id_group);
    	foreach($groupsharing as $idgs => $grpshare){
    		if($grpshare['id_base']==$id_dataset) {
    			$result2 = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type  FROM dataset WHERE archive=0 and id='.$grpshare['id_base']);
    			while($r2 = mysqli_fetch_row($result2)) {
					$rows[$r2[0]] = $r2;
				}	
    		}
    	}
		}
		//LOOK IN THE SHARE DATASET WITH ME (OR PUBLIC)
    $usersharing = query_array('SELECT id,id_base,how,id_who FROM sharing WHERE base="dataset" and ( id_who='.$id_people.' or id_who=0) ');
    foreach($usersharing as $idus => $usershare){
    		if($usershare['id_base']==$id_dataset) {
    			$result2 = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type   FROM dataset WHERE archive=0 and id='.$usershare['id_base']);
    			while($r2 = mysqli_fetch_row($result2)) {
					$rows[$r2[0]] = $r2;
				}	
    		}
		}

		$jrows = array();
		foreach($rows as $ids => $row) $jrows[]=$row;
 	
		return $jrows ;
}
}



?>
