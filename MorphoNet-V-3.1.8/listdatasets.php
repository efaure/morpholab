<?php

session_start();


include 'functions.php';

$isAdmin=0;
$id_people=0;
if(isset($_SESSION['ID'])) {
	$id_people=$_SESSION["ID"];
	$isAdmin=getAdmin($id_people);
}




function getNbQuality($id_dataset)
{
	return query_first('SELECT count(DISTINCT quality) FROM mesh WHERE id_dataset='.$id_dataset);
}

function addType($id_NCBI,$id_dataset){
	global $datasetNCBI;
	if (!array_key_exists($id_NCBI,$datasetNCBI)) $datasetNCBI[$id_NCBI]=array();
	if (!array_key_exists($id_dataset,$datasetNCBI[$id_NCBI])) array_push($datasetNCBI[$id_NCBI],$id_dataset);
}

function getFullCategorie($id_NCBI){
	global $link;
	if($id_NCBI==0) return "Unclassified";
	$parent=[];
	$n_id_NCBI=$id_NCBI;
	while($n_id_NCBI>1){
		$result = mysqli_query($link,'SELECT id,id_parent,name FROM NCBI_tree where id='.$n_id_NCBI);
	  	if (!$result) echo ('Error : ' . mysqli_error($link));
	  	else {
	    	while($r = mysqli_fetch_row($result)) {
	      		array_push($parent,$r); 
	      		$n_id_NCBI=$r[1];
	      	}
	    }
	  }
	$r[0]='1'; $r[1]='1'; $r[2]='Root'; 
	array_push($parent,$r); 
	$categorie="";
	for($i=count($parent)-1;$i>=0;$i--) {
			$categorie=$categorie.$parent[$i][2];
			if($i>0) $categorie=$categorie." : ";
	}
	return $categorie;
}

function getLastCategorie($id_NCBI){
	if($id_NCBI==0) return "Unclassified";
	return query_first('SELECT name FROM NCBI_tree where id='.$id_NCBI);
}


//RIGHT 0:CREATOR, 1:OWNER(USER) 2:READER 3:NOTHING
$datasets = array();
$datasetNCBI= array();
					
//IF ADMIN SELECT ALL DATASET
if($isAdmin){
	$result = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_NCBI,type,rotation,translation,scale,spf,dt FROM dataset  ORDER by id');
	if (!$result) echo UTF('Error : ' . mysqli_error($link));
	else {
		while($r = mysqli_fetch_assoc($result)) {
			$r['quality']=getNbQuality($r['id']);
			addType(intval($r['id_NCBI']),intval($r['id']));
			$datasets[$r['id']] = $r;
			$datasets[$r['id']]['right']=0;
		}
	}
} 
			 		
//CREATOR DATASET
$result = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_NCBI,type,rotation,translation,scale,spf,dt FROM dataset WHERE archive=0 and id_people='.$id_people.' and bundle>=-1 order by id');
if (!$result) echo UTF('Error : ' . mysqli_error($link));
else {
	while($r = mysqli_fetch_assoc($result)) {
		if(!array_key_exists($r['id'],$datasets)) {
			$r['quality']=getNbQuality($r['id']);
			addType(intval($r['id_NCBI']),intval($r['id']));
			$datasetNCBI[$id_NCBI]+=1;
			$datasets[$r['id']] = $r;
			$datasets[$r['id']]['right']=0;
		}
	}
}
			 		
//LOOK IN MY GROUPS FOR THE SHARED DATASET
$mygroups = query_array('SELECT id,id_group,status FROM people_group WHERE id_people='.$id_people);
foreach ($mygroups  as $id_t => $group){
	$id_group=$group['id_group'];
	$groupsharing = query_array('SELECT id,id_base,how FROM sharing WHERE base="dataset" and id_group='.$id_group);
	foreach($groupsharing as $idgs => $grpshare){
		if(!array_key_exists($grpshare['id_base'],$datasets)) {
			$result2 = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_NCBI,type,rotation,translation,scale,spf,dt FROM dataset WHERE archive=0 and id='.$grpshare['id_base']);
			while($r2 = mysqli_fetch_assoc($result2)) {
				$r2['quality']=getNbQuality($r2['id']);
				addType(intval($r2['id_NCBI']),intval($r2['id']));
				$r2['right']=3;
				$datasets[$r2['id']] = $r2;
			}
		}	
	
		if($grpshare['how']==0 && $datasets[$grpshare['id_base']]['right']>2) $datasets[$grpshare['id_base']]['right']=2; //READER
		if($grpshare['how']==1 && $datasets[$grpshare['id_base']]['right']>1) $datasets[$grpshare['id_base']]['right']=1;	//OWNER
	}
}
			 		
			 		
//LOOK IN THE SHARE DATASET WITH ME (OR PUBLIC)
$usersharing = query_array('SELECT id,id_base,how,id_who FROM sharing WHERE base="dataset" and ( id_who='.$id_people.' or id_who=0) ');
foreach($usersharing as $idus => $usershare){
	if(!array_key_exists($usershare['id_base'],$datasets)) {
		$result2 = mysqli_query($link,'SELECT id,name,date,minTime,maxTime,id_people,bundle,id_NCBI,type,rotation,translation,scale,spf,dt FROM dataset WHERE archive=0 and id='.$usershare['id_base']);
		while($r2 = mysqli_fetch_assoc($result2)) {
			if($r2['id']==124) print_r($r2['id']);
			$r2['quality']=getNbQuality($r2['id']);
			addType(intval($r2['id_NCBI']),intval($r2['id']));
			$r2['right']=3;
			$datasets[$r2['id']] = $r2;
		}	
	}
	if($usershare['how']==0 && $datasets[$usershare['id_base']]['right']>2) $datasets[$usershare['id_base']]['right']=2; //READER
	if($usershare['how']==1 && $datasets[$usershare['id_base']]['right']>1) $datasets[$usershare['id_base']]['right']=1;	//OWNER
}

//Update public access
foreach ($datasets as $id => $dataset) $datasets[$dataset['id']]['isPublic']=countQuery('SELECT id  FROM sharing WHERE base="dataset" and id_base='.$dataset['id'].' and  id_who=0' );	


//By Default , order by ID (so by name)
$orderby='tree';
if(isset($_POST['orderby']))$orderby=$_POST['orderby'];

if($orderby=='date'){}
if($orderby=='invdate'){
	$new_datasets=array();
	$i=0;
	foreach ($datasets as $id => $dataset){$new_datasets[count($neworder)-1-$i]=$dataset;$i++;}
	ksort($new_datasets);
	$datasets=$new_datasets;
}
if($orderby=='name'){//BY NAME
	$neworder=array();
	foreach ($datasets as $id => $dataset) $neworder[strtolower($dataset['name'].'-'.$id)]=$dataset;
	ksort($neworder);
	$datasets=$neworder;
}
if($orderby=='invname'){//BY NAME REVERSE
	$neworder=array();
	foreach ($datasets as $id => $dataset) $neworder[strtolower($dataset['name'].'-'.$id)]=$dataset;
	ksort($neworder);
	$datasets=array();
	$i=0;
	foreach ($neworder as $id => $dataset){$datasets[count($neworder)-1-$i]= $dataset;$i++;}
	ksort($datasets);
}

if($orderby=='categorie'){//BY CATEGORIE
	$neworder=array();
	foreach ($datasets as $id => $dataset) $neworder[strtolower(getLastCategorie($dataset['id_NCBI']).'-'.$dataset['name'].'-'.$id)]=$dataset;
	ksort($neworder);
	$datasets=$neworder;
}

if($orderby=='invcategorie'){//BY INVERSE CATEGORIE
	$neworder=array();
	foreach ($datasets as $id => $dataset) $neworder[strtolower(getLastCategorie($dataset['id_NCBI']).'-'.$dataset['name'].'-'.$id)]=$dataset;
	ksort($neworder);
	$datasets=array();
	$i=0;
	foreach ($neworder as $id => $dataset){$datasets[count($neworder)-1-$i]= $dataset;$i++;}
	ksort($datasets);
}




$categorie='longname';
if(isset($_POST['categorie']))$categorie=$_POST['categorie'];

$closecat='all';
if(isset($_POST['closecat']))$closecat=$_POST['closecat'];


if($closecat=='public'){//PUBLIC ONLY
	$neworder=array();
	foreach ($datasets as $id => $dataset)
		 if($dataset['isPublic']!=0)
			$neworder[$id]=$dataset;
	$datasets=$neworder;
}

if($closecat=='my'){//MY ONLY
	$neworder=array();
	foreach ($datasets as $id => $dataset)
		 if($dataset['id_people']==$id_people)
			$neworder[$id]=$dataset;
	$datasets=$neworder;
}


function createNode($id_NCBI){
	global $link;
	$result = mysqli_query($link,'SELECT id,id_parent,name FROM NCBI_tree where id='.$id_NCBI);
  	if (!$result) echo ('Error : ' . mysqli_error($link));
  	else {
    	while($r = mysqli_fetch_row($result)) {
      		//echo "Create Node for ".$id_NCBI;
      		$node=array();
			$node['id']=$r[0]; 
			$node['id_parent']=$r[1];
			$node['name']=$r[2]; 
			return $node;
      	}
    }
	return NULL;
}


function addTree($id_NCBI,&$tree){
	while($id_NCBI>1 && !array_key_exists($id_NCBI,$tree)){
		$node=createNode($id_NCBI);
		$tree[$id_NCBI]=$node;
		$id_NCBI=$node['id_parent'];
	}
}



function createTree($tree,$id_NCBI,$n){
	if(array_key_exists($id_NCBI,$tree)){
		$node=$tree[$id_NCBI];
		///echo "createTree ".$node['name']."</br>";
		$childs=array();
		foreach ($tree as $id => $sub_tree) {
			if($sub_tree['id_parent']==$id_NCBI && $id!=$id_NCBI){
				array_push($childs,$sub_tree);
			}
 	    }
 	    if(sizeof($childs)==0){ //LAST NODE
 	    	echo $node['name'].":".$n;
 	    }
 	    if(sizeof($childs)==1){  //SAME
 	    	createTree($tree,$childs[0]['id'],$n+1);
 	    }
 	    if(sizeof($childs)>1){  //DIVISION TREE
 	    	echo "(";
 	    	$ll=0;
 	    	foreach($childs as $child){
 	    		createTree($tree,$child['id'],1);
 	    		if($ll<sizeof($childs)-1) echo ",";
 	    		$ll++;
 	    	}
 	    	echo "):".$n;
 	    }
	}
}




if($orderby=='tree'){//BY PHIOGENIC TREE 
	//FIRST ORDER BY CATEGORIE
	$neworder=array();
	foreach ($datasets as $id => $dataset) $neworder[strtolower(getLastCategorie($dataset['id_NCBI']).'-'.$dataset['name'].'-'.$id)]=$dataset;
	ksort($neworder);
	$datasets=$neworder;
	
	$tree=array();
	$tree[1]=createNode(1);
	$Cat=array();
	foreach ($datasets as $id => $dataset) {
		$id_NCBI=$dataset['id_NCBI'];
		//echo "$id_NCBI</br>";
		if(!in_array($id_NCBI,$Cat))array_push($Cat,$id_NCBI);
		if($id_NCBI==0) { //UNCLASSIFIED
			if(!array_key_exists($id_NCBI,$tree)){
				$node=array();
				$node['id']=$id_NCBI; 
				$node['id_parent']=1; //ROOT
				$node['name']="Unclassified"; 
				$tree[$id_NCBI]=$node;
			}
		}
		else addTree($id_NCBI,$tree);
	}
}

$type=-1;
if(isset($_POST['type']))$type=intval($_POST['type']);




?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
       
    <script src="jquery/jquery-2.1.0.min.js"></script>

    <link rel="stylesheet" href="css/morphonet.css" >
    <link rel="stylesheet" href="css/switch.css">

    <meta name="MorphoNet" content="MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching." />

    <script type="application/javascript" src="https://cdn.rawgit.com/phylocanvas/phylocanvas-quickstart/v2.8.1/phylocanvas-quickstart.js"></script>
<style>
	.button {
		  background-color: white; /* Green */
		  border: 1px solid #62B2E0;
		  color: gray;
		  padding: 5px 6px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  font-size: 12px;
		  border-radius: 12px;
   }
   .buttonSelected {
		  background-color: #62B2E0; /* Green */
		  border: 1px solid gray;
		  color: white;
		  padding: 5px 6px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  font-size: 12px;
		  border-radius: 12px;
   }
</style>
  </head>

  <body style="text-align:left;">
    <?php include "headbar.php";   ?>
    <?php if(isset($_SESSION["ID"])){ ?>
    	<div style="width:100%;margin:auto;text-align:center;margin-bottom:10px" >	
			<span style="font-size:20px;color:black;margin:10px" ><?php echo 'Welcome '.$_SESSION['surname'].' '.$_SESSION['name'];?></span>
			<?php 
				$quota=getQuotaByPeople($id_people);
				$size=getSizeByPeople($id_people);
			    echo '('.formatSizeUnits($size). ' / ' . formatSizeUnits($quota) .')';
   		 	  ?>
    	</div>
    <?php } ?>
    		
	<div style="width:80%;margin:0 auto"  id="loadEmbryo">
		<form action="listdatasets.php" method="POST" id="reload">
			<div style="width:850px;margin:auto;height:60px;">
				<div style="display:inline-block;width:240px;text-align:center;height:60px;">
					<span style="font-size:18px;color:gray;">Taxonomy</span></br>
										<image  width="30px" height="30px"  src="images/plant.png" style="margin:5px" onclick="change_show('categorie','longname')" title="Show full taxonomy hierarchy"/> 
					<image  width="30px" height="30px"  src="images/leaf.png" style="margin:5px" onclick="change_show('categorie','shortname')" title="Show only species name"/>
					<image  width="30px" height="30px"  src="images/phylogenetics.png" style="margin:5px" onclick="change_show('orderby','tree')" title="Show phylogenetic tree"/> 
<image  width="30px" height="30px"  src="images/sort-by-alphabet.png" style="margin:5px" onclick="change_show('orderby','categorie')" title="Sort by categorie name "/>
					<image  width="30px" height="30px"  src="images/sort-reverse-alphabetical-order.png" style="margin:5px" onclick="change_show('orderby','invcategorie')" title="Sort by reverse categorie name "/>
				</div>

				<div style=" display: inline-block;width:310px;height:60px;text-align:center;margin-left:50px;">
					<span style="font-size:18px;color:gray;">Dataset</span></br>
					<image  width="30px" height="30px"  src="images/date.png" style="margin:5px;" onclick="change_show('orderby','date')" title="Sort by date"/>
					<image  width="30px" height="30px"  src="images/invdate.png" style="margin:5px" onclick="change_show('orderby','invdate')" title="Sort by last first"/>
					<image  width="30px" height="30px"  src="images/sort-by-alphabet.png" style="margin:5px;" onclick="change_show('orderby','name')" title="Sort by alphabetic order"/>
					<image  width="30px" height="30px"  src="images/sort-reverse-alphabetical-order.png" style="margin:5px" onclick="change_show('orderby','invname')" title="Sort by reverse alphabetic order"/>
					<?php if(isset($_SESSION["ID"])){ ?> <image  width="30px" height="30px"  src="images/users-group.png" style="margin:5px" onclick="change_show('closecat','all')" title="Show all datasets"/>
					<image  width="30px" height="30px"  src="images/man-user.png" style="margin:5px" onclick="change_show('closecat','my')" title="Show only my datasets"/>
					<image  width="30px" height="30px"  src="images/opendata.png" style="margin:5px" onclick="change_show('closecat','public')" title="Show only public dataset"/>
					<?php } ?>
				</div>
			
				<div style=" display: inline-block;width:180px;height:60px;text-align:center;margin-left:50px;">
					<span style="font-size:18px;color:gray;">Type</span></br>
					<input type="image" src="images/microscope.png" alt="microscope" width="30" style="margin:5px;"  onclick="change_show('type',0)" title="show only Observed dataset" >
					<input type="image"src="images/simulated.png" alt="writing" width="30" style="margin:5px;"   onclick="change_show('type',1)"  title="show only  Simulated dataset">
					<input type="image" src="images/drawing.png" alt="drawing" width="30" style="margin:5px;"  onclick="change_show('type',2)"  title="show only  Drawing dataset">
					<input type="image" src="images/oload.png" alt="reset" width="30" style="margin:5px;"  onclick="change_show('type',-1)" title="show all datasets" >
					
				</div>
			</div>
			<input type="submit" id="send_categorie" style="visibility:hidden;">
			<input type="text" name="categorie" id="categorie" value="<?php echo $categorie;?>" style="visibility:hidden;" >
			<input type="text" name="orderby" id="orderby" value="<?php echo $orderby;?>"  style="visibility:hidden;"  >
			<input type="text" name="closecat" id="closecat" value="<?php echo $closecat;?>" style="visibility:hidden;">
			<input type="text" name="type" id="type" value="<?php echo $type;?>" style="visibility:hidden;">
			
		</form>
		
		
		 
		<?php if($orderby!='tree'){ ?>
		<div style=" width:500px;margin:auto;padding-right:60px;text-align:center;height:60px;">
			<input type="input"  id="datasetssearch" placeholder="Type to search a specific dataset name" style="height:30px;width:250px;margin-left:100px;" value="" onkeyup="dataSearch()">
    	</div>
		<?php } ?>
	</div>
	
	<?php if($orderby=='tree'){ ?>
	<!--  PHYOGENETIC TREE VIEW -->
	<div style="width:80%;margin:0 auto;text-align: center">
		<div style="margin-bottom:10px">
	 	<span style="font-size:18px;color:gray;font-style: italic;margin-top:4px">click on a node to access the corresponding datasets</span>
	
		<div id="phylocanvas" style="margin-left:15%"></div>
	</div>
	<!--  END PHYOGENETIC TREE VIEW -->
	<?php } ?>
	
	<div style="width:80%;margin:0 auto"  id="embryos">
		<?php 
				$prevId_NCBI=-1;
				$nbByCategorie=array();
				foreach ($datasets as $id => $dataset)
					if($type==-1 || $type==intval($dataset['type'])){
					$right=$dataset['right']; 
					$bundle=$dataset['bundle'];
					//CHECK IF THIS DATASET IS PUBLIC
					
					$id_NCBI=$dataset['id_NCBI'];
					$short_categorie_name=getLastCategorie($id_NCBI);
					if($categorie=='longname') $categorie_name=getFullCategorie($id_NCBI); //FULL NAME
					if($categorie=='shortname') $categorie_name=$short_categorie_name; //LAST PAR OF THE NAME
					if(!array_key_exists($id_NCBI,$nbByCategorie)) $nbByCategorie[$id_NCBI]=0;

					if($id_NCBI!=$prevId_NCBI){  $nbByCategorie[$id_NCBI]+=1;
						$name_cat=str_replace(' ','_',$short_categorie_name);
						if($orderby!='tree')$name_cat=$name_cat.'_'.$nbByCategorie[$id_NCBI];
						if($prevId_NCBI!=-1){  ?> </div> <?php } 
						$prevId_NCBI=$id_NCBI; ?>
						<span id="<?php echo $name_cat;?>_name" style="font-size:18px;color:gray;margin:10px;display: inline-block;vertical-align: middle;line-height: normal;<?php if($orderby=='tree') echo 'display:none'; ?>"><?php echo $categorie_name; ?></span>
						<div id="<?php echo $name_cat;?>"  <?php if($orderby=='tree') echo ' style="display:none" '; ?>>
					<?php } ?>				 			
							<div id="dataset_<?php echo $dataset['id'];?>" style="text-align: left;width:100%;margin-left:100px;margin-top:10px;background-color:#DBF0FC;"  >
								<div class='cursor' style="display: inline-block;padding-right:50px" onclick="describe(<?php echo $dataset['id'];?>)">
										<?php if($dataset['isPublic']!=0) {?><img width="30px" height="30px"  src="images/opendata.png" style="float:left;margin-right:10px;margin-left:20px;margin-top:5px" title="This dataset is public"/> <?php } ?>
										<?php 
											$img_type="";$title="";
											//if($dataset['type']==0) { $img_type="microscope";$title="Observed dataset";}
											if($dataset['type']==1) { $img_type="simulated";$title="Simulated dataset";}
											if($dataset['type']==2) { $img_type="drawing";$title="Drawing dataset";}
										if($img_type!=""){ ?>
										<img width="30px" height="30px"  src="images/<?php echo $img_type;?>.png" style="float:left;margin-right:10px;margin-left:10px;margin-top:5px" title="<?php echo $title;?>"/> <?php } ?>
										<span id="dataset_<?php echo $dataset['id'];?>_name" style="font-size:20px;color:black;margin:10px;display: inline-block;vertical-align: middle;line-height: normal;"><?php echo $dataset['name']; if($dataset['minTime']!=$dataset['maxTime']){$diff=1+intval($dataset['maxTime'])-intval($dataset['minTime']);   echo ' ('.$diff.' time points)';} ?></span>
								</div> 			
								<input type="image" width="30px" height="30px"  src="images/load.png" style="float:right;margin-right:10px;margin-left:20px;margin-top:5px" title="open dataset" onclick="loadEmbryo(<?php echo $dataset['id'].','.$right;?>)"/>
								<?php if($right<=1){ ?> 
									<input type="image" width="30px" height="30px"  src="images/parameters.png" style="float:right;margin-right:10px;margin-left:20px;margin-top:5px" onclick="modifyEmbryo(<?php echo $dataset['id'];?>)"  title="Settings"/>
									<input type="image" width="30px" height="30px"  src="images/share.png" style="float:right;margin-right:10px;margin-left:20px;margin-top:5px" onclick="shareEmbryo(<?php echo $dataset['id'];?>)"  title="Share"/>
								<?php } ?>	
								<?php if($dataset['quality']>1){ ?>
				 					<div style="float:right;text-align: center;">
				 						<input type="range" id="quality_<?php echo $dataset['id'];?>" value="<?php echo round($dataset['quality']/2);?>" max="<?php echo $dataset['quality'];?>" min="1" step="1" style="width:100px;" onchange="updateslider(<?php echo $dataset['id'],','.$dataset['quality'];?>)">
				 						</br>
				 						<span id="value_<?php echo $dataset['id'];?>" style="font-size:15px;color:blue;" ><?php
					 						if($dataset['quality']==5) echo "medium";
					 						if($dataset['quality']==4) echo "low";
					 						if($dataset['quality']==3) echo "medium";
					 						if($dataset['quality']==2) echo "low";
										?></span>
				 						<span id="valuei_<?php echo $dataset['id'];?>" style="visibility:hidden">3</span>
						 			</div>
								<?php } ?>
						    </div>
			<?php }  ?>
			</div>
			<form action="datasetproperties.php" method="POST" id="datasetproperties">
				<input type="text" name="id_dataset" id="id_dataset" value="" style="visibility:hidden;">
				<input type="submit" id="send_datasetproperties" style="visibility:hidden;">
			</form>
      
    </div>

    <script type="text/javascript">
	    
	    function dataSearch(){
		    var search=$('#datasetssearch').val().toLowerCase();
		    $('#embryos').children('div').each(function () { //FOR EACH NCBI GROUP
				var nbdataset=0;
				$('#'+this.id).children('div').each(function () { //FOR EACH DATASET
					var name=$('#'+this.id+"_name").html().toLowerCase();
					if(name.search(search)==-1) {
						$('#'+this.id).hide();
					}
					else {
						$('#'+this.id).show();
						nbdataset+=1;
					}
				});
				if(nbdataset>0){
					$('#'+this.id).show();
					$('#'+this.id+"_name").show();
				}
				else {
					$('#'+this.id+"_name").hide();
					$('#'+this.id).hide();
				}
			});
	    }
	  
	    
    	function change_show(type,value){
    		$('#'+type).val(value);
    		$('#send_categorie').click();
    	}

    	var id_dataset_shared=-1;
    	function modifyEmbryo(id_dataset){
    		$('#id_dataset').val(id_dataset);
			$('#send_datasetproperties').click();
		}
		function updateslider(id_dataset,maxq){
			var valof = $("#quality_"+id_dataset).val();
			if(maxq==5){
				if(valof==1) $("#value_"+id_dataset).text("very low");
				if(valof==2) $("#value_"+id_dataset).text("low");
				if(valof==3) $("#value_"+id_dataset).text("medium");
				if(valof==4) $("#value_"+id_dataset).text("high");
				if(valof==5) $("#value_"+id_dataset).text("very high");
			}
			if(maxq==4){
				if(valof==1) $("#value_"+id_dataset).text("very low");
				if(valof==2) $("#value_"+id_dataset).text("low");
				if(valof==3) $("#value_"+id_dataset).text("high");
				if(valof==4) $("#value_"+id_dataset).text("very high");
			}
			if(maxq==3){
				if(valof==1) $("#value_"+id_dataset).text("low");
				if(valof==2) $("#value_"+id_dataset).text("medium");
				if(valof==3) $("#value_"+id_dataset).text("high");
			}
			if(maxq==2){
				if(valof==1) $("#value_"+id_dataset).text("low");
				if(valof==2) $("#value_"+id_dataset).text("high");
			}
			$("#valuei_"+id_dataset).text(valof);
		}

		function loadEmbryo(id_dataset,right){
			var quality = $("#valuei_"+id_dataset).text();
			if(quality!="") quality=quality-1;
			else quality="0";
			$('#embryos').append(' <form style="display: hidden" action="embryos.php" method="POST" id="loadEmbryoform"><input type="hidden" id="id_dataset" name="id_dataset" value="'+id_dataset+'"/><input type="hidden" id="quality" name="quality" value="'+quality+'"/><input type="hidden" id="right" name="right" value="'+right+'"/></form>');
			$('#loadEmbryoform').submit();
		}


    	function shareEmbryo(id_dataset){
			if(id_dataset_shared!=-1) $('#share').remove();
			if(id_dataset_shared!=id_dataset){
				$('#dataset_'+id_dataset).append('<div id="share" style="width:100%;">'+id_dataset_shared+'</div>');
				$('#share').hide(); 
				id_dataset_shared=id_dataset;
				 $.post("sharewith.php", {
          				base:"dataset",
          				id_base:id_dataset,
          				view:1
        		}, function(data,status){$('#share').html(data);$('#share').show();  } );
 			}else id_dataset_shared=-1;
		}


    	function describe(id_dataset){
			if(id_dataset_shared!=-1) $('#share').remove();
			if(id_dataset_shared!=id_dataset){
				$('#dataset_'+id_dataset).append('<div id="share" style="width:100%;">'+id_dataset_shared+'</div>');
				$('#share').hide(); 
				id_dataset_shared=id_dataset;
				 $.post("describe.php", {
          				id_dataset:id_dataset
        		}, function(data,status){$('#share').html(data); $('#share').show(); } );
 			}else id_dataset_shared=-1;
       	}


    <?php if($orderby=='tree'){ ?>
    var morpho_tree = "<?php  createTree($tree,1,1); ?>";
    
    var tree = Phylocanvas.createTree('phylocanvas', {
	  history: false,
	  scalebar: { active: false },
	  disableZoom:true,
	  hoverLabel:true,
	  fillCanvas:true,
	  internalNodesSelectable:false,
	  showLabels:true

	});

    tree.on('beforeFirstDraw', function(){
        tree.setNodeSize(10);
        tree.branchLengthLabelPredicate();
        tree.setTextSize(17);
        tree.highlightColour = '#62B2E0';
        tree.selectedColour = '#62B2E0';
        tree.getBounds(tree.leaves);
        tree.hoverLabel = true
    });
    
    
        
        
	var nbBranches=<?php echo count($Cat); ?>;
	tree.setSize(1000,25*nbBranches);
    tree.setNodeSize(10);
    tree.setTreeType('diagonal');
    tree.load(morpho_tree);
   	tree.on('click', function (e) {
			var node = tree.getNodeAtMousePosition(e);
			if (node) {
				var node_name=node.id.replace(' ','_');
				$('#embryos').children('div').each(function () { //FOR EACH NCBI GROUP
					if(this.id==node_name){
						//console.log("SHOW "+this.id);
						$('#'+this.id).show();
						$('#'+this.id+"_name").show();
					}
					else{
					 	$('#'+this.id).hide();
					 	$('#'+this.id+"_name").hide();
					 	//console.log("HIDE "+this.id);
					}
				});
			}
	});


	<?php } ?>


  </script>



    <?php include "footer.php"; ?> 
  </body>
</html>
