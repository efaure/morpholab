<?php


include 'functions.php';

if ($connected){
	
    $now=new DateTime();
                
    if(isset($_POST["login"]) && isset($_POST["passwd"])){ //CONNECTION TEST
        $login = anti_injection_login($_POST["login"]); //I use that function to protect against SQL injection
        $pass = anti_injection_login($_POST["passwd"]);
        $SQL = "SELECT id,password,name,surname FROM people WHERE login = '" . $login . "' and password='".$pass."' and exist=1;";
        $result_id = @mysqli_query($link,$SQL) or die("ERROR : DATABASE CONNECTION FAILED!");
        $total = mysqli_num_rows($result_id);
        if($total) {
            $datas = @mysqli_fetch_array ($result_id);
            $id_people=$datas["id"];
            //We Make an insert in the log
            $IP="API";
            if(isset($_POST["IP"]) )$IP=$_POST["IP"];
        } else  echo "ERROR : who are you ?";       
        if(isset($_POST["guys"])){ //WHO IS THIS GUY ?
         echo query_json('SELECT id,surname,name,login FROM people WHERE id>0'); #NO PULBIC
        }
        elseif(isset($_POST["whoname"])){ //WHO IS THIS GUY BY NAME ?
          $name=explode(" ",trim($_POST["whoname"]));
          if(count($name)==2){
            echo query_json('SELECT id,surname,name,login FROM people WHERE ( LOWER(name)="'.strtolower($name[0]).'" and LOWER(surname)="'.strtolower($name[1]).'") OR  ( LOWER(name)="'.strtolower($name[1]).'" and LOWER(surname)="'.strtolower($name[0]).'")');
          }else {
	          $name=strtolower(trim($_POST["whoname"]));
			  echo query_json('SELECT id,surname,name,login FROM people WHERE ( LOWER(name)="'.$name.'" or LOWER(surname)="'.$name.'" OR LOWER(login)="'.$name.'")');
          }
        }
        elseif(isset($_POST["getncbitype"])){ //RETURN NCBI TYPE
            echo query_first('SELECT name FROM NCBI_tree WHERE id='.$_POST["getncbitype"]);
        }
        
        elseif(isset($_POST["listmydataset"])){ //LIST MY DATASET
           echo query_json('SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type,date FROM dataset WHERE id_people='.$id_people);   
        }
         elseif(isset($_POST["listmydatasetsimple"])){ //LIST MY DATASET
	         
           print_r(query_array('SELECT id,name FROM dataset WHERE id_people='.$id_people.' order by name'));   
        }
        elseif(isset($_POST["listdataset"])){ //LIST ALL DATASET
           
           
           	$rows = array();
		
					
			//CREATOR DATASET
			$result = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type,date FROM dataset WHERE archive=0 and id_people='.$id_people.' and bundle>=-1 order by id');
			if (!$result) echo UTF('Error : ' . mysqli_error($link));
	 		else {
				while($r = mysqli_fetch_row($result)) {
					if(!array_key_exists($r[0],$rows))  $rows[$r[0]] = $r;
					#$rows[$r['id']]['right']=0;
				}
	 		}
	 		//LOOK IN MY GROUPS FOR THE SHARED DATASET
	 		$mygroups = query_array('SELECT id,id_group,status FROM people_group WHERE id_people='.$id_people);
	 		foreach ($mygroups  as $id_t => $group){
	 			$id_group=$group['id_group'];
	 			$groupsharing = query_array('SELECT id,id_base,how FROM sharing WHERE base="dataset" and id_group='.$id_group);
            	foreach($groupsharing as $idgs => $grpshare){
            		if(!array_key_exists($grpshare['id_base'],$rows)) {
            			$result2 = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type,date FROM dataset WHERE archive=0 and id='.$grpshare['id_base']);
            			while($r2 = mysqli_fetch_row($result2)) {
							$rows[$r2[0]] = $r2;
						}	
            		}
            	}
	 		}
	 		//LOOK IN THE SHARE DATASET WITH ME (OR PUBLIC)
            $usersharing = query_array('SELECT id,id_base,how,id_who FROM sharing WHERE base="dataset" and ( id_who='.$id_people.' or id_who=0) ');
            foreach($usersharing as $idus => $usershare){
            		if(!array_key_exists($usershare['id_base'],$rows)) {
            			$result2 = mysqli_query($link,'SELECT id,name,minTime,maxTime,id_people,bundle,id_NCBI,type,date  FROM dataset WHERE archive=0 and id='.$usershare['id_base']);
            			while($r2 = mysqli_fetch_row($result2)) {
							$rows[$r2[0]] = $r2;
						}	
            		}//else echo "ok< with ".$usershare['id_base']."/br>";
	 		}

	 		$jrows = array();
	 		foreach($rows as $ids => $row) $jrows[]=$row;
		 	
	 		echo jsonRemoveUnicodeSequences($jrows);

        }
        elseif(isset($_POST["createdataset"])){ //CREATE  A DATASET
            //LOOK IN THE SHARE DATASET
            $bundle=0;
            if(isset($_POST["bundle"])) $bundle=$_POST["bundle"];
            $id_NCBI=0;  if(isset($_POST["id_NCBI"])) $id_NCBI=$_POST["id_NCBI"];
            $id_type=0;  if(isset($_POST["id_type"])) $id_type=$_POST["id_type"];
            echo query_id('INSERT INTO dataset (id_people,date,name,minTime,maxTime,bundle,id_NCBI,type,spf,dt) VALUES ('.$id_people.',"'.$now->format('Y-m-d h:s:i').'","'.$_POST["createdataset"].'",'.$_POST["minTime"].','.$_POST["maxTime"].','.$bundle.','.$id_NCBI.','.$id_type.','.$_POST['spf'].','.$_POST['dt'].')');  
        }
        elseif(isset($_POST["dataset"])){ //FIND A DATASET BY ID
	        $jrows=getDatasetById($id_people,$_POST["dataset"]);
		     echo jsonRemoveUnicodeSequences($jrows);
		   	 		              
        }
        elseif(isset($_POST["datasetname"])){ //FIND A DATASET BY NAME
         	$result = mysqli_query($link,'SELECT id FROM dataset WHERE LOWER(name) like "%'.strtolower($_POST["datasetname"]).'%"  and id_people='.$id_people);
		 	if (!$result) echo UTF('Error : ' . mysqli_error($link));
			else {
				while($r = mysqli_fetch_row($result)) {
					$jrows=getDatasetById($id_people,$r[0]);
					echo jsonRemoveUnicodeSequences($jrows);
					exit;
					
				}
			}
            
        }
        elseif(isset($_POST["updatedataset"])){ //FIND A DATASET BY NAME
         echo query('UPDATE dataset set minTime='.$_POST["minTime"].',maxTime='.$_POST["maxTime"].',id_NCBI='.$_POST["id_NCBI"].',type='.$_POST["id_type"].',name='.$_POST["dataname"].' WHERE id='.$_POST["updatedataset"]);
       }  
        elseif(isset($_POST["deletedataset"])){ //DELETE A COMPLETE DATASET
            $id_dataset=$_POST["deletedataset"];
            if(query('DELETE FROM  dataset  WHERE id='.$id_dataset.' and id_people='.$id_people)!="")  echo "ERROR DELETE DATASET";
            else if(query('DELETE FROM  mesh  WHERE id_dataset='.$id_dataset)!="") echo "ERROR DELETE MESH";
            else if(query('DELETE FROM  correspondence  WHERE id_dataset='.$id_dataset)!="")  echo "ERROR DELETE CORRESPONDENCE";
            else if(query('DELETE FROM  share WHERE id_people='.$id_people.' and base="dataset" and id_base='.$id_dataset)!="") echo "ERROR DELETE DATASET SHARE";
             else {
            	#DELETE LINKS
            	 $filename='/data/Bundle/WebGL/DATASET_'.$id_dataset;
		         system('rm -rf ' .$filename);
		         $filename='/data/Bundle/Android/DATASET_'.$id_dataset;
		         system('rm -rf ' .$filename);
            }
        }
        elseif(isset($_POST["cleardataset"])){ //CLEAR DATASET
            $id_dataset=$_POST["cleardataset"];
            $id_check=query_first('SELECT id FROM dataset WHERE id='.$id_dataset.' and id_people='.$id_people);  
            if($id_check!=$id_dataset) echo "ERROR DATASET NOT FOUND";
            else {
            	if(query('DELETE FROM  mesh WHERE id_dataset='.$id_dataset)!="")echo "ERROR DELETE DATASET TIME";
                if(query('DELETE FROM  correspondence  WHERE id_dataset='.$id_dataset)!="")  echo "ERROR DELETE CORRESPONDENCE";
                
                #DELETE LINKS
            	$filename='/data/Bundle/WebGL/DATASET_'.$id_dataset;
		        system('rm -rf ' .$filename);
		        $filename='/data/Bundle/Android/DATASET_'.$id_dataset;
		        system('rm -rf ' .$filename);

            }
        }
        
        elseif(isset($_POST["uploadmesh"]) && checkQuota($id_people)){ //UPLOAD A TIME POINT
	        
	        $id_dataset=$_POST["uploadmesh"];
            $t=$_POST["t"];
            $quality=$_POST["quality"];
            $channel=$_POST["channel"];
            $center="NULL"; if(isset($_POST["center"]))$center=$_POST["center"];
            $blink="NULL"; if(isset($_POST["link"])) $blink=$_POST["link"];
            //TEMP
            file_put_contents("test_imagej.obj.bz2",$_POST['data']);
            //ENDTEMP
            $id_uploadtime=query_first('SELECT id FROM mesh WHERE id_dataset='.$id_dataset.' and quality='.$quality.' and channel="'.$channel.'" and t='.$t);
            if($id_uploadtime==""){//NEW
                echo query_id('INSERT INTO mesh (id_dataset,t,channel,quality,obj,date,link,center) VALUES ('.$id_dataset.','.$t.',"'.$channel.'",'.$quality.',"'.mysqli_escape_string($link,$_POST['data']).'","'.$now->format('Y-m-d h:s:i').'","'.$blink.'","'.$center.'")'); 
             }
             else{
                 $filename='/data/Bundle/WebGL/DATASET_'.$id_dataset.'/Channel_'.$channel.'/Quality_'.$quality.'/Time_'.$t.'/'.$id_uploadtime;
	             if(file_exists($filename)) unlink($filename);
	             echo query('UPDATE mesh SET obj="'.mysqli_escape_string($link,$_POST['data']).'",link="'.$blink.'" ,center="'.$center.'" WHERE id='.$id_uploadtime.' and id_dataset='.$id_dataset.' and quality='.$quality.' and channel="'.$channel.'" and t='.$t);
                 echo $id_uploadtime;
             }
        }

        elseif(isset($_POST["uploadlargemesh"])  && checkQuota($id_people)){ //UPLOAD A TIME POINT
            $id_dataset=$_POST["uploadlargemesh"];
            $t=$_POST["t"];
            $quality=$_POST["quality"];
            $channel=$_POST["channel"];
            $center="NULL"; if(isset($_POST["center"]))$center=$_POST["center"];
            $file=$_FILES['file'];
            $blink="NULL"; if(isset($_POST["link"])) $blink=$_POST["link"];
            $id_uploadtime=query_first('SELECT id FROM mesh WHERE id_dataset='.$id_dataset.' and quality='.$quality.' and channel="'.$channel.'" and t='.$t);
            if($id_uploadtime==""){//NEW
                echo query_id('INSERT INTO mesh (id_dataset,t,channel,quality,obj,date,link,center) VALUES ('.$id_dataset.','.$t.',"'.$channel.'",'.$quality.',"'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'","'.$now->format('Y-m-d h:s:i').'","'.$blink.'","'.$center.'")'); 
             }
             else{
	             $filename='/data/Bundle/WebGL/DATASET_'.$id_dataset.'/Channel_'.$channel.'/Quality_'.$quality.'/Time_'.$t.'/'.$id_uploadtime;
	             if(file_exists($filename)) unlink($filename);
	         
                 echo query('UPDATE mesh SET obj="'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'",link="'.$blink.'" ,center="'.$center.'" WHERE id='.$id_uploadtime.' and id_dataset='.$id_dataset.' and quality='.$quality.' and channel="'.$channel.'" and t='.$t);
                 echo $id_uploadtime;
             }
             
             $maxT=query_first('SELECT maxtime FROM dataset WHERE id='.$id_dataset);
             if(intval($t)>intval($maxT))query_first("UPDATE dataset SET maxtime=".$t." WHERE id=".$id_dataset);
             
        }

        elseif(isset($_POST["uploadmultiplemesh"])  && checkQuota($id_people) ){ //UPLOAD A TIME POINT
            $id_dataset=$_POST["uploadmultiplemesh"];
            $t=$_POST["t"];
            $quality=$_POST["quality"];
            $channel=$_POST["channel"];
            $blink="NULL"; if(isset($_POST["link"])) $blink=$_POST["link"];
             $center="NULL"; if(isset($_POST["center"]))$center=$_POST["center"];

            echo query_id('INSERT INTO mesh (id_dataset,t,channel,quality,obj,date,link,center) VALUES ('.$id_dataset.','.$t.',"'.$channel.'",'.$quality.',"'.mysqli_escape_string($link,$_POST['data']).'","'.$now->format('Y-m-d h:s:i').'","'.$blink.'","'.$center.'")'); 
        }
        elseif(isset($_POST["uploadlargemultiplemesh"])  && checkQuota($id_people) ){ //UPLOAD A TIME POINT
            $id_dataset=$_POST["uploadlargemultiplemesh"];
            $t=$_POST["t"];
            $quality=$_POST["quality"];
            $channel=$_POST["channel"];
            $file=$_FILES['file'];
            $blink="NULL"; if(isset($_POST["link"])) $blink=$_POST["link"];
            $center="NULL"; if(isset($_POST["center"]))$center=$_POST["center"];
            echo query_id('INSERT INTO mesh (id_dataset,t,channel,quality,obj,date,link,center) VALUES ('.$id_dataset.','.$t.',"'.$channel.'",'.$quality.',"'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'","'.$now->format('Y-m-d h:s:i').'","'.$blink.'","'.$center.'")'); 
        } 
        elseif(isset($_POST["getmesh"])){ //UPLOAD A TIME POINT
            $id_dataset=$_POST["getmesh"];
            $t=$_POST["t"];
            $quality=$_POST["quality"];
            $channel=$_POST["channel"];
            echo query_first('SELECT obj FROM mesh WHERE id_dataset='.$id_dataset.' and quality='.$quality.' and channel="'.$channel.'" and t='.$t);
        }

            //////////////////////// INFOS
        
        elseif(isset($_POST["getinfos"])){ //GET FILED OF  INFOS
            $id_dataset=$_POST["getinfos"];
            $infos=$_POST["infos"];
            echo query_first('SELECT field FROM correspondence WHERE id_dataset='.$id_dataset.' and infos="'.$infos.'"');
        }
         elseif(isset($_POST["getinfosid"])){ //GET FILED OF  INFOS
            $id_dataset=$_POST["getinfosid"];
            $infos=$_POST["idinfos"];
            echo query_first('SELECT field FROM correspondence WHERE id_dataset='.$id_dataset.' and id="'.$infos.'"');
        }
        elseif(isset($_POST["listinfos"])){ //LIST ALL AVAIRALBLE INFOS
           	 $id_dataset=$_POST["listinfos"];
            
            //First Get Guy Status on this dataset
			 	$isAdmin=getAdmin($id_people);
			 	$rows=[];
			 	$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE (id_people='.$id_people. ' OR  common=1) AND id_dataset='.$id_dataset; //DEFAULT QUERY
			 	if($isAdmin){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE id_dataset='.$id_dataset;}
			 	else{ //Check if this dataset is in  Groups Status
				 	//First we look if this guy as specific right on this dataset
				 	//HOW ==0 READER
				 	//HOW ==1 AS OWNER

					$how=query_first('select how from sharing where base="dataset" and id_base='.$_POST["id_dataset"].' AND id_who='.$id_people);
				 	if($how==""){ //NO SPECIFIC RIGHT IN FOR THE USER  WE LOOK IN GROUP
					 	$how=-1;
					 	$result = mysqli_query($link,'select how,id_group from sharing where base="dataset" and id_base='.$_POST["id_dataset"].' AND id_group!="NULL"');	
			     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
				 		else  
				 			while($r = mysqli_fetch_assoc($result))  {
				 				$status=query_first('select status from people_group where id_people='.$id_people.' and id_group='.$r['id_group']);
				 				if($status=="member" && $how!=1)$how=0;
				 				if($status=="manager")$how=1;
				 				
				 		   }
				 
				 	}
				 	if($how==0){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE (id_people='.$id_people. ' OR  common=1) AND id_dataset='.$id_dataset;} //READER
					if($how==1){$query='SELECT id,infos,id_people,link,version,date,datatype,type,common FROM correspondence WHERE id_dataset='.$id_dataset;} //MANAGER
				 	
				 	
			 	}
			 	
			 		 		
		 		if($query!=""){
			 		$result = mysqli_query($link,$query);	
		     		if (!$result) echo UTF('Error : ' . mysqli_error($link));
			 		else  
			 			while($r = mysqli_fetch_assoc($result))  {
			 				$rows[] = $r;
	
			 		}
			 	}
		 		
		 		print  jsonRemoveUnicodeSequences($rows);
		 		
        }
        elseif(isset($_POST["uploadcorrespondence"])  && checkQuota($id_people)){ //UPLOAD INFOS
            $id_dataset=$_POST["uploadcorrespondence"];
            $type=$_POST["type"];
            $infos=$_POST["infos"];
            if($infos=="time" || $infos=="space" || $infos=="group") $type=1;
            echo query_id('INSERT INTO correspondence (id_dataset,infos,field,date,id_people,type,datatype) VALUES ('.$id_dataset.',"'.$infos.'","'.mysqli_escape_string($link,$_POST['field']).'","'.$now->format('Y-m-d h:s:i').'",'.$id_people.','.$type.',"'.$_POST["datatype"].'")'); 
        }

        elseif(isset($_POST["uploadlargecorrespondence"])  && checkQuota($id_people) ){ //UPLOAD INFOS
            $id_dataset=$_POST["uploadlargecorrespondence"];
            $type=$_POST["type"];
            $infos=$_POST["infos"];
            if($infos=="time" || $infos=="space" || $infos=="group") $type=1;
            $file=$_FILES['file'];
            echo query_id('INSERT INTO correspondence (id_dataset,infos,field,date,id_people,type,datatype) VALUES ('.$id_dataset.',"'.$infos.'","'.mysqli_escape_string($link,file_get_contents($file['tmp_name'])).'","'.$now->format('Y-m-d h:s:i').'",'.$id_people.','.$type.',"'.$_POST["datatype"].'")'); 
        }
        
        elseif(isset($_POST["deletecorrespondence"])){ //UPLOAD INFOS
            $id_dataset=$_POST["deletecorrespondence"];
            $infos=$_POST["infos"];
            $id_uploadinfos=query_first('SELECT id FROM correspondence WHERE id_dataset='.$id_dataset.' and infos="'.$infos.'" and id_people='.$id_people); //LOOK FIRST IF EXIST
            if($id_uploadinfos=="")  echo "does not exist";
             else{ //EXIST
                echo query('DELETE FROM  correspondence WHERE id_dataset='.$id_dataset.' and infos="'.$infos.'" and id='.$id_uploadinfos.' and id_people='.$id_people);
                 echo $id_uploadinfos;
             }
        }elseif(isset($_POST["deletecorrespondenceid"])){ //UPLOAD INFOS
            $id_dataset=$_POST["deletecorrespondenceid"];
            $idinfos=$_POST["idinfos"];
            $id_uploadinfos=query_first('SELECT id FROM correspondence WHERE id_dataset='.$id_dataset.' and id='.$idinfos.' and id_people='.$id_people); //LOOK FIRST IF EXIST
            if($id_uploadinfos=="")  echo "does not exist";
             else{ //EXIST
                echo query('DELETE FROM  correspondence WHERE id_dataset='.$id_dataset.' and id='.$id_uploadinfos.' and id_people='.$id_people);
                 echo $id_uploadinfos;
             }
        }
        elseif(isset($_POST["uploadescription"])){ //Upload a description  
	        $id_dataset=$_POST["uploadescription"];
            echo query('UPDATE dataset SET comments="'.$_POST["description"].'" WHERE id='.$id_dataset);  
        }
        
        
        else echo $id_people;

    }

    mysqli_close($link);
} else echo "ERROR : DATABASE CONNECTION FAILED!";

?>
