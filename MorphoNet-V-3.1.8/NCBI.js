///https://github.com/patternfly/patternfly-bootstrap-treeview
        function resetTree(){
          $('#treesearch').hide();
          $('#treeview').show();
          wtree='treeview';
          $('#input-search').val("");
        }
        function createNode(name,id_node,nb_childs,id_parent){
            //console.log("createNode "+name + " with id="+id_node);
            //expandIcon: 'glyphicon glyphicon-refresh glyphicon-refresh-animate',
            checked=false;
            if(nb_childs==0)checked=true;
            node={ text: name,
                     id:id_node,
                     id_parent:id_parent,
                     state: {  expanded: false ,checked:checked },
                     nb_childs:nb_childs,
                     tags: [""+id_node]
                }
            
            return node;
        }

        function init_Tree(){
           showCheckbox=false;
           if(wtree=="treeview")showCheckbox=true;

            $('#'+wtree).treeview({
              color: "#428bca",
              data: [ root_node  ],
              levels: 1, // expanded to 2 levels

              // custom icons
              expandIcon: 'glyphicon glyphicon-plus',
              collapseIcon: 'glyphicon glyphicon-minus',
              emptyIcon: 'glyphicon',
              nodeIcon: '',
              selectedIcon: '',
              checkedIcon: 'glyphicon',
              uncheckedIcon: 'glyphicon  glyphicon-hourglass',

              // colors
              backColor: undefined, // '#FFFFFF',
              borderColor: undefined, // '#dddddd',
              onhoverColor: '#F5F5F5',
              selectedColor: '#FFFFFF',
              selectedBackColor: '#428bca',
              searchResultColor: '#D9534F',
              searchResultBackColor: undefined, //'#FFFFFF',

              // enables links
              enableLinks: false,

              // highlights selected items
              highlightSelected: true,

              // highlights search results
              highlightSearchResults: true,

              // shows borders
              showBorder: true,

              // shows icons
              showIcon: true,

              // shows checkboxes
              showCheckbox: showCheckbox,

              // shows tags
              showTags: true,

              // enables multi select
              multiSelect: false
            });
        }

        function getNode(id_node){
            var c_node = $("#"+wtree).treeview('findNodes', [id_node, 'id']);
            var node_c=c_node[0];
            //console.log("parent.length="+parent.length)
            for(var j=0;j<c_node.length;j++){
              //console.log(" --> "+id_parent+"=="+parent[j].id)
              if(id_node==c_node[j].id) node_c=c_node[j];
            }
            return node_c;
        }



        var root_node=createNode('Root',1,5,-1);
        Nodes=[];
        wtree='treeview';


        //BROWSER FUNCTION
        function moreNode(node_parent){
            node={ text: "more...",
                     id:""+node_parent.id+"-1",
                     id_parent:node_parent.id,
                     state: {  expanded: false ,checked:true }
                }
             return $('#treeview').treeview('addNode', [ node, node_parent, true , { silent: true }]);
        }
        
        function init_Event(){
          $('#treeview').on('nodeExpanded', function(event, node) {
              node.nodes.forEach(function(child) {
                      if(child['nb_childs']>0)     Nodes.push(child); //Look for the other node
                });
                getNextNode();
     
          });

          $('#treeview').on('click', function (event) {
                var nodes_selected=$('#treeview').treeview('getSelected');
                //console.log("nodes_selected "+nodes_selected.id);
                nodes_selected.forEach(function(child) {
                  if(child.text=="more..."){
                      var id_parent=child.id_parent;
                      $('#treeview').treeview('removeNode', [ child, { silent: true } ]);
                      getMoreNode(child.id_parent);
                  }else change_Categorie(child);

                 });
            });

         
        }

        function init_Search_Event(){
              $('#treesearch').on('click', function (event) {
                  var nodes_selected=$('#treesearch').treeview('getSelected');
                  nodes_selected.forEach(function(child) {
                  change_Categorie(child);
                });

           });
        }
        function start_Browser(){
          $('#treesearch').hide();
          $('#treeview').show();
          wtree='treesearch'; init_Tree();
          wtree='treeview'; init_Tree();
          init_Event();
          Nodes.push(root_node);
          getNextNode();
        }

        function getGrandParentNode(parentNode,all){
                 var node_parent=getNode(parentNode['id']);
                 //if(node_parent.nodes==undefined){
                    $.post("NCBIgetNode.php", {  id_root:parentNode['id'],all:all  }, function(data,status){
                      childs=data.split(String.fromCharCode(10));
                      //console.log(data);
                      childs.forEach(function(child) {
                        if(child.length>0){
                            elt=child.split(':');
                            //console.log(child);
                            if(elt.length>=4) {
                              new_id=elt[0];
                              var new_node=getNode(new_id);
                              if(new_node==undefined){
                                  nbchild=elt[2];
                                  var name=elt[3];  if(elt.length>4) for(var j=4;j+=1;j<elt.length) name+=':'+elt[j];
                                  console.log(" ---> create "+new_id+"->"+name);
                                  var new_parent=getNode(elt[1]);
                                  new_node=createNode(name,new_id,nbchild,new_parent.id_parent);
                                  
                                  $('#treeview').treeview('addNode', [ new_node, new_parent, true , { silent: true }]);
                                  //$('#treeview').treeview('collapseNode', [ [new_parent] , { silent: true }]);
                                }
                            }
                        }
                      });
                      $('#treeview').treeview('collapseNode', [ [node_parent] , { silent: true }]);
                     });
                 //}//else console.log(node_parent);
         }
          
        
        function getNextNode(){
            if(Nodes.length>0){
              node=Nodes.shift();
              getChildNodes(node);
            }
        }

        //When we click on more to add more things ...
        function getMoreNode(id_parent){
            var node_parent = getNode(id_parent);
            $.post("NCBIgetNode.php", {  id_root:id_parent,nb_nodes:node_parent.nodes.length,nbsearch:count()  }, function(data,status){
                childs=data.split(String.fromCharCode(10));
                childs.forEach(function(child) {
                  if(child.length>0){
                    elt=child.split(':');
                    if(elt.length>=3) {
                      var name=elt[2];  if(elt.length>3) for(var j=3;j+=1;j<elt.length) name+=':'+elt[j];
                      new_node=createNode(name,elt[0],elt[1],node_parent.id);
                      $('#treeview').treeview('addNode', [ new_node, node_parent, true , { silent: true }]);
                      if(new_node.nb_childs>0) Nodes.push(new_node);
                     }
                   }
                });
                if(node_parent.nodes.length<node_parent.nb_childs)  moreNode(node_parent);
                 setTimeout(getNextNode(),500);
                //console.log(node_parent.text+" nbchilds="+node_parent.nodes.length+ " -- "+node_parent.nb_childs);
             });    
        }
        

        function getChildNodes(parentNode){
                var node_parent = getNode(parentNode['id']);
                if(node_parent.nodes==undefined){
                  //console.log(parentNode);
                  $.post("NCBIgetNode.php", {  id_root:parentNode['id'] ,nbsearch:count() }, function(data,status){
                      childs=data.split(String.fromCharCode(10));
                      childs.forEach(function(child) {
                        if(child.length>0){
                          elt=child.split(':');
                          if(elt.length>=3) {
                            var name=elt[2];  if(elt.length>3) for(var j=3;j+=1;j<elt.length) name+=':'+elt[j];
                            new_node=createNode(name,elt[0],elt[1],node_parent.id);
                            $('#treeview').treeview('addNode', [ new_node, node_parent, true , { silent: true }]);                            
                           }
                         }
                      });
                      if(node_parent.nodes.length<node_parent.nb_childs)  moreNode(node_parent);
                      //console.log(node_parent.text+" nbchilds="+node_parent.nodes.length+ " -- "+node_parent.nb_childs);
                      $('#treeview').treeview('collapseNode', [ [node_parent] , { silent: true }]);
                      $('#treeview').treeview('checkNode', [ [node_parent], { silent: true }]);
                      //node_parent.state.checked=true;
                      //console.log(node_parent.state.checked);//#: 'glyphicon glyphicon-refresh glyphicon-refresh-animate',
                      setTimeout(getNextNode(),500);
                   });
                }
        }


      
        /*function getSearchNode(id_search){
            if(SearchNodes[id_search]!=undefined) return SearchNodes[id_search];
            SearchNodes.forEach(function(child) { 
              if(child.id==id_search)return child;
             });
            return undefined;

        }*/
        //NOW FOR SEARCH FUNCTIONS
         function createNodes(node_to_add){
          if(node_to_add==undefined) return node_to_add;
          var new_node=getNode(node_to_add.id);
          if(new_node!=undefined) return new_node;
          //console.log("createNodes "+node_to_add.text);
          if(node_to_add.id_parent=="") return getNode(node_to_add.id); //ROOT 
          if(node_to_add.id_parent==node_to_add.id) return getNode(node_to_add.id); //ROOT 
          //console.log("id_parent "+node_to_add.id_parent);
          var node_parent=getNode(node_to_add.id_parent);
          //console.log("node_parent=="+node_parent);
          if(node_parent==undefined)   node_parent=createNodes(SearchNodes[node_to_add.id_parent]);
           if(node_parent==undefined)  {
                //SearchNodes.forEach(function(child) { console.log(child);  });
                console.log("ERROR");
                console.log(SearchNodes[node_to_add.id_parent]);
              }
          //console.log("node_parent="+node_parent.text);
         
          $('#treesearch').treeview('addNode', [ node_to_add, node_parent, true , { silent: true }]);
          new_node=getNode(node_to_add.id);
          //console.log("new_node->");console.log(new_node);

          delete SearchNodes[node_to_add.id];
          //console.log("delete " +node_to_add.id); 
          return new_node;
         }
        
        function count(){
            var nbsearch=parseInt($('#nb-search').val(),0);
            if (isNaN(nbsearch)){ nbsearch=10;$('#nb-search').val('10');}
            return nbsearch;
          }

         
         
          SearchNodes=[];
         function updateTree(){
                $('#treesearch').hide();
                $('#treeview').hide();
                $('#waitsearch').show();
                wtree='treesearch';
                var search=$('#input-search').val();
                $('#treesearch').treeview('remove');
                init_Tree('treesearch');
                init_Search_Event();

    

                $.post("NCBIgetNode.php", {  search:search ,nbsearch:count() }, function(data,status){
                    childs=data.split(String.fromCharCode(10));
                    SearchNodes=[];
                    ///console.log(data);
                    childs.forEach(function(child) {
                        if(child.length>0){
                          elt=child.split(':');
                          if(elt.length>=4) { //$id.':'.$id_parent.':'.$nb child .':'.$name;
                            var name=elt[3];  if(elt.length>4) for(var j=4;j+=1;j<elt.length) name+=':'+elt[j];
                            new_node=createNode(name,elt[0],elt[2],elt[1]);
                            SearchNodes[new_node.id]=new_node;
                          }
                         }
                    });
                   SearchNodes.forEach(function(child) {   createNodes(child);  });
                   $('#waitsearch').hide();
                   $('#treesearch').show();
                  });
                  
          }