#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys
import MySQLdb
from time import gmtime, strftime
import errno
import bz2
import zipfile

def mymkdir(dirname):
    try:
        os.mkdir(dirname)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
        pass
        
        
id_people=int(sys.argv[1])
id_dataset=int(sys.argv[2])
uploadPath="UploadDATASET/"
main_path=uploadPath+"DATASET_"+str(id_dataset)+"/"
mesh_path=main_path+"MESH/"
desc_path=main_path+"DESC/"




#BUNDLE STATUS
bundleOK=0
bundleCreation=-2; #UNDER CREATION
bundleUpload=-3; #UNDER UPLOAD 
bundleConversion=-4; #UNDER Conversion 
bundleError=-5; #UNDER ERROR 


def changeStatus(status):
	cursor.execute('UPDATE dataset SET bundle='+str(status)+' WHERE id='+str(id_dataset)+' AND  id_people='+str(id_people));
	db.commit()

def write(filename,text):
	text_file = open(filename, "w")
	text_file.write(text)
	text_file.close()
    
db = MySQLdb.connect(host="localhost",    user="user_morphonet",    passwd="password_morphonet",   db="MorphoNet",port=80)     
cursor = db.cursor()


def moveSubDirTo(sub_path,dest_path):
	#print "-> moveSubDirTo "+sub_path+ " TO "+dest_path
	for f in os.listdir(sub_path):
		if os.path.isdir(sub_path+f):
			moveSubDirTo(sub_path+f+"/",dest_path)
		else:
			if dest_path!=sub_path:
				print "->  move "+sub_path+f + " TO " + dest_path+f
				os.rename(sub_path+f,dest_path+f)
	if sub_path!=dest_path:
		os.rmdir(sub_path)

def unzipPath(path):
	for f in os.listdir(path):
		if not os.path.isdir(path+f):
			filename, file_extension = os.path.splitext(path+f)
			if file_extension==".zip":
				print 'Unzip '+path+f
				zip_ref = zipfile.ZipFile(path+f, 'r')
				zip_ref.extractall(path)
				zip_ref.close()
				os.remove(path+f)
			elif file_extension==".tar.gz":
				print 'Un targz '+path+f
				os.system('tar -xf '+path+f)
				os.remove(path+f)
			elif file_extension==".gz":
				print 'gunzip '+path+f
				os.system('gunzip '+path+f)
				

def removeInDir(path):
	for f in os.listdir(path):
		if os.path.isdir(path+f):
			removeInDir(path+f+"/")
			os.rmdir(path+f)
		else:
			os.remove(path+f)
		
	
    


#DESCRIPTION
print "Process Information files "
cursor.execute('DELETE FROM  correspondence WHERE id_dataset='+str(id_dataset)) #TEMP
db.commit()
if os.path.isdir(desc_path):
	unzipPath(desc_path) #UNZIP FIRST
	moveSubDirTo(desc_path,desc_path) #NOW WE MOVE ALL SUBDIRECTORES TO DESC PATH	

	#NOW WE UPLOAD
	for f in os.listdir(desc_path):
		if not os.path.isdir(desc_path+f):
			filename, file_extension = os.path.splitext(desc_path+f)
			if file_extension==".txt" and f[0]!=".":
				name=f.replace(".txt","");
				field = open(desc_path+f, 'r').read()
				now=strftime("%Y-%m-%d %H:%M:%S", gmtime())
	
	
				tab=field.split('\n')
				nbL=0
				datatype=""
				while datatype=="" and nbL<len(tab):
					if len(tab[nbL])>0:
						types=tab[nbL].split(":")
						if len(types)==2 and types[0]=="type":
							datatype=types[1]
					nbL+=1
				if datatype=="":
					print('ERROR , You did not specify your type inside the file '+f)
				else:
					dtype=2 #TYPE =1 For direclty load upload and 2 for load on click
					if datatype=="time" or datatype=="group"  or datatype=="space" :
						dtype=1


					cursor.execute('INSERT INTO correspondence (id_dataset,infos,field,date,id_people,datatype,type) VALUES ('+str(id_dataset)+',"'+name+'","'+MySQLdb.escape_string(bz2.compress(field))+'","'+now+'",'+str(id_people)+',"'+datatype+'",'+str(dtype)+')')
					db.commit()
					print "UPLOAD Infos " + f
	

#MESH	
print "Process Mesh files "
cursor.execute('DELETE FROM  mesh WHERE id_dataset='+str(id_dataset)) #TEMP
db.commit()
if os.path.isdir(mesh_path):
	unzipPath(mesh_path) #UNZIP FIRST
	moveSubDirTo(mesh_path,mesh_path) #NOW WE MOVE ALL SUBDIRECTORES TO DESC PATH	
	minT=1000000
	maxT=0
	quality=0
	#NOW WE UPLOAD
	for f in os.listdir(mesh_path):
		if not os.path.isdir(mesh_path+f):
			filename, file_extension = os.path.splitext(f)
			if file_extension==".obj" and f[0]!=".":
				obj = open(mesh_path+f, 'r').read()
				objA=obj.split("\n")
				X=0.0; Y=0.0; Z=0.0; nb=0;
				nbl=0
				t=0
				channel=0
				for line in objA:
					if line.find("mtllib")==-1 and line.find("usemtl")==-1:
						if len(line)>2 and line[0]=='v' and line[1]!='n' :
							while line.find("  ")>=0:
								line=line.replace("  "," ")
							line=line.replace(",",".")
							tab=line.split(" ")
							X+=float(tab[1])
							Y+=float(tab[2])
							Z+=float(tab[3])
							nb+=1
						elif len(line)>2 and line[0]=='g'  : #OBJECT 
							while line.find("  ")>=0:
								line=line.replace("  "," ")
							tab=line.split(" ")
							obbject=tab[1].split(",")
							if len(obbject)==3: #T,ID,CHANNEL
								t=int(obbject[0])
								channel=obbject[2]
							elif len(obbject)==2: #T,ID
								t=int(obbject[0])
							if t>maxT:
								maxT=t
							if t<minT:
								minT=t
							#print str(minT)+ ' < '+ str(t)+ ' < ' +str(maxT)
					else:
						objA[nbl]=""
					nbl+=1
						
				X/=nb
				Y/=nb
				Z/=nb
		
				print  f+'-> X='+str(X)+' Y='+str(Y)+" Z="+str(Z)
				newOBJ="\n".join(objA)
				
				now=strftime("%Y-%m-%d %H:%M:%S", gmtime())

				cursor.execute('INSERT INTO mesh (id_dataset,t,channel,quality,obj,date,center) VALUES ('+str(id_dataset)+','+str(t)+',"'+str(channel)+'",'+str(quality)+',"'+MySQLdb.escape_string(bz2.compress(newOBJ))+'","'+now+'","'+str(round(X,2))+','+str(round(Y,2))+','+str(round(Z,2))+'")')
				db.commit()	
				

				
if minT!=1000000:
	cursor.execute('UPDATE dataset SET minTime='+str(minT)+' WHERE id='+str(id_dataset))
	db.commit()	
if maxT!=0:
	cursor.execute('UPDATE dataset SET maxTime='+str(maxT)+' WHERE id='+str(id_dataset))
	db.commit()		

print(" Time Min "+str(minT)+ " Max "+str(maxT))		

changeStatus(bundleOK)

removeInDir(main_path) #REMOVE ALL OBJ
os.rmdir(main_path) #REMOVE DIRECTOTY UPLOAD

cursor.close()
db.close()
