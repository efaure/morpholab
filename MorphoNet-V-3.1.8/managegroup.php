<?php

session_start();

include 'functions.php';
$now=new DateTime();
$id_owner=$_SESSION['ID'];

$id_group=-1;
$isAdmin=0;


if ($connected){
  $isAdmin=getAdmin($_SESSION['ID']);
 
  $id_group_admin=query_first('SELECT id FROM groups WHERE name="admin"');
  if(isset($_POST['createnewgroup'])){ //CREATE A NEW GROUP
      $id_group=query_id('INSERT INTO groups (name,id_owner,date,privacy) VALUES ("'.$_POST['createnewgroup'].'",'.$id_owner.',"'.$now->format('Y-m-d h:s:i').'","'.$_POST['private'].'")');
      query('INSERT INTO people_group (id_people,id_group,status,date) VALUES ('.$id_owner.','.$id_group.',"member","'.$now->format('Y-m-d h:s:i').'")');
      query('INSERT INTO people_group (id_people,id_group,status,date) VALUES ('.$id_owner.','.$id_group.',"manager","'.$now->format('Y-m-d h:s:i').'")');
      
  }
  if(isset($_POST['archiveGroup'])){ //CREATE A NEW GROUP
      query('UPDATE groups set status=-1 WHERE id='.$_POST['archiveGroup']);
  }
  if(isset($_POST['activegroup'])){ //CREATE A NEW GROUP
      query('UPDATE groups set status=0 WHERE id='.$_POST['activegroup']);
  }
  if(isset($_POST['deletegroup'])){ //CREATE A NEW GROUP
      query('DELETE FROM groups  WHERE id='.$_POST['deletegroup']);
      query('DELETE FROM people_group WHERE id_group='.$_POST['deletegroup']);
  }

  if(isset($_POST['private'])){ //MAKE GROUP PRIVATE
      query('UPDATE groups set privacy="private" WHERE id='.$_POST['private']);
  }
  if(isset($_POST['public'])){ //MAKE GROUP PUBLIC
      query('UPDATE groups set privacy="public" WHERE id='.$_POST['public']);
  }
  

  if(isset($_POST['visualizegroup'])){ //CREATE A NEW GROUP
      $id_group=$_POST['visualizegroup'];
  }

  $groups = query_array('SELECT id,name,id_owner,date,status,privacy FROM groups ');
  $peoples = query_array('SELECT id,name,surname,id_Institution,id_Laboratory,id_Team FROM people WHERE exist=1 ORDER by name,surname');
  $Institutions = query_array('SELECT id,name FROM Institution');
  $Laboratorys = query_array('SELECT id,name FROM Laboratory');
  $Teams = query_array('SELECT id,name FROM Team');
  
} else echo "PROBLEM CONNECTION</br>";

//echo "id_group=$id_group</br>isAdmin=$isAdmin</br>";
//print_r($groups);




?>

<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MorphoNet</title>
    <script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="jquery/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/switch.css">
    <link rel="stylesheet" href="css/morphonet.css">
  </head>

  <body>
    <?php include "headbar.php"; ?>


    <div  style="width: 100%;text-align: center;">
      <span style="font-size:30px;color:grey;">Groups Manager </span>
      <input type=image width="75px" height="75px"  src="images/newgroup.png" style="vertical-align:middle;margin-left:20px"  title="Create a new group " onclick="showNewGroup()"/>
    </div>

    
    <?php if($id_group<0){ ?>
     <!-- CREATE A GROUP -->
     <div id="newgroup"  style="display: none; width: 100%;margin-top:50px">
          <form action="managegroup.php" method="post" enctype="multipart/form-data"  onsubmit="return validateCreateGroup()" style="width:600px;margin: 0 auto;margin-top:20px">
            <table >
              <tr></tr>
            <tr> 
              <td> <span style="font-size:25px;color:grey;">Create a new group </span></td>
              <td> <input id="createnewgroup" name="createnewgroup" width="200px" height="50px" style="font-size:15px;margin-left:20px;margin-right:20px" placeholder="Enter group name" /></td>
            </tr>
               <tr></tr>
            <tr>  
             <td>  <span style="font-size:25px;color:grey;margin-left:80px">is private ?</span></td>
             <td>  <label class="switch" style="margin-left:10px">   <input id="privatechecked" type="checkbox" checked="" onchange="makePrivate()" > <span class="slider round"></span> </label>
             <input id="private" name="private"  style="display:none" value="private"/>
           </td>
             </tr>
             <tr></tr>
            <tr> 
              <td></td><td>    <input type="submit" value="Create"  class="myButton"/> </td>
            </tr>
          </table>
         </form>
    </div>
  
    <!-- LIST ALL GROUPS -->
    <div id="pickgroup" style=" display: table; width: 100%;margin-top:50px">
     <div style=" width: 600px;margin: 0 auto;margin-top:20px">
    <table id="listgroups">
        <tr>
          <td class="tdtitle"> Users</td>
          <td class="tdtitle"> Actions</td>
          <td class="tdtitle"> Name</td>
          <td class="tdtitle"> Private </td>
          <td class="tdtitle"> Create by </td>
          <td class="tdtitle"> Creation date  </td>
        </tr>
        <?php
        foreach ($groups as $id_group_t => $group){
          $isGroupManager=0;
          $id_grpadmin=query_first('SELECT id FROM people_group WHERE id_people='.$id_owner.' AND id_group='.$id_group_t.' AND status="manager"');
          if($id_grpadmin!="") $isGroupManager=1;
          $isPrivate=0;  if($group['privacy']=="private") $isPrivate=1;

          echo "<tr>";
          //USERS
           if($group['status']==0) {    //NOT ARCHIVED
              if($isAdmin || $isGroupManager || !$isPrivate) { echo '<td><input type=image width="25px" height="25px"  style="margin-left:10px" src="images/eye.png"  onclick="visualizegroup('.$id_group_t .')"//></td>'; }
              else echo "<td></td>"; //PRIVATE
            }  else echo "<td>Archived</td>"; //ARCHIVED
          
          //ACTIONS
            echo "<td>";
          if($id_group_admin!=$id_group_t) { //NOT THE ADMIN GROUP
              if($isAdmin || $isGroupManager){
                if($group['status']==0) {
                  echo '<input type=image width="25px" height="25px"  style="margin-left:10px" src="images/garbage.png" onclick="archiveGroup('.$id_group_t .',\''.$group['name'].'\')"/>';
                }else{
                   echo '<input type=image width="25px" height="25px"  style="margin-left:10px" src="images/recycle.png" onclick="activegroup('.$id_group_t .')"/>';
                   if($isAdmin) echo '<input type=image width="25px" height="25px"  style="margin-left:10px" src="images/delete.png" onclick="deletegroup('.$id_group_t .',\''.$group['name'].'\')"/>';
                }
             }
           }
          echo "</td>";
        
          echo "<td>".$group['name']."</td>"; 
          //PRIVATE
          echo "<td>";
            
            if($isAdmin || $isGroupManager){
                  echo '<label class="switch">   <input id="private_'.$id_group_t.'" type="checkbox" '; 
                 if( $isPrivate ) { echo ' checked   onchange="onPublic('.$id_group_t.')" '; }
                  else { echo '    onchange="onPrivate('.$id_group_t.')" '; }
                  echo '> <span class="slider round"></span> </label>'; 
             }else {
                if( $isPrivate ) echo '<image src="images/valid.png" width="20px"  style="margin-left:20px"/>'; 
            }

          echo "</td>"; 

          echo "<td>".$peoples[$group['id_owner']]["name"].' '.$peoples[$group['id_owner']]["surname"]."</td>"; 
          echo "<td>".$group['date']."</td>";  
          echo "</tr>";
        }
      ?>
      </table>
    </div>
    </div>

    <?php } else {  //VIEW A SPECIFIC GROUPS LIST USERS
      $isGroupManager=0;
      $id_grpadmin=query_first('SELECT id FROM people_group WHERE id_people='.$id_owner.' AND id_group='.$id_group.' AND status="manager"');
      if($id_grpadmin!="") $isGroupManager=1;
      if($isAdmin) $isGroupManager=1;
      //CHECK STATUS
      $result = my_query('SELECT id,id_people,status FROM people_group WHERE id_group='.$id_group);
      $member = array();
      $manager = array();
      if ($result) {
        while($r = mysqli_fetch_assoc($result)) {
          if($r['status']=="member")$member[$r['id_people']]=true;
          if($r['status']=="manager")$manager[$r['id_people']]=true;
        }
      }
      $people_group = query_array('SELECT id,id_people,status FROM people_group WHERE id_group='.$id_group);

      $group=$groups[$id_group];
      $isPrivate=0;  if($group['privacy']=="private") $isPrivate=1;
    ?>
    <div id="group"  style=" display: table; width: 100%;margin-top:50px">
     <div style=" width: 600px;margin: 0 auto;margin-top:20px">
      <span style="font-size:25px;color:grey; width: 100%">Group <?php echo $group['name'];?> </span>
      <span style="font-size:15px;color:black; width: 100%">created the <?php echo $group['date'];?> </span>
      </br>
      <table id="listusers">
        <tr>
	        <td class="tdtitle"> Manager</td>
        <?php if($id_group_admin!=$id_group) echo "<td class='tdtitle'> Member </td>";   ?>
          <td class="tdtitle"> Name </td>
          <td class="tdtitle"> SurName </td>
          <td class="tdtitle"> Institution </td>
          <td class="tdtitle"> Laboratory </td>
          <td class="tdtitle"> Team </td>
        </tr>
        <?php
        foreach ($peoples as $id_people => $people){
          $showThisGuy=0;  
          if($isGroupManager)$showThisGuy=1;
          else {  if(array_key_exists($id_people,$manager) || array_key_exists($id_people,$member)) $showThisGuy=1;  }
          if(!$isPrivate && $id_owner==$id_people) $showThisGuy=1;
          if($showThisGuy){
            echo "<tr>";
          
          $isManager=array_key_exists($id_people,$manager);
           //MANAGER
            if($isGroupManager){
                  echo '<td><div id="emanager_'.$id_people.'" > <label class="switch">   <input id="manager_'.$id_people.'" type="checkbox" '; 
                  if($isManager) echo " checked ";
                  echo ' onchange="onChangeStatus('.$id_people.','.$id_group.',\'manager\')"> <span class="slider round"></span> </label></div></td>'; //Manager
            }
            else{
                echo '<td>';
                if($isManager) echo '<image src="images/valid.png" width="30px" style="margin-left:20px"/>';
                echo '</td>';
            }

            //MEMBER
            $isMember=array_key_exists($id_people,$member);
            if($id_group_admin!=$id_group){
                if($isGroupManager  || (!$isPrivate && $id_owner==$id_people) ){
                    echo '<td> <div id="emember_'.$id_people.'" '; if($isManager) echo ' style="display:none" ';
                    echo '><label class="switch">   <input id="member_'.$id_people.'" type="checkbox" ';
                    if($isMember) echo " checked ";
                    echo ' onchange="onChangeStatus('.$id_people.','.$id_group.',\'member\')"> <span class="slider round"></span> </label></div></td>'; //Member
                
                 }
                 else{
                    echo '<td>';
                    if($isMember) echo '<image src="images/valid.png" width="30px" style="margin-left:20px"/>';
                    echo '</td>';
                 }
               
            }
            echo "<td>".$people['name']."</td>"; 
            echo "<td>".$people['surname']."</td>"; 
            echo "<td>"; if(array_key_exists($people['id_Institution'], $Institutions)) echo $Institutions[$people['id_Institution']]; echo "</td>"; 
            echo "<td>"; if(array_key_exists($people['id_Laboratory'], $Laboratorys)) echo $Laboratorys[$people['id_Laboratory']]; echo "</td>"; 
            echo "<td>"; if(array_key_exists($people['id_Team'], $Teams)) echo $Teams[$people['id_Team']]; echo "</td>";   
            echo "</tr>";
          }
      }
      ?>
      </table>
    </div>
    </div>
    <?php } ?>
    
     <?php include "footer.php"; ?> 
     <script>
      function showNewGroup(){  $("#newgroup").show(); $("#pickgroup").hide(); }
       function deletegroup(id_group,nameg){  
        if(confirm("Are you sure to delete the group " +nameg+ "? "))
         window.open('managegroup.php?deletegroup='+id_group,'_self',false); 
     }
      function activegroup(id_group){  window.open('managegroup.php?activegroup='+id_group,'_self',false); }
       function visualizegroup(id_group){  window.open('managegroup.php?visualizegroup='+id_group,'_self',false); }
      function onPrivate(id_group){  window.open('managegroup.php?private='+id_group,'_self',false); }
      function onPublic(id_group){  window.open('managegroup.php?public='+id_group,'_self',false); }
     
     function makePrivate(){
        if($('#private').val()=="private") $('#private').val("public");
        else $('#private').val("private");

      }

     function archiveGroup(id_group,nameg){  
        if(confirm("Are you sure to archive the group " +nameg+ "? "))
         window.open('managegroup.php?archiveGroup='+id_group,'_self',false); 
     }

     

      
    function validateCreateGroup(){
        var Groups = [ <?php  
        $i=0;
        foreach($groups as $group) {
          echo '"'.strtolower($group['name']).'"'; 
          $i+=1;
          if($i<sizeof($groups)) echo ",";
         }
       ?> ];
        var groupname=$("#createnewgroup").val();
        groupname=groupname.trim().toLowerCase();
        if(groupname=="") { alert("You must specify a group name");return false;}
        if(Groups.indexOf(groupname)>-1) { alert("This group already exist ");return false;}
        return true;
       }
       function onChangeStatus(id_people,id_group,sta){
        var v=$('#'+sta+'_'+id_people).is(":checked");
        $.post("manageadmin.php", {
              add_group:v,
              status:sta,
              id_people:id_people,
              id_group:id_group
            }, function(data,status){ 
              if(sta=="manager") {
                if(v) $('#emember_'+id_people).hide();
                else $('#emember_'+id_people).show();
              }
            } 
          );
       }
       
  
    </script>
    
  </body>
</html>

<?
mysqli_close($link);


?>