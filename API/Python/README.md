# MorphoNet Python API

This API is used to interact with MorphoNet with python script.

You can find the help in the [HelpAPI.html](HELP/HelpAPI.html) file with a small example of API use for upload.


## Dependencies installation

This API work on [python 2.7](https://www.python.org/download/releases/2.7/)


* [bz2](https://docs.python.org/3/library/bz2.html)
* [httplib](https://docs.python.org/2/library/httplib.html)
* [urllib](https://docs.python.org/2.7/library/urllib.html)
* [hashlib](https://docs.python.org/2.7/library/hashlib.html)
* [json](https://docs.python.org/fr/2.7/library/json.html)
* [requests](http://docs.python-requests.org/en/master/)

## Recommandation

We recommend to use the python packages manager [pip](https://pypi.org/project/pip/)
```
apt-get install python-pip
```
