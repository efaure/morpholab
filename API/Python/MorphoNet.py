#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
import os
import time
import bz2
import httplib,urllib
import hashlib
import json
import requests


def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def tryParseInt(value):
    try:
        return int(value), True
    except ValueError:
        return value, False

class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

ss=" --> "
def strblue(strs):
    return bcolors.BLUE+strs+bcolors.ENDC
def strred(strs):
    return bcolors.RED+strs+bcolors.ENDC
def strgreen(strs):
    return bcolors.BOLD+strs+bcolors.ENDC


class MorphoNet:

    #COMMUNICATION PARAMETERS TO MorphoNet
    url="www.morphonet.org"
    #url="localhost" #For DB installed in local
    apipath="/api.php"
    port=80
    Hash="34morpho:"
    headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
    def __init__(self,login,passwd): 
        
        self.id_people=-1
        self.id_dataset=-1
        self.id_dataset_owner=-1
        self.minTime=-1
        self.maxTime=-1
        self.login=login
        self.passwd=passwd
        self.bundle=0
        self.id_NCBI=1 #0 -> Unclassified 
        self.id_type=0; #0 Observed Data, 1 Simulated Data, 2 Drawing Data
        self.guys={}
        self.datasettype={}

        
    #CONNECTION
    def setUrl(self,newurl,path="",port=-1):
        self.url=newurl
        if path!="":
            self.apipath=path
        if port!=-1:    
            self.port=port  
    def connect(self):
        #httplib.HTTPConnection.debuglevel = 1
        if self.url=="localhost":
            conn = httplib.HTTPConnection(self.url+":%d" % self.port)
        else:
            conn = httplib.HTTPConnection(self.url,self.port)
        params = urllib.urlencode({'hash': self.Hash, 'port':self.port,'login': self.login, 'passwd': hashlib.md5(self.passwd).hexdigest()})
        conn.request("POST", self.apipath,params, self.headers)
        response=conn.getresponse()
        if response.status==200:
            data=response.read()
            conn.close()
        
            if len(data)>4 and data[0:5]=="ERROR":
                print(strred(data))
                return False
            else:
                self.id_people,ok=tryParseInt(data)
                print(strblue(self.login+' is connected to MorphoNet'))
                return True
        else:
            print(strred('CONNECTION ERROR '+str(response.status)+" "+response.reason))
        conn.close()
        return False
    def _isConnected(self):
        if self.id_people==-1:
            print(strred(' ERROR : You are not connected '))
            return False
        return True
    def _request(self,param):
        if self._isConnected():
            if self.url=="localhost":
                conn = httplib.HTTPConnection(self.url+":%d" % self.port,timeout=100)
            else:
                conn = httplib.HTTPConnection(self.url,self.port,timeout=100)
            #Add Default Parameters
            param['hash']=self.Hash
            param['port']=self.port
            param['login']=self.login
            param['passwd']=hashlib.md5(self.passwd).hexdigest()
            params = urllib.urlencode(param)
            try:
                conn.request("POST", self.apipath,params, self.headers)
                response=conn.getresponse()
                if response.status==200:
                    da=response.read()
                    conn.close()
                    return da
                else:
                    print(strred('CONNECTION ERROR '+str(response.status)+" "+response.reason))
                    quit()
            except  Exception as e:
                print('Error cannot request ... '+str(e))
                time.sleep(5)
                print(' --> Retry')
                return self._request(param)
            conn.close()   
    def _large_request(self,param,data):
        if self._isConnected():
            param['hash']=self.Hash
            param['port']=self.port
            param['login']=self.login
            param['passwd']=hashlib.md5(self.passwd).hexdigest()
            params = urllib.urlencode(param)
            try:
               
                f=open("temp.bzip","w")
                f.write(data)
                f.close()

                files = {'file': open("temp.bzip", 'rb')}
                session = requests.Session()
                del session.headers['User-Agent']
                del session.headers['Accept-Encoding']
                r = session.post("http://"+self.url+self.apipath, files=files,data=param)
                if r.status_code == requests.codes.ok:
                    return r.text
                else:
                    print(strred('CONNECTION ERROR '+str(r.status_code)))
                    quit()
                if os.path.isfile("temp.bzip"):
                    os.system('rm -f temp.bzip')
            except  Exception as e:
                print('Error cannot request ... '+str(e))
                quit()
     
    #LOOK FOR SOMEONE IN THE DATABASE
    def getGuys(self): 
        data=self._request({"guys":1})
        datas=json.loads(data)
        for g in datas:
            self.guys[int(g[0])]=g[1].encode('utf-8')+" "+g[2].encode('utf-8')    
    def getGuyByID(self,id_guy): # RETURN NAME + SURNAME + LOGIN FORM SPECIFIC ID
        id_guy=int(id_guy)
        if self.guys=={}:
            self.getGuys()
        if id_guy in self.guys:
            return self.guys[id_guy]
        data=self._request({"who":id_guy})
        if data=="[]":
            return strred("unkown")
        else:
            return data
    def getGuyByName(self,name): # RETURN NAME + SURNAME + LOGIN FORM SPECIFIC ID
        data=self._request({"whoname":name})
        if data=="":
            print(strred('Please input as "name surname"'))
            quit()
        if data=="[]":
            print(strblue(ss+str(name)+" is unkown"))
            return -1
        else:
            dataset=json.loads(data)[0]
            return int(dataset[0])
    #NCBI Taxonomy
    def _getNCBITypeByID(self,id_NCBI):
        if self._isConnected():
            data=self._request({"getncbitype":id_NCBI})
            return data
    #TYPE 
    def _getTypeName(self,id_type): #0 Observed Data, 1 Simulated Data, 2 Drawing Data
        if id_type==0:
            return "Observed Data"
        if id_type==1:
            return "Simulated Data"
        if id_type==2:
            return "Drawing Data"
        return "Unknown Data"


    #DATASET 
    def _isDataSet(self):
        if not self._isConnected():
            return False
        if self.id_dataset==-1:
            print(strgreen(ss+'you first have to select a dataset'))
            return False
        return True
    def _ownDataSet(self):
        if not self._isDataSet():
            return False
        if str(self.id_dataset_owner)!=str(self.id_people):
            print(strgreen(ss+'you are not the owner of this dataset, ask '+self.getGuyByID(self.id_dataset_owner)))
            return False
        return True  
    def _initTimePoint(self,minTime,maxTime): #INTERNAL FUNCTION TO INITIALISE TIME POINT
        self.minTime=int(minTime)
        self.maxTime=int(maxTime)
    def _parseDataSet(self,data): #Parse id,name,minTime,maxTime,id_people to dataset structure
        if data=="[]" or data=="":
            print(strred(ss+'dataset not found'))
        else:
            dataset=json.loads(data)[0]
            ids,ok=tryParseInt(dataset[0])
            if not ok: 
                print(strgreen(ss+'dataset not found '+data))
            else: 
                name=dataset[1]
                self._initTimePoint(dataset[2],dataset[3])
                self.id_dataset_owner=dataset[4]
                self.bundle,ok=tryParseInt(dataset[5])
                self.id_NCBI,ok=tryParseInt(dataset[6])
                self.id_type,ok=tryParseInt(dataset[7])
                self.id_dataset=ids
                print(ss+'found dataset '+name+' with id ' + str(self.id_dataset)+' from '+str(self.minTime)+' to ' +str(self.maxTime)+' owned by '+str(self.id_dataset_owner)+' with type='+str(self.id_NCBI))
    def _showDataSets(self,data):
        dataset=json.loads(data)
        for datas in dataset:  #id,name,minTime,maxTime,id_people,bundle,id_NCBI,type,date
            #print(datas)
            s='('+datas[0]+') '+datas[1]
            if int(datas[2])!=int(datas[3]):
                s+=' from '+datas[2]+' to '+datas[3]
            s+=' is '+self._getTypeName(int(datas[7]))
            s+=' of '+self._getNCBITypeByID(datas[6])
            s+=' created by '+self.getGuyByID(datas[4])
            s+=' the '+datas[8]
            print(s)
    def listMyDataSet(self): #LIST ALL MY DATASET 
        if self._isConnected():
            data=self._request({"listmydataset":1})
            self._showDataSets(data)
    def listDataSet(self): #LIST ALL  DATASET 
        if self._isConnected():
            data=self._request({"listdataset":1})
            self._showDataSets(data)
    def createDataSet(self,dataname,minTime=0,maxTime=0,id_NCBI=0,id_type=0,spf=-1,dt=-1): #CREATE A NEW DATA SET
        self.id_NCBI=id_NCBI
        self.id_type=id_type
        if self._isConnected():
            if self.bundle!=0:
                data=self._request({"createdataset":dataname,"minTime":minTime,"maxTime":maxTime,"bundle":self.bundle,"id_NCBI":self.id_NCBI,"id_type":self.id_type,"spf":spf,"dt":dt})
            else:
                data=self._request({"createdataset":dataname,"minTime":minTime,"maxTime":maxTime,"id_NCBI":self.id_NCBI,"id_type":self.id_type,"spf":spf,"dt":dt})
            self.id_dataset_owner=self.id_people
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : Dataset not created '+data))
            else :
                self.id_dataset=ids
                self.id_dataset_owner=self.id_people
                self._initTimePoint(minTime,maxTime)
                print(ss+"your id dataset '"+dataname+"' is created (with id "+str(self.id_dataset)+')')
    def uploadDescription(self,description): #Upload a description 
        if self._ownDataSet():
            data=self._request({"uploadescription":self.id_dataset,"description":description})
            print(data)        
    def updateDataSet(self,dataname="",minTime=-1,maxTime=-1,id_NCBI=-1,id_type=-1): #COMPLETE DELETE OF A DATASET
        if dataname!="":
            self.dataname=dataname
        if minTime!=-1:
            self.minTime=minTime
        if maxTime!=-1:
            self.maxTime=maxTime
        if id_NCBI!=-1:
            self.id_NCBI=id_NCBI 
        if id_type!=-1:
            self.id_type=id_type 
        if self._ownDataSet():
            data=self._request({"updatedataset":self.id_dataset,"minTime":self.minTime,"maxTime":self.maxTime,"id_NCBI":self.id_NCBI,"id_type":self.id_type,"dataname":self.dataname})
            if data=="": 
                self._initTimePoint(self.minTime,self.maxTime)
            else:
                print(strred(' ERROR : '+data))
    def selectDataSetById(self,ids): #SELECT A DATASET BY ID
        if self._isConnected():
            self.id_dataset=-1
            data=self._request({"dataset":ids})
            self._parseDataSet(data)           
    def selectDataSetByName(self,name): #SELECT A DATASET BY NAME
        if self._isConnected():
            self.id_dataset=-1
            data=self._request({"datasetname":name})
            self._parseDataSet(data)
    def deleteDataSet(self): #COMPLETE DELETE OF A DATASET
        if self._ownDataSet():
            data=self._request({"deletedataset":self.id_dataset})
            if data=="": 
                print(ss+'dataset deleted')
                self.id_dataset=-1
                self.id_dataset_owner=-1
                self.minTime=-1
                self.maxTime=-1
            else:
                print(strred(' ERROR : '+data))
    def clearDataSet(self): # CLEAR ALL TIME POINT AND INFOS
        if self._ownDataSet():
            data=self._request({"cleardataset":self.id_dataset})
            if data=="": 
                print(ss+'dataset cleared')
            else:
                print(strred(' ERROR : '+data))
    def getNumberofMeshAt(self,t,quality=-1,channel=-1):
        if self._ownDataSet():
            data=self._request({"getnumberofmeshat":self.id_dataset,"t":t,"quality":quality,"channel":channel})
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : cannot count number of mesh'))
            else :
                return ids
    def clearMeshAt(self,t,quality=-1,channel=-1):
        if self._ownDataSet():
            data=self._request({"clearmeshat":self.id_dataset,"t":t,"quality":quality,"channel":channel})
            if data=="": 
                if quality==-1 and channel==-1:
                    print(ss+'mesh cleared at '+str(t))
                elif quality==-1:
                    print(ss+'mesh cleared at '+str(t)+ " with channel "+str(channel))
                elif channel==-1:
                    print(ss+'mesh cleared at '+str(t)+ " with quality "+str(quality))
                else:
                    rint(ss+'mesh cleared at '+str(t)+ " with quality "+str(quality)+ " and channel "++str(channel))
            else:
                print(strred(' ERROR : '+data))

    def _computeCenter(self,obj):
       objA=obj.split("\n")
       X=0.0; Y=0.0; Z=0.0; nb=0;
       for line in objA:
           if len(line)>2 and line[0]=='v' and line[1]!='n'  and line[1]!='t' :
               while line.find("  ")>=0:
                   line=line.replace("  "," ")
               tab=line.strip().split(" ")
               if len(tab)==4:
                   X+=float(tab[1])
                   Y+=float(tab[2])
                   Z+=float(tab[3])
                   nb+=1
       if nb==0:
           print('ERROR your obj does not contains vertex ')
           quit()                
       X/=nb
       Y/=nb
       Z/=nb
       return str(round(X,2))+','+str(round(Y,2))+','+str(round(Z,2))
    def uploadMesh(self,t,obj,quality=0,channel=0,link="null"): #UPLOAD TIME POINT IN DATASET
        if self._ownDataSet():
            center=self._computeCenter(obj)
            #print("-> calcultate barycenter at "+str(center))
            if self.url=="localhost":
                data=self._request({"uploadmesh":self.id_dataset,"t":t,"quality":quality,"channel":channel,"center":center,"link":link,"data":bz2.compress(obj)})
            else:
                data=self._large_request({"uploadlargemesh":self.id_dataset,"t":t,"quality":quality,"channel":channel,"center":center,"link":link},bz2.compress(obj))
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : time point not uploaded '+data))
            else :
                print(ss+"meshes at time point "+str(t)+" uploaded ( with id "+str(ids)+' )')
            return ids
    def uploadMultipleMesh(self,t,obj,quality=0,channel=0): #UPLOAD TIME POINT IN DATASET
        if self._ownDataSet():
            center=self._computeCenter(obj)
            if self.url=="localhost":
                data=self._request({"uploadmultiplemesh":self.id_dataset,"t":t,"quality":quality,"channel":channel,"center":center,"data":bz2.compress(obj)})
            else:
                data=self._large_request({"uploadlargemultiplemesh":self.id_dataset,"t":t,"quality":quality,"channel":channel,"center":center},bz2.compress(obj))
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : time point not uploaded '+data))
            else :
                print(ss+"meshes at time point "+str(t)+" uploaded for quality "+str(quality)+ " ( with id "+str(ids)+' )')
            return ids
    def getMesh(self,t,quality=0,channel=0):
        if self._isDataSet():
            data=self._request({"getmesh":self.id_dataset,"t":t,"quality":quality,"channel":channel})
            obj=bz2.decompress(data)
            return obj

    #INFOS
    def showInfosType(self):
        MorphoFormat={}
        MorphoFormat ["time"] = " objectID:objectID";
        MorphoFormat ["space"] = "objectID:objectID";
        MorphoFormat ["float"] = "objectID:float";
        MorphoFormat ["string"] = "objectID:string";
        MorphoFormat ["group"] = "objectID:string";
        MorphoFormat ["selection"] = "objectID:int";
        MorphoFormat ["color"] = "objectID:r,g,b";
        MorphoFormat ["dict"] = "objectID:objectID:float";
        MorphoFormat ["sphere"] = "objectID:x,y,z,r";
        MorphoFormat ["vector"] = "objectID:x,y,z,r:x,y,z,r";
        print("\nUpload Type : ")
        for s in MorphoFormat:
            print("   "+s+'->'+MorphoFormat[s])
        print('   where objectID : <t,id,ch> or <t,id> or <id>')
        print('\n')
    def listInfos(self):
        if self._isDataSet():
            data=self._request({"listinfos":self.id_dataset})
            dataset=json.loads(data)
            return dataset
    def uploadInfos(self,infos,field): 
         if self._isDataSet():
            tab=field.split('\n')
            nbL=0
            datatype=""
            while datatype=="" and nbL<len(tab):
                if len(tab[nbL])>0:
                    types=tab[nbL].split(":")
                    if len(types)==2 and types[0]=="type":
                        datatype=types[1]
                nbL+=1
            if datatype=="":
                self.showInfosType()
                print('You did not specify your type inside the file')
                quit()
            dtype=2 #TYPE =1 For direclty load upload and 2 for load on click
            if datatype=="time" or datatype=="group"  or datatype=="space" :
                dtype=1
            if self.url=="localhost":
                data=self._request({"uploadcorrespondence":self.id_dataset,"infos":infos,"type":dtype,"datatype":datatype,"field":bz2.compress(field)})
            else:
                data=self._large_request({"uploadlargecorrespondence":self.id_dataset,"infos":infos,"type":dtype,"datatype":datatype},bz2.compress(field))
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : '+infos+' not uploaded '+data))
            else :
                print(ss+infos+" uploaded (with id "+str(ids)+')')
            return ids
    def deleteInfosByName(self,infos):
        if self._isDataSet():
            data=self._request({"deletecorrespondence":self.id_dataset,"infos":infos})
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : '+infos+' '+data))
            else :
                print(ss+infos+" with id "+str(ids)+" deleted")
    def deleteInfosById(self,idinfos):
        if self._isDataSet():
            data=self._request({"deletecorrespondenceid":self.id_dataset,"idinfos":idinfos})
            ids,ok=tryParseInt(data)
            if not ok: 
                print(strred(' ERROR : '+idinfos+' '+data))
            else :
                print(ss+"Infos with id "+str(ids)+" deleted")
    def getInfosByName(self,infos):
        if self._isDataSet():
            data=self._request({"getinfos":self.id_dataset,"infos":infos})
            infos=bz2.decompress(data)
            return infos
    def getInfosById(self,idinfos):
        if self._isDataSet():
            data=self._request({"getinfosid":self.id_dataset,"idinfos":idinfos})
            infos=bz2.decompress(data)
            return infos
    def _getObjects(self,infos):
        infos=infos.split('\n')
        objects={}
        for line in infos:
            if len(line)>0 and line[0]!="#":
                if line.find("type")==0:
                    dtype=line
                else:
                    tab=line.split(":")
                    objects[tab[0]]=tab[1]
        return objects
    def getObjectsFromInfosId(self,idinfos):
        infos=self.getInfosById(idinfos)
        return self._getObjects(infos)
    def getObjectsFromInfos(self,ninfos):
        infos=self.getInfosByName(ninfos)
        return self._getObjects(infos)




    
        

