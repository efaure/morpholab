# Integration of external Genetic Database in MorphoNet 

The API is an example of integration of a external genetic database in MorphoNet

## Requirements
* [MySQLdb](http://mysql-python.sourceforge.net/MySQLdb.html)
* [json](https://docs.python.org/fr/2.7/library/json.html)
* [requests](http://docs.python-requests.org/en/master/)

## Installation

You first have to create the corresponding database in you server.

```
mysql < ANISEED.sql

```

And then to synchronize it with the ANISEED API

```
python aniseed.py

```