#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import json, requests
import MySQLdb



def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def tryParseInt(value):
    try:
        return int(value), True
    except ValueError:
        return value, False

class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

ss=" --> "
def strblue(strs):
    return bcolors.BLUE+strs+bcolors.ENDC
def strred(strs):
    return bcolors.RED+strs+bcolors.ENDC
def strgreen(strs):
    return bcolors.BOLD+strs+bcolors.ENDC


#COMMUNICATION PARAMETERS TO ANISEED
url="https://www.aniseed.cnrs.fr/api/"

#MorphoNet Database
db = MySQLdb.connect(host="localhost",    user="user_morphonet",    passwd="password_morphonet",   db="Aniseed",port=80)     
cursor = db.cursor()



#INSERT ALL GENES
req=requests.get(url=url+"all_genes:?organism_id=112")
datas = json.loads(req.text)
cursor.execute('DELETE FROM anissed_all_genes') 
db.commit()
for data in datas:
	print data#['gene_model'] #gene_name #unique_gene_id
	cmdsql=('INSERT INTO anissed_all_genes (gene_model,gene_name,unique_gene_id) VALUES ("'+data['gene_model']+'","'+data['gene_name']+'","'+data['unique_gene_id']+'")')
	print cmdsql
	cursor.execute(cmdsql)
	db.commit()



#Create Stage 
cursor.execute('DELETE FROM anissed_all_stages') 
db.commit()
def createStage(stage):
	global cursor,db
	cmdsql='INSERT INTO anissed_all_stages (stage) VALUES ("Stage '+stage+'")'
	print cmdsql
	cursor.execute(cmdsql)
	db.commit()

for s in range(28):	
	if s==5 or s==6:
		createStage(str(s)+"a")
		createStage(str(s)+"b")
	else :
		createStage(str(s))



cursor.execute('SELECT id,stage FROM anissed_all_stages')
Stages={}
for (id,stage) in cursor:
	Stages[stage]=id

	
#First We Query All Cells By Stage
CellByStage={}
cursor.execute('SELECT name,cells,stage FROM developmental_lineage')
for (name,cells,stage) in cursor:
	couple=(name,stage)
	if couple not in CellByStage:
		CellByStage[couple]=cells
print 'Found ' +str(len(CellByStage)) +' couples '


cursor.execute('DELETE FROM anissed_cells_by_gene_by_stage'); db.commit()
cursor.execute('DELETE FROM anissed_mutant') ; db.commit()
cursor.execute('DELETE FROM anissed_mutant_cells_WT'); db.commit()
cursor.execute('DELETE FROM anissed_mutant_cells_mutant') ; db.commit()
cursor.execute('DELETE FROM aniseed_deregulations') ; db.commit()

cursor.execute('SELECT id,unique_gene_id,gene_name FROM anissed_all_genes')
for (id,unique_gene_id,gene_name) in cursor:

	print 'Process '+str(unique_gene_id)+ " "+str(gene_name)+ "->"+url+"all_territories_by_gene_WT_and_mutant:?gene="+unique_gene_id+"&organism_id=112"
	req=requests.get(url=url+"all_territories_by_gene_WT_and_mutant:?gene="+unique_gene_id+"&organism_id=112")
	all_genes = json.loads(req.text)
	nbCelInserted=0
	if all_genes is not None:
		for wt_mut in all_genes:
			if wt_mut=="WT": #WILDTYPE
				territories=all_genes[wt_mut]
				for territorie in territories:
					anatomical_territories=territorie['anatomical_territory'].split(',')
					stage=territorie['stage']
					for at in anatomical_territories:
						couple=(at,stage)
						if couple in CellByStage:
							#print 'couple='+str(couple)
							#print CellByStage[couple]
							cells=CellByStage[couple].split(",")
							for cell in cells:
								if cell!="":
									cmdsql='INSERT INTO anissed_cells_by_gene_by_stage (gene_id,cell,stage) VALUES ('+str(id)+',"'+cell+'",'+str(Stages[stage])+')'
									print str(unique_gene_id)+cmdsql
									cursor.execute(cmdsql);db.commit()
									nbCelInserted+=1
			elif wt_mut=="Experiments_Mutant": #Experiments Mutant
				mutants=all_genes[wt_mut]
				for mutant in mutants:
					print 'Process a new mutant '
					print mutant
					
					#GET WT
					mutant_WT=mutant['WT']
					if mutant_WT is not None:
						biomaterial_id=mutant_WT['biomaterial_id']
						#GET MUTANT
						mutant_mutant=mutant['Mutant']
						mutant_id=mutant_mutant['mutant_id']
						#CREATE THE MUTANT 
						cmdsql='INSERT INTO anissed_mutant (gene_id,biomaterial_id,mutant_id) VALUES ('+str(id)+','+str(biomaterial_id)+','+str(mutant_id)+')'
						cursor.execute(cmdsql);db.commit()	
						id_mutant=cursor.lastrowid
						
						nbCell=0
						
						#PROCESS CELL FOR WT
						stage=mutant_WT['stage']
						anatomical_territories=mutant_WT['anatomical_territory'].split(',')
						for at in anatomical_territories:
							couple=(at,stage)
							if couple in CellByStage:
								cells=CellByStage[couple].split(",")
								for cell in cells:
									if cell!="":
										cmdsql='INSERT INTO anissed_mutant_cells_WT (gene_id,biomaterial_id,cell,stage) VALUES ('+str(id)+','+str(biomaterial_id)+',"'+cell+'",'+str(Stages[stage])+')'
										print str(unique_gene_id)+cmdsql
										cursor.execute(cmdsql) ;db.commit()
										nbCelInserted+=1
										nbCell+=1
										
						
						#PROCESS CELL FOR MUTANT
						stage=mutant_mutant['stage']
						anatomical_territories=mutant_mutant['anatomical_territory'].split(',')
						for at in anatomical_territories:
							couple=(at,stage)
							if couple in CellByStage:
								cells=CellByStage[couple].split(",")
								for cell in cells:
									if cell!="":
										cmdsql='INSERT INTO anissed_mutant_cells_mutant (gene_id,mutant_id,cell,stage) VALUES ('+str(id)+','+str(mutant_id)+',"'+cell+'",'+str(Stages[stage])+')'
										print str(unique_gene_id)+cmdsql
										cursor.execute(cmdsql) ;db.commit()
										nbCelInserted+=1
										nbCell+=1
						
						if nbCell>0:
							for d in mutant_mutant['Deregulations']:
								from_devstage=0
								if d['from_devstage'] is not None and d['from_devstage'] in Stages:
									from_devstage=Stages[d['from_devstage']]
								to_devstage=0
								if d['to_devstage'] is not None and d['to_devstage'] in Stages:
									to_devstage=Stages[d['to_devstage']]
									
									
								moltool_name=d['moltool_name']
								regulation_type=d['regulation_type']
								target_feature=d['target_feature']
								
								if regulation_type is None:
									regulation_type=""
								if target_feature is None:
									target_feature=""
								if moltool_name is None :
									moltool_name=""
									
							
								cmdsql='INSERT INTO aniseed_deregulations (mutant_id,from_devstage,to_devstage,moltool_name,regulation_type,target_feature) VALUES ('+str(mutant_id)+','+str(from_devstage)+','+str(to_devstage)+',"'+moltool_name.encode('utf-8').strip()+'","'+regulation_type.encode('utf-8').strip()+'","'+target_feature.encode('utf-8').strip()+'")'
								print str(unique_gene_id)+cmdsql
								cursor.execute(cmdsql); db.commit()
						else:
							print ' --> Remove Mutant ' +str(id_mutant)
							cursor.execute('DELETE FROM anissed_mutant WHERE id='+str(id_mutant)); db.commit()
			
		
	if nbCelInserted==0:
		print ' --> Remove ' +str(unique_gene_id)
		cursor.execute('DELETE FROM anissed_all_genes WHERE id='+str(id)) db.commit()
	
cursor.close()
db.close()
   

   

