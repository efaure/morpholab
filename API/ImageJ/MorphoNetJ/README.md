######################################################################
#		MORPHONET Plugin for Image J (or Fij) SOURCE CODE
######################################################################

From Eclipse , Import a new project and open this folder as an "Existing Maven Projects"


We highly recommend the use of the last Fiji version that include automatically the last 3D library (https://fiji.sc/#download). 

You'll find more information, how to use the plugin, on http://www.morphonet.crbm.cnrs.fr/Tutorials/MorphoNet_Tutorial2.html

