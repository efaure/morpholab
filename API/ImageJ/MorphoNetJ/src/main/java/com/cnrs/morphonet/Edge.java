package com.cnrs.morphonet;

public class Edge {

	private final Vertex v1, v2;

	Edge(final Vertex v1, final Vertex v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	@Override
	public final boolean equals(final Object ob) {
		final Edge e = (Edge) ob;
		return e == this || (e.v1 == this.v1 && e.v2 == this.v2) ||
			(e.v1 == this.v2 && e.v2 == this.v1);
	}

	final void averageVertices() {
		this.v1.average(this.v2);
	}
}
