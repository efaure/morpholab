package com.cnrs.morphonet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.scijava.vecmath.Point3f;

//FROM https://github.com/mikolalysenko/taubin-smooth/blob/master/smooth.js
public class TaubinSmooth {

		//double passBand=0.1f;
		//double iters=10;
	
		TaubinSmooth(){}
		
		static public void Smooth(final List<Point3f> vertices,int iters,double passBand){
			final int len = vertices.size();
			final int[] indices = new int[len];
			
			final HashMap<Point3f, Integer> ht_points = new HashMap<Point3f, Integer>();
			// index over index array, to make faces later
			int k = 0;
			int j = 0;
			
			 List<Point3f> pointA =new ArrayList<Point3f>(); 
			 
			for (final Point3f p : vertices) {
				// check if point already exists
				if (ht_points.containsKey(p)) {
					indices[k] = ht_points.get(p);
				}
				else {
					indices[k] = j; // new point
					ht_points.put(p, j); // record
					pointA.add(p);
					
					j++;
				}
				k++;
			}
			System.out.println("vertices="+vertices.size());
			System.out.println("ht_points="+ht_points.size());
			System.out.println("indices="+indices.length);
			
			List<Point3f>cells =new ArrayList<Point3f>(); //FAces
			for (int i = 0; i < indices.length; i += 3) {
				Point3f pt=new Point3f(indices[i],indices[i+1],indices[i+2]);
				cells.add(pt);
			 }
		
			System.out.println("cells="+cells.size());

			List<Point3f> pointB =new ArrayList<Point3f>(); 
			for(int i=0;i<pointA.size();i++)pointB.add(new Point3f(0.0f,0.0f,0.0f));
			
			
			  double A = -1;
			  double B = passBand;
			  double C = 2;

			  double discr = Math.sqrt(B * B - 4 * A * C);
			  double r0 = (-B + discr) / (2 * A * C);
			  double r1 = (-B - discr) / (2 * A * C);

			  double lambda = Math.max(r0, r1);
			  lambda=0.5;
			  double mu = Math.min(r0, r1);
			  System.out.println("lambda="+lambda+ "l mu="+mu);
			  
			  double [] trace=new double[len];

			  for (int i = 0; i < iters; ++i) {
				    smoothStep(cells, pointA, pointB, trace, lambda);
				    smoothStep(cells, pointB, pointA, trace, mu);
			  }
			
		}
			
					
		static protected double hypot (double x, double y, double z) {
			  return  Math.sqrt(  Math.pow(x, 2) +  Math.pow(y, 2) + Math.pow(z, 2));
		}


		static protected void accumulate (Point3f out,Point3f inp, double w) {
			out.x+= inp.x * w;
			out.y+= inp.y * w;
			out.z+= inp.z * w;
		}
		

		static protected void smoothStep( List<Point3f> cells, List<Point3f>  positions, List<Point3f>  outAccum,double [] trace, double weight) {
			
				  int numVerts = positions.size();
				  int numCells = cells.size();

			
				  for (Point3f ov : outAccum) {   ov.x=0;  ov.y=0;  ov.z=0;  }

				  for (int i = 0; i < numVerts; ++i)  trace[i] = 0f;
				  

				  
				  for (int i = 0; i < numCells; ++i) {
					Point3f cell = cells.get(i);
				    int ia = (int)cell.x;
				    int ib = (int)cell.y;
				    int ic = (int)cell.z;
							    		
				    Point3f a = positions.get(ia);
				    Point3f b = positions.get(ib);
				    Point3f c = positions.get(ic);

				    double abx = a.x - b.x;
				    double aby = a.y - b.y;
				    double abz = a.z - b.z;

				    double bcx = b.x - c.x;
				    double bcy = b.y - c.y;
				    double bcz = b.z - c.z;

				    double cax = c.x - a.x;
				    double cay = c.y - a.y;
				    double caz = c.z - a.z;

				    double area = 0.5 * hypot(  aby * caz - abz * cay,  abz * cax - abx * caz,  abx * cay - aby * cax);

				    if (area < 1e-8)  { return;  }

				    double w = -0.5f / area;
				    double wa = w * (abx * cax + aby * cay + abz * caz);
				    double wb = w * (bcx * abx + bcy * aby + bcz * abz);
				    double wc = w * (cax * bcx + cay * bcy + caz * bcz);

				    
				    trace[ia] += wb + wc;
				    trace[ib] += wc + wa;
				    trace[ic] += wa + wb;

				    Point3f oa = outAccum.get(ia);
				    Point3f ob = outAccum.get(ib);
				    Point3f oc = outAccum.get(ic);

				    accumulate(ob, c, wa);
				    accumulate(oc, b, wa);
				    accumulate(oc, a, wb);
				    accumulate(oa, c, wb);
				    accumulate(oa, b, wc);
				    accumulate(ob, a, wc);
				  }

				  for (int i = 0; i < numVerts; ++i) {
					  Point3f o = outAccum.get(i);
					  Point3f p = positions.get(i);
					  double tr = trace[i];
					  
					  o.x=(float) (p.x+ weight * (o.x / tr - p.x));
					  o.y=(float) (p.y+ weight * (o.y / tr - p.y));
					  o.z=(float) (p.z+ weight * (o.z / tr - p.z));  
				  }
				}
		
		
			

			
	
}
