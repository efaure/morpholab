package com.cnrs.morphonet;

import java.util.ArrayList;

import org.scijava.vecmath.Point3f;

public final class Vertex {

	// The original
	final private Point3f p;
	// The collection of vertices from the mesh that equal this one
	final ArrayList<Point3f> copies = new ArrayList<Point3f>();
	// The averaged and later on the smoothed result
	final private Point3f tmp;
	// The number of times it's been averaged
	private int n;

	Vertex(final Point3f p) {
		this.p = p;
		this.tmp = new Point3f(0, 0, 0);
		this.copies.add(p);
	}

	final void reset() {
		this.tmp.set(0, 0, 0);
		this.n = 0;
	}

	@Override
	public final boolean equals(final Object ob) {
		final Vertex v = (Vertex) ob;
		return v.p == this.p ||
			(v.p.x == this.p.x && v.p.y == this.p.y && v.p.z == this.p.z);
	}

	public final void average(final Vertex v) {
		// Increment counter for both
		++this.n;
		++v.n;
		// compute average of the original coordinates
		final Point3f a =
			new Point3f((this.p.x + v.p.x) / 2, (this.p.y + v.p.y) / 2,
				(this.p.z + v.p.z) / 2);
		// Add average to each vertices' tmp
		this.tmp.add(a);
	}

	final void smooth() {
		// Compute smoothed coordinates
		final float f = 0.5f / n;
		tmp.set(0.5f * p.x + f * tmp.x, 0.5f * p.y + f * tmp.y, 0.5f * p.z + f *
			tmp.z);
		// Apply them to all copies
		// It doesn't matter if the copies are not unique.
		for (final Point3f p : copies) {
			p.set(tmp);
		}
	}
}