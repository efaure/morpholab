/*
" * To the extent possible under law, the ImageJ developers have waived
 * all copyright and related or neighboring rights to this tutorial code.
 *
 * See the CC0 1.0 Universal license for details:
 *     http://creativecommons.org/publicdomain/zero/1.0/
 */

package com.cnrs.morphonet;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import customnode.CustomMesh;
import customnode.CustomTriangleMesh;
import customnode.EdgeContraction;
import customnode.FullInfoMesh;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import ij3d.Image3DUniverse;
import isosurface.MeshEditor;
import marchingcubes.MCTriangulator;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.scijava.vecmath.Point3d;
import org.scijava.vecmath.Point3f;
import org.scijava.vecmath.Vector3d;



/**
 * @author Emmanuel Faure
 */
public class MorphoNet_ implements PlugIn {
	
	// image property members
	private static String login;
	private static String passwd;
	private static boolean connected=false;
	private static String hash="34morpho:";
	private static String morphoNetURL="http://www.morphonet.org/api.php";
	private static String datasetname;
	private static int id_dataset=-1; //Morphonet Dataset Identification
	
	
	private static int nFrames=1;//Number of time step
	private static int startF=1; //First Frame to Upload
	private static int endF=1; //Last Frame to Upload
	
	private static Map<String,String> datasets ;
	private static Map<Integer,double[]>centers; //To have the center of the obj
	private static String objectnameprefix="object_";
	
	private static int MAXOBJ=10000;
	private static int MAXBYTES=50*1024*1024; //50MB
	
	private static String temp_info_name="TEMP_MorphoNetInfos.txt";
	
	//Object Name Construction
	private static String getName(int t,String ido) { return Integer.toString(t)+","+objectnameprefix+ido;}
	private static String getName(int t, int ido) { return getName(t,Integer.toString(ido));}
	private static String getTempName(int t) {return "TEMP_MorphoNetMeshes_t_"+t+".obj"; }
	
	@Override
	public void run(String arguments) {
 	   
		//IJ.error( "args  "+arguments);
		if(arguments.equals("connect"))  connected=Connect();
		
		if(arguments.equals("create_dataset"))
			try {
				create_dataset();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if(arguments.equals("pick_dataset")) pick_dataset();
		
		if(arguments.equals("compute_meshes")) {
			ImagePlus imp = IJ.getImage(); //IMAGE CHECK
			if( imp == null ) { IJ.error( "Must select a currently open image" ); return ; }
			if (!imp.lock()) {IJ.error( "Your image is locked" ); return ;}
			if ( imp.getNSlices()<=1) {IJ.error( "Your image should be in 3D " ); return ;}
			if(imp.getType()!=ImagePlus.GRAY8 && imp.getType()!=ImagePlus.GRAY16 && imp.getType()!=ImagePlus.GRAY32) {IJ.error( "Your image is not in gray 8, 16 or 32bits. You must convert it before " ); return ;}
			compute_meshes(imp);
			imp.unlock();
		}
		
		if	(arguments.equals("upload_meshes")) upload_meshes();
		
		if(arguments.equals("upload_infos")) {
			ResultsTable tb=ResultsTable.getResultsTable();
			if(tb==null || tb.getLastColumn()==-1) { IJ.error( "No table selected " ); return ; }
			else upload_table(tb);
			
		}

			 
	}
	
	
	//CONNECT TO MORPHONET SERVER
	public static boolean Connect() {
		//WINDOW CONNECTION
		PasswordDialog gdconnection = new PasswordDialog("MorphoNet Connection");
		gdconnection.addMessage( "Enter your MorphoNet identification :" );
		gdconnection.addStringField("login", login,20);
		gdconnection.addPasswordField("password", "",20);
		gdconnection.showDialog();
		if(gdconnection.wasCanceled())
			return false;
		
		login=(String)gdconnection.getNextString();
		passwd=(String)gdconnection.getNextString();
		
		try {
			if(!connect_MorphoNet())IJ.error("Cannot connect to MorphoNet, please check your ids");
			else {
				IJ.showMessage("You are connected to MorphoNet");
				return true;
			}
		} catch (NoSuchAlgorithmException | IOException e) {
			//e.printStackTrace();
			System.out.println("RUN ERROR ...."+e.toString());
		}
		return false;
	}
	public static boolean connect_MorphoNet() throws IOException, NoSuchAlgorithmException  {
        
        URL url = new URL(morphoNetURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("hash", hash);
        params.put("login", login);
        params.put("passwd", hashPasswd(passwd));
        
        byte[] postDataBytes = buildURLParams(params);

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        //System.out.println(":"+output+":");
    
        if(Integer.parseInt(output)>0)
        	return true;  
        return false;
        
	}
	public static boolean not_connected() {
		IJ.error("You are not connected to MorphoNet");
		return false;
	}
	
	//CREATE A DATASET
	public boolean create_dataset() throws NoSuchAlgorithmException, IOException {
		if(!connected)return  not_connected(); 
		GenericDialog gtd = new GenericDialog("MorphoNet New Dataset");
		gtd.setSize(1500,500);
		gtd.addStringField("New dataset name", datasetname);
		gtd.showDialog();
		if(gtd.wasCanceled()) return false ;
		datasetname = gtd.getNextString();
		return createDataset(datasetname);
	}
	private static boolean createDataset(String name) throws IOException, NoSuchAlgorithmException  {
		System.out.println("Create Dataset" + name);
        URL url = new URL(morphoNetURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("hash", hash);
        params.put("login", login);
        params.put("passwd", hashPasswd(passwd));
        params.put("createdataset", name);
        params.put("minTime", "0");
        params.put("maxTime", "0");
        params.put("id_dataset_type", "1");
        params.put("spf", "0");
        params.put("dt", "0");
        
        byte[] postDataBytes = buildURLParams(params);

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        System.out.println("Create dataset :"+output+":");
        /*if(!output.contains("ERROR")) {
        	IJ.error(output);
        	return false;
        }*/
        id_dataset=Integer.parseInt(output);
        //System.out.println(" -> id_dataset="+id_dataset);
        //else  IJ.error("Connection to MorphoNet success");
        return true;
}
	public static boolean not_dataset() {
		IJ.error("You first have to pick a dataset in MorphoNet");
		return false;
	}
	
	//PICK A DATASET
	public static boolean pick_dataset() {
		if(!connected)return  not_connected(); 
		try {
			listdataset();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(datasets.size()==0) {
			IJ.showMessage("You do not have any dataset in MorphoNet, create one first");
			return false;
		}
		
		String[]titles = new String[datasets.size()];
		int id=0;
		for (Map.Entry<String,String> param : datasets.entrySet()) {
			 titles[ id ] =param.getValue();
			 id+=1;
		}
       
		GenericDialog gd = new GenericDialog("MorphoNet Dataset");
		gd.addMessage( "Pick an existing dataset in MorphoNet :" );
		gd.addChoice( "", titles, "New " );
		gd.showDialog();
		if(gd.wasCanceled()) 	return false;
		
		String dt=gd.getNextChoice();
		for (Map.Entry<String,String> param : datasets.entrySet()) {
			if(dt.equals(param.getValue())) id_dataset=Integer.parseInt(param.getKey());
		}
		if(id_dataset==-1)  {return false;}
		datasetname=dt;
		//IJ.showMessage("dataset id="+id_dataset);
		return true;
	}
	public static void  listdataset() throws IOException, NoSuchAlgorithmException  {
	        
	        URL url = new URL(morphoNetURL);
	        Map<String,Object> params = new LinkedHashMap<>();
	        params.put("hash", hash);
	        params.put("login", login);
	        params.put("passwd", hashPasswd(passwd));
	        params.put("listmydatasetsimple", 1);
	
	        byte[] postDataBytes = buildURLParams(params);
	
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);
	
	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	        String output="";
	        for (int c; (c = in.read()) >= 0;)
	        	output+=(char)c;
	        //System.out.println(output);
	        
	        output=output.substring(8, output.length()-2);
			String []aez=output.split("\\n");
			Map<String, String> treeMap  = new HashMap<>();
	
			for (String s: aez) {  
				String []ts=s.trim().split("=>");
				String dname=ts[1].trim();
				dname = dname.substring(0, 1).toUpperCase() + dname.substring(1);
				//System.out.println(ts[1]+"::"+dname);
				treeMap.put(ts[0].substring(1, ts[0].length()-2), dname);
		    }
			datasets= sortByValue(treeMap);
			
			//for (Map.Entry<String,String> param : datasets.entrySet())  	 System.out.println(param.getKey()+ "=> " +param.getValue());		 
	 }
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;

    }
 
	//COMPUTE MESH ON OPENEND IMAGE
	public static boolean compute_meshes(ImagePlus imp) {
		 //IMAGE CHECK	
		  final int width = imp.getWidth(); 
          final int height = imp.getHeight(); 
          final int nChannels = imp.getNChannels(); 
          final int nSlices = imp.getNSlices(); 
          nFrames = imp.getNFrames(); 
          final int bitDepth=imp.getBitDepth();
          

			//DIALOG PARAMETRS FOR CONVERSION IN MESHES
			GenericDialog gdc = new GenericDialog("MorphoNet Conversion");
			
			gdc.addMessage( "Convert 3D Volumes to Meshes" );
			//gdc.addStringField("Objects name prefix", objectnameprefix);
			if(nFrames>1) {
				gdc.addMessage( "Found "+nFrames+" time steps" );
				gdc.addNumericField("From  time step n°", 1,0);
				gdc.addNumericField("To  time step n°", nFrames,0);
			}
			//else gdc.addNumericField("Upload Time", startF,startF);*/
			
			gdc.addNumericField("Images Objects Threshold", 0, 0);
			gdc.addNumericField("Images Resampling factor", 4, 0);
			//gdc.addNumericField("Laplacian Smooth iterations",4, 0);
			gdc.addNumericField("Meshes Simplification factor", 0, 0);
			gdc.showDialog();
			if(gdc.wasCanceled()) 	return false;
			//objectnameprefix=gdc.getNextString();
			
			if(nFrames>1) {
				startF=(int)gdc.getNextNumber();
				endF = (int)gdc.getNextNumber();
			}else { startF=1; endF=1; }
			
			int threshold = (int)gdc.getNextNumber();
			int factor = (int)gdc.getNextNumber();
			//int iterations = (int)gdc.getNextNumber();
			int simply = (int)gdc.getNextNumber();
			
			boolean[] channels = new boolean[] {true,true,true};
			
			try {
				
				for ( int t = startF; t <= endF; ++t )  {
					Image3DUniverse univ   = new Image3DUniverse(500,500);
					univ.show();
					
					//for ( int c = 1; c <= nChannels; ++c ) {
					int c=1;
					List<Integer> listIDS=new ArrayList<Integer>();
					for ( int z = 1; z <= nSlices; ++z )
					{
						final int index = imp.getStackIndex( c, z, t );
						final ImageProcessor ip = imp.getStack().getProcessor( index );
						
						if(imp.getType()==ImagePlus.GRAY8) { //8 Bits
							ByteProcessor bp=ip.convertToByteProcessor(false);
							byte[] pixels = (byte[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v>threshold && !listIDS.contains(v)) listIDS.add(v);
							}	
						}
						else if(imp.getType()==ImagePlus.GRAY16) { //16 Bits
							ShortProcessor bp=ip.convertToShortProcessor(false);
							short[] pixels = (short[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v>threshold && !listIDS.contains(v)) listIDS.add(v);
							}
						}
						else if(imp.getType()==ImagePlus.GRAY32) { 	//32 Bits
							FloatProcessor bp=ip.convertToFloatProcessor();
							float[] pixels = (float[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v>threshold && !listIDS.contains(v)) listIDS.add(v);
							}
						}	
					}
					 if(listIDS.size()>MAXOBJ) {
						 IJ.error("Cannot upload, This dataset contains more than "+MAXOBJ+" objects");
						 return false;
					 }
					 if(listIDS.size()==0) {
						 IJ.error("We didn't find any objects");
						 return false;
					 }
					//For Each Index we convert in meshes
					Map<String, CustomMesh> meshes =new HashMap<String, CustomMesh>();
					for (Integer id : listIDS) {
						String object_name=getName(t,id);
						
						//System.out.println("Process time " +t+ " -> "+object_name);
					
						//First create and empty Matrix Stack
						ImageStack stack2 = new ImageStack(width, height);
						for ( int z = 1; z <= nSlices; ++z ) {
							final int index = imp.getStackIndex( c, z, t );
							final ImageProcessor ip = imp.getStack().getProcessor( index );
							
							if(imp.getType()==ImagePlus.GRAY8) { //8 Bits
								ByteProcessor bp=ip.convertToByteProcessor(false);
								byte[] pixels = (byte[])bp.getPixels();
								ByteProcessor ip2 = new ByteProcessor(width, height);
								for (int x = 0; x <pixels.length; x++) {
									int v=(int)pixels[x];
									if(v==id) ip2.set(x, 255);
									else ip2.set(x, 0);
								}
								stack2.addSlice(ip2);
							}
							else if(imp.getType()==ImagePlus.GRAY16) { //16 Bits
								ShortProcessor bp=ip.convertToShortProcessor(false);
								short[] pixels = (short[])bp.getPixels();
								ByteProcessor ip2 = new ByteProcessor(width, height);
								for (int x = 0; x <pixels.length; x++) {
									int v=(int)pixels[x];
									if(v==id) ip2.set(x, 255);
									else ip2.set(x, 0);
								}
								stack2.addSlice(ip2);
							}
							else if(imp.getType()==ImagePlus.GRAY32) { //32 Bits
								FloatProcessor bp=ip.convertToFloatProcessor();
								float[] pixels = (float[])bp.getPixels();
								ByteProcessor ip2 = new ByteProcessor(width, height);
								for (int x = 0; x <pixels.length; x++) {
									int v=(int)pixels[x];
									if(v==id) ip2.set(x, 255);
									else ip2.set(x, 0);
								}
								stack2.addSlice(ip2);
							}
						}
						ImagePlus impp=IJ.createImage("object_"+id, width, height, nSlices,bitDepth);
						impp.setStack(stack2);
				
						//Now We Triangulate the output
						MCTriangulator triangulator = new MCTriangulator();
						List<Point3f> tri = triangulator.getTriangles(impp, 0, channels, factor);
		    		    CustomTriangleMesh ctm = new CustomTriangleMesh(tri);
		    		   
		    		    
		    		   /* if(iterations>=1) {
		    		    	smooth(ctm, iterations);
		    		    	//TaubinSmooth.Smooth( ctm.getMesh(),iterations,0.1);
		    		    }*/
			    			  
		    		    // System.out.println("CTM "+ctm.toString());
		    		    
		    		    FullInfoMesh fim = new FullInfoMesh(ctm.getMesh());
			    		EdgeContraction ec = new EdgeContraction(fim, false);
			    			
		
		    		    //Simplification
			    		if(simply>1) {
			    		  int outnb=(int)Math.round((float)ec.getVertexCount()/simply);
			    		 // System.out.println(outnb+ " wanted outnb vertices"); 
			    		  
			    		  int prev=ec.getVertexCount();
			    		  final int step = ((prev-outnb) / 10);
			    		  int next=prev-1;
			    		  while(prev>next && next>outnb) { 
			    			  prev=next; 
			    			  next=ec.removeNext(step); 
			    			//  System.out.println(ec.getVertexCount() + " remaining vertices"+ "-> "+next);
			    		   }
			    		
			    		}
			    		 
			    		ArrayList<FullInfoMesh> fm=ec.getMeshes();
			    		for (FullInfoMesh fmi : fm)   meshes.put(object_name,new CustomTriangleMesh(fmi.getMesh()));
			    		 
			    		System.out.println("-> "+object_name+" contains "+ec.getVertexCount() + " remaining vertices"); 
			    		
			    		  
			    		   
			    		//meshes.put(object_name,ctm);
			    		
			    		univ.addCustomMesh(meshes.get(object_name),object_name);	//FOR VISUALISATION	 ONLY 	
					 }	
					String temp_name=getTempName(t);
					MorphoExporter.save(meshes,temp_name );
					File fileobj =new File(temp_name);
					if(!fileobj.exists())return false;  //UNKONW ERROR...
					if(fileobj.length()>MAXBYTES) {
						IJ.error("Cannot upload, this dataset is too heavy, please increase the resampling factor");
						deletefile(temp_name);
						return false;
					}
					calculCenter(t,meshes);
				  }
			}catch (IOException e1) {
				for ( int t = startF; t <= endF; ++t )  deletefile(getTempName(t));
				System.out.println("RUN ERROR ...."+e1.toString());
			}
		  IJ.showMessage("You are dataset is converted in meshes");
		  return true;
	}

	static private final Vertex uniqueVertex(final Point3f p,
			final HashMap<Point3f, Vertex> verts)
		{
			Vertex v = verts.get(p);
			if (null == v) {
				v = new Vertex(p);
				verts.put(p, v);
			}
			else {
				v.copies.add(p);
			}
			return v;
		}
	
	static protected void smooth(CustomTriangleMesh ctm, final int iterations)
		{
		
			final List<Point3f> triangles=ctm.getMesh();
			System.out.println("Smooth "+triangles.size()+ " triangles");
			final HashMap<Point3f, Vertex> verts = new HashMap<Point3f, Vertex>();
			final HashSet<Edge> edges = new HashSet<Edge>();

			
			// Find unique edges made of unique vertices
			for (int i = 0; i < triangles.size(); i += 3) {
				// process one triangle at a time
				final Vertex v1 = uniqueVertex(triangles.get(i), verts), 
						     v2 = uniqueVertex(triangles.get(i + 1), verts), 
						     v3 = uniqueVertex(triangles.get(i + 2), verts);

				// Add unique edges only
				edges.add(new Edge(v1, v2));
				edges.add(new Edge(v2, v3));
				edges.add(new Edge(v1, v3));

				if (0 == i % 300 && Thread.currentThread().isInterrupted()) return;
			}
			
			for (int i = 0; i < iterations; ++i) {
				System.out.println("Smooth iteration " + i);
				if (Thread.currentThread().isInterrupted()) return;

				// First pass: accumulate averages
				//for (final Edge e : edges) e.averageVertices();
				
				// Second pass: compute the smoothed coordinates and apply them
				for (final Vertex v : verts.values()) v.smooth();
				
				// Prepare for next iteration
				if (i + 1 < iterations) {
					for (final Vertex v : verts.values()) {
						v.reset();
					}
				}
			}			
		}
	

	
	
	
	
	public static int simplify(final EdgeContraction ec, final int n) {
		final int part = n / 10;
		final int last = n % 10;
		int ret = 0;
		for (int i = 0; i < 10; i++) {
			IJ.showProgress(i + 1, 10);
			ret = ec.removeNext(part);
		}
		if (last != 0) ret = ec.removeNext(last);
		IJ.showProgress(1);

		return ret;
	}
	public static ImagePlus getPixels(ImagePlus imp,float p) {
		int width = imp.getWidth();
		int height = imp.getHeight();
		int size = width*height;
		boolean isFloat = imp.getType()==ImagePlus.GRAY32;
		int currentSlice =  imp.getCurrentSlice();
		int nSlices = imp.getStackSize();
		ImageStack stack1 = imp.getStack();
		ImageStack stack2 = new ImageStack(width, height);
		ImageProcessor ip = imp.getProcessor();
		
		float value;
		ImageProcessor ip1, ip2;
		IJ.showStatus("Converting Object "+p+" to mask");
		for (int i=1; i<=nSlices; i++) {
			IJ.showProgress(i, nSlices);
			String label = stack1.getSliceLabel(i);
			ip1 = stack1.getProcessor(i);
			ip2 = new ByteProcessor(width, height);
			for (int j=0; j<size; j++) {
				value = ip1.getf(j);
				if (value==p) ip2.set(j, 255);
				else ip2.set(j, 0);
			}
			stack2.addSlice(label, ip2);
		}
		IJ.showStatus("");
		ImagePlus impp = imp.createImagePlus();
		impp.setStack(stack2);
		return impp;
		
	}
	

	public static void calculCenter(int t,final Map<String, CustomMesh> meshes) {
		//System.out.println("Calcul Centers");
		if(centers==null)centers=new HashMap<Integer,double[]>();
		
		double[] tcenters=new double[3]; for(int i=0;i<3;i++)tcenters[i]=0; 
		int nbCenters=0;
		for (final String name : meshes.keySet()) {
			final CustomMesh cmesh = meshes.get(name);
			final List<Point3f> vertices = cmesh.getMesh();
			for (final Point3f p : vertices) {
				tcenters[0]+=p.x;tcenters[1]+=p.y;tcenters[2]+=p.z;
				nbCenters++;
			}
		}
		if(nbCenters>0) for(int i=0;i<3;i++)tcenters[i]/=nbCenters;
		System.out.println("Found "+nbCenters+" vertices centered at "+ tcenters[0]+","+tcenters[1]+","+tcenters[2]);
		centers.put(t, tcenters);
	}
	

	
	//UPLOAD MESEHES
	public static boolean upload_meshes() {
		if(!connected)return  not_connected(); 
		if(id_dataset==-1) return not_dataset();
		//Check Files well converted
		for ( int t = startF; t <= endF; ++t ) {
			String temp_name=getTempName(t);
			if(!existfile(temp_name)){ IJ.error("Please first convert your dataset in meshes"); return false; }
		}
		
		try {
			for ( int t = startF; t <= endF; ++t )  {
				String temp_name=getTempName(t);
				compressBZ2FileToFile(temp_name,temp_name+".bz2");
				deletefile(temp_name);
				uploadMeshFile(temp_name+".bz2",t);
				deletefile(temp_name+".bz2");
			}
			IJ.showMessage("Your data are uploaded on MorphoNet ");
			//BrowserLauncher.openURL("http://www.morphonet.crbm.cnrs.fr");
			return true;
		} catch (IOException e) { e.printStackTrace(); }
		
		return false;	
	}
	public static void compressBZ2FileToFile(String filenamein,String filename) throws IOException {
		//System.out.println("Compress "+filenamein+" in bz2 to "+ filename);
		InputStream in = Files.newInputStream(Paths.get(filenamein));
		OutputStream fout = Files.newOutputStream(Paths.get(filename));
		BufferedOutputStream out = new BufferedOutputStream(fout);
		BZip2CompressorOutputStream bzOut = new BZip2CompressorOutputStream(out);
		final byte[] buffer = new byte[2048];
		int n = 0;
		while (-1 != (n = in.read(buffer))) {
		    bzOut.write(buffer, 0, n);
		}
		bzOut.close();
		in.close();
	}
	public static void uploadMeshFile(String filename,int t) throws IOException {
		System.out.println("Upload  Meshes on MorphoNet");
		int quality=0;
		int channel=0;
		
		Map<String,Object> params = new LinkedHashMap<>();
        params.put("hash", hash);
        params.put("login", login);
        params.put("passwd", hashPasswd(passwd));
        params.put("uploadlargemesh",""+id_dataset);
        params.put("t",""+t);
        params.put("quality",""+quality);
        params.put("channel",""+channel);
        double [] tcenters=centers.get(t);
        params.put("center",""+tcenters[0]+","+tcenters[1]+","+tcenters[2]);
        params.put("link","null");
       // for (Map.Entry<String,Object> param : params.entrySet())  	System.out.println(param.getKey()+"->"+String.valueOf(param.getValue()));
       
        String charset = "UTF-8";
		File binaryFile = new File(filename);
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
	
		URL url = new URL(morphoNetURL);
		URLConnection connection = (URLConnection)url.openConnection();
	   
	    connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
	
		
		try (
		    OutputStream output = connection.getOutputStream();
		    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
		) {
		    // Send  param.
			 for (Map.Entry<String,Object> param : params.entrySet()) {
				 writer.append("--" + boundary).append(CRLF);
				 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
				 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
				 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
			 }
		    // Send binary file.
		    writer.append("--" + boundary).append(CRLF);
		    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
		    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
		    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
		    writer.append(CRLF).flush();
		    Files.copy(binaryFile.toPath(), output);
		    output.flush(); // Important before continuing with writer!
		    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
	
		    // End of multipart/form-data.
		    writer.append("--" + boundary + "--").append(CRLF).flush();
		}
	
		// Request is lazily fired whenever you need to obtain information about response.
		int responseCode = ((HttpURLConnection) connection).getResponseCode();
		System.out.println("Response "+responseCode); // Should be 200
		
		Reader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        System.out.println("Upload DONE !! -> "+output+" !!");
	}
	public static byte[] buildURLParams(Map<String,Object> params) throws UnsupportedEncodingException {
		//System.out.println("Build Params");
		StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            //System.out.println(param.getKey()+"="+param.getValue()+ " -> "+URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        return postDataBytes;
		
	}
	public static String hashPasswd(String passwordToHash)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
        	System.out.println("hashPasswd ERROR ...."+e.toString());
        	e.printStackTrace();
        }
        return generatedPassword;
    }
	
	//UPLOAD INFOS
	public static boolean upload_table(ResultsTable tb) {
			if(!connected)return  not_connected(); 
			if(id_dataset==-1) return not_dataset();
			String []headings=tb.getHeadings();
			//CONVERT IN MESHES
			GenericDialog gd = new GenericDialog("MorphoNet Upload Informations");
			gd.addChoice( "Choose object id", headings, "New " );
			//gd.addStringField("Objects name prefix", objectnameprefix);
			//gd.addNumericField("Time", t_dataset,t_dataset);
			
			
			gd.addChoice( "Choose information to upload ", headings, "New " );
			String []types=new String[7];
			types[0]="string";
			types[1]="float";
			types[2]="selection";
			types[3]="group";
			types[4]="genetic";
			types[5]="space";
			types[6]="time";
			gd.addChoice( "Choose your type information ", types, "New " );
			gd.showDialog();
			if(gd.wasCanceled())  return false;
			 
			
			//String objectid=gd.getNextChoice();
			//int oid=tb.getColumnIndex(objectid);
			int oid=gd.getNextChoiceIndex();
			String objectid=headings[oid];
			//objectnameprefix=gd.getNextString();
			//t_dataset=(int)gd.getNextNumber();
			
			
			//String infoid=gd.getNextChoice();
			//int iid=tb.getColumnIndex(infoid);
			int iid=gd.getNextChoiceIndex();
			String infoid=headings[iid];
			
			String typeinfos=gd.getNextChoice();
			
			String infos="#Upload from ImageJ\n";
			Date dt=new Date();
			infos+="#the "+dt.toString()+"\n";
			infos+="#by "+login+"\n";
			infos+="#Dataset "+datasetname+"\n";
			infos+="type:"+typeinfos+"\n";
			for(int i=0;i<tb.size();i+=1) {
				infos+=getName(startF,tb.getStringValue(oid-1, i))+":"+tb.getStringValue(iid-1, i)+"\n";
			}
			
			try {
				writefile(temp_info_name,infos);
				compressBZ2FileToFile(temp_info_name,temp_info_name+".bz2");
				deletefile(temp_info_name);
				upload_infos(infoid,temp_info_name+".bz2",typeinfos);
				deletefile(temp_info_name+".bz2");
				return true;
			} catch (IOException e) { e.printStackTrace(); }
			return false;
		}
	private static boolean upload_infos(String InfoName,String filename,String typeinfos) throws IOException {
			if(!connected)return  not_connected(); 
			if(id_dataset==-1) return not_dataset();
		
			//data=self._large_request({"uploadlargecorrespondence":self.id_dataset,"infos":infos,"type":dtype,"datatype":datatype},bz2.compress(field))
					
			System.out.println("Upload  Infos "+InfoName+" on MorphoNet");
			Map<String,Object> params = new LinkedHashMap<>();
	        params.put("hash", hash);
	        params.put("login", login);
	        params.put("passwd", hashPasswd(passwd));
	        params.put("uploadlargecorrespondence",""+id_dataset);
	        params.put("infos",InfoName);
	        params.put("type","2");
	        params.put("datatype",typeinfos);
	        
	       
	        String charset = "UTF-8";
			File binaryFile = new File(filename);
			String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
			String CRLF = "\r\n"; // Line separator required by multipart/form-data.
		
			URL url = new URL(morphoNetURL);
			URLConnection connection = (URLConnection)url.openConnection();
		   
		    connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		
			
			try (
			    OutputStream output = connection.getOutputStream();
			    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			) {
			    // Send  param.
				 for (Map.Entry<String,Object> param : params.entrySet()) {
					 writer.append("--" + boundary).append(CRLF);
					 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
					 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
					 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
				 }
			    // Send binary file.
			    writer.append("--" + boundary).append(CRLF);
			    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
			    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			    writer.append(CRLF).flush();
			    Files.copy(binaryFile.toPath(), output);
			    output.flush(); // Important before continuing with writer!
			    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		
			    // End of multipart/form-data.
			    writer.append("--" + boundary + "--").append(CRLF).flush();
			}
		
			// Request is lazily fired whenever you need to obtain information about response.
			int responseCode = ((HttpURLConnection) connection).getResponseCode();
			System.out.println("Response "+responseCode); // Should be 200
			
			Reader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
	        String output="";
	        for (int c; (c = in.read()) >= 0;)
	        	output+=(char)c;
	        System.out.println("Upload DONE !! -> "+output+" !!");
	        IJ.showMessage("Your results are  uploaded on MorphoNet");
	        return true;
		}
		
	//FILES 
	private static void deletefile(String filename)
	    {	
	    	try{
	    		
	    		File file = new File(filename);
	        	
	    		if(file.delete()){
	    			//System.out.println(file.getName() + " is deleted!");
	    			return;
	    		}else{
	    			//System.out.println("Delete operation is failed.");
	    		}
	    	   
	    	}catch(Exception e){
	    		
	    		e.printStackTrace();
	    		
	    	}
	    }
	private static void writefile(String filename,String towrite) {
			BufferedWriter writer = null;
			try
			{
			    writer = new BufferedWriter( new FileWriter( filename));
			    writer.write( towrite);

			}
			catch ( IOException e)
			{
			}
			finally
			{
			    try
			    {
			        if ( writer != null)
			        writer.close( );
			    }
			    catch ( IOException e)
			    {
			    }
			}
			
		}
	private static boolean existfile(String filename) 
	{	
    	try{
    		File file = new File(filename);
        	if(file.exists()) return true;
    		return false;
    	}catch(Exception e){ e.printStackTrace(); }
    	return false;
	}
		

	//ABOUT MORPHONET
	public void showAbout() {
		IJ.showMessage("MorphoNet", "ImageJ Plugin to upload segmentation data on MorphoNet" );
	}

	
	
	/**
	 * Main method for debugging.
	 */
	
	public static void main(String[] args) {		

			
	}
}
