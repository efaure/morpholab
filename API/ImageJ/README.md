# MorphoNet ImageJ Plugin 

This plugin is used to upload data to MorphoNet using [ImageJ](https://imagej.nih.gov/ij/) or [Fiji](https://fiji.sc/)

You can find the help in the [MorphoNet_Tutorial2.html](Tutorials/MorphoNet_Tutorial2.html) file with a tutorial example.


## Recommandation

We highly recommend the use of the last Fiji version that include automatically the last 3D library [https://fiji.sc](https://fiji.sc/#download)


## Installation 
Drag the plugin "MorphoNet_-0.1.0.jar" in you ImageJ/Fiji plugin folder.


## Compile your plugin on with your own instance of MorphoNet 
- Open Eclise (http://eclipse.org)
- Open Projects from File system
- Choose the MorphoNetJ path
- Configure Build Path
- Add External Jar
- Chose in the plugin Path the 3D_Viewer-4.0.2.jar fille
- And compile your plugin