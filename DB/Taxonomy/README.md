# MorphoNet  NCBI Taxonomy

This part is used to import the NCBI Taxonomy in MorphoNet 
The original Taxonomy can be find here : https://www.ncbi.nlm.nih.gov/taxonomy


## Requirements
To Install the Taxonomy, please first create the MorphoNet Database as explained in the file InstallMorphoNet.md
* [MySQLdb](http://mysql-python.sourceforge.net/MySQLdb.html)

## Installation

You first must specify your database name in the file import_NCBI.py.

Then run the command 
```
python import_NCBI.py
```
