#!/usr/bin/python3
# coding: utf-8

# THIS FILE IS PYTHON2 compatible, please test every change with python3 AND python

# for python2 compatibility
from __future__ import print_function

'''
This module :
- builds a (classical) adjacency list implementation of a tree and a dictionnary of (rank: rankid) from NCBI taxonomic data
- converts this implementation to a nested set (http://en.wikipedia.org/wiki/Nested_set_model)
'''

import utils
import sys

class taxonomic_tree(object):
    """ self.tree: adjacency list implementation of a tree. Each node contains:
        - its name
        - a list of its chidren ids
        - its rank id
        - its depth
        - its father id
        self.rank: dict {"rank"(str) : rankid(int) }
    """
    
    def __compute_depth(self, n_id, depth):
        self.tree[n_id][3] = depth
        for child_id in self.get_children(n_id):
            self.__compute_depth(child_id, depth + 1)

    def __init__(self, NCBI_taxo, lm=utils.logmanager()):
        '''build an adjacency list tree structure from the NCBI taxonomic data, conserving 
        NCBI_taxo: {1: ['Root', 1, 'no rank'],
                 2: ["CD's", 1, 'phylum'],
                 3: ['Magazines', 2, 'genus'],
                 4: ['Hard Cover', 1, 'phylum'],
                 5: ['Large Format', 1, 'phylum'],
                 6: ['Vintage', 3, 'species']}
        self.tree: dict {id: (name, rank, tuple of children ids, depth, father)}
        self.rank: dict {rank: rankid}
        '''

        self.tree = {}
        self.ranks = {}
        lm.print_verbose("\t1/4 retrieving basic information...", 1)
        # retrieve basic information
        for n_id in NCBI_taxo:
            name = NCBI_taxo[n_id][0]
            father_id = NCBI_taxo[n_id][1]
            rank = NCBI_taxo[n_id][2]
            self.tree[n_id] = [name, rank, [], 0, father_id]
            self.__add_ranks(rank)

        # order ranks
        lm.print_verbose("\t2/4 computing rank order...", 1)
        self.__order_ranks()

        # compute tree structure
        lm.print_verbose("\t3/4 computing tree structure...", 1)
        
        # remove the first node from computation to avoid self-loop (father of node 1 is 1 !!)
        keys_to_process = list(NCBI_taxo.keys())
        keys_to_process.remove(1)
        for n_id in keys_to_process:
            father_id = NCBI_taxo[n_id][1]
            self.tree[father_id][2].append(n_id)
            self.tree[n_id][1] = self.ranks[self.tree[n_id][1]]
        # do the rank replacement for node 1
        self.tree[1][1] = self.ranks[self.tree[1][1]]

        # clean up children list
        for n_id in self.tree:
            self.tree[n_id][2] = tuple(self.tree[n_id][2])

        # compute depth
        lm.print_verbose("\t4/4 computing depth...", 1)
        self.__compute_depth(1, 0)
        # clean up
        for n_id in self.tree:
            self.tree[n_id] = tuple(self.tree[n_id])

    def __order_ranks(self):
        rank_list = [(k, v) for k, v in self.ranks.items()]
        rank_list.sort(key = lambda x: x[1])
        rank_l = [a for a, b in rank_list]   
        rank_l.remove('no rank')
        rank_l = ['no rank'] + rank_l
        self.ranks = {}
        for i in range(len(rank_l)):
            self.ranks[rank_l[i]] = i+1

    def __add_ranks(self, rank):
        if rank not in self.ranks:
            self.ranks[rank] = 1
        else:
            self.ranks[rank] += 1

    def get_name(self, i_id):
        return self.tree[i_id][0]

    def get_rank(self, i_id):
        return self.tree[i_id][1]

    def get_children(self, i_id):
        return self.tree[i_id][2]

    def get_depth(self, i_id):
        return self.tree[i_id][3]

    def get_father(self, i_id):
        return self.tree[i_id][4]
        
    def get_tree(self):
        """return the internal representation of the tree"""
        return self.tree

    def get_ranks(self):
        """return the internal representation of the tree"""
        return self.ranks

    def write_tree(self, filename):
        # TODO: still lacking list of children, but only used for debug
        """write part of the data to CSV (mostly for testing purpose)
            1$Root$1$0
            2$CD's$4$1
            3$Magazines$3$2
            4$Hard Cover$4$1
            5$Large Format$4$1
            6$Vintage$2$3
        """
        destination = open(filename, "w")
        for element in self.get_tree():
            data = self.tree[element]
            destination.write(str(element) + '$' + str(data[0]) + '$' + str(data[1]) + '$' + str(data[3]) + '\n')
        destination.close()
                

    def write_ranks(self, filename):
        """write part of the data to CSV (mostly for testing purpose)
            1$no rank
            2$species
            3$genus
            4$phylum
        """
        destination = open(filename, "w")
        l = [(v, k) for k, v in self.get_ranks().items()]
        l.sort(key = lambda x: x[0])
        for a, b in l:
            destination.write(str(a) + '$' + str(b) + '\n')
        destination.close()

class nested_set(object):
    """This class converts a taxonomic tree to a nested set 
        http://en.wikipedia.org/wiki/Nested_set_model
    """    

    def __init__(self, adj_list_tree):
        '''
        Transform an adjacency list implementation of a tree to a nested set implementation
        returns: nested set implementation of a tree:
                { taxid: (lft, rgt, name, rank, depth), ... }
        '''
        self.__tree = adj_list_tree
        self.__i_count = 1
        self.__nested_set = {}
        self.__traverse_aux(1)
        self.__tree = None # free the memory

    
    def __traverse_aux(self, i_id):
        i_lft = self.__i_count
        self.__i_count += 1
        children = self.__tree.get_children(i_id)
        for child_id in children:
            self.__traverse_aux(child_id)
        i_rgt = self.__i_count
        self.__i_count += 1
        a_name = self.__tree.get_name(i_id)     
        a_rank = self.__tree.get_rank(i_id)
        a_depth = self.__tree.get_depth(i_id)
        self.__add_to_nested_set(i_id, i_lft, i_rgt, a_name, a_rank, a_depth)

    def __add_to_nested_set(self, i_id, i_lft, i_rgt, a_name, a_rank, a_depth):
        self.__nested_set[i_id] = (i_lft, i_rgt, a_name, a_rank, a_depth)
    
    def data(self):
        """returns the internal representation of the nested set"""
        return self.__nested_set


    # remove the [:-1] to add the depth
    def __str__(self):
        return '\n'.join(['$'.join( [str(k)] + list(map(str, self.__nested_set[k]))[:-1] ) for k in sorted(self.data())])

    def write(self, filename):
        '''Write the nested set to CSV
        returns : None
        exemple of lines :  1$1$12$Root$no rank
                            2$2$7$CD's$phylum
                            3$3$6$Magazines$genus
                            4$8$9$Hard Cover$phylum
                            5$10$11$Large Format$phylum
                            6$4$5$Vintage$species
        '''
        destination = open(filename, "w")
        destination.write(str(self))
        destination.close()


if __name__ == "__main__":
    NCBI_taxo = {1: ['Root', 1, 'no rank'],
                 2: ["CD's", 1, 'phylum'],
                 3: ['Magazines', 2, 'genus'],
                 4: ['Hard Cover', 1, 'phylum'],
                 5: ['Large Format', 1, 'phylum'],
                 6: ['Vintage', 3, 'species']}
    mytree = taxonomic_tree(NCBI_taxo)
    print(mytree.get_ranks())
    print(mytree.get_ranks() == {'phylum': 4, 'no rank': 1, 'genus': 3, 'species': 2})
    print(mytree.get_tree())
    print(mytree.get_tree() == {1: ('Root', 1, (2, 4, 5), 0, 1),
                                2: ("CD's", 4, (3,), 1, 1),
                                3: ('Magazines', 3, (6,), 2, 2),
                                4: ('Hard Cover', 4, (), 1, 1),
                                5: ('Large Format', 4, (), 1, 1),
                                6: ('Vintage', 2, (), 3, 3)})
    mytree.write_ranks('test/test_ranks.csv')
    mytree.write_tree('test/test_tree.csv')
    res = nested_set(mytree)
    print(res.data())
    print(res.data() == {1: (1, 12, 'Root', 1, 0),
                         2: (2, 7, "CD's", 4, 1),
                         3: (3, 6, 'Magazines', 3, 2),
                         4: (8, 9, 'Hard Cover', 4, 1),
                         5: (10, 11, 'Large Format', 4, 1),
                         6: (4, 5, 'Vintage', 2, 3)})
    res.write('test/test_nested_set.csv')
