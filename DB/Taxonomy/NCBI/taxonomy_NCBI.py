#!/usr/bin/python3
# coding: utf-8

# THIS FILE IS PYTHON2 compatible, please test every change with python3 AND python

# for python2 compatibility ####################
from __future__ import print_function

try:
    from urllib.request import urlopen
except ImportError:
    from urllib import urlopen
#################################################

'''Access to taxonomy files on a given website'''

import utils
import sys
import zipfile
from tree import taxonomic_tree




# TODO: fetch as well the order of ranks from the root to 'species': it's far better than an arbitrary order

remote_NCBI_URL = 'ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdmp.zip'
local_NCBI_file = 'data/taxdmp.zip'


def __process_names(tree, names_file, lm=utils.logmanager()):
    # Load  NCBI names file ("names.dmp")
    for line in names_file:
        fields = line.decode('ascii').strip().split("\t")
        taxid = int(fields[0])
        name = fields[2].capitalize()
        notion = fields[6]
        if notion == "scientific name":
            if taxid not in tree: # optimization: this test seems unnecessary
                tree[taxid] = [name]
            else:
                lm.print_verbose("taxid " + str(taxid) + " already in tree", 1)


def __process_nodes(tree, nodes_file, lm=utils.logmanager()):
    # Load taxonomy
    # Process the nodes.dmp
    for line in nodes_file:
        fields = line.decode('ascii').strip().split("\t")
        taxid = int(fields[0])
        parent = int(fields[2])
        rank = fields[4]
        if taxid in tree: # optimization: this test seems unnecessary
            tree[taxid] = tuple(tree[taxid] + [parent, rank])
        else:
            lm.print_verbose("taxid " + str(taxid) + " missing", 1)


def fetch(remote=True, lm=utils.logmanager()):
    '''Access to taxonomy files on a given website and load the names.dmp file and the nodes.dmp file (taxonomy)
    remote: True if we fetch from the internet, False if we open a local file
    returns: tree : {'12': ['Exemple', '11', 'species'],
                     '975694': ['Nemaliales sp. BOLD:AAO8731', '902710', 'species']}
    '''
    if remote:
        lm.print_verbose("\t1/3 fetching remote %s file... " % remote_NCBI_URL, 1)
        import io
        try:
            url = urlopen(remote_NCBI_URL)
        except IOError as e:
            lm.print_verbose("\nError: remote URL or network unreachable!\n\teither your URL is malformed\n\tor you may want to try again using a local datafile with option remote=False", 1)
            sys.exit(1)
        datafile = io.BytesIO(url.read())
    else:
        lm.print_verbose("\t1/3 reading local %s file instead of fetching from the internet... " % local_NCBI_file, 1)
        datafile = local_NCBI_file
    NCBI_taxo = {}
    with zipfile.ZipFile(datafile, 'r') as myzip:
        lm.print_verbose("\t2/3 processing names.dmp... ", 1)
        names_file = myzip.open('names.dmp')
        __process_names(NCBI_taxo, names_file , lm)
        lm.print_verbose("\t3/3 processing nodes.dmp... ", 1)
        nodes_file = myzip.open('nodes.dmp')
        __process_nodes(NCBI_taxo, nodes_file, lm)
    return NCBI_taxo


def write(NCBI_taxo, filename):
    '''Open and write to a CSV file
    which will contain the adjacency list of the NCBI taxonomy
    tree : test_tree = {1: ['Root', 1, 'no rank'],
                        2: ["CD's", 1, 'species'],
                        3: ['Magazines', 2, 'genus'],
                        4: ['Hard Cover', 1, 'species'],
                        5: ['Large Format', 1, 'phylum'],
                        6: ['Vintage', 3, 'species']}
    returns : None
    exemple of lines : 1$Root$1$no rank
                       3$Magazines$2$genus
                       2$CD's$1$species
                       5$Large Format$1$phylum
                       4$Hard Cover$1$species
                       6$Vintage$3$species
    '''
    destination = open(filename, "w")
    for taxid in NCBI_taxo:
        destination.write(str(taxid) + "$" + "$".join(map(str, NCBI_taxo[taxid])) + '\n')
    destination.close()


if __name__ == "__main__":

    NCBI_taxo=fetch()
    mytree = taxonomic_tree(NCBI_taxo)
    print(mytree.get_ranks())
    print(mytree.get_tree())


    '''
    d = {}
    with zipfile.ZipFile('test/testdmp.zip', 'r') as myzip:
        __process_names(d, myzip.open('testnames.dmp'))
        __process_nodes(d, myzip.open('testnodes.dmp'))
    write(d, "test/test.csv")
    with open("test/test.csv", "r") as f:
        contents = f.readlines()
    print(contents == ['1$Root$1$no rank\n', "2$Cd's$1$species\n", '3$Magazines$2$genus\n', '4$Hard cover$1$species\n', '5$Large format$1$phylum\n', '6$Vintage$3$species\n'])
    '''