#!/usr/bin/python3
# coding: utf-8

# THIS FILE IS PYTHON2 compatible, please test every change with python3 AND python

# python2 compatibility
from __future__ import print_function

import sys
from datetime import datetime

class logmanager():
    """
        verbose_level = 0, no verbosity
        verbose_level = 1, print messages to the standard error output to indicate the progress of the analysis
        verbose_level = 2, debug messages to the standard error output for developers
    """
    def __init__(self, verbose_level=0, logfile="stderr", printtime=True):
        """
        :param verbose_level verbosity level of the running session (int)
        :param logfile       path to desired logfile (string)
        :param printtime     print current time (boolean)
        :return:
        """
        self.verbose_level = verbose_level
        self.logfile = logfile
        self.printtime = printtime

    def print_verbose(self, message, message_verbose_level=0) :
        """Print a message depending on user-specified verbosity level.
        verbosity = 1, print messages to the standard error output to indicate the progress of the analysis
        verbosity = 2, debug messages to the standard error output for developers

        :param message: the message to be printed (string)
        :param message_verbose_level: minimal verbosity level from which the message should be printed
        :return:

        """
        if self.logfile == "stderr":
            f = sys.stderr
        else:
            f = open(self.logfile, 'a')
        current_time = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        if (self.verbose_level >= message_verbose_level):
            s = ''
            if self.printtime:
                s += current_time + "\t"
            print(s + message, file = f)
        if self.logfile != "stderr":
            f.close()


def update_date(filename):
    '''Edit (or create the file if it doesn't exists) the file "NCBI_taxonomy_update_date.csv
    user : input data : enter the name of the user in the Terminal
    returns : csv file named "NCBI_taxonomy_update_date.csv"
    exemple of line : "2015-06-22_08:06:30;Justine D."
    '''

    import time
    import os.path
    import getpass
    # TODO: fetch as well hostname (and/or @IP)
    user = getpass.getuser()
        
    if not os.path.isfile(filename):
        destination = open(filename, "w")
        destination.write("Date;Updater\n")
        destination.close()

    destination = open(filename, "a")
    destination.write(
        time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime()) + ";" + user + "\n")
    destination.close()

if __name__ == "__main__":
    update_date("test/test_update_date.csv")
    lm = logmanager(1)
    lm.print_verbose ("first step", 1) # should be printed out
    lm.print_verbose ("debug info", 2) # should NOT be printed out
    lm.print_verbose ("second step", 1) # should be printed out

    lm = logmanager(0)
    lm.print_verbose ("third step", 1) # should NOT be printed out
    lm.print_verbose ("debug info", 2) # should NOT be printed out
    lm.print_verbose ("fourth step", 1) # should NOT be printed out

    outfile = 'test/test_logfile'
    f = open(outfile, 'w')             # to delete the content of logfile
    f.close()
    lm = logmanager(2, logfile = outfile, printtime = False)
    lm.print_verbose ("fifth step", 1)
    lm.print_verbose ("debug info", 2)
    lm.print_verbose ("sixth step", 1)
    print(open(outfile, 'r').read() == open('test/expected_logfile', 'r').read())


