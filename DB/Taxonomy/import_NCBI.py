#!/usr/bin/python3
# coding: utf-8

# THIS FILE IS PYTHON2 compatible, please test every change with python3 AND python

# for python2 compatibility
from __future__ import print_function


import sys,os
sys.path.append('NCBI')
import taxonomy_NCBI
import tree
import utils


import MySQLdb
db = MySQLdb.connect(host="localhost",    user="user_morphonet",    passwd="password_morphonet",   db="MorphoNet",port=80)         
cur = db.cursor()

os.system('mkdir -p data')
lm = utils.logmanager(1)
lm.print_verbose("1/6 - fetching and parsing taxonomy from NCBI... ", 1)
NCBI_taxo = taxonomy_NCBI.fetch(lm=lm)



#Insert RANK
sql = "INSERT INTO NCBI_rank (id, name) VALUES (%s, %s)"
f=open('data/NCBI_taxo_ranks.csv','r')
val = []
rank = {}
for line in f:
    tab=line.split('$')
    val.append((tab[0],tab[1]))
    rank[tab[1].strip()]=int(tab[0])
f.close()
print("commit "+sql)
cur.executemany(sql, val)
db.commit()


#First We Insert the ID and parent
sql = "INSERT INTO NCBI_tree (id, name, id_parent,rank) VALUES (%s, %s,%s , %s )"
val = []
nbCommit=0
for idx in NCBI_taxo:
    elt=NCBI_taxo[idx]
    val.append((idx,elt[0],elt[1],rank[elt[2]]))
    if len(val)>1000:
        print("commit NCBI_tree "+str(nbCommit))
        cur.executemany(sql, val)
        db.commit()
        nbCommit+=1
        val = []
print("last commit for "+sql)
cur.executemany(sql, val)
db.commit()


#Create Childs
Childs={}
for idx in NCBI_taxo:
    elt=NCBI_taxo[idx]
    id_parent=elt[1]
    if id_parent not in Childs:
        Childs[id_parent]=[]
    if idx!=id_parent:
        Childs[id_parent].append(idx)

#Insert Childs
sql = "INSERT INTO NCBI_childs (id_parent, id_child) VALUES (%s, %s)"
val = []
nbCommit=0
for id_parent in Childs:
    for child in Childs[id_parent]:
        val.append((id_parent,child))
        if len(val)>1000:
            print("commit NCBI_childs "+str(nbCommit))
            cur.executemany(sql, val)
            db.commit()
            nbCommit+=1
            val = []
print("last commit for "+sql)
cur.executemany(sql, val)
db.commit()


sql = "UPDATE NCBI_tree SET nb_childs=%s WHERE id=%s"
val = []
nbCommit=0
for idx in NCBI_taxo:
    nb_childs=0
    if idx in Childs:
        nb_childs=len(Childs[idx])
    val.append((nb_childs,idx))
    if len(val)>1000:
        print("commit NCBI_tree "+str(nbCommit))
        cur.executemany(sql, val)
        db.commit()
        nbCommit+=1
        val = []
print("last commit for "+sql)
cur.executemany(sql, val)
db.commit()


db.close()


