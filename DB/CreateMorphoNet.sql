-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: MorphoNet
-- ------------------------------------------------------
-- Server version	5.5.59-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS `MorphoNet`;
CREATE DATABASE MorphoNet;
USE MorphoNet;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_scenario` int(10) unsigned NOT NULL,
  `idx` int(10) unsigned NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `position` varchar(500) DEFAULT NULL,
  `rotation` varchar(500) DEFAULT NULL,
  `scale` varchar(500) DEFAULT NULL,
  `framerate` int(10) unsigned DEFAULT '10',
  `interpolate` tinyint(1) DEFAULT '1',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `colormap`
--

DROP TABLE IF EXISTS `colormap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colormap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) unsigned NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `colors` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `comments` text,
  `date` datetime DEFAULT NULL,
  `done` int(10) unsigned DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `correspondence`
--

DROP TABLE IF EXISTS `correspondence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correspondence` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) unsigned NOT NULL,
  `id_dataset` int(10) unsigned NOT NULL,
  `infos` varchar(500) NOT NULL,
  `field` longblob,
  `date` datetime DEFAULT NULL,
  `type` int(10) unsigned DEFAULT '0',
  `datatype` varchar(255) NOT NULL,
  `link` varchar(500) DEFAULT NULL,
  `version` int(10) unsigned DEFAULT '0',
  `common` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_dataset` (`id_dataset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE `correspondence` ADD INDEX(`id_dataset`);
ALTER TABLE `correspondence` ADD INDEX(`id_people`);

--
-- Table structure for table `curration`
--

DROP TABLE IF EXISTS `curration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) unsigned NOT NULL,
  `date` datetime DEFAULT NULL,
  `id_dataset` int(10) unsigned NOT NULL,
  `id_correspondence` int(11) NOT NULL,
  `id_object` varchar(250) NOT NULL,
  `value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dataset` (`id_dataset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE `curration` ADD INDEX(`id_dataset`);
ALTER TABLE `curration` ADD INDEX(`id_people`);
ALTER TABLE `curration` ADD INDEX(`id_correspondence`);


--
-- Table structure for table `dataset`
--

DROP TABLE IF EXISTS `dataset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) NOT NULL,
  `minTime` int(10) NOT NULL,
  `maxTime` int(10) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `bundle` tinyint(4) NOT NULL DEFAULT '0',
  `archive` int(4) NOT NULL DEFAULT '0',
  `rotation` varchar(500) DEFAULT NULL,
  `translation` varchar(500) DEFAULT NULL,
  `scale` varchar(500) DEFAULT NULL,
  `spf` int(10) DEFAULT '1',
  `dt` int(10) DEFAULT '0',
  `comments` longtext,
  `link` text,
  `id_NCBI` int(10) DEFAULT '0',
  `type` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE `dataset` ADD INDEX(`id_people`);



--
-- Table structure for table `developmental_table`
--

DROP TABLE IF EXISTS `developmental_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developmental_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_datasettype` int(10) unsigned NOT NULL,
  `period` varchar(500) DEFAULT NULL,
  `stage` varchar(500) DEFAULT NULL,
  `developmentaltstage` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `hpf` varchar(500) DEFAULT NULL,
  `hatch` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Institution`
--

DROP TABLE IF EXISTS `Institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Institution` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Laboratory`
--

DROP TABLE IF EXISTS `Laboratory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Laboratory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` varchar(500) NOT NULL,
  `IP` varchar(500) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `mesh`
--

DROP TABLE IF EXISTS `mesh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mesh` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_dataset` int(10) unsigned NOT NULL,
  `t` int(10) NOT NULL,
  `channel` varchar(500) DEFAULT '0',
  `quality` int(10) unsigned DEFAULT '0',
  `obj` longblob,
  `link` varchar(500) DEFAULT NULL,
  `version` int(10) unsigned DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `center` varchar(500) DEFAULT NULL,
  `size` int(10) unsigned DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `id_dataset` (`id_dataset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


ALTER TABLE `mesh` ADD INDEX(`id_dataset`);
ALTER TABLE `mesh` ADD INDEX(`link`);
-- --------------------------------------------------------

--
-- Structure de la table `people_group`
--

CREATE TABLE `people_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_people` int(10) NOT NULL,
  `id_group` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `id_owner` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `privacy` varchar(500) DEFAULT 'private'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `people_right`
--

DROP TABLE IF EXISTS `people_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_right` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `dataset` varchar(500) DEFAULT 'no',
  `unity` tinyint(1) DEFAULT '0',
  `u_manage` tinyint(1) DEFAULT '0',
  `git` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `surname` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `login` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `exist` int(10) unsigned DEFAULT '1',
  `id_right` varchar(500) DEFAULT '3',
  `tel` varchar(500) DEFAULT '',
  `adress` varchar(500) DEFAULT '',
  `fonction` varchar(500) DEFAULT '',
  `id_Institution` int(10) unsigned DEFAULT NULL,
  `id_Laboratory` int(10) unsigned DEFAULT NULL,
  `id_Team` int(10) unsigned DEFAULT NULL,
  `quota` BIGINT(10) DEFAULT '1073741824' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `scenario`
--

DROP TABLE IF EXISTS `scenario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scenario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) unsigned NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `id_dataset` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;




--
-- Table structure for table `share`
--

DROP TABLE IF EXISTS `share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_people` int(10) unsigned NOT NULL,
  `base` varchar(500) DEFAULT NULL,
  `id_base` int(10) unsigned DEFAULT NULL,
  `id_who` int(10) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


ALTER TABLE `share` ADD INDEX(`id_people`);
--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `sharing`
--

CREATE TABLE `sharing` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_people` int(10) UNSIGNED NOT NULL,
  `base` varchar(500) DEFAULT NULL,
  `id_base` int(10) UNSIGNED DEFAULT NULL,
  `id_who` int(10) UNSIGNED DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `how` int(11) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `sharing` ADD INDEX(`id_people`);


LOCK TABLES `people_right` WRITE;
/*!40000 ALTER TABLE `people_right` DISABLE KEYS */;
INSERT INTO `people_right` VALUES (1,'admin','all',1,1,1),(2,'dev','no',1,0,1),(3,'user','semi_acces',0,0,0);
/*!40000 ALTER TABLE `people_right` ENABLE KEYS */;
UNLOCK TABLES;




--
-- Table structure for table `NCBI_childs` ,  `NCBI_tree` ,  `NCBI_rank`
--

CREATE TABLE `NCBI_childs` (  `id_parent` int(11) NOT NULL, `id_child` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `NCBI_tree` ( `id` int(11) NOT NULL, `id_parent` int(11) NOT NULL, `name` varchar(255) NOT NULL,  `rank` int(11) NOT NULL,  `nb_childs` int(11) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `NCBI_rank` ( `id` int(11) NOT NULL,  `name` varchar(255) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `NCBI_childs` ADD KEY `FK_myKey` (`id_child`),  ADD KEY `FK_myKey2` (`id_parent`);
ALTER TABLE `NCBI_tree`  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);
ALTER TABLE `NCBI_rank` ADD PRIMARY KEY (`id`);
ALTER TABLE `NCBI_childs` ADD CONSTRAINT `FK_myKey` FOREIGN KEY (`id_child`) REFERENCES `NCBI_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,  ADD CONSTRAINT `FK_myKey2` FOREIGN KEY (`id_parent`) REFERENCES `NCBI_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;





